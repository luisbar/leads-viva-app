# Instalación

```
1 git clone https://gitlab.com/luisbar/leads-viva-app.git leads-viva-app
2 cd /path/to/leads-viva-app
3 npm install
4 npm start
```

### Para iOS

```
5 npm run ios
```

### Para Android

```
5 npm run android
 ```

# Sugerencias y/o posibles problemas despues de la instalación de las siguientes librerias

### [react-native-root-toast](https://github.com/magicismight/react-native-root-toast)

- Unable to resolve module react-native/Libraries/vendor/emitter/EventEmitter, para resolver el problema cambiar en {+ /node_modules/react-native-root-siblings/lib/AppRegistryInjection +}

```diff
- import EventEmitter from 'react-native/Libraries/vendor/emitter/EventEmitter';
+ import EventEmitter from 'react-native/Libraries/EventEmitter/EventEmitter';
```

### [react-native](http://facebook.github.io/react-native/)

- `config.h file not found`, para resolver el problema:
  - ejecutar el script que esta en {+ /node_modules/react-native/scripts/ios-configure-glog.sh +}
  - o instalar glog manualmente
  
- `cannot find entry file index.android.js in any of the roots`, para resolver el problema, en el {+ archivo /node_modules/react-native/react.gradle +} debe cambiar lo siguiente:

```diff
- def entryFile = config.entryFile ?: "index.android.js"
+ def entryFile = config.entryFile ?: "index.js"
```

- si da el siguiente error con el packager corriendo, `unable to load script from assets 'index.android.bundle'`, para areglar se debe ejecutar lo siguiente:
  - (opcion 1) adb reverse tcp:8081 tcp:8081 luego correr el packager react-native start
  - (opcion 2) si no se tiene el directorio, {+ mkdir android/app/src/main/assets +}
  - luego {+ react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res +}, una vez vuelva a correr la app en modo dev se puede borrar los archivos creados en assets

### [react-native-camera](https://github.com/react-native-community/react-native-camera)

- `GoogleMobileVision/GoogleMobileVision.h file not found`, para resolver el problema eliminar el modulo de deteccion facial que esta en {+ /Pods/Development Pods/react-native-camera/FaceDetector +}, como sugiere [aquí](https://github.com/react-native-community/react-native-camera#no-face-detection-steps) , si el problema continua borrarlo de la siguiente dirección {+ /node_modules/react-native-camera/ios/FaceDetector +} 

### [react-native-vector-icons](https://github.com/oblador/react-native)

- `while resolving module react-native-vector-icons/MaterialIcons, the Haste package react-native-vector-icons was found. However the module MaterialIcons could not be found within the package. Indeed, none of these files exist`, para resolver el problema ejecutar lo siguiente **rm ./node_modules/react-native/local-cli/core/__fixtures__/files/package.json** y reiniciar el packager

### [react-native-calendars](https://github.com/wix/react-native-calendars)

- para cambiar el color de fondo del contenedor de dias que esta en la parte superior en la agenda, se debe modificar el archivo {+ /node_modules/react-native-calendars/src/agenda/platform-style.js +} con lo siguiente:

```diff
export default function platformStyles(appStyle) {
  return {
    knob: {
      width: 38,
      height: 7,
      marginTop: 10,
      borderRadius: 3,
      backgroundColor: appStyle.agendaKnobColor
    },
    weekdays: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingLeft: 24,
      paddingRight: 24,
      paddingTop: 15,
      paddingBottom: 7,
-     backgroundColor: appStyle.calendarBackground,
+     backgroundColor: '#C0E83E',
    },
  };
}
```

- para cambiar la fuente de los dias del header, se debe modificar el archivo {+ /node_modules/react-native-calendars/src/agenda/style.js +} con lo siguiente:

```diff
export default function platformStyles(appStyle) {
  return {
    ...
    
    weekday: {
      width: 32,
      textAlign: 'center',
+     fontFamily: 'Museo700',
+     fontSize: 15,
-     fontSize: 13,
      color: appStyle.textSectionTitleColor,
    },
    
    ...
  };
}
```

- para cambiar la fuente de los dias del lado lateral izquierdo, se debe modificar el archivo {+ /node_modules/react-native-calendars/src/agenda/reservation-list/style.js +} con lo siguiente:

```diff
export default function styleConstructor(theme = {}) {
  const appStyle = {...defaultStyle, ...theme};
  return  StyleSheet.create({
    container: {
      flexDirection: 'row'
    },
    dayNum: {
      fontSize: 28,
+     fontFamily: 'Museo300',
      fontWeight: '200',
      color: appStyle.agendaDayNumColor
    },
    dayText: {
      fontSize: 14,
+     fontFamily: 'Museo300',
      color: appStyle.agendaDayTextColor,
      marginTop: -5,
      backgroundColor: 'rgba(0,0,0,0)'
    },
    day: {
      width: 63,
      alignItems: 'center',
      justifyContent: 'flex-start',
      marginTop: 32
    },
    today: {
      color: appStyle.agendaTodayColor
    },
    ...(theme[STYLESHEET_ID] || {})
  });
}
```

### [react-native-tag-select](https://github.com/rafaelmotta/react-native-tag-select)

- agregar al archivo {+ /node_modules/react-native-tag-select/src/TagSelect.js +} lo siguiente

```diff
+ /**
+  * Remove an item from selectItems
+  */
+ removeItem = (item) => {
+   const value = { ...this.state.value };
+   delete value[item[this.props.keyAttr]];
  
+   this.setState({ value });
+ }
```

### [react-native-google-places](https://github.com/tolu360/react-native-google-places)

- modificar el archivo RNGooglePlacesModule.java en la linea 303 para que funcione con android api 16

```diff
@ReactMethod
    public void lookUpPlacesByIDs(ReadableArray placeIDs, final Promise promise) {
        List<Object> placeIDsObjects = placeIDs.toArrayList();
        List<String> placeIDsStrings = new ArrayList<>(placeIDsObjects.size());
        for (Object item : placeIDsObjects) {
-           placeIDsStrings.add(Objects.toString(item, null));
+           placeIDsStrings.add(String.valueOf(item));
        }
        ....
    }
```
### [react-native-image-resizer](https://github.com/tolu360/react-native-google-places)

- modificar el archivo metodo createResizedImageWithExceptions de la clase ImageResizerModule

```
private void createResizedImageWithExceptions(String imagePath, int newWidth, int newHeight,
                                           String compressFormatString, int quality, int rotation, String outputPath,
                                           final Callback successCb, final Callback failureCb) throws IOException {
    Bitmap.CompressFormat compressFormat = Bitmap.CompressFormat.valueOf(compressFormatString);
    Uri imageUri = Uri.parse(imagePath);

    File resizedImage = ImageResizer.createResizedImage(this.context, imageUri, newWidth,
            newHeight, compressFormat, quality, rotation, outputPath);

    // If resizedImagePath is empty and this wasn't caught earlier, throw.
    if (resizedImage.isFile()) {
        WritableMap response = Arguments.createMap();
        response.putString("path", resizedImage.getAbsolutePath());
        response.putString("uri", Uri.fromFile(resizedImage).toString());
        response.putString("name", resizedImage.getName());
        response.putDouble("size", resizedImage.length());
        response.putString("base64", Base64.encodeToString(readFileToByteArray(resizedImage), Base64.DEFAULT));
        // Invoke success
        successCb.invoke(response);
    } else {
        failureCb.invoke("Error getting resized image path");
    }
}
```
