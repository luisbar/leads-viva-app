/***********************
 * Node modules import *
 ***********************/
import { Navigation } from 'react-native-navigation';
/******************
 * Project import *
 ******************/
import { registerScenes } from './src/register.js';

registerScenes();

Navigation.startSingleScreenApp({
  screen: {
    screen: 'leads.viva.Main',
    navigatorStyle: {
      navBarHidden: true,
    },
  },
  appStyle: {
    orientation: 'portrait',//Orientación de toda la app
  },
  drawer: {
    left: {
      screen: 'leads.viva.DrawerMenu',//Vista que se mostrara en el drawer
    },
    disableOpenGesture: true,
  },
  animationType: 'slide-down',//Tipo de animación de la transición('none', 'slide-down', 'fade')
});
