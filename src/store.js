/***********************
 * Node modules import *
 ***********************/
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'remote-redux-devtools';
import thunk from 'redux-thunk';
/******************
 * Project import *
 ******************/
import rootReducer from './reducer';
/**
 * Para habilitar la herramienta remote-redux-devtools
 * realtime = true, habilita la herramienta
 * hostname = ip de la pc donde esta corriendo react-native,
 * para que el celular sepa donde enviar los datos
 * port = 8000, puerto abierto de la maquina al cual se comunicara el celular
 */
const composeEnhancers = composeWithDevTools({
  realtime: false,
  hostname: '192.168.0.12',
  port: 8000,
});
/**
 * Metodo que crea el store con todos los reducers, ademas se le agrego thunk,
 * para realizar tareas asynchronas
 */
const store = createStore(
  rootReducer,
  composeEnhancers(
    applyMiddleware(thunk)
));

export default store;
