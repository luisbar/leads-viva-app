/***********************
 * Node modules import *
 ***********************/
import { combineReducers } from 'redux';
/******************
 * Project import *
 ******************/
import sessionReducer from './session/reducer.js';
import configurationReducer from './configuration/reducer.js';
import leadReducer from './lead/reducer.js';
import actionReducer from './action/reducer.js';
import commentReducer from './comment/reducer.js';
import notificationReducer from './notification/reducer.js';

export default combineReducers({
  sessionReducer,
  configurationReducer,
  leadReducer,
  actionReducer,
  commentReducer,
  notificationReducer,
});
