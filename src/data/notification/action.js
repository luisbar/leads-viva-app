/******************
 * Project import *
 ******************/
import { requester, logger } from '../../service/index.js';
import { GET_NOTIFICATIONS_URI, SEE_A_NOTIFICATION_URI } from '../../config.js';
import { executeUpdateToken } from '../session/action.js';
import {
  WILL_GET_NOTIFICATIONS,
  GET_NOTIFICATIONS,
  DID_GET_NOTIFICATIONS,
  
  WILL_GET_MORE_NOTIFICATIONS,
  GET_MORE_NOTIFICATIONS,
  DID_GET_MORE_NOTIFICATIONS,
  
  WILL_SEE_A_NOTIFICATION,
  SEE_A_NOTIFICATION,
  DID_SEE_A_NOTIFICATION,
} from './actionType.js';
/**
 * Obtiene las notificaciones
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token de actualización
 */
export function executeGetNotifications(
  accessToken,
  refreshToken
) {

  return dispatch => {
    dispatch(willGetNotifications());

    requester.getWithToken(
      GET_NOTIFICATIONS_URI.replace('page', 1),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(getNotifications({
        notifications: requester.getNotificationObjects(data.notificaciones),
        totalOfPagesOfNotifications: data.cantidad_paginas,
      }, false));
      dispatch(didGetNotifications());
    })
    .catch((error) => {
      dispatch(getNotifications(error, true));
      dispatch(didGetNotifications());
      logger.recordError(String(error))
      logger.log('Notification: error al obtener las notificaciones por primera vez')
    });
  }
}
/**
 * Actualiza el objeto statusOfGettingNotifications para saber
 * que el metodo getNotifications va a ser ejecutado
 */
function willGetNotifications() {

  return {
    type: WILL_GET_NOTIFICATIONS,
  };
}
/**
 * Actualiza el objeto notifications si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingNotifications
 * para mostrar un mensaje de error
 * @param  {object} payload array de notificaciones u objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getNotifications(payload, error) {

  return {
    type: GET_NOTIFICATIONS,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingNotifications para saber
 * que el metodo getNotifications ha sido ejecutado
 */
function didGetNotifications() {

  return {
    type: DID_GET_NOTIFICATIONS,
  };
}
/**
 * Obtiene las notificaciones por pagina
 * @param  {number} page pagina nueva
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token de actualización
 */
export function executeGetMoreNotifications(
  page,
  accessToken,
  refreshToken
) {

  return dispatch => {
    dispatch(willGetMoreNotifications());

    requester.getWithToken(
      GET_NOTIFICATIONS_URI.replace('page', page),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(getMoreNotifications({
        notifications: requester.getNotificationObjects(data.notificaciones),
        totalOfPagesOfNotifications: data.cantidad_paginas,
      }, false));
      dispatch(didGetMoreNotifications());
    })
    .catch((error) => {
      dispatch(getMoreNotifications(error, true));
      dispatch(didGetMoreNotifications());
      logger.recordError(String(error))
      logger.log('Notification: error al obtener mas notificaciones')
    });
  }
}
/**
 * Actualiza el objeto statusOfGettingMoreNotifications para saber
 * que el metodo getMoreNotifications va a ser ejecutado
 */
function willGetMoreNotifications() {

  return {
    type: WILL_GET_MORE_NOTIFICATIONS,
  };
}
/**
 * Actualiza el objeto notifications si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingMoreNotifications
 * para mostrar un mensaje de error
 * @param  {object} payload array de notificaciones u objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getMoreNotifications(payload, error) {

  return {
    type: GET_MORE_NOTIFICATIONS,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingMoreNotifications para saber
 * que el metodo getMoreNotifications ha sido ejecutado
 */
function didGetMoreNotifications() {

  return {
    type: DID_GET_MORE_NOTIFICATIONS,
  };
}
/**
 * Cambia el estado de una notificacion a vista
 * @param  {string} notificationId identificador de la notificacion
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token para refrescar el token de acceso
 */
export function executeSeeANotification(notificationId, accessToken, refreshToken) {
  
  return dispatch => {
    dispatch(willSeeANotification());
    
    requester.postWithToken(
      SEE_A_NOTIFICATION_URI.replace('notificationId', notificationId),
      {},
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(seeANotification(data, false));
      dispatch(didSeeANotification());
    })
    .catch((error) => {
      dispatch(seeANotification(error, true));
      dispatch(didSeeANotification());
      logger.recordError(String(error));
      logger.log('Notification: error al poner en visto una notificacion');
    })
  };
}
/**
 * Actualiza el objeto statusOfSeeingANotification para saber
 * que el metodo seeANotification va a ser ejecutado
 */
function willSeeANotification() {

  return {
    type: WILL_SEE_A_NOTIFICATION,
  };
}
/**
 * No modifica nada en el store si todo salió bien,
 * caso contrario actualiza el objeto statusOfSeeingANotification
 * para mostrar un mensaje de error
 * @param  {object} payload objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function seeANotification(payload, error) {

  return {
    type: SEE_A_NOTIFICATION,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfSeeingANotification para saber
 * que el metodo seeANotification ha sido ejecutado
 */
function didSeeANotification() {

  return {
    type: DID_SEE_A_NOTIFICATION,
  };
}
