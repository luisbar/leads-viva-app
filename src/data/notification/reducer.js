/******************
 * Project import *
 ******************/
import {
  WILL_GET_NOTIFICATIONS,
  GET_NOTIFICATIONS,
  DID_GET_NOTIFICATIONS,
  
  WILL_GET_MORE_NOTIFICATIONS,
  GET_MORE_NOTIFICATIONS,
  DID_GET_MORE_NOTIFICATIONS,
  
  WILL_SEE_A_NOTIFICATION,
  SEE_A_NOTIFICATION,
  DID_SEE_A_NOTIFICATION,
} from './actionType.js';
/*************
 * Constants *
 *************/
const initialState = {
  notifications: [],
  totalOfPagesOfNotifications: 0,
  statusOfGettingNotifications: {
    notificationsAreBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  statusOfGettingMoreNotifications: {
    moreNotificationsAreBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  statusOfSeeingANotification: {
    seeingANotificationIsBeingExecuted: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
};
/**
 * Gestiona el objeto notificacion y todo lo referente a el
 */
export default function notification(state = initialState, action) {

  switch (action.type) {

    case WILL_GET_NOTIFICATIONS:
      return Object.assign({}, state, {
        statusOfGettingNotifications: {
          notificationsAreBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        notifications: [],
        totalOfPagesOfNotifications: 0,
      });

    case GET_NOTIFICATIONS:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingNotifications: {
            notificationsAreBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          notifications: action.payload.notifications,
          totalOfPagesOfNotifications: action.payload.totalOfPagesOfNotifications,
        });

    case DID_GET_NOTIFICATIONS:
      return Object.assign({}, state, {
        statusOfGettingNotifications: {
          notificationsAreBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_GET_MORE_NOTIFICATIONS:
      return Object.assign({}, state, {
        statusOfGettingMoreNotifications: {
          moreNotificationsAreBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case GET_MORE_NOTIFICATIONS:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingMoreNotifications: {
            moreNotificationsAreBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          notifications: state.notifications.concat(action.payload.notifications),
          totalOfPagesOfNotifications: action.payload.totalOfPagesOfNotifications,
        });

    case DID_GET_MORE_NOTIFICATIONS:
      return Object.assign({}, state, {
        statusOfGettingMoreNotifications: {
          moreNotificationsAreBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_SEE_A_NOTIFICATION:
      return Object.assign({}, state, {
        statusOfSeeingANotification: {
          seeingANotificationIsBeingExecuted: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case SEE_A_NOTIFICATION:
      if (action.error)
        return Object.assign({}, state, {
          statusOfSeeingANotification: {
            seeingANotificationIsBeingExecuted: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return state;

    case DID_SEE_A_NOTIFICATION:
      return Object.assign({}, state, {
        statusOfSeeingANotification: {
          seeingANotificationIsBeingExecuted: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
    
    default:
      return state;
  }
}
