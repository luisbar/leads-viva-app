/******************
 * Project import *
 ******************/
import {
  WILL_GET_CONFIGURATION_DATA,
  GET_CONFIGURATION_DATA,
  DID_GET_CONFIGURATION_DATA,
  
  WILL_GET_MATCHED_PLACES,
  GET_MATCHED_PLACES,
  DID_GET_MATCHED_PLACES,
  
  WILL_GET_AVAILABILITY_OF_SERVICE,
  GET_AVAILABILITY_OF_SERVICE,
  DID_GET_AVAILABILITY_OF_SERVICE,
  
  WILL_GET_HOME_DATA,
  GET_HOME_DATA,
  DID_GET_HOME_DATA,
  
  WILL_GET_LEADS_BY_SUB_STATUS,
  GET_LEADS_BY_SUB_STATUS,
  DID_GET_LEADS_BY_SUB_STATUS,
} from './actionType.js';
/*************
 * Constants *
 *************/
const initialState = {
  configurationData: null,
  statusOfGettingConfigurationData: {
    configurationDataIsBeingFetching: true,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  matchedPlaces: [],
  statusOfGettingMatchedPlaces: {
    matchedPlacesIsBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  availabilityOfService: null,
  statusOfGettingAvailabilityOfService: {
    availabilityOfServiceIsBeingFetched: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  pendingAssignments: 0,
  observedLeads: 0,
  notifications: 0,
  statusOfGettingHomeData: {
    homeDataIsBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  leadsBySubStatus: null,
  statusOfGettingLeadsBySubStatus: {
    leadsBySubStatusAreBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
};
/**
 * Gestiona datos de configuración de la app
 */
export default function configuration(state = initialState, action) {

  switch (action.type) {

    case WILL_GET_CONFIGURATION_DATA:
      return Object.assign({}, state, {
        statusOfGettingConfigurationData: {
          configurationDataIsBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        configurationData: null,
      });

    case GET_CONFIGURATION_DATA:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingConfigurationData: {
            configurationDataIsBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          configurationData: action.payload,
        });

    case DID_GET_CONFIGURATION_DATA:
      return Object.assign({}, state, {
        statusOfGettingConfigurationData: {
          configurationDataIsBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_GET_MATCHED_PLACES:
      return Object.assign({}, state, {
        statusOfGettingMatchedPlaces: {
          matchedPlacesIsBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case GET_MATCHED_PLACES:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingMatchedPlaces: {
            matchedPlacesIsBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          matchedPlaces: action.payload,
        });

    case DID_GET_MATCHED_PLACES:
      return Object.assign({}, state, {
        statusOfGettingMatchedPlaces: {
          matchedPlacesIsBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        matchedPlaces: [],
      });
      
    case WILL_GET_AVAILABILITY_OF_SERVICE:
      return Object.assign({}, state, {
        statusOfGettingAvailabilityOfService: {
          availabilityOfServiceIsBeingFetched: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case GET_AVAILABILITY_OF_SERVICE:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingAvailabilityOfService: {
            availabilityOfServiceIsBeingFetched: true,
            wasAnError: true,
            error: action.payload
          },
        });
      else
        return Object.assign({}, state, {
          availabilityOfService: action.payload,
        });

    case DID_GET_AVAILABILITY_OF_SERVICE:
      return Object.assign({}, state, {
        statusOfGettingAvailabilityOfService: {
          availabilityOfServiceIsBeingFetched: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        availabilityOfService: null,
      });
      
    case WILL_GET_HOME_DATA:
      return Object.assign({}, state, {
        statusOfGettingHomeData: {
          homeDataIsBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        pendingAssignments: 0,
        observedLeads: 0,
        notifications: 0,
      });

    case GET_HOME_DATA:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingHomeData: {
            homeDataIsBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          pendingAssignments: action.payload.pendingAssignments,
          observedLeads: action.payload.observedLeads,
          notifications: action.payload.notifications,
        });

    case DID_GET_HOME_DATA:
      return Object.assign({}, state, {
        statusOfGettingHomeData: {
          homeDataIsBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_GET_LEADS_BY_SUB_STATUS:
      return Object.assign({}, state, {
        statusOfGettingLeadsBySubStatus: {
          leadsBySubStatusAreBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        leadsBySubStatus: null,
      });

    case GET_LEADS_BY_SUB_STATUS:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingLeadsBySubStatus: {
            leadsBySubStatusAreBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          leadsBySubStatus: action.payload,
        });

    case DID_GET_LEADS_BY_SUB_STATUS:
      return Object.assign({}, state, {
        statusOfGettingLeadsBySubStatus: {
          leadsBySubStatusAreBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    default:
      return state;
  }
}
