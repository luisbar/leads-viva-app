/***********************
 * Node modules import *
 ***********************/
 import RNGooglePlaces from 'react-native-google-places';

/******************
 * Project import *
 ******************/
import { requester, logger } from '../../service/index.js';
import { executeUpdateToken } from '../session/action.js';
import {
  CONFIGURATION_DATA_URI,
  SERVICE_AVAILABLE_URI,
  GET_HOME_DATA_URI,
  LEADS_BY_SUB_STATUS_URI,
} from '../../config.js';
import {
  WILL_GET_CONFIGURATION_DATA,
  GET_CONFIGURATION_DATA,
  DID_GET_CONFIGURATION_DATA,
  
  WILL_GET_MATCHED_PLACES,
  GET_MATCHED_PLACES,
  DID_GET_MATCHED_PLACES,
  
  WILL_GET_AVAILABILITY_OF_SERVICE,
  GET_AVAILABILITY_OF_SERVICE,
  DID_GET_AVAILABILITY_OF_SERVICE,
  
  WILL_GET_HOME_DATA,
  GET_HOME_DATA,
  DID_GET_HOME_DATA,
  
  WILL_GET_LEADS_BY_SUB_STATUS,
  GET_LEADS_BY_SUB_STATUS,
  DID_GET_LEADS_BY_SUB_STATUS,
} from './actionType.js';
/**
 * Metodo que obtiene los datos de configuración para el
 * registro de leads desde la api
 */
export function executeGetConfigurationData(token, refreshToken) {

  return dispatch => {
    dispatch(willGetConfigurationData());

    requester.getWithToken(CONFIGURATION_DATA_URI, token, refreshToken, executeUpdateToken, dispatch)
    .then((data) => {
      dispatch(getConfigurationData(requester.getConfigurationData(data), false));
      dispatch(didGetConfiguratioData());
    })
    .catch((error) => {
      dispatch(getConfigurationData(error, true));
      dispatch(didGetConfiguratioData());
      logger.recordError(String(error));
      logger.log('Configuration: error al obtener los datos de configuración para el registro de leads');
    });
  };
}
/**
 * Actualiza el objeto statusOfGettingConfigurationData para saber
 * que el metodo getConfigurationData va a ser ejecutado
 */
function willGetConfigurationData() {

  return {
    type: WILL_GET_CONFIGURATION_DATA,
  };
}
/**
 * Actualiza el objeto configurationData si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingConfigurationData
 * para mostrar un mensaje de error
 * @param  {object} payload objeto con los datos de configuración para el registro de lead
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getConfigurationData(payload, error) {

  return {
    type: GET_CONFIGURATION_DATA,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingConfigurationData para saber
 * que el metodo getConfigurationData ha sido ejecutado
 */
function didGetConfiguratioData() {

  return {
    type: DID_GET_CONFIGURATION_DATA,
  };
}
/**
 * Metodo que obtiene lugares con google place, estos lugares
 * deben coincidir con la dirección introducida por el usuario
 */
export function executeGetMatchedPlaces(placeToSearch, city) {

  return dispatch => {
    dispatch(willGetMatchedPlaces());
    
    RNGooglePlaces.getAutocompletePredictions(`${placeToSearch}, ${city}`, {
      country: 'BO',
    })
    .then((results) => results.length !== 0
      ? RNGooglePlaces.lookUpPlacesByIDs(results.map((item) => item.placeID))
      : results)
    .then((results) => {
      dispatch(getMatchedPlaces(results, false));
      dispatch(didGetMatchedPlaces());
    })
    .catch((error) => {
      dispatch(getMatchedPlaces(error, true));
      dispatch(didGetMatchedPlaces());
      logger.recordError(String(error));
      logger.log('Configuration: hubo un problema al comunicarse con google places');
    });
    
  };
}
/**
 * Actualiza el objeto statusOfGettingMatchedPlaces para saber
 * que el metodo getMatchedPlaces va a ser ejecutado
 */
function willGetMatchedPlaces() {

  return {
    type: WILL_GET_MATCHED_PLACES,
  };
}
/**
 * Actualiza el objeto matchedPlaces si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingMatchedPlaces
 * para mostrar un mensaje de error
 * @param  {object} payload array de lugares
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getMatchedPlaces(payload, error) {

  return {
    type: GET_MATCHED_PLACES,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingMatchedPlaces para saber
 * que el metodo getMatchedPlaces ha sido ejecutado
 */
function didGetMatchedPlaces() {

  return {
    type: DID_GET_MATCHED_PLACES,
  };
}
/**
 * Metodo que obtiene la disponibilidad del servicio
 * en la ubición enviada por parametro
 */
export function executeGetAvailabilityOfService(latitude, longitude, token, refreshToken) {

  return dispatch => {
    dispatch(willGetAvailabilityOfService());

    requester.postWithToken(
      SERVICE_AVAILABLE_URI,
      requester.getBodyForGettingAvailabilityOfService(latitude, longitude),
      token,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(getAvailabilityOfService(requester.getAvailabilityOfService(data), false));
      dispatch(didGetAvailabilityOfService());
    })
    .catch((error) => {
      dispatch(getAvailabilityOfService(error, true));
      dispatch(didGetAvailabilityOfService());
      logger.recordError(String(error));
      logger.log('Configuration: error al obtener la disponibilidad del servicio');
    });
  };
}
/**
 * Actualiza el objeto statusOfGettingAvailabilityOfService para saber
 * que el metodo getAvailabilityOfService va a ser ejecutado
 */
function willGetAvailabilityOfService() {

  return {
    type: WILL_GET_AVAILABILITY_OF_SERVICE,
  };
}
/**
 * Actualiza el objeto availabilityOfService si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingAvailabilityOfService
 * para mostrar un mensaje de error
 * @param  {string} payload string que muestra la disponibilidad del servicio
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getAvailabilityOfService(payload, error) {

  return {
    type: GET_AVAILABILITY_OF_SERVICE,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingAvailabilityOfService para saber
 * que el metodo getAvailabilityOfService ha sido ejecutado
 */
function didGetAvailabilityOfService() {

  return {
    type: DID_GET_AVAILABILITY_OF_SERVICE,
  };
}
/**
 * Metodo que obtiene los datos a mostrar
 * en el home
 */
export function executeGetHomeData(token, refreshToken) {

  return dispatch => {
    dispatch(willGetHomeData());

    requester.getWithToken(GET_HOME_DATA_URI, token, refreshToken, executeUpdateToken, dispatch)
    .then((data) => {
      dispatch(getHomeData(requester.getHomeDataObject(data), false));
      dispatch(didGetHomeData());
    })
    .catch((error) => {
      dispatch(getHomeData(error, true));
      dispatch(didGetHomeData());
      logger.recordError(String(error));
      logger.log('Configuration: error al obtener los datos del home');
    });
  };
}
/**
 * Actualiza el objeto statusOfGettingHomeData para saber
 * que el metodo getHomeData va a ser ejecutado
 */
function willGetHomeData() {

  return {
    type: WILL_GET_HOME_DATA
  };
}
/**
 * Actualiza el valor de pendingAssignments, observedLeads y notifications si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingHomeData
 * para mostrar un mensaje de error
 * @param  {object} payload objeto con los datos a mostrar en el home u objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getHomeData(payload, error) {

  return {
    type: GET_HOME_DATA,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingHomeData para saber
 * que el metodo getHomeData ha sido ejecutado
 */
function didGetHomeData() {

  return {
    type: DID_GET_HOME_DATA
  };
}
/**
 * Metodo que obtiene los sub estados
 * de los leads por estado, ademas la cantidad de leads
 * por sub estado
 * @param  {string} status asignacion, gestion, cierre u observado
 */
export function executeGetLeadsBySubStatus(status, token, refreshToken) {

  return dispatch => {
    dispatch(willGetLeadsBySubStatus());

    requester.getWithToken(
      LEADS_BY_SUB_STATUS_URI.replace('status', status),
      token,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(getLeadsBySubStatus(requester.getLeadsBySubStatus(data), false));
      dispatch(didGetLeadsBySubStatus());
    })
    .catch((error) => {
      dispatch(getLeadsBySubStatus(error, true));
      dispatch(didGetLeadsBySubStatus());
      logger.recordError(String(error));
      logger.log('Configuration: error al obtener los sub estados de leads por estado');
    });
  };
}
/**
 * Actualiza el objeto statusOfGettingLeadsBySubStatus para saber
 * que el metodo getLeadsBySubStatus va a ser ejecutado
 */
function willGetLeadsBySubStatus() {

  return {
    type: WILL_GET_LEADS_BY_SUB_STATUS
  };
}
/**
 * Actualiza el objeto leadsBySubStatus si todo salio bien,
 * caso contrario actualiza el objeto statusOfGettingLeadsBySubStatus
 * para mostrar un mensaje de error
 * @param  {object} payload objeto con los sub estados de los leads u objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getLeadsBySubStatus(payload, error) {

  return {
    type: GET_LEADS_BY_SUB_STATUS,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingLeadsBySubStatus para saber
 * que el metodo getLeadsBySubStatus ha sido ejecutado
 */
function didGetLeadsBySubStatus() {

  return {
    type: DID_GET_LEADS_BY_SUB_STATUS
  };
}