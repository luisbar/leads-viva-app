/******************
 * Project import *
 ******************/
import {
  WILL_GET_LEADS,
  GET_LEADS,
  DID_GET_LEADS,

  WILL_GET_MORE_LEADS,
  GET_MORE_LEADS,
  DID_GET_MORE_LEADS,
  
  WILL_SAVE_LEAD,
  SAVE_LEAD,
  DID_SAVE_LEAD,
  
  WILL_GET_A_LEAD,
  GET_A_LEAD,
  DID_GET_A_LEAD,
  
  WILL_UPDATE_LEAD,
  UPDATE_LEAD,
  DID_UPDATE_LEAD,
  
  WILL_GET_A_LEAD_WITH_STATUS_RECORD,
  GET_A_LEAD_WITH_STATUS_RECORD,
  DID_GET_A_LEAD_WITH_STATUS_RECORD,
  
  WILL_GET_A_LEAD_FOR_SAVING_AN_ACTION,
  GET_A_LEAD_FOR_SAVING_AN_ACTION,
  DID_GET_A_LEAD_FOR_SAVING_AN_ACTION,
  
  WILL_GET_PENDING_LEADS,
  GET_PENDING_LEADS,
  DID_GET_PENDING_LEADS,
  
  WILL_GET_MORE_PENDING_LEADS,
  GET_MORE_PENDING_LEADS,
  DID_GET_MORE_PENDING_LEADS,
  
  WILL_GET_A_PENDING_LEAD,
  GET_A_PENDING_LEAD,
  DID_GET_A_PENDING_LEAD,
  
  WILL_APPROVE_A_PENDING_LEAD,
  APPROVE_A_PENDING_LEAD,
  DID_APPROVE_A_PENDING_LEAD,

  WILL_REJECT_A_PENDING_LEAD,
  REJECT_A_PENDING_LEAD,
  DID_REJECT_A_PENDING_LEAD,
  
  WILL_GET_OBSERVED_LEADS,
  GET_OBSERVED_LEADS,
  DID_GET_OBSERVED_LEADS,

  WILL_GET_MORE_OBSERVED_LEADS,
  GET_MORE_OBSERVED_LEADS,
  DID_GET_MORE_OBSERVED_LEADS,
  
  WILL_RE_OPEN_A_LEAD,
  RE_OPEN_A_LEAD,
  DID_RE_OPEN_A_LEAD,
} from './actionType.js';
/*************
 * Constants *
 *************/
const initialState = {
  leads: [],
  totalOfPages: 0,
  statusOfGettingLeads: {
    leadsAreBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  statusOfGettingMoreLeads: {
    moreLeadsAreBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  statusOfSavingALead: {
    savingLeadIsBeingExecuted: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  lead: null,
  configurationData: null,
  statusOfGettingALead: {
    leadIsBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  statusOfUpdatingLead: {
    updatingLeadIsBeingExecuted: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  leadWithStatusRecord: null,
  statusOfGettingALeadWithStatusRecord: {
    leadWithStatusRecordIsBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  leadForSavingAnAction: null,
  configurationDataForSavingAnAction: null,
  statusOfGettingALeadForSavingAnAction: {
    leadForSavingAnActionIsBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  pendingLeads: [],
  totalOfPagesOfPendingLeads: 0,
  statusOfGettingPendingLeads: {
    pendingLeadsAreBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  statusOfGettingMorePendingLeads: {
    morePendingLeadsAreBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  pendingLead: null,
  statusOfGettingAPendingLead: {
    pendingLeadIsBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  statusOfApprovingOrRejectingAPendingLead: {
    approveOrRejectAPendingLeadIsBeingExecuted: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  observedLeads: [],
  totalOfPagesOfObservedLeads: 0,
  statusOfGettingObservedLeads: {
    observedLeadsAreBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  statusOfGettingMoreObservedLeads: {
    moreObservedLeadsAreBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  statusOfReOpeningALead: {
    reOpeningALeadIsBeingExecuted: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
};
/**
 * Gestiona el objeto leads y todo lo referente a el
 */
export default function leads(state = initialState, action) {

  switch (action.type) {

    case WILL_GET_LEADS:
      return Object.assign({}, state, {
        statusOfGettingLeads: {
          leadsAreBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        leads: [],
        totalOfPages: 0,
      });

    case GET_LEADS:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingLeads: {
            leadsAreBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          leads: action.payload.leads,
          totalOfPages: action.payload.totalOfPages,
        });

    case DID_GET_LEADS:
      return Object.assign({}, state, {
        statusOfGettingLeads: {
          leadsAreBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case WILL_GET_MORE_LEADS:
      return Object.assign({}, state, {
        statusOfGettingMoreLeads: {
          moreLeadsAreBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case GET_MORE_LEADS:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingMoreLeads: {
            moreLeadsAreBeingFetching: true,
            wasAnError: true,
            error: action.payload
          },
        });
      else
        return Object.assign({}, state, {
          leads: state.leads.concat(action.payload.leads),
          totalOfPages: action.payload.totalOfPages,
        });

    case DID_GET_MORE_LEADS:
      return Object.assign({}, state, {
        statusOfGettingMoreLeads: {
          moreLeadsAreBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_SAVE_LEAD:
      return Object.assign({}, state, {
        statusOfSavingALead: {
          savingLeadIsBeingExecuted: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case SAVE_LEAD:
      if (action.error)
        return Object.assign({}, state, {
          statusOfSavingALead: {
            savingLeadIsBeingExecuted: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return state;

    case DID_SAVE_LEAD:
      return Object.assign({}, state, {
        statusOfSavingALead: {
          savingLeadIsBeingExecuted: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
        
    case WILL_GET_A_LEAD:
      return Object.assign({}, state, {
        statusOfGettingALead: {
          leadIsBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        lead: null,
        configurationData: null,
      });

    case GET_A_LEAD:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingALead: {
            leadIsBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          lead: action.payload.lead,
          configurationData: action.payload.configurationData,
        });

    case DID_GET_A_LEAD:
      return Object.assign({}, state, {
        statusOfGettingALead: {
          leadIsBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_UPDATE_LEAD:
      return Object.assign({}, state, {
        statusOfUpdatingLead: {
          updatingLeadIsBeingExecuted: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case UPDATE_LEAD:
      if (action.error)
        return Object.assign({}, state, {
          statusOfUpdatingLead: {
            updatingLeadIsBeingExecuted: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return state;

    case DID_UPDATE_LEAD:
      return Object.assign({}, state, {
        statusOfUpdatingLead: {
          updatingLeadIsBeingExecuted: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_GET_A_LEAD_WITH_STATUS_RECORD:
      return Object.assign({}, state, {
        statusOfGettingALeadWithStatusRecord: {
          leadWithStatusRecordIsBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        leadWithStatusRecord: null,
      });

    case GET_A_LEAD_WITH_STATUS_RECORD:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingALeadWithStatusRecord: {
            leadWithStatusRecordIsBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          leadWithStatusRecord: action.payload,
        });

    case DID_GET_A_LEAD_WITH_STATUS_RECORD:
      return Object.assign({}, state, {
        statusOfGettingALeadWithStatusRecord: {
          leadWithStatusRecordIsBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_GET_A_LEAD_FOR_SAVING_AN_ACTION:
      return Object.assign({}, state, {
        statusOfGettingALeadForSavingAnAction: {
          leadForSavingAnActionIsBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        leadForSavingAnAction: null,
        configurationDataForSavingAnAction: null,
      });

    case GET_A_LEAD_FOR_SAVING_AN_ACTION:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingALeadForSavingAnAction: {
            leadForSavingAnActionIsBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          leadForSavingAnAction: action.payload.lead,
          configurationDataForSavingAnAction: action.payload.configurationData,
        });

    case DID_GET_A_LEAD_FOR_SAVING_AN_ACTION:
      return Object.assign({}, state, {
        statusOfGettingALeadForSavingAnAction: {
          leadForSavingAnActionIsBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_GET_PENDING_LEADS:
      return Object.assign({}, state, {
        statusOfGettingPendingLeads: {
          pendingLeadsAreBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        pendingLeads: [],
        totalOfPagesOfPendingLeads: 0,
      });

    case GET_PENDING_LEADS:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingPendingLeads: {
            pendingLeadsAreBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          pendingLeads: action.payload.pendingLeads,
          totalOfPagesOfPendingLeads: action.payload.totalOfPagesOfPendingLeads,
        });

    case DID_GET_PENDING_LEADS:
      return Object.assign({}, state, {
        statusOfGettingPendingLeads: {
          pendingLeadsAreBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_GET_MORE_PENDING_LEADS:
      return Object.assign({}, state, {
        statusOfGettingMorePendingLeads: {
          morePendingLeadsAreBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case GET_MORE_PENDING_LEADS:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingMorePendingLeads: {
            morePendingLeadsAreBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          pendingLeads: state.pendingLeads.concat(action.payload.pendingLeads),
          totalOfPagesOfPendingLeads: action.payload.totalOfPagesOfPendingLeads,
        });

    case DID_GET_MORE_PENDING_LEADS:
      return Object.assign({}, state, {
        statusOfGettingMorePendingLeads: {
          morePendingLeadsAreBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_GET_A_PENDING_LEAD:
      return Object.assign({}, state, {
        statusOfGettingAPendingLead: {
          pendingLeadIsBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        pendingLead: null,
      });

    case GET_A_PENDING_LEAD:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingAPendingLead: {
            pendingLeadIsBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          pendingLead: action.payload,
        });

    case DID_GET_A_PENDING_LEAD:
      return Object.assign({}, state, {
        statusOfGettingAPendingLead: {
          pendingLeadIsBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_APPROVE_A_PENDING_LEAD:
      return Object.assign({}, state, {
        statusOfApprovingOrRejectingAPendingLead: {
          approveOrRejectAPendingLeadIsBeingExecuted: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case APPROVE_A_PENDING_LEAD:
      if (action.error)
        return Object.assign({}, state, {
          statusOfApprovingOrRejectingAPendingLead: {
            approveOrRejectAPendingLeadIsBeingExecuted: true,
            wasAnError: true,
            error: action.payload
          },
        });
      else
        return state;

    case DID_APPROVE_A_PENDING_LEAD:
      return Object.assign({}, state, {
        statusOfApprovingOrRejectingAPendingLead: {
          approveOrRejectAPendingLeadIsBeingExecuted: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_REJECT_A_PENDING_LEAD:
      return Object.assign({}, state, {
        statusOfApprovingOrRejectingAPendingLead: {
          approveOrRejectAPendingLeadIsBeingExecuted: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case REJECT_A_PENDING_LEAD:
      if (action.error)
        return Object.assign({}, state, {
          statusOfApprovingOrRejectingAPendingLead: {
            approveOrRejectAPendingLeadIsBeingExecuted: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return state;

    case DID_REJECT_A_PENDING_LEAD:
      return Object.assign({}, state, {
        statusOfApprovingOrRejectingAPendingLead: {
          approveOrRejectAPendingLeadIsBeingExecuted: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case WILL_GET_OBSERVED_LEADS:
      return Object.assign({}, state, {
        statusOfGettingObservedLeads: {
          observedLeadsAreBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        observedLeads: [],
        totalOfPagesOfObservedLeads: 0,
      });

    case GET_OBSERVED_LEADS:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingObservedLeads: {
            observedLeadsAreBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          observedLeads: action.payload.observedLeads,
          totalOfPagesOfObservedLeads: action.payload.totalOfPagesOfObservedLeads,
        });

    case DID_GET_OBSERVED_LEADS:
      return Object.assign({}, state, {
        statusOfGettingObservedLeads: {
          observedLeadsAreBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_GET_MORE_OBSERVED_LEADS:
      return Object.assign({}, state, {
        statusOfGettingMoreObservedLeads: {
          moreObservedLeadsAreBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case GET_MORE_OBSERVED_LEADS:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingMoreObservedLeads: {
            moreObservedLeadsAreBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          observedLeads: state.observedLeads.concat(action.payload.observedLeads),
          totalOfPagesOfObservedLeads: action.payload.totalOfPagesOfObservedLeads,
        });

    case DID_GET_MORE_OBSERVED_LEADS:
      return Object.assign({}, state, {
        statusOfGettingMoreObservedLeads: {
          moreObservedLeadsAreBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_RE_OPEN_A_LEAD:
      return Object.assign({}, state, {
        statusOfReOpeningALead: {
          reOpeningALeadIsBeingExecuted: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case RE_OPEN_A_LEAD:
      if (action.error)
        return Object.assign({}, state, {
          statusOfReOpeningALead: {
            reOpeningALeadIsBeingExecuted: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return state;

    case DID_RE_OPEN_A_LEAD:
      return Object.assign({}, state, {
        statusOfReOpeningALead: {
          reOpeningALeadIsBeingExecuted: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    default:
      return state;
  }
}
