/******************
 * Project import *
 ******************/
import { requester, logger } from '../../service/index.js';
import { executeUpdateToken } from '../session/action.js';
import {
  LEADS_URI,
  SAVE_LEAD_URI,
  GET_A_LEAD_URI,
  UPDATE_LEAD_URI,
  GET_A_LEAD_WITH_STATUS_RECORD_URI,
  GET_A_LEAD_FOR_SAVING_AN_ACTION_URI,
  GET_PENDING_LEADS_URI,
  GET_LEAD_TO_REJECT_URI,
  GET_LEAD_TO_APPROVE_URI,
  APPROVE_A_PENDING_LEAD_URI,
  REJECT_A_PENDING_LEAD_URI,
  GET_OBSERVED_LEADS_URI,
  RE_OPEN_A_LEAD_URI,
} from '../../config.js';
import {
  WILL_GET_LEADS,
  GET_LEADS,
  DID_GET_LEADS,

  WILL_GET_MORE_LEADS,
  GET_MORE_LEADS,
  DID_GET_MORE_LEADS,
  
  WILL_SAVE_LEAD,
  SAVE_LEAD,
  DID_SAVE_LEAD,
  
  WILL_GET_A_LEAD,
  GET_A_LEAD,
  DID_GET_A_LEAD,
  
  WILL_UPDATE_LEAD,
  UPDATE_LEAD,
  DID_UPDATE_LEAD,
  
  WILL_GET_A_LEAD_WITH_STATUS_RECORD,
  GET_A_LEAD_WITH_STATUS_RECORD,
  DID_GET_A_LEAD_WITH_STATUS_RECORD,
  
  WILL_GET_A_LEAD_FOR_SAVING_AN_ACTION,
  GET_A_LEAD_FOR_SAVING_AN_ACTION,
  DID_GET_A_LEAD_FOR_SAVING_AN_ACTION,
  
  WILL_GET_PENDING_LEADS,
  GET_PENDING_LEADS,
  DID_GET_PENDING_LEADS,
  
  WILL_GET_MORE_PENDING_LEADS,
  GET_MORE_PENDING_LEADS,
  DID_GET_MORE_PENDING_LEADS,
  
  WILL_GET_A_PENDING_LEAD,
  GET_A_PENDING_LEAD,
  DID_GET_A_PENDING_LEAD,
  
  WILL_APPROVE_A_PENDING_LEAD,
  APPROVE_A_PENDING_LEAD,
  DID_APPROVE_A_PENDING_LEAD,

  WILL_REJECT_A_PENDING_LEAD,
  REJECT_A_PENDING_LEAD,
  DID_REJECT_A_PENDING_LEAD,
  
  WILL_GET_OBSERVED_LEADS,
  GET_OBSERVED_LEADS,
  DID_GET_OBSERVED_LEADS,

  WILL_GET_MORE_OBSERVED_LEADS,
  GET_MORE_OBSERVED_LEADS,
  DID_GET_MORE_OBSERVED_LEADS,
    
  WILL_RE_OPEN_A_LEAD,
  RE_OPEN_A_LEAD,
  DID_RE_OPEN_A_LEAD,
} from './actionType.js';
/**
 * Obtiene los leads filtrado por los parametros
 * @param  {int} page numero de pagina, es para la paginación
 * @param  {string} state estado del lead
 * @param  {string} search texto que introdujo el usuario en el campo de busqueda
 * @param  {array} source fuentes
 * @param  {string} initialDateOfCreation fecha inicial de creación del lead
 * @param  {string} endDateOfCreation fecha final de creación del lead
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token de actualización
 */
export function executeGetLeads(
  page,
  state,
  search,
  source,
  initialDateOfCreation,
  endDateOfCreation,
  accessToken,
  refreshToken
) {

  return dispatch => {
    dispatch(willGetLeads());

    requester.postWithToken(
      LEADS_URI,
      requester.getBodyForGettingLeads(
        page,
        state,
        search,
        source,
        initialDateOfCreation,
        endDateOfCreation
      ),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(getLeads({
        leads: requester.getLeadObjects(data.leads),
        totalOfPages: data.cantidad_paginas,
      }, false));
      dispatch(didGetLeads());
    })
    .catch((error) => {
      dispatch(getLeads(error, true));
      dispatch(didGetLeads());
      logger.recordError(String(error))
      logger.log('Lead: error al obtener los leads por primera vez')
    });
  }
}
/**
 * Actualiza el objeto statusOfGettingLeads para saber
 * que el metodo getLeads va a ser ejecutado
 */
function willGetLeads() {

  return {
    type: WILL_GET_LEADS,
  };
}
/**
 * Actualiza el objeto leads si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingLeads
 * para mostrar un mensaje de error
 * @param  {object} payload array de leads
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getLeads(payload, error) {

  return {
    type: GET_LEADS,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingLeads para saber
 * que el metodo getLeads ha sido ejecutado
 */
function didGetLeads() {

  return {
    type: DID_GET_LEADS,
  };
}
/**
 * Obtiene mas leads filtrado por los parametros
 * @param  {int} page numero de pagina, es para la paginación
 * @param  {string} state estado del lead
 * @param  {string} search texto que introdujo el usuario en el campo de busqueda
 * @param  {array} source fuentes
 * @param  {string} initialDateOfCreation fecha inicial de creación del lead
 * @param  {string} endDateOfCreation fecha final de creación del lead
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token de actualización
 */
export function executeGetMoreLeads(
  page,
  state,
  search,
  source,
  initialDateOfCreation,
  endDateOfCreation,
  accessToken,
  refreshToken
) {

  return dispatch => {
    dispatch(willGetMoreLeads());

    requester.postWithToken(
      LEADS_URI,
      requester.getBodyForGettingLeads(
        page,
        state,
        search,
        source,
        initialDateOfCreation,
        endDateOfCreation
      ),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(getMoreLeads({
        leads: requester.getLeadObjects(data.leads),
        totalOfPages: data.cantidad_paginas,
      }, false));
      dispatch(didGetMoreLeads());
    })
    .catch((error) => {
      dispatch(getMoreLeads(error, true));
      dispatch(didGetMoreLeads());
      logger.recordError(String(error))
      logger.log('Lead: error al obtener mas leads')
    });
  }
}
/**
 * Actualiza el objeto statusOfGettingMoreLeads para saber
 * que el metodo getMoreLeads va a ser ejecutado
 */
function willGetMoreLeads() {

  return {
    type: WILL_GET_MORE_LEADS,
  };
}
/**
 * Actualiza el objeto leads si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingMoreLeads
 * para mostrar un mensaje de error
 * @param  {object} payload array de leads
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getMoreLeads(payload, error) {

  return {
    type: GET_MORE_LEADS,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingMoreLeads para saber
 * que el metodo getMoreLeads ha sido ejecutado
 */
function didGetMoreLeads() {

  return {
    type: DID_GET_MORE_LEADS,
  };
}
/**
 * Guarda un nuevo lead en la base de datos remota
 * mediante la api
 */
export function executeSaveLead(leadToSave, accessToken, refreshToken) {
  
  return dispatch => {
    dispatch(willSaveLead());
    
    requester.postWithToken(
      SAVE_LEAD_URI,
      requester.getBodyForSavingALead(leadToSave),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(saveLead(null, false));
      dispatch(didSaveLead());
    })
    .catch((error) => {
      dispatch(saveLead(error, true));
      dispatch(didSaveLead());
      logger.recordError(String(error));
      logger.log('Lead: error al guardar un lead');
    })
  };
}
/**
 * Actualiza el objeto statusOfSavingALead para saber
 * que el metodo saveLead va a ser ejecutado
 */
function willSaveLead() {

  return {
    type: WILL_SAVE_LEAD,
  };
}
/**
 * No modifica nada en el store si todo salió bien,
 * caso contrario actualiza el objeto statusOfSavingALead
 * para mostrar un mensaje de error
 * @param  {object} payload objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function saveLead(payload, error) {

  return {
    type: SAVE_LEAD,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfSavingALead para saber
 * que el metodo saveLead ha sido ejecutado
 */
function didSaveLead() {

  return {
    type: DID_SAVE_LEAD,
  };
}
/**
 * Obtiene el lead mediante la api con el id especificado
 * por parametro
 * @param  {string} leadId identificador del lead
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token para solicitar un nuevo token de acceso
 */
export function executeGetALead(leadId, accessToken, refreshToken) {

  return dispatch => {
    dispatch(willGetALead());
    
    requester.getWithToken(
      GET_A_LEAD_URI.replace('leadId', leadId),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => requester.getConfigurationDataAndLeadObject(data))
    .then((payload) => {
      dispatch(getALead(payload, false));
      dispatch(didGetALead());
    })
    .catch((error) => {
      dispatch(getALead(error, true));
      dispatch(didGetALead());
      logger.recordError(String(error))
      logger.log('Lead: error al obtener un lead')
    });
  }
}
/**
 * Actualiza el objeto statusOfGettingALead para saber
 * que el metodo getALead va a ser ejecutado
 */
function willGetALead() {

  return {
    type: WILL_GET_A_LEAD,
  };
}
/**
 * Modifica el objeto lead y configurationData si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingALead
 * para mostrar un mensaje de error
 * @param  {object} payload Objeto lead u objeto error 
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getALead(payload, error) {

  return {
    type: GET_A_LEAD,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingALead para saber
 * que el metodo getALead ha sido ejecutado
 */
function didGetALead() {

  return {
    type: DID_GET_A_LEAD,
  };
}
/**
 * Actualiza un lead en la base de datos remota
 * mediante la api
 */
export function executeUpdateLead(leadId, leadToUpdate, accessToken, refreshToken) {
  
  return dispatch => {
    dispatch(willUpdateLead());
    
    requester.postWithToken(
      UPDATE_LEAD_URI.replace('leadId', leadId),
      requester.getBodyForSavingALead(leadToUpdate),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(updateLead(null, false));
      dispatch(didUpdateLead());
    })
    .catch((error) => {
      dispatch(updateLead(error, true));
      dispatch(didUpdateLead());
      logger.recordError(String(error));
      logger.log('Lead: error al actualizar un lead');
    })
  };
}
/**
 * Actualiza el objeto statusOfUpdatingLead para saber
 * que el metodo updateLead va a ser ejecutado
 */
function willUpdateLead() {

  return {
    type: WILL_UPDATE_LEAD,
  };
}
/**
 * No modifica nada en el store si todo salió bien,
 * caso contrario actualiza el objeto statusOfUpdatingLead
 * para mostrar un mensaje de error
 * @param  {object} payload objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function updateLead(payload, error) {

  return {
    type: UPDATE_LEAD,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfUpdatingLead para saber
 * que el metodo updateLead ha sido ejecutado
 */
function didUpdateLead() {

  return {
    type: DID_UPDATE_LEAD,
  };
}
/**
 * Obtiene un lead y su historial de estados mediante la api con el id especificado
 * por parametro
 * @param  {string} leadId identificador del lead
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token para solicitar un nuevo token de acceso
 */
export function executeGetALeadWithStatusRecord(leadId, accessToken, refreshToken) {

  return dispatch => {
    dispatch(willGetALeadWithStatusRecord());
    
    requester.getWithToken(
      GET_A_LEAD_WITH_STATUS_RECORD_URI.replace('leadId', leadId),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => requester.getALeadWithStatusRecordObject(data))
    .then((payload) => {
      dispatch(getALeadWithStatusRecord(payload, false));
      dispatch(didGetALeadWithStatusRecord());
    })
    .catch((error) => {
      dispatch(getALeadWithStatusRecord(error, true));
      dispatch(didGetALeadWithStatusRecord());
      logger.recordError(String(error))
      logger.log('Lead: error al obtener un lead con su historial de estados')
    });
  }
}
/**
 * Actualiza el objeto statusOfGettingALeadWithStatusRecord para saber
 * que el metodo getALeadWithStatusRecord va a ser ejecutado
 */
function willGetALeadWithStatusRecord() {

  return {
    type: WILL_GET_A_LEAD_WITH_STATUS_RECORD,
  };
}
/**
 * Modifica el objeto leadWithStatusRecord si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingALeadWithStatusRecord
 * para mostrar un mensaje de error
 * @param  {object} payload Objeto lead u objeto error 
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getALeadWithStatusRecord(payload, error) {

  return {
    type: GET_A_LEAD_WITH_STATUS_RECORD,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingALeadWithStatusRecord para saber
 * que el metodo getALeadWithStatusRecord ha sido ejecutado
 */
function didGetALeadWithStatusRecord() {

  return {
    type: DID_GET_A_LEAD_WITH_STATUS_RECORD,
  };
}
/**
 * Obtiene un lead para registrar una acción en ese lead
 * @param  {string} leadId identificador del lead
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token para solicitar un nuevo token de acceso
 */
export function executeGetALeadForSavingAnAction(leadId, accessToken, refreshToken) {

  return dispatch => {
    dispatch(willGetALeadForSavingAnAction());
    
    requester.getWithToken(
      GET_A_LEAD_FOR_SAVING_AN_ACTION_URI.replace('leadId', leadId),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => requester.getALeadForSavingAnAction(data))
    .then((payload) => {
      dispatch(getALeadForSavingAnAction(payload, false));
      dispatch(didGetALeadForSavingAnAction());
    })
    .catch((error) => {
      dispatch(getALeadForSavingAnAction(error, true));
      dispatch(didGetALeadForSavingAnAction());
      logger.recordError(String(error))
      logger.log('Lead: error al obtener un lead para guardar una acción')
    });
  }
}
/**
 * Actualiza el objeto statusOfGettingALeadForSavingAnAction para saber
 * que el metodo getALeadForSavingAnAction va a ser ejecutado
 */
function willGetALeadForSavingAnAction() {

  return {
    type: WILL_GET_A_LEAD_FOR_SAVING_AN_ACTION,
  };
}
/**
 * Modifica el objeto leadForSavingAnAction si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingALeadForSavingAnAction
 * para mostrar un mensaje de error
 * @param  {object} payload Objeto lead u objeto error 
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getALeadForSavingAnAction(payload, error) {

  return {
    type: GET_A_LEAD_FOR_SAVING_AN_ACTION,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingALeadForSavingAnAction para saber
 * que el metodo getALeadForSavingAnAction ha sido ejecutado
 */
function didGetALeadForSavingAnAction() {

  return {
    type: DID_GET_A_LEAD_FOR_SAVING_AN_ACTION,
  };
}
/**
 * Obtiene los leads pendientes por pagina
 * @param  {int} page numero de pagina, es para la paginación
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token de actualización
 */
export function executeGetPendingLeads(
  page,
  accessToken,
  refreshToken
) {

  return dispatch => {
    dispatch(willGetPendingLeads());

    requester.getWithToken(
      GET_PENDING_LEADS_URI.replace('page', page),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(getPendingLeads({
        pendingLeads: requester.getPendingLeadObjects(data.asignaciones_pendientes),
        totalOfPagesOfPendingLeads: data.cantidad_paginas,
      }, false));
      dispatch(didGetPendingLeads());
    })
    .catch((error) => {
      dispatch(getPendingLeads(error, true));
      dispatch(didGetPendingLeads());
      logger.recordError(String(error))
      logger.log('Lead: error al obtener los leads pendientes por primera vez')
    });
  }
}
/**
 * Actualiza el objeto statusOfGettingPendingLeads para saber
 * que el metodo getPendingLeads va a ser ejecutado
 */
function willGetPendingLeads() {

  return {
    type: WILL_GET_PENDING_LEADS,
  };
}
/**
 * Actualiza el objeto pendingLeads si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingPendingLeads
 * para mostrar un mensaje de error
 * @param  {object} payload array de leads pendientes u objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getPendingLeads(payload, error) {

  return {
    type: GET_PENDING_LEADS,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingPendingLeads para saber
 * que el metodo getPendingLeads ha sido ejecutado
 */
function didGetPendingLeads() {

  return {
    type: DID_GET_PENDING_LEADS,
  };
}
/**
 * Obtiene mas leads pendientes
 * @param  {int} page numero de pagina, es para la paginación
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token de actualización
 */
export function executeGetMorePendingLeads(
  page,
  accessToken,
  refreshToken
) {

  return dispatch => {
    dispatch(willGetMorePendingLeads());

    requester.getWithToken(
      GET_PENDING_LEADS_URI.replace('page', page),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(getMorePendingLeads({
        pendingLeads: requester.getPendingLeadObjects(data.asignaciones_pendientes),
        totalOfPagesOfPendingLeads: data.cantidad_paginas,
      }, false));
      dispatch(didGetMorePendingLeads());
    })
    .catch((error) => {
      dispatch(getMorePendingLeads(error, true));
      dispatch(didGetMorePendingLeads());
      logger.recordError(String(error))
      logger.log('Lead: error al obtener mas leads pendientes')
    });
  }
}
/**
 * Actualiza el objeto statusOfGettingMorePendingLeads para saber
 * que el metodo getMorePendingLeads va a ser ejecutado
 */
function willGetMorePendingLeads() {

  return {
    type: WILL_GET_MORE_PENDING_LEADS,
  };
}
/**
 * Actualiza el objeto pendingLeads si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingMorePendingLeads
 * para mostrar un mensaje de error
 * @param  {object} payload array de leads pendientes u objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getMorePendingLeads(payload, error) {

  return {
    type: GET_MORE_PENDING_LEADS,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingMorePendingLeads para saber
 * que el metodo getMorePendingLeads ha sido ejecutado
 */
function didGetMorePendingLeads() {

  return {
    type: DID_GET_MORE_PENDING_LEADS,
  };
}
/**
 * Obtiene el lead a ser aprobado
 * @param  {bool} approbation bandera para saber si se aprueba o rechaza un lead pendiente
 * @param  {int} leadId identificador del lead a obtener mediante la api
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token de actualización
 */
export function executeGetAPendingLead(
  approbation,
  leadId,
  accessToken,
  refreshToken
) {

  return dispatch => {
    dispatch(willGetAPendingLead());

    requester.getWithToken(
      approbation
        ? GET_LEAD_TO_APPROVE_URI.replace('leadId', leadId)
        : GET_LEAD_TO_REJECT_URI.replace('leadId', leadId),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(getAPendingLead(requester.getAPendingLeadObject(data), false));
      dispatch(didGetAPendingLead());
    })
    .catch((error) => {
      dispatch(getAPendingLead(error, true));
      dispatch(didGetAPendingLead());
      logger.recordError(String(error))
      logger.log('Lead: error al obtener un lead pendiente')
    });
  }
}
/**
 * Actualiza el objeto statusOfGettingAPendingLead para saber
 * que el metodo getAPendingLead va a ser ejecutado
 */
function willGetAPendingLead() {

  return {
    type: WILL_GET_A_PENDING_LEAD,
  };
}
/**
 * Actualiza el objeto pendingLead si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingAPendingLead
 * para mostrar un mensaje de error
 * @param  {object} payload lead pendiente u objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getAPendingLead(payload, error) {

  return {
    type: GET_A_PENDING_LEAD,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingAPendingLead para saber
 * que el metodo getAPendingLead ha sido ejecutado
 */
function didGetAPendingLead() {

  return {
    type: DID_GET_A_PENDING_LEAD,
  };
}
/**
 * Aprueba un lead pendiente de asignacion
 * @param  {string} leadId id del lead a aprobar
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token para actualizar el token de acceso
 */
export function executeApproveAPendingLead(leadId, accessToken, refreshToken) {
  
  return dispatch => {
    dispatch(willApproveAPendingLead());
    
    requester.postWithToken(
      APPROVE_A_PENDING_LEAD_URI.replace('leadId', leadId),
      {},
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(approveAPendingLead(data, false));
      dispatch(didApproveAPendingLead());
    })
    .catch((error) => {
      dispatch(approveAPendingLead(error, true));
      dispatch(didApproveAPendingLead());
      logger.recordError(String(error));
      logger.log('Lead: error al aprobar un lead pendiente de asignacion');
    })
  };
}
/**
 * Actualiza el objeto statusOfApprovingOrRejectingAPendingLead para saber
 * que el metodo approveAPendingLead va a ser ejecutado
 */
function willApproveAPendingLead() {

  return {
    type: WILL_APPROVE_A_PENDING_LEAD,
  };
}
/**
 * No modifica nada en el store si todo salió bien,
 * caso contrario actualiza el objeto statusOfApprovingOrRejectingAPendingLead
 * para mostrar un mensaje de error
 * @param  {object} payload objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function approveAPendingLead(payload, error) {

  return {
    type: APPROVE_A_PENDING_LEAD,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfApprovingOrRejectingAPendingLead para saber
 * que el metodo approveAPendingLead ha sido ejecutado
 */
function didApproveAPendingLead() {

  return {
    type: DID_APPROVE_A_PENDING_LEAD,
  };
}
/**
 * Aprueba un lead pendiente de asignacion
 * @param  {string} leadId id del lead a aprobar
 * @param  {string} justification justificacion de rechazo del lead
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token para actualizar el token de acceso
 */
export function executeRejectAPendingLead(leadId, justification, accessToken, refreshToken) {
  
  return dispatch => {
    dispatch(willRejectAPendingLead());
    
    requester.postWithToken(
      REJECT_A_PENDING_LEAD_URI.replace('leadId', leadId),
      requester.getBodyForRejectingAPendingLead(justification),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(rejectAPendingLead(data, false));
      dispatch(didRejectAPendingLead());
    })
    .catch((error) => {
      dispatch(rejectAPendingLead(error, true));
      dispatch(didRejectAPendingLead());
      logger.recordError(String(error));
      logger.log('Lead: error al rechazar un lead pendiente de asignacion');
    })
  };
}
/**
 * Actualiza el objeto statusOfApprovingOrRejectingAPendingLead para saber
 * que el metodo rejectAPendingLead va a ser ejecutado
 */
function willRejectAPendingLead() {

  return {
    type: WILL_REJECT_A_PENDING_LEAD,
  };
}
/**
 * No modifica nada en el store si todo salió bien,
 * caso contrario actualiza el objeto statusOfApprovingOrRejectingAPendingLead
 * para mostrar un mensaje de error
 * @param  {object} payload objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function rejectAPendingLead(payload, error) {

  return {
    type: REJECT_A_PENDING_LEAD,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfApprovingOrRejectingAPendingLead para saber
 * que el metodo rejectAPendingLead ha sido ejecutado
 */
function didRejectAPendingLead() {

  return {
    type: DID_REJECT_A_PENDING_LEAD,
  };
}
/**
 * Obtiene los leads observados por pagina
 * @param  {int} page numero de pagina, es para la paginación
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token de actualización
 */
export function executeGetObservedLeads(
  page,
  accessToken,
  refreshToken
) {

  return dispatch => {
    dispatch(willGetObservedLeads());

    requester.getWithToken(
      GET_OBSERVED_LEADS_URI.replace('page', page),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(getObservedLeads({
        observedLeads: requester.getObservedLeadObjects(data.leads_observados),
        totalOfPagesOfObservedLeads: data.cantidad_paginas,
      }, false));
      dispatch(didGetObservedLeads());
    })
    .catch((error) => {
      dispatch(getObservedLeads(error, true));
      dispatch(didGetObservedLeads());
      logger.recordError(String(error))
      logger.log('Lead: error al obtener los leads observados por primera vez')
    });
  }
}
/**
 * Actualiza el objeto statusOfGettingObservedLeads para saber
 * que el metodo getObservedLeads va a ser ejecutado
 */
function willGetObservedLeads() {

  return {
    type: WILL_GET_OBSERVED_LEADS,
  };
}
/**
 * Actualiza el objeto observedLeads si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingObservedLeads
 * para mostrar un mensaje de error
 * @param  {object} payload array de leads observados u objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getObservedLeads(payload, error) {

  return {
    type: GET_OBSERVED_LEADS,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingObservedLeads para saber
 * que el metodo getObservedLeads ha sido ejecutado
 */
function didGetObservedLeads() {

  return {
    type: DID_GET_OBSERVED_LEADS,
  };
}
/**
 * Obtiene mas leads observados
 * @param  {int} page numero de pagina, es para la paginación
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token de actualización
 */
export function executeGetMoreObservedLeads(
  page,
  accessToken,
  refreshToken
) {

  return dispatch => {
    dispatch(willGetMoreObservedLeads());

    requester.getWithToken(
      GET_OBSERVED_LEADS_URI.replace('page', page),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(getMoreObservedLeads({
        observedLeads: requester.getObservedLeadObjects(data.leads_observados),
        totalOfPagesOfObservedLeads: data.cantidad_paginas,
      }, false));
      dispatch(didGetMoreObservedLeads());
    })
    .catch((error) => {
      dispatch(getMoreObservedLeads(error, true));
      dispatch(didGetMoreObservedLeads());
      logger.recordError(String(error))
      logger.log('Lead: error al obtener mas leads observados')
    });
  }
}
/**
 * Actualiza el objeto statusOfGettingMoreObservedLeads para saber
 * que el metodo getMoreObservedLeads va a ser ejecutado
 */
function willGetMoreObservedLeads() {

  return {
    type: WILL_GET_MORE_OBSERVED_LEADS,
  };
}
/**
 * Actualiza el objeto observedLeads si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingMoreObservedLeads
 * para mostrar un mensaje de error
 * @param  {object} payload array de leads observados u objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getMoreObservedLeads(payload, error) {

  return {
    type: GET_MORE_OBSERVED_LEADS,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingMoreObservedLeads para saber
 * que el metodo getMoreObservedLeads ha sido ejecutado
 */
function didGetMoreObservedLeads() {

  return {
    type: DID_GET_MORE_OBSERVED_LEADS,
  };
}
/**
 * Reapertura un lead observado por el id de observación
 * @param  {string} observationId id de observación
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token para actualizar el token de acceso
 */
export function executeReOpenALead(observationId, accessToken, refreshToken) {
  
  return dispatch => {
    dispatch(willReOpenALead());
    
    requester.getWithToken(
      RE_OPEN_A_LEAD_URI.replace('observationId', observationId),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(reOpenALead(data, false));
      dispatch(didReOpenALead());
    })
    .catch((error) => {
      dispatch(reOpenALead(error, true));
      dispatch(didReOpenALead());
      logger.recordError(String(error));
      logger.log('Lead: error al reaperturar un lead observado');
    })
  };
}
/**
 * Actualiza el objeto statusOfReOpeningALead para saber
 * que el metodo reOpenALead va a ser ejecutado
 */
function willReOpenALead() {

  return {
    type: WILL_RE_OPEN_A_LEAD,
  };
}
/**
 * No modifica nada en el store si todo salió bien,
 * caso contrario actualiza el objeto statusOfReOpeningALead
 * para mostrar un mensaje de error
 * @param  {object} payload objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function reOpenALead(payload, error) {

  return {
    type: RE_OPEN_A_LEAD,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfReOpeningALead para saber
 * que el metodo reOpenALead ha sido ejecutado
 */
function didReOpenALead() {

  return {
    type: DID_RE_OPEN_A_LEAD,
  };
}