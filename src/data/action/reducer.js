/******************
 * Project import *
 ******************/
import {
  WILL_GET_ACTIONS,
  GET_ACTIONS,
  DID_GET_ACTIONS,
  
  WILL_SAVE_AN_ACTION,
  SAVE_AN_ACTION,
  DID_SAVE_AN_ACTION,
  
  WILL_GET_ACTIONS_BY_DATE,
  GET_ACTIONS_BY_DATE,
  DID_GET_ACTIONS_BY_DATE,
  
  WILL_GET_AN_ACTION_FOR_EDITION,
  GET_AN_ACTION_FOR_EDITION,
  DID_GET_AN_ACTION_FOR_EDITION,
  
  WILL_UPDATE_AN_ACTION,
  UPDATE_AN_ACTION,
  DID_UPDATE_AN_ACTION,
  
  WILL_GET_AN_ACTION,
  GET_AN_ACTION,
  DID_GET_AN_ACTION,
} from './actionType.js';
/*************
 * Constants *
 *************/
const initialState = {
  actions: null,
  lead: null,
  statusOfGettingActions: {
    actionsAreBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  statusOfSavingAnAction: {
    savingAnActionIsBeingExecuted: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  actionsByDate: null,
  statusOfGettingActionsByDate: {
    actionsByDateAreBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  actionForEdition: null,
  statusOfGettingAnActionForEdition: {
    actionForEditionIsBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  statusOfUpdatingAnAction: {
    updatingAnActionIsBeingExecuted: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  action: null,
  statusOfGettingAnAction: {
    actionIsBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
};
/**
 * Gestiona las acciones de un lead
 */
export default function action(state = initialState, action) {

  switch (action.type) {

    case WILL_GET_ACTIONS:
      return Object.assign({}, state, {
        statusOfGettingActions: {
          actionsAreBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        actions: null,
        lead: null,
      });

    case GET_ACTIONS:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingActions: {
            actionsAreBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          actions: action.payload.actions,
          lead: action.payload.lead,
        });

    case DID_GET_ACTIONS:
      return Object.assign({}, state, {
        statusOfGettingActions: {
          actionsAreBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
    
    case WILL_SAVE_AN_ACTION:
      return Object.assign({}, state, {
        statusOfSavingAnAction: {
          savingAnActionIsBeingExecuted: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case SAVE_AN_ACTION:
      if (action.error)
        return Object.assign({}, state, {
          statusOfSavingAnAction: {
            savingAnActionIsBeingExecuted: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return state;

    case DID_SAVE_AN_ACTION:
      return Object.assign({}, state, {
        statusOfSavingAnAction: {
          savingAnActionIsBeingExecuted: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_GET_ACTIONS_BY_DATE:
      return Object.assign({}, state, {
        statusOfGettingActionsByDate: {
          actionsByDateAreBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        actionsByDate: null,
      });

    case GET_ACTIONS_BY_DATE:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingActionsByDate: {
            actionsByDateAreBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          actionsByDate: action.payload,
        });

    case DID_GET_ACTIONS_BY_DATE:
      return Object.assign({}, state, {
        statusOfGettingActionsByDate: {
          actionsByDateAreBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_GET_AN_ACTION_FOR_EDITION:
      return Object.assign({}, state, {
        statusOfGettingAnActionForEdition: {
          actionForEditionIsBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        actionForEdition: null,
      });

    case GET_AN_ACTION_FOR_EDITION:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingAnActionForEdition: {
            actionForEditionIsBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          actionForEdition: action.payload,
        });

    case DID_GET_AN_ACTION_FOR_EDITION:
      return Object.assign({}, state, {
        statusOfGettingAnActionForEdition: {
          actionForEditionIsBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_UPDATE_AN_ACTION:
      return Object.assign({}, state, {
        statusOfUpdatingAnAction: {
          updatingAnActionIsBeingExecuted: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case UPDATE_AN_ACTION:
      if (action.error)
        return Object.assign({}, state, {
          statusOfUpdatingAnAction: {
            updatingAnActionIsBeingExecuted: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return state;

    case DID_UPDATE_AN_ACTION:
      return Object.assign({}, state, {
        statusOfUpdatingAnAction: {
          updatingAnActionIsBeingExecuted: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_GET_AN_ACTION:
      return Object.assign({}, state, {
        statusOfGettingAnAction: {
          actionIsBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        action: null,
      });

    case GET_AN_ACTION:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingAnAction: {
            actionIsBeingFetching: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          action: action.payload,
        });

    case DID_GET_AN_ACTION:
      return Object.assign({}, state, {
        statusOfGettingAnAction: {
          actionIsBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    default:
      return state;
  }
}
