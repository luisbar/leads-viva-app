/******************
 * Project import *
 ******************/
import { requester, logger } from '../../service/index.js';
import { executeUpdateToken } from '../session/action.js';
import {
  ACTIONS_RECORD_URI,
  SAVE_AN_ACTION_URI,
  GET_ACTIONS_BY_DATE_URI,
  ACTION_FOR_EDITION_URI,
  UPDATE_AN_ACTION_URI,
  GET_AN_ACTION_URI,
} from '../../config.js';
import {
  WILL_GET_ACTIONS,
  GET_ACTIONS,
  DID_GET_ACTIONS,
  
  WILL_SAVE_AN_ACTION,
  SAVE_AN_ACTION,
  DID_SAVE_AN_ACTION,
  
  WILL_GET_ACTIONS_BY_DATE,
  GET_ACTIONS_BY_DATE,
  DID_GET_ACTIONS_BY_DATE,
  
  WILL_GET_AN_ACTION_FOR_EDITION,
  GET_AN_ACTION_FOR_EDITION,
  DID_GET_AN_ACTION_FOR_EDITION,
  
  WILL_UPDATE_AN_ACTION,
  UPDATE_AN_ACTION,
  DID_UPDATE_AN_ACTION,
  
  WILL_GET_AN_ACTION,
  GET_AN_ACTION,
  DID_GET_AN_ACTION,
} from './actionType.js';
/**
 * Metodo que obtiene el historial de acciones de un lead
 * @param  {string} leadId id del lead
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token para renovar el token de acceso
 */
export function executeGetActions(leadId, accessToken, refreshToken) {

  return dispatch => {
    dispatch(willGetActions());

    requester.getWithToken(
      ACTIONS_RECORD_URI.replace('leadId', leadId),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(getActions(requester.getActionsObject(data), false));
      dispatch(didGetActions());
    })
    .catch((error) => {
      dispatch(getActions(error, true));
      dispatch(didGetActions());
      logger.recordError(String(error));
      logger.log('Action: error al obtener el historial de acciones');
    });
  };
}
/**
 * Actualiza el objeto statusOfGettingActions para saber
 * que el metodo getActions va a ser ejecutado
 */
function willGetActions() {

  return {
    type: WILL_GET_ACTIONS,
  };
}
/**
 * Actualiza el objeto actions y lead si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingActions
 * para mostrar un mensaje de error
 * @param  {object} payload objeto con las acciones u objeto con el error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getActions(payload, error) {

  return {
    type: GET_ACTIONS,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingActions para saber
 * que el metodo getActions ha sido ejecutado
 */
function didGetActions() {

  return {
    type: DID_GET_ACTIONS,
  };
}
/**
 * Guarda una accion en la base de datos remota
 * mediante la api
 */
export function executeSaveAnAction(
  leadId,
  actionToSave,
  accessToken,
  refreshToken
) {

  return dispatch => {
    dispatch(willSaveAnAction());
    
    requester.postWithToken(
      SAVE_AN_ACTION_URI.replace('leadId', leadId),
      requester.getBodyForSavingAnAction(actionToSave),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(saveAnAction(null, false));
      dispatch(didSaveAnAction());
    })
    .catch((error) => {
      dispatch(saveAnAction(error, true));
      dispatch(didSaveAnAction());
      logger.recordError(String(error));
      logger.log('Action: error al guardar una acción');
    })
  };
}
/**
 * Actualiza el objeto statusOfSavingAnAction para saber
 * que el metodo saveAnAction va a ser ejecutado
 */
function willSaveAnAction() {

  return {
    type: WILL_SAVE_AN_ACTION,
  };
}
/**
 * No modifica nada en el store si todo salió bien,
 * caso contrario actualiza el objeto statusOfSavingAnAction
 * para mostrar un mensaje de error
 * @param  {object} payload objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function saveAnAction(payload, error) {

  return {
    type: SAVE_AN_ACTION,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfSavingAnAction para saber
 * que el metodo saveAnAction ha sido ejecutado
 */
function didSaveAnAction() {

  return {
    type: DID_SAVE_AN_ACTION,
  };
}
/**
 * Obtiene las acciones por fecha
 * @param  {string} date fecha
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token para renovar el token de acceso
 */
export function executeGetActionsByDate(
  date,
  accessToken,
  refreshToken
) {

  return dispatch => {
    dispatch(willGetActionsBytDate());
    
    requester.postWithToken(
      GET_ACTIONS_BY_DATE_URI,
      requester.getBodyForGettingActionsByDate(date),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(getActionsByDate(requester.getActionObjectsForAppointmentView(data), false));
      dispatch(didGetActionsBytDate());
    })
    .catch((error) => {
      dispatch(getActionsByDate(error, true));
      dispatch(didGetActionsBytDate());
      logger.recordError(String(error));
      logger.log('Action: error al obtener acciones por fecha');
    })
  };
}
/**
 * Actualiza el objeto statusOfGettingActionsByDate para saber
 * que el metodo getActionsByDate va a ser ejecutado
 */
function willGetActionsBytDate() {

  return {
    type: WILL_GET_ACTIONS_BY_DATE,
  };
}
/**
 * Modifica el objeto actionsByDate si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingActionsByDate
 * para mostrar un mensaje de error
 * @param  {object} payload objeto con acciones u objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getActionsByDate(payload, error) {

  return {
    type: GET_ACTIONS_BY_DATE,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingActionsByDate para saber
 * que el metodo getActionsByDate ha sido ejecutado
 */
function didGetActionsBytDate() {

  return {
    type: DID_GET_ACTIONS_BY_DATE,
  };
}
/**
 * Metodo que obtiene una accion para ser editada
 * @param  {string} actionId id de la acción
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token para renovar el token de acceso
 */
export function executeGetAnActionForEdition(actionId, accessToken, refreshToken) {

  return dispatch => {
    dispatch(willGetAnActionForEdition());

    requester.getWithToken(
      ACTION_FOR_EDITION_URI.replace('actionId', actionId),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(getAnActionForEdition(requester.getAnActionForEditionObject(data), false));
      dispatch(didGetAnActionForEdition());
    })
    .catch((error) => {
      dispatch(getAnActionForEdition(error, true));
      dispatch(didGetAnActionForEdition());
      logger.recordError(String(error));
      logger.log('Action: error al obtener una acción para edición');
    });
  };
}
/**
 * Actualiza el objeto statusOfGettingAnActionForEdition para saber
 * que el metodo getAnActionForEdition va a ser ejecutado
 */
function willGetAnActionForEdition() {

  return {
    type: WILL_GET_AN_ACTION_FOR_EDITION,
  };
}
/**
 * Actualiza el objeto actionForEdition si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingAnActionForEdition
 * para mostrar un mensaje de error
 * @param  {object} payload objeto con la acción a editar u objeto con el error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getAnActionForEdition(payload, error) {

  return {
    type: GET_AN_ACTION_FOR_EDITION,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingAnActionForEdition para saber
 * que el metodo getAnActionForEdition ha sido ejecutado
 */
function didGetAnActionForEdition() {

  return {
    type: DID_GET_AN_ACTION_FOR_EDITION,
  };
}
/**
 * Actualiza una acción en la base de datos remota
 * mediante la api
 * @param  {string} actionId id de la acción a editar
 * @param  {object} actionToUpdate acción editada
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token para renovar el token de acceso
 */
export function executeUpdateAnAction(actionId, actionToUpdate, accessToken, refreshToken) {

  return dispatch => {
    dispatch(willUpdateAnAction());
    
    requester.postWithToken(
      UPDATE_AN_ACTION_URI.replace('actionId', actionId),
      requester.getBodyForUpdateAnAction(actionToUpdate),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(updateAnAction(null, false));
      dispatch(didUpdateAnAction());
    })
    .catch((error) => {
      dispatch(updateAnAction(error, true));
      dispatch(didUpdateAnAction());
      logger.recordError(String(error));
      logger.log('Action: error al actualizar una acción');
    })
  };
}
/**
 * Actualiza el objeto statusOfUpdatingAnAction para saber
 * que el metodo updateAnAction va a ser ejecutado
 */
function willUpdateAnAction() {

  return {
    type: WILL_UPDATE_AN_ACTION,
  };
}
/**
 * No modifica nada en el store si todo salió bien,
 * caso contrario actualiza el objeto statusOfUpdatingAnAction
 * para mostrar un mensaje de error
 * @param  {object} payload objeto error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function updateAnAction(payload, error) {

  return {
    type: UPDATE_AN_ACTION,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfUpdatingAnAction para saber
 * que el metodo updateAnAction ha sido ejecutado
 */
function didUpdateAnAction() {

  return {
    type: DID_UPDATE_AN_ACTION,
  };
}
/**
 * Metodo que obtiene una accion para ser vista
 * @param  {string} actionId id de la acción
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token para renovar el token de acceso
 */
export function executeGetAnAction(actionId, accessToken, refreshToken) {

  return dispatch => {
    dispatch(willGetAnAction());

    requester.getWithToken(
      GET_AN_ACTION_URI.replace('actionId', actionId),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(getAnAction(requester.getActionObject(data), false));
      dispatch(didGetAnAction());
    })
    .catch((error) => {
      dispatch(getAnAction(error, true));
      dispatch(didGetAnAction());
      logger.recordError(String(error));
      logger.log('Action: error al obtener una acción para ser vista');
    });
  };
}
/**
 * Actualiza el objeto statusOfGettingAnAction para saber
 * que el metodo getAnAction va a ser ejecutado
 */
function willGetAnAction() {

  return {
    type: WILL_GET_AN_ACTION,
  };
}
/**
 * Actualiza el objeto action si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingAnAction
 * para mostrar un mensaje de error
 * @param  {object} payload objeto con la acción a ver u objeto con el error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getAnAction(payload, error) {

  return {
    type: GET_AN_ACTION,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingAnAction para saber
 * que el metodo getAnAction ha sido ejecutado
 */
function didGetAnAction() {

  return {
    type: DID_GET_AN_ACTION,
  };
}