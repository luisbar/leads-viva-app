export const WILL_GET_COMMENTS = 'WILL_GET_COMMENTS';
export const GET_COMMENTS = 'GET_COMMENTS';
export const DID_GET_COMMENTS = 'DID_GET_COMMENTS';

export const WILL_SAVE_COMMENT = 'WILL_SAVE_COMMENT';
export const SAVE_COMMENT = 'SAVE_COMMENT';
export const DID_SAVE_COMMENT = 'DID_SAVE_COMMENT';