/******************
 * Project import *
 ******************/
import {
  WILL_GET_COMMENTS,
  GET_COMMENTS,
  DID_GET_COMMENTS,

  WILL_SAVE_COMMENT,
  SAVE_COMMENT,
  DID_SAVE_COMMENT,
} from './actionType.js';
/*************
 * Constants *
 *************/
const initialState = {
  comments: null,
  lead: null,
  statusOfGettingComments: {
    commentsAreBeingFetching: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  statusOfSavingComment: {
    commentIsBeingSaving: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
};
/**
 * Gestiona los comentarios de un lead
 */
export default function comment(state = initialState, action) {

  switch (action.type) {

    case WILL_GET_COMMENTS:
      return Object.assign({}, state, {
        statusOfGettingComments: {
          commentsAreBeingFetching: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
        comments: null,
        lead: null,
      });

    case GET_COMMENTS:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingComments: {
            commentsAreBeingFetching: true,
            wasAnError: true,
            error: action.payload
          },
        });
      else
        return Object.assign({}, state, {
          comments: action.payload.comments,
          lead: action.payload.lead,
        });

    case DID_GET_COMMENTS:
      return Object.assign({}, state, {
        statusOfGettingComments: {
          commentsAreBeingFetching: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });
      
    case WILL_SAVE_COMMENT:
      return Object.assign({}, state, {
        statusOfSavingComment: {
          commentIsBeingSaving: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case SAVE_COMMENT:
      if (action.error)
        return Object.assign({}, state, {
          statusOfSavingComment: {
            commentIsBeingSaving: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          comments: [action.payload].concat(state.comments),
        });

    case DID_SAVE_COMMENT:
      return Object.assign({}, state, {
        statusOfSavingComment: {
          commentIsBeingSaving: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    default:
      return state;
  }
}
