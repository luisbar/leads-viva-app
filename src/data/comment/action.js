/******************
 * Project import *
 ******************/
import { requester, logger } from '../../service/index.js';
import { COMMENTS_URI, SAVE_COMMENT_URI } from '../../config.js';
import { executeUpdateToken } from '../session/action.js';
import {
  WILL_GET_COMMENTS,
  GET_COMMENTS,
  DID_GET_COMMENTS,

  WILL_SAVE_COMMENT,
  SAVE_COMMENT,
  DID_SAVE_COMMENT,
} from './actionType.js';
/**
 * Metodo que obtiene los comentarios de un lead
 * @param  {string} leadId id del lead
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token para renovar el token de acceso
 */
export function executeGetComments(leadId, accessToken, refreshToken) {

  return dispatch => {
    dispatch(willGetComments());

    requester.getWithToken(
      COMMENTS_URI.replace('leadId', leadId),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(getComments(requester.getCommentsObject(data), false));
      dispatch(didGetComments());
    })
    .catch((error) => {
      dispatch(getComments(error, true));
      dispatch(didGetComments());
      logger.recordError(String(error));
      logger.log('Comment: error al obtener los comentarios');
    });
  };
}
/**
 * Actualiza el objeto statusOfGettingComments para saber
 * que el metodo getComments va a ser ejecutado
 */
function willGetComments() {

  return {
    type: WILL_GET_COMMENTS,
  };
}
/**
 * Actualiza el objeto comments y lead si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingComments
 * para mostrar un mensaje de error
 * @param  {object} payload objeto con las acciones u objeto con el error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getComments(payload, error) {

  return {
    type: GET_COMMENTS,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfGettingComments para saber
 * que el metodo getComments ha sido ejecutado
 */
function didGetComments() {

  return {
    type: DID_GET_COMMENTS,
  };
}
/**
 * Metodo que guarda un comentario en un lead
 * @param  {string} leadId id del lead
 * @param  {string} comment comentario a guardar
 * @param  {string} accessToken token de acceso
 * @param  {string} refreshToken token para renovar el token de acceso
 */
export function executeSaveComment(leadId, comment, accessToken, refreshToken) {

  return dispatch => {
    dispatch(willSaveComment());

    requester.postWithToken(
      SAVE_COMMENT_URI.replace('leadId', leadId),
      requester.getBodyForSaveComment(comment),
      accessToken,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(saveComment(requester.getCommentObject(data), false));
      dispatch(didSaveComment());
    })
    .catch((error) => {
      dispatch(saveComment(error, true));
      dispatch(didSaveComment());
      logger.recordError(String(error));
      logger.log('Comment: error al guardar un comentario');
    });
  };
}
/**
 * Actualiza el objeto statusOfSavingComment para saber
 * que el metodo saveComment va a ser ejecutado
 */
function willSaveComment() {

  return {
    type: WILL_SAVE_COMMENT,
  };
}
/**
 * Actualiza el objeto comments si todo salió bien,
 * caso contrario actualiza el objeto statusOfSavingComment
 * para mostrar un mensaje de error
 * @param  {object} payload objeto con las acciones u objeto con el error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function saveComment(payload, error) {

  return {
    type: SAVE_COMMENT,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfSavingComment para saber
 * que el metodo saveComment ha sido ejecutado
 */
function didSaveComment() {

  return {
    type: DID_SAVE_COMMENT,
  };
}