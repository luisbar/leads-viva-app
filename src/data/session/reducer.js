/******************
 * Project import *
 ******************/
import {
  GET_SESSION,

  WILL_SIGN_IN,
  SIGN_IN,
  DID_SIGN_IN,

  WILL_SIGN_OUT,
  SIGN_OUT,
  DID_SIGN_OUT,

  WILL_CHANGE_PASSWORD,
  CHANGE_PASSWORD,
  DID_CHANGE_PASSWORD,

  UPDATE_TOKEN,
} from './actionType.js';
/*************
 * Constants *
 *************/
const initialState = {
  session: undefined,
  statusOfGettingSession: {
    wasAnError: false,
  },
  statusOfSigningIn: {
    signInIsBeingExecuted: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
  statusOfSigningOut: {
    signOutIsBeingExecuted: false,
    wasAnError: false,
  },
  statusOfChangingPassword: {
    changePasswordIsBeingExecuted: false,
    wasAnError: false,
    error: {
      erroCode: null,
      errorMessage: '',
    },
  },
};
/**
 * Gestiona el objeto sesión y todo lo referente a el
 */
export default function session(state = initialState, action) {

  switch (action.type) {

    case GET_SESSION:
      if (action.error)
        return Object.assign({}, state, {
          statusOfGettingSession: {
            wasAnError: true,
          },
        });
      else
        return Object.assign({}, state, {
          session: action.payload ? action.payload : null,//Igual a null para que se dispare el render otra vez
        });

    case WILL_SIGN_IN:
      return Object.assign({}, state, {
        statusOfSigningIn: {
          signInIsBeingExecuted: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case SIGN_IN:
      if (action.error)
        return Object.assign({}, state, {
          statusOfSigningIn: {
            signInIsBeingExecuted: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return Object.assign({}, state, {
          session: action.payload,
        });

    case DID_SIGN_IN:
      return Object.assign({}, state, {
        statusOfSigningIn: {
          signInIsBeingExecuted: false,
          wasAnError: false,
          error: Object.assign({}, state.statusOfSigningIn.error),
        },
      });

    case WILL_SIGN_OUT:
      return Object.assign({}, state, {
        statusOfSigningOut: {
          signOutIsBeingExecuted: true,
        },
      });

    case SIGN_OUT:
      if (action.error)
        return Object.assign({}, state, {
          statusOfSigningOut: {
            signOutIsBeingExecuted: true,
            wasAnError: true,
          },
        });
      else
        return Object.assign({}, state, {
          session: null,
        });

    case DID_SIGN_OUT:
      return Object.assign({}, state, {
        statusOfSigningOut: {
          signOutIsBeingExecuted: false,
          wasAnError: false,
        },
      });

    case WILL_CHANGE_PASSWORD:
      return Object.assign({}, state, {
        statusOfChangingPassword: {
          changePasswordIsBeingExecuted: true,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case CHANGE_PASSWORD:
      if (action.error)
        return Object.assign({}, state, {
          statusOfChangingPassword: {
            changePasswordIsBeingExecuted: true,
            wasAnError: true,
            error: action.payload,
          },
        });
      else
        return state;

    case DID_CHANGE_PASSWORD:
      return Object.assign({}, state, {
        statusOfChangingPassword: {
          changePasswordIsBeingExecuted: false,
          wasAnError: false,
          error: {
            erroCode: null,
            errorMessage: '',
          },
        },
      });

    case UPDATE_TOKEN:
      return Object.assign({}, state, {
        session: action.payload,
      });

    default:
      return state;
  }
}
