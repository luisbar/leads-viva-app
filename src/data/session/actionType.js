export const GET_SESSION = 'GET_SESSION';

export const WILL_SIGN_IN = 'WILL_SIGN_IN';
export const SIGN_IN = 'SIGN_IN';
export const DID_SIGN_IN = 'DID_SIGN_IN';

export const WILL_SIGN_OUT = 'WILL_SIGN_OUT';
export const SIGN_OUT = 'SIGN_OUT';
export const DID_SIGN_OUT = 'DID_SIGN_OUT';

export const WILL_CHANGE_PASSWORD = 'WILL_CHANGE_PASSWORD';
export const CHANGE_PASSWORD = 'CHANGE_PASSWORD';
export const DID_CHANGE_PASSWORD = 'DID_CHANGE_PASSWORD';

export const UPDATE_TOKEN = 'UPDATE_TOKEN';
