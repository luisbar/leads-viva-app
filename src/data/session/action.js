/******************
 * Project import *
 ******************/
import { database, requester, logger } from '../../service/index.js';
import { SIGN_IN_URI, CHANGE_PASSWORD_URI } from '../../config.js';
import {
  GET_SESSION,

  WILL_SIGN_IN,
  SIGN_IN,
  DID_SIGN_IN,

  WILL_SIGN_OUT,
  SIGN_OUT,
  DID_SIGN_OUT,

  WILL_CHANGE_PASSWORD,
  CHANGE_PASSWORD,
  DID_CHANGE_PASSWORD,
  
  UPDATE_TOKEN,
} from './actionType.js';
/**
 * Obtiene la sesión desde la base de datos local
 */
export function executeGetSession() {

  return dispatch => {
    database.query('Session')
    .then((result) => {
      dispatch(getSession(result[0], false));
    })
    .catch((error) => {
      dispatch(getSession(error, true));
      logger.recordError(String(error));
      logger.log('Session: error al obtener la sesión');
    });
  };
}
/**
 * Actualiza el objeto session si todo salió bien,
 * caso contrario actualiza el objeto statusOfGettingSession
 * para mostrar un mensaje de error
 * @param  {object} payload objeto sesión
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function getSession(payload, error) {

  return {
    type: GET_SESSION,
    payload: payload,
    error: error,
  };
}
/**
 * Metodo que obtiene la sesión haciendo
 * una petición a la api
 */
export function executeSignIn(username, password, fcmId) {

  return dispatch => {
    dispatch(willSignIn());

    requester.post(SIGN_IN_URI, requester.getBodyForSigningIn(username, password, fcmId))
    .then((data) => database.write('Session', requester.getSessionObject(data)))//Guarda la sesión en la base de datos
    .then((session) => {
      dispatch(signIn(session, false));
      dispatch(didSignIn());
    })
    .catch((error) => {
      dispatch(signIn(error, true));
      dispatch(didSignIn());
      logger.recordError(String(error));
      logger.log('Session: error en el inicio de sesión');
    });
  };
}
/**
 * Actualiza el objeto statusOfSigningIn para saber
 * que el metodo signIn va a ser ejecutado
 */
function willSignIn() {

  return {
    type: WILL_SIGN_IN,
  };
}
/**
 * Actualiza el objeto session si todo salió bien,
 * caso contrario actualiza el objeto statusOfSigningIn
 * para mostrar un mensaje de error
 * @param  {object} payload objeto sesión
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function signIn(payload, error) {

  return {
    type: SIGN_IN,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfSigningIn para saber
 * que el metodo signIn ha sido ejecutado
 */
function didSignIn() {

  return {
    type: DID_SIGN_IN,
  };
}
/**
 * Metodo que cierra sesión activa
 */
export function executeSignOut() {

  return dispatch => {
    dispatch(willSignOut());

    database.delete('Session')
    .then(() => {
      dispatch(signOut(false));
      dispatch(didSignOut());
    })
    .catch((error) => {
      dispatch(signOut(true));
      dispatch(didSignOut());
      logger.recordError(String(error));
      logger.log('Session: error al cerrar sesión');
    });
  };
}
/**
 * Actualiza el objeto statusOfSigningOut para saber
 * que el metodo signOut va a ser ejecutado
 */
function willSignOut() {

  return {
    type: WILL_SIGN_OUT,
  };
}
/**
 * Actualiza el objeto session si todo salió bien,
 * caso contrario actualiza el objeto statusOfSigningOut
 * para mostrar un mensaje de error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function signOut(error) {

  return {
    type: SIGN_OUT,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfSigningOut para saber
 * que el metodo signOut ha sido ejecutado
 */
function didSignOut() {

  return {
    type: DID_SIGN_OUT,
  };
}
/**
 * Metodo que cambia la contraseña
 */
export function executeChangePassword(currentPassword, newPassword, confirmationOfNewPassword, token, refreshToken) {

  return dispatch => {
    dispatch(willChangePassword());

    requester.postWithToken(CHANGE_PASSWORD_URI,
      requester.getBodyForChangingPassword(currentPassword, newPassword, confirmationOfNewPassword),
      token,
      refreshToken,
      executeUpdateToken,
      dispatch
    )
    .then((data) => {
      dispatch(changePassword(null, false));
      dispatch(didChangePassword());
    })
    .catch((error) => {
      dispatch(changePassword(error, true));
      dispatch(didChangePassword());
      logger.recordError(String(error));
      logger.log('Session: error al cambiar el password');
    });
  };
}
/**
 * Actualiza el objeto statusOfChangingPassword para saber
 * que el metodo changePassword va a ser ejecutado
 */
function willChangePassword() {

  return {
    type: WILL_CHANGE_PASSWORD,
  };
}
/**
 * No modifica nada en el store si todo sale bien,
 * caso contrario actualiza el objeto statusOfChangingPassword
 * para mostrar un mensaje de error
 * @param  {bool} error si es igual true, entonces hubo un error
 */
function changePassword(payload, error) {

  return {
    type: CHANGE_PASSWORD,
    payload: payload,
    error: error,
  };
}
/**
 * Actualiza el objeto statusOfChangingPassword para saber
 * que el metodo changePassword ha sido ejecutado
 */
function didChangePassword() {

  return {
    type: DID_CHANGE_PASSWORD,
  };
}
/**
 * Actualiza el objeto session
 * @param  {object} sessionUpdated nueva sesión
 */
export function executeUpdateToken(sessionUpdated) {

  return {
    type: UPDATE_TOKEN,
    payload: sessionUpdated,
  };
}