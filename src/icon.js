/******************
 * Project import *
 ******************/
 import {
   navBarButtonColor,
 } from './color.js';
/*************
 * Constants *
 *************/
const MIICon = require('react-native-vector-icons/MaterialIcons');
const FAIcon = require('react-native-vector-icons/FontAwesome');

export let menu;
export let filter;
/**
 * Clase para cargar los iconos necesarios
 */
export default class Icon {
  /**
   * Carga los iconos y devuelve un promise
   */
  populateIcons() {
    return new Promise(function (resolve, reject) {
      Promise.all(
        [
          MIICon.getImageSource('menu', 30, navBarButtonColor),
          FAIcon.getImageSource('filter', 30, navBarButtonColor),
        ]
      ).then((values) => {

        menu = values[0];
        filter = values[1];

        resolve(true);
      }).catch((error) => {

        reject(error);
      }).done();
    });
  };
}
