export default class Session {

  constructor() {
    this.name = 'Session';
    this.primaryKey = 'id';
    this.properties = {
      id: { type: 'int' },
      username: { type: 'string' },
      city: { type: 'string' },
      accessToken: { type: 'string' },
      expiresIn: { type: 'string' },
      expiresAt: { type: 'string' },
      tokenType: { type: 'string' },
      scope: { type: 'string' },
      refreshToken: { type: 'string' },
      maxDecimalQuantityOfLatlng: { type: 'int' }
    };
  }
}
