/**
 * Wrapper para realm
 */
export default class Database {
  
  constructor(database) {
    this.database = database;
  }
  /**
   * Obtiene datos de la tabla especificada
   * @param  {string} table nombre de la tabla
   */
  query(table) {
    
    return new Promise((resolve, reject) => {

      try {
        const result = this.database.objects(table);
        resolve(result);
      } catch(error) {
        reject(error);
      }
    });
  }
  /**
   * Escribe datos en la tabla especificada
   * @param  {string} table nombre de la tabla
   * @param  {object} data datos a guardar en la tabla
   */
  write(table, data) {
    
    return new Promise((resolve, reject) => {
      
      try {
        this.database.write(() => {
          const result = this.database.create(table, data);
          resolve(result);
        });
      } catch(error) {
        reject(error);
      }
    })
  }
  /**
   * Actualiza datos en la tabla especificada
   * @param  {string} table nombre de la tabla
   * @param  {object} data datos a actualizar en la tabla
   */
  update(table, data) {
    
    return new Promise((resolve, reject) => {
      
      try {
        this.database.write(() => {
          const result = this.database.create(table, data, true);
          resolve(result);
        });
      } catch(error) {
        reject(error);
      }
    })
  }
  /**
   * Elimina los datos de la tabla especificada
   * @param  {string} table nombre de la tabla
   */
  delete(table) {

    return new Promise((resolve, reject) => {
      
      this.query(table)
      .then((result) => {
        this.database.write(() => {
          this.database.delete(result);
          resolve();
        });
      })
      .catch((error) => reject(error))
    });
  }
}