/******************
 * Project import *
 ******************/
import Router from '../../router.js';
import { notification, appNotification } from '../../config.js';
import { logger } from '../index.js';
/**
 * Wrapper para las notificaciones push
 */
export default class PushNotification {
  
  constructor(messaging, notification) {
    this.messaging = messaging;
    this.notification = notification;
  }
  /**
   * Inicializa escuchador para recibir las notificaciones
   * de tipo notification, no tipo data
   */
  onReceiveNotification() {
    this.notification.onNotification((notificationReceived) => {
      //Se muestra la notificacion en la parte posterior de la app
      Router.getInstance().navigator.showInAppNotification({
       screen: appNotification,
       passProps: {
         title: notificationReceived.title,
         body: notificationReceived.body,
       },
       autoDismissTimerSec: 3,
      });
      //Se muestra la notificacion en el status bar
      notificationReceived.android.setChannelId(notificationReceived.android.group);//Pone el tag como channel, sin channel no funciona
      notificationReceived.android.setAutoCancel(true);//Para que se remueva la notificacion cuando es presionada
      this.notification.displayNotification(notificationReceived);
    });
  }
  /**
   * Retorna el fmc id
   */
  getFcmId() {

    return this.messaging.getToken()
    .then((data) => data)
    .catch((error) => {
      logger.recordError(String(error));
      logger.log('PushNotification: error en el metodo getFcmId');
    });
  }
  /**
   * Escuchador que se dispara cuando la app es abierta
   * presionando a la notificacion
   */
  onAppOpenedByPressingANotification() {

    return this.notification.getInitialNotification()
    .then((notificationPressed) => notificationPressed)
    .catch((error) => {
      logger.recordError(String(error));
      logger.log('PushNotification: error en el metodo onAppOpenedByPressingANotification');
    })
  }
  /**
   * Escuchador que se dispara cuando se presiona sobre
   * una notificacion cuando la app esta corriendo
   */
  onNotificationPressed() {
    this.notification.onNotificationOpened(() => {
      Router.getInstance().push(notification);
    })
  }
}