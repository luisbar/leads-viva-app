/******************
 * Project import *
 ******************/
import CustomError from './customError.js';
import Mapper from './mapper.js';
import { SIGN_IN_URI, appVersion } from '../../config.js';
import { database } from '../index.js';
/**
 * Wrapper para utilizar la api fetch
 */
export default class Requester extends Mapper {
  /**
   * Realiza una petición por post al recurso specificado
   * @param  {string} endpoint recurso al cual se quiere acceder
   * @param  {object} body datos a enviar
   */
  post(endpoint, body) {

    return new Promise((resolve, reject) => {
      fetch(endpoint, {
        method: 'POST',
        timeout: 5000,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Version-App': appVersion,
        },
        body: JSON.stringify(body),
      })
      .then(this.errorHandler)
      .then((data) => resolve(data))
      .catch((error) => reject(error));
    });
  }
  /**
   * Manejador de errores para el metodo post que solo se utiliza
   * para el inicio de sesión y para actualizar el token
   * @param  {object} response objeto response
   */
  errorHandler(response) {

    return new Promise((resolve, reject) => {
      
      response.json()
      .then((data) => {

        switch (response.status) {

          case 200:
            resolve(data);
            break;

          default:
            reject(new CustomError({
              errorMessage: data.error,
              errorCode: response.status,
              fieldErrors: data.errores_campos,
              helpMessage: `Error ${response.status} en errorHandler`
            }));
        }
      })
      .catch((error) => reject(new CustomError({
        errorMessage: error.errorMessage || error,
        errorCode: error.errorCode || -1,
        fieldErrors: error.fieldErrors,
        helpMessage: error.helpMessage || `Error en el metodo errorHandler: ${error}`,
      })))
    })
  }
  /**
   * Realiza una petición por post al recurso specificado
   * @param  {string} endpoint recurso al cual se quiere acceder
   * @param  {object} body datos a enviar
   * @param  {string} token token
   * @param  {string} refreshToken refresh token para solicitar un nuevo access token
   * @param  {function} executeUpdateToken para actualizar la session en el store
   * @param  {function} dispatch para disparar una acción con redux
   */
  postWithToken(endpoint, body, token, refreshToken, executeUpdateToken, dispatch) {

    return new Promise((resolve, reject) => {
      fetch(endpoint, {
        method: 'POST',
        timeout: 5000,
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Version-App': appVersion,
        },
        body: JSON.stringify(body),
      })
      .then((response) => this.errorHandlerWithToken(
        response,
        endpoint,
        body,
        refreshToken,
        executeUpdateToken,
        dispatch,
        'post'
      ))
      .then((data) => resolve(data))
      .catch((error) => reject(error));
    });
  }
  /**
   * Realiza una petición por get al recurso specificado
   * @param  {string} endpoint recurso el cual se quiere obtener
   */
  getWithToken(endpoint, token, refreshToken, executeUpdateToken, dispatch) {

    return new Promise((resolve, reject) => {
      fetch(endpoint, {
        method: 'GET',
        timeout: 5000,
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Version-App': appVersion,
        },
      })
      .then((response) => this.errorHandlerWithToken(
        response,
        endpoint,
        null,
        refreshToken,
        executeUpdateToken,
        dispatch,
        'get'
      ))
      .then((data) => resolve(data))
      .catch((error) => reject(error));
    });
  }
  /**
   * Metodo que maneja los errores de las peticiones con token
   */
  errorHandlerWithToken(response, endpoint, body, refreshToken, executeUpdateToken, dispatch, method) {

    return new Promise((resolve, reject) => {
      if (response.status !== 413) {
        response.json()
        .then((data) => {

          switch (response.status) {
            case 200:
              resolve(data);
              break;

            case 401:
              this.updateToken(
                endpoint,
                body,
                refreshToken,
                executeUpdateToken,
                dispatch,
                method,
                resolve,
                reject
              );
              break;

            default:
              reject(new CustomError({
                errorMessage: data.error,
                errorCode: response.status,
                fieldErrors: data.errores_campos,
                helpMessage: `Error ${response.status} en errorHandlerWithToken`
              }));
          }
        })
        .catch((error) => reject(new CustomError({
          errorMessage: error.errorMessage || error,
          errorCode: error.errorCode || -1,
          fieldErrors: error.fieldErrors,
          helpMessage: error.helpMessage || `Error en el metodo errorHandlerWithToken: ${error}`,
        })))
      } else
        reject(new CustomError({
          errorMessage: '',
          errorCode: response.status,
          fieldErrors: null,
          helpMessage: `Error ${response.status} en errorHandlerWithToken`
        }));
    })
  }
  /**
   * Handler del error 401, el cual se dispara cuando el token
   * a expirado, este manejador actualiza el token y vuelve a hacer la petición
   * anterior
   */
  updateToken(endpoint, body, refreshToken, executeUpdateToken, dispatch, method, resolve, reject) {

    this.post(SIGN_IN_URI, this.getBodyForUpdatingToken(refreshToken))//Solicita nuevo token
    .then((data) => this.getSessionFromLocalDatabase(data))//Obtiene sesión de la base de datos local
    .then(([session, data]) => database.update('Session', this.getSessionObjectUpdated(session, data)))//Guarda la sesión en la base de datos local
    .then((sessionUpdated) => {
      //Dispara acción para actualizar la sesión en el store
      dispatch(executeUpdateToken(sessionUpdated));

      if (method === 'post')//Reenvia la anterior petición
        this.postWithToken(endpoint, body, sessionUpdated.accessToken, sessionUpdated.refreshToken, executeUpdateToken, dispatch)
        .then((data) => resolve(data))
        .catch((error) => reject(new CustomError({
          errorMessage: error.errorMessage || 'Error inesperado',
          errorCode: error.errorCode || -1,
          fieldErrors: error.fieldErrors || undefined,
          helpMessage: `Error al reenviar una petición post, despues de actualizar el token: ${error}`,
        })));
      else if (method === 'get')//Reenvia la anterior petición
        this.getWithToken(endpoint, sessionUpdated.accessToken, sessionUpdated.refreshToken, executeUpdateToken, dispatch)
        .then((data) => resolve(data))
        .catch((error) => reject(new CustomError({
          errorMessage: error.errorMessage || 'Error inesperado',
          errorCode: error.errorCode || -1,
          fieldErrors: error.fieldErrors || undefined,
          helpMessage: `Error al reenviar una petición get, despues de actualizar el token: ${error}`,
        })));
    })
    .catch((error) => reject(new CustomError({
      errorMessage: error.errorMessage || error,
      errorCode: error.errorCode || -1,
      fieldErrors: error.fieldErrors || undefined,
      helpMessage: error.helpMessage || `Error en el metodo update token: ${error}`
    })));
  }
  /**
   * Obtiene la sesión actual desde la base de datos
   */
  getSessionFromLocalDatabase(data) {
    
    return new Promise((resolve, reject) => {

      database.query('Session')
      .then((result) => resolve([result[0], data]))
      .catch((error) => reject(new CustomError({
        errorMessage: 'Error inesperado',
        errorCode: -1,
        fieldErrors: undefined,
        helpMessage: `Error en el metodo getSessionObjectUpdated: ${error}`,
      })));
    });
  }
}
