/**
 * Clase para customizar los errores
 */
export default class CustomError extends Error {
  
  constructor(error) {
    super(JSON.stringify(error));
    this.name = 'CustomError';
    this.errorCode = error.errorCode;
    this.errorMessage = error.errorMessage;
    this.helpMessage = error.helpMessage;
    this.fieldErrors = {};
    
    if (error.fieldErrors && Array.isArray(error.fieldErrors))//Si no es array, ya fue transformado en un json

      error.fieldErrors.map((field) => {//Para traducir los errores de los campos a ingles
          /*Cambiar contraseña*/
          if (field.contrasena)
            this.fieldErrors.currentPassword = field.contrasena;
          if (field.nueva_contrasena)
            this.fieldErrors.newPassword = field.nueva_contrasena;
          if (field.confirmacion_contrasena)
            this.fieldErrors.confirmationOfNewPassword = field.confirmacion_contrasena;
          /*Registrar lead*/
          //Mapa
          if (field.ubicacion) {
            this.fieldErrors.latitude = field.ubicacion.latitud;
            this.fieldErrors.longitude = field.ubicacion.longitud;
            this.fieldErrors.availabilityOfService = field.ubicacion.cobertura;
          }
          //Formulario
          if (field.informacion_contacto) {
            //Natural
            this.fieldErrors.name = field.informacion_contacto.nombre;
            this.fieldErrors.identityNumber = field.informacion_contacto.cedula_identidad;
            this.fieldErrors.foreignIdentityNumber = field.informacion_contacto.cedula_extranjera;
            //Juridico
            this.fieldErrors.owner = field.informacion_contacto.persona_contacto;
            this.fieldErrors.enterpriseName = field.informacion_contacto.empresa;
            this.fieldErrors.nit = field.informacion_contacto.nit;
            //Siempre visible
            this.fieldErrors.phone = field.informacion_contacto.telefono_contacto;
            this.fieldErrors.email = field.informacion_contacto.correo_electronico;
            this.fieldErrors.firstReferencePhone = field.informacion_contacto.numero_referencia_1;
            this.fieldErrors.secondReferencePhone = field.informacion_contacto.numero_referencia_2;
            this.fieldErrors.note = field.informacion_contacto.observacion;
          }
          //Producto
          if (field.productos) {
            this.fieldErrors.errorsOnPlansAdded = {};
            //Telefonia publica
            if (field.productos.telefonia_publica) {
              this.fieldErrors.errorsOnPlansAdded.publicTelephony = field.productos.telefonia_publica.map((item) => {
                
                return {
                  plan: item.plan,
                  quantity: item.cantidad,
                  line: item.linea,
                };
              });
            }
            //Postpago
            if (field.productos.postpago) {
              this.fieldErrors.errorsOnPlansAdded.postPaid = field.productos.postpago.map((item) => {
                
                return {
                  purchaseType: item.tipo_compra,
                  plan: item.plan,
                  quantity: item.cantidad,
                };
              });
            }
            //Lte
            if (field.productos.lte) {
              this.fieldErrors.errorsOnPlansAdded.lte = field.productos.lte.map((item) => {
                
                return {
                  type: item.tipo,
                  procedure: item.procedimiento,
                  plan: item.plan,
                  quantity: item.cantidad,
                  line: item.linea,
                  imei: item.imei_cpe,
                  enodeb: item.enodeb,
                };
              });
            }
            //Otros
            if (field.productos.otros) {
              this.fieldErrors.errorsOnPlansAdded.other = field.productos.otros.map((item) => {
                
                return {
                  service: item.servicio,
                  quantity: item.cantidad,
                };
              });
            }
            //Vpn
            if (field.productos.vpn) {
              this.fieldErrors.errorsOnPlansAdded.vpn = field.productos.vpn.map((item) => {
                
                return {
                  plan: item.plan,
                  quantity: item.cantidad,
                };
              });
            }
            //Bolsas
            if (field.productos.bolsas) {
              this.fieldErrors.errorsOnPlansAdded.bag = field.productos.bolsas.map((item) => {
                
                return {
                  plan: item.plan,
                  quantity: item.cantidad,
                };
              });
            }
          }
          /*Registrar acción*/
          if (field.fecha)
            this.fieldErrors.date = field.fecha;
          if (field.hora)
            this.fieldErrors.time = field.hora;
          if (field.observacion)
            this.fieldErrors.note = field.observacion;
          /*Registrar acción*/
          if (field.fecha_creacion_inicial)
            this.fieldErrors.initialDate = field.fecha_creacion_inicial;
          if (field.fecha_creacion_final)
            this.fieldErrors.initialDate = field.fecha_creacion_final;
          /*Editar acción*/
          if (field.accion) {
            this.fieldErrors.date = field.accion.fecha;
            this.fieldErrors.time = field.accion.hora;
            this.fieldErrors.duration = field.accion.duracion;
            this.fieldErrors.action = field.accion.accion;
            this.fieldErrors.note = field.accion.observacion;
            this.fieldErrors.status = field.accion.estado;
            this.fieldErrors.answer = field.accion.respuesta;
          }
          
          if (field.motivo_cierre) {
            this.fieldErrors.reason = field.motivo_cierre.motivo;
            this.fieldErrors.justification = field.motivo_cierre.justificacion;
          }
          
          if (field.venta_exitosa)
            this.fieldErrors.numberOfPurchaseProcess = field.venta_exitosa.numero_proceso_venta;
            
          if (field.siguiente_accion) {
            this.fieldErrors.newDate = field.siguiente_accion.fecha;
            this.fieldErrors.newTime = field.siguiente_accion.hora;
            this.fieldErrors.newDuration = field.siguiente_accion.duracion;
            this.fieldErrors.newAction = field.siguiente_accion.siguiente_accion;
            this.fieldErrors.newNote = field.siguiente_accion.observacion;
          }
      });
    else
      this.fieldErrors = error.fieldErrors;
  }
}