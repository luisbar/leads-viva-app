/***********************
 * Node modules import *
 ***********************/
import moment from 'moment';
import 'moment/locale/es';
/******************
 * Project import *
 ******************/
import { GRANT_TYPE_ONE, GRANT_TYPE_TWO, CLIENT_ID, CLIENT_SECRET, CHAT_AVATAR_URI } from '../../config.js';
/**
 * Mapea los datos devueltos por la api y mapea
 * los datos a enviar a la api
 */
export default class Mapper {
  /**
   * Retorna el body para el inicio de sesión
   */
  getBodyForSigningIn(username, password, fcmId) {

    return {
      username: username,
      password: password,
      grant_type: GRANT_TYPE_ONE,
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      origin_request: 'app_movil',
      token_fcm: fcmId,
    };
  }
  /**
   * Retorna el objeto session para guardarlo en la base
   * de datos local
   */
  getSessionObject(data) {

    return {
      id: 1,
      username: data.username,
      city: data.city,
      accessToken: data.access_token,
      expiresIn: String(data.expires_in),
      expiresAt: data.expires_at,
      tokenType: data.token_type,
      scope: data.scope,
      refreshToken: data.refresh_token,
      maxDecimalQuantityOfLatlng: data.number_decimals_map,
    };
  }
  /**
   * Devuelve el body para solicitar la actualización
   * del token de acceso
   * @param  {string} refreshToken token para solicitar nuevo token de acceso
   */
  getBodyForUpdatingToken(refreshToken) {

    return {
      refresh_token: refreshToken,
      grant_type: GRANT_TYPE_TWO,
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
    };
  }
  /**
   * Devuelve un objeto sesión para ser almacenado
   * en la base de datos local
   * @param  {object} localSession objeto sesión obtenido de la base de datos local
   * @param  {object} cloudSession objeto sesión obtenido desde la nube
   * @return {object} objecto sesión actualizado
   */
  getSessionObjectUpdated(localSession, cloudSession) {

    return {
      id: localSession.id,
      accessToken: cloudSession.access_token,
      expiresIn: String(cloudSession.expires_in),
      expiresAt: cloudSession.expires_at,
      tokenType: cloudSession.token_type,
      scope: cloudSession.scope,
      refreshToken: cloudSession.refresh_token,
    };
  }
  /**
   * Devuelve el body para cambiar contraseña
   */
  getBodyForChangingPassword(currentPassword, newPassword, confirmationOfNewPassword) {
    
    return {
      contrasena: currentPassword,
      nueva_contrasena: newPassword,
      confirmacion_contrasena: confirmationOfNewPassword,
    }
  }
  /**
   * Devuelve objeto mapeado con los datos
   * de configuración para el registro de leads
   */
  getConfigurationData(configurationData) {

    return {
      extensionInputItems: this.translateConfigurationDataKeys(configurationData.extensiones),
      documentTypeInputItems: [{ id: '', name: 'Seleccione un tipo de documento' }].concat(this.translateConfigurationDataKeys(configurationData.tipos_documentos)),
      sourceInputItems: this.translateConfigurationDataKeys(configurationData.origenes),
      nationalityInputItems: this.translateConfigurationDataKeys(configurationData.paises),
      chanelInputItems: configurationData.canales
        ? this.translateConfigurationDataKeys(configurationData.canales)
        : [],
      cityInputItems: configurationData.ciudades
        ? this.translateConfigurationDataKeys(configurationData.ciudades)
        : [],
      typeInputItems: configurationData.tipos_leads
        ? this.translateConfigurationDataKeys(configurationData.tipos_leads)
        : [],
      restriction: {
        restrictedProducts: configurationData.restricciones_ubicacion
        ? this.translateProductsRestricted(configurationData.restricciones_ubicacion.productos_restringidos)
        : [],
      },
      postPaid: {
        purchaseType: [{ id: '', name: 'Seleccione un tipo de compra' }].concat(this.translateConfigurationDataKeys(configurationData.postpago.tipos_compra)),
        plan: [{ id: '', name: 'Seleccione un plan' }].concat(this.translateConfigurationDataKeys(configurationData.postpago.planes)),
        data: [{ id: '', name: '' }].concat(this.translateConfigurationDataKeys(configurationData.postpago.planes_datos)),
        minut: [{ id: '', name: '' }].concat(this.translateConfigurationDataKeys(configurationData.postpago.planes_minutos)),
        credit: [{ id: '', name: '' }].concat(this.translateConfigurationDataKeys(configurationData.postpago.planes_credito)),
        friendGroup: [{ id: '', name: '' }].concat(this.translateConfigurationDataKeys(configurationData.postpago.planes_grupo_amigo)),
      },
      lte: {
        type: [{ id: '', name: 'Seleccione un tipo' }].concat(this.translateConfigurationDataKeys(configurationData.lte_fijo.tipos)),
        plan: [{ id: '', name: 'Seleccione un plan' }].concat(this.translateConfigurationDataKeys(configurationData.lte_fijo.planes)),
        procedure: [{ id: '', name: 'Seleccione un procedimiento' }].concat(this.translateConfigurationDataKeys(configurationData.lte_fijo.procedimientos)),
      },
      publicTelephony: {
        plan: [{ id: '', name: 'Seleccione un plan' }].concat(this.translateConfigurationDataKeys(configurationData.telefonia_publica.planes)),
      },
      vpn: {
        plan: [{ id: '', name: 'Seleccione un plan' }].concat(this.translateConfigurationDataKeys(configurationData.vpn.planes)),
      },
      bag: {
        plan: [{ id: '', name: 'Seleccione un plan' }].concat(this.translateConfigurationDataKeys(configurationData.bolsas.planes)),
      },
      pictureTypes: this.translateConfigurationDataKeys(configurationData.tipos_foto),
      maximumDistance: configurationData.distancia_maxima_lead
      ? {
          distanceInMeters: configurationData.distancia_maxima_lead.distancia_metros,
        }
      : {
          distanceInMeters: null,
        }
    }
  }
  /**
   * Traduce las claves de la configuración del formulario para registro
   * de leads a ingles
   */
  translateConfigurationDataKeys(array) {

    return array.map((item) => {
      return {
        id: item.id,
        name: item.nombre,
        price: item.tarifa,
        calculateDistance: item.validar_distancia,
      };
    });
  }
  /**
   * Traduce los productos restringidos
   */
  translateProductsRestricted(productsRestricted) {
    
    return productsRestricted.map((product) => {

      switch (product) {
        
        case 'bolsas':
          return 'bag';
          
        case 'lte_fijo':
          return 'lte';
          
        case 'otros':
          return 'other';
          
        case 'postpago':
          return 'postPaid';
          
        case 'telefonia_publica':
          return 'publicTelephony';
          
        case 'vpn':
          return 'vpn';
          
      }
    });
  }
  /**
   * Retorna el body para guardar un lead
   */
  getBodyForSavingALead({ information, products, location, pictures }) {

    return {
      informacion_contacto:{
        tipo: information.type || '',
        nombre: information.name || '',
        tipo_documento: information.documentType || '',
        empresa: information.enterpriseName || '',
        nit: information.nit || '',
        persona_contacto: information.owner || '',
        cedula_identidad: information.identityNumber || '',
        extension: information.extension || '',
        cedula_extranjera: information.foreignIdentityNumber || '',
        nacionalidad: information.nationality || '',
        telefono_contacto: information.phone || '',
        correo_electronico: information.email || '',
        numero_referencia_1: information.firstReferencePhone || '',
        numero_referencia_2: information.secondReferencePhone || '',
        ciudad: information.city || '',
        origen: information.source || '',
        observacion: information.note || '',
        canal: information.chanel || '',
      },
      productos: {
        postpago: products.postPaid.map((item) => this.getPostPaidObjectForSeding(item)),
        lte: products.lte.map((item) => this.getLteObjectForSeding(item)),
        telefonia_publica: products.publicTelephony.map((item) => this.getPublicTelephonyObjectForSeding(item)),
        vpn: products.vpn.map((item) => this.getVpnObjectForSeding(item)),
        bolsas: products.bag.map((item) => this.getBagObjectForSeding(item)),
        otros: products.other.map((item) => this.getOtherObjectForSeding(item)),
      },
      ubicacion:{
        latitud: location.latitude, 
        longitud: location.longitude,
        cobertura: location.availabilityOfService,
        direccion: location.address,
      },
      foto: {
        fotos: pictures.map((item) => this.getPictureForSending(item)),
      },
    };
  }
  /**
   * Retorna los productos postpago para enviar
   * mediante la api
   */
  getPostPaidObjectForSeding(product) {
    
    return {
      detalle_producto: product.id || '',
      tipo_compra: product.purchaseTypeId || '',
      plan: product.planId || '',
      cantidad: product.quantity || '',
      datos: product.dataId || '',
      minutos: product.minutId || '',
      credito: product.creditId || '',
      grupo_amigo: product.friendGroupId || '',
    };
  }
  /**
   * Retorna los productos lte para enviar
   * mediante la api
   */
  getLteObjectForSeding(product) {
    
    return {
      detalle_producto: product.id || '',
      tipo: product.typeId || '',
      plan: product.planId || '',
      cantidad: product.quantity || '',
      linea: product.line || '',
      imei_cpe: product.imei || '',
      enodeb: product.enodeb || '',
      procedimiento: product.procedureId || '',
    };
  }
  /**
   * Retorna los productos telefonia publica para enviar
   * mediante la api
   */
  getPublicTelephonyObjectForSeding(product) {
    
    return {
      detalle_producto: product.id || '',
      plan: product.planId || '',
      cantidad: product.quantity || '',
      linea: product.line || '',
    };
  }
  /**
   * Retorna los productos vpn para enviar
   * mediante la api
   */
  getVpnObjectForSeding(product) {
    
    return {
      detalle_producto: product.id || '',
      plan: product.planId || '',
      cantidad: product.quantity || '',
    };
  }
  /**
   * Retorna los productos bolsas para enviar
   * mediante la api
   */
  getBagObjectForSeding(product) {
    
    return {
      detalle_producto: product.id || '',
      plan: product.planId || '',
      cantidad: product.quantity || '',
    };
  }
  /**
   * Retorna los productos otros para enviar
   * mediante la api
   */
  getOtherObjectForSeding(product) {
    
    return {
      detalle_producto: product.id || '',
      servicio: product.service || '',
      cantidad: product.quantity || '',
    };
  }
  /**
   * Retorna body para obtener la disponibilidad
   * del servicio
   */
  getBodyForGettingAvailabilityOfService(latitude, longitude) {
    
    return {
      latitud: latitude,
      longitud: longitude,
    };
  }
  /**
   * Retorna la disponibilidad de servicio
   */
  getAvailabilityOfService(data) {
    
    return data.cobertura;
  }
  /**
   * Retorna las fotos para enviar por la api
   */
  getPictureForSending(item) {
    
    return {
      id: item.id,
      foto: item.picture,
      latitud: item.latitude,
      longitud: item.longitude,
      tipo_foto: item.pictureTypeId,
      distancia_a_lead: item.distance || 0,
    }
  }
  /**
   * Retorna el body para obtener los leads
   */
  getBodyForGettingLeads(page,
    state,
    search,
    source,
    initialDateOfCreation,
    endDateOfCreation
  ) {
    
    return {
      pagina: page || '',
      estado: state || '',
      buscar: search || '',
      origenes: source || '',
      fecha_creacion_inicial: initialDateOfCreation || null,
      fecha_creacion_final: endDateOfCreation || null,
    };
  }
  /**
   * Retorna la lista de leads para mostrarlos
   */
  getLeadObjects(leads) {
    
    return leads.map((item) => {

      return {
        id: item.lead_id,
        state: item.estado,
        creationDate: item.fecha_creacion,
        number: item.numero,
        name: item.nombre,
        phone: item.telefono,
        actions: {
          editLead: item.acciones.editar_lead,
          addComment: item.acciones.ver_comentarios,
          seeLead: item.acciones.ver_lead,
          seeActionsRecord: item.acciones.ver_historial_acciones,
          createAction: item.acciones.registrar_accion,
        },
        numberOfPurchaseProcess: item.extra && item.extra.exitoso && `N.P.V: ${item.extra.exitoso.numero_proceso_venta}`,
        reasonForClousure: item.extra && item.extra.caido && item.extra.caido.motivo_cierre,
        justification: item.extra && item.extra.caido && item.extra.caido.justificacion,
      };
    })
  }
  /**
   * Retorna el objeto configurationData y el objeto lead
   * para mostrarlo en la vista LeadEdition
   */
  getConfigurationDataAndLeadObject(data) {
    let configurationData = this.getConfigurationData(data.informacion_lead);
    configurationData.restriction.restrictedProducts = this.translateProductsRestricted(data.restricciones_ubicacion.productos_restringidos);
    configurationData.maximumDistance.distanceInMeters = data.distancia_maxima_lead && data.distancia_maxima_lead.distancia_metros;
    
    return {
      configurationData: configurationData,
      lead: this.getLeadObject(data.objeto_lead, configurationData),
    };
  }
  /**
   * Retorna el objeto lead mapeado
   */
  getLeadObject(lead, configurationData) {

    return {
      information:{
        type: lead.informacion_contacto.tipo && {
          id: lead.informacion_contacto.tipo.id,
          name: lead.informacion_contacto.tipo.nombre,
        },
        name: lead.informacion_contacto.nombre,
        documentType: lead.informacion_contacto.tipo_documento && {
          id: lead.informacion_contacto.tipo_documento.id,
          name: lead.informacion_contacto.tipo_documento.nombre,
        },
        enterpriseName: lead.informacion_contacto.empresa,
        nit: lead.informacion_contacto.nit,
        owner: lead.informacion_contacto.persona_contacto,
        identityNumber: lead.informacion_contacto.cedula_identidad,
        extension: lead.informacion_contacto.extension && {
          id: lead.informacion_contacto.extension.id,
          name: lead.informacion_contacto.extension.nombre,
        },
        foreignIdentityNumber: lead.informacion_contacto.cedula_extranjera,
        nationality: lead.informacion_contacto.nacionalidad && {
          id: lead.informacion_contacto.nacionalidad.id,
          name: lead.informacion_contacto.nacionalidad.nombre,
        },
        phone: lead.informacion_contacto.telefono_contacto,
        email: lead.informacion_contacto.correo_electronico,
        firstReferencePhone: lead.informacion_contacto.numero_referencia_1,
        secondReferencePhone: lead.informacion_contacto.numero_referencia_2,
        city: lead.informacion_contacto.ciudad,
        source: lead.informacion_contacto.origen && {
          id: lead.informacion_contacto.origen.id,
          name: lead.informacion_contacto.origen.nombre,
        },
        note: lead.informacion_contacto.observacion,
        chanel: lead.informacion_contacto.canal,
      },
      products: {
        postPaid: lead.productos.postpago.map((item) => this.getPostPaidObject(item, configurationData)),
        lte: lead.productos.lte_fijo.map((item) => this.getLteObject(item, configurationData)),
        publicTelephony: lead.productos.telefonia_publica.map((item) => this.getPublicTelephonyObject(item, configurationData)),
        vpn: lead.productos.vpn.map((item) => this.getVpnObject(item, configurationData)),
        bag: lead.productos.bolsas.map((item) => this.getBagObject(item, configurationData)),
        other: lead.productos.otros.map((item) => this.getOtherObject(item, configurationData)),
      },
      location:{
        latitude: lead.ubicacion.latitud, 
        longitude: lead.ubicacion.longitud,
        availabilityOfService: lead.ubicacion.cobertura,
        address: lead.ubicacion.direccion,
      },
      pictures: lead.fotos.map((item) => this.getPicture(item)),
    };
  }
  /**
   * Retorna los productos postpago para mostrarlos
   * en la vista LeadEdition
   */
  getPostPaidObject(product, configurationData) {
    const planIndex = configurationData.postPaid.plan.findIndex((item) => item.id === product.plan.id);
    const dataIndex = configurationData.postPaid.data.findIndex((item) => item.id === product.datos.id);
    const minutIndex = configurationData.postPaid.minut.findIndex((item) => item.id === product.minutos.id);
    const creditIndex = configurationData.postPaid.credit.findIndex((item) => item.id === product.credito.id);
    const friendGroupIndex = configurationData.postPaid.friendGroup.findIndex((item) => item.id === product.grupo_amigo.id);
    
    const priceByPlan = planIndex ? configurationData.postPaid.plan[planIndex].price : 0;
    const priceByData = dataIndex ? configurationData.postPaid.data[dataIndex].price : 0;
    const priceByMinut = minutIndex ? configurationData.postPaid.minut[minutIndex].price : 0;
    const priceByCredit = creditIndex ? configurationData.postPaid.credit[creditIndex].price : 0;
    const priceByFriendGroup = friendGroupIndex ? configurationData.postPaid.friendGroup[friendGroupIndex].price : 0;
    
    const subTotal = parseInt(priceByPlan) + parseInt(priceByData) + parseInt(priceByMinut) + parseInt(priceByCredit) + parseInt(priceByFriendGroup);
    const price = String(subTotal * product.cantidad);
    
    return {
      id: product.detalle_producto,
      purchaseType: product.tipo_compra.nombre,
      purchaseTypeId: product.tipo_compra.id,
      plan: product.plan.nombre,
      planId: product.plan.id,
      planIndex: planIndex,
      quantity: String(product.cantidad),
      data: product.datos.nombre,
      dataIndex: dataIndex,
      dataId: product.datos.id,
      minut: product.minutos.nombre,
      minutIndex: minutIndex,
      minutId: product.minutos.id,
      credit: product.credito.nombre,
      creditIndex: creditIndex,
      creditId: product.credito.id,
      friendGroup: product.grupo_amigo.nombre,
      friendGroupIndex: friendGroupIndex,
      friendGroupId: product.grupo_amigo.id,
      price: price,
    };
  }
  /**
   * Retorna los productos lte para mostrarlos
   * en la vista LeadEdition
   */
  getLteObject(product, configurationData) {
    const planIndex = configurationData.lte.plan.findIndex((item) => item.id === product.plan.id);
    const price = String(configurationData.lte.plan[planIndex].price * product.cantidad);
    
    return {
      id: product.detalle_producto,
      type: product.tipo.nombre,
      typeId: product.tipo.id,
      plan: product.plan.nombre,
      planId: product.plan.id,
      planIndex: planIndex,
      quantity: String(product.cantidad),
      line: product.linea,
      imei: product.imei_cpe,
      enodeb: product.enodeb,
      procedure: product.procedimiento.nombre,
      procedureId: product.procedimiento.id,
      price: price,
    };
  }
  /**
   * Retorna los productos telefonia publica para mostrarlos
   * en la vista LeadEdition
   */
  getPublicTelephonyObject(product, configurationData) {
    const planIndex = configurationData.publicTelephony.plan.findIndex((item) => item.id === product.plan.id);
    const price = String(configurationData.publicTelephony.plan[planIndex].price * product.cantidad);
    
    return {
      id: product.detalle_producto,
      plan: product.plan.nombre,
      planId: product.plan.id,
      planIndex: planIndex,
      quantity: String(product.cantidad),
      line: product.linea,
      price: price,
    };
  }
  /**
   * Retorna los productos vpn para mostrarlos
   * en la vista LeadEdition
   */
  getVpnObject(product, configurationData) {
    const planIndex = configurationData.vpn.plan.findIndex((item) => item.id === product.plan.id);
    const price = String(configurationData.vpn.plan[planIndex].price * product.cantidad);
    
    return {
      id: product.detalle_producto,
      plan: product.plan.nombre,
      planId: product.plan.id,
      planIndex: planIndex,
      quantity: String(product.cantidad),
      price: price,
    };
  }
  /**
   * Retorna los productos bolsas para mostrarlos
   * en la vista LeadEdition
   */
  getBagObject(product, configurationData) {
    const planIndex = configurationData.bag.plan.findIndex((item) => item.id === product.plan.id);
    const price = String(configurationData.bag.plan[planIndex].price * product.cantidad);
    
    return {
      id: product.detalle_producto,
      plan: product.plan.nombre,
      planId: product.plan.id,
      planIndex: planIndex,
      quantity: String(product.cantidad),
      price: price,
    };
  }
  /**
   * Retorna los productos otros para mostrarlos
   * en la vista LeadEdition
   */
  getOtherObject(product) {
    
    return {
      id: product.detalle_producto,
      service: product.servicio,
      quantity: String(product.cantidad),
    };
  }
  /**
   * Retorna las fotos para mostrarlas
   * en la vista LeadEdition
   */
  getPicture(item) {

    return {
      id: item.id,
      picture: item.foto_url,
      latitude: item.latitud,
      longitude: item.longitud,
      pictureTypeId: item.tipo_foto.id,
      pictureTypeName: item.tipo_foto.nombre,
      date: item.fecha_creacion_timestamp,
      distance: item.distancia_a_lead
    }
  }
  /**
   * Retorna las acciones para mostrarlas
   * en la vista ActionsRecord
   */
  getActionsObject(action) {
  
    return {
      lead: {
        chanel: action.lead.canal,
        name: action.lead.nombre,
        number: action.lead.numero,
        city: action.lead.ciudad,
      },
      actions: action.acciones.map((item) => this.getActionObject(item)),
    }
  }
  /**
   * Retorna objeto acción
   */
  getActionObject(action) {
    
    return {
      date: action.fecha,
      reason: action.motivo_cierre,
      action: action.accion,
      justification: action.justificacion,
      answer: action.respuesta,
      note: action.observacion,
      duration: action.duracion,
      status: action.estado,
    }
  }
  /**
   * Retorna los comentarios para mostrarlos
   * en la vista Comment
   */
  getCommentsObject(comment) {
  
    return {
      lead: {
        name: comment.lead.nombre,
        number: comment.lead.numero,
      },
      comments: comment.comentarios.map((item) => this.getCommentObject(item)).reverse(),
    }
  }
  /**
   * Retorna objeto comentario
   */
  getCommentObject(comment) {

    return {
      _id: comment.id,
      text: comment.mensaje || comment.comentario,
      createdAt: new Date(comment.fecha_timestamp),
      user: {
        _id: comment.usuario,
        name: comment.usuario,
        avatar: CHAT_AVATAR_URI,
      },
    }
  }
  /**
   * Retorna el body para guardar un comentario
   */
  getBodyForSaveComment(comment) {
    
    return {
      comentario: comment,
    };
  }
  /**
   * Retorna objeto lead para ser mostrado
   */
  getALeadWithStatusRecordObject(lead) {

    return {
      information:{
        name: lead.informacion_contacto.nombre,
        enterpriseName: lead.informacion_contacto.empresa,
        nit: lead.informacion_contacto.nit,
        owner: lead.informacion_contacto.persona_contacto,
        identityNumber: lead.informacion_contacto.cedula_identidad,
        extension: lead.informacion_contacto.extension,
        foreignIdentityNumber: lead.informacion_contacto.cedula_extranjero,
        nationality: lead.informacion_contacto.nacionalidad,
        phone: lead.informacion_contacto.telefono_contacto,
        email: lead.informacion_contacto.correo_electronico,
        firstReferencePhone: lead.informacion_contacto.numero_referencia_1,
        secondReferencePhone: lead.informacion_contacto.numero_referencia_2,
        city: lead.informacion_contacto.ciudad,
        note: lead.informacion_contacto.observacion,
        chanel: lead.informacion_contacto.canal,
        leadNumber: lead.informacion_contacto.numero,
        numberOfPurchaseProcess: lead.informacion_contacto.numero_proceso_venta,
        status: lead.informacion_contacto.estado,
        creationDate: lead.informacion_contacto.fecha_creacion,
        createdBy: lead.informacion_contacto.creado_por,
        source: lead.informacion_contacto.origen,
      },
      products: lead.productos.postpago.map((item) => this.getProductObject('postpago', item))
        .concat(lead.productos.lte_fijo.map((item) => this.getProductObject('lte', item)))
        .concat(lead.productos.telefonia_publica.map((item) => this.getProductObject('telefonía pública', item)))
        .concat(lead.productos.vpn.map((item) => this.getProductObject('vpn', item)))
        .concat(lead.productos.bolsas.map((item) => this.getProductObject('bolsas', item)))
        .concat(lead.productos.otros.map((item) => this.getProductObject('otros', item))),
      statusRecord: {
        seller: lead.historial_estados.vendedor,
        creationDate: lead.historial_estados.fecha_creacion,
        statusRecords: lead.historial_estados.historial_estados.map((item) => this.getStatusRecord(item)),
      },
      location: {
        latlng: {
          latitude: lead.ubicacion.latitud,
          longitude: lead.ubicacion.longitud,
        },
        availabilityOfService: lead.ubicacion.cobertura,
        address: lead.ubicacion.direccion,
      },
      pictures: lead.fotos.map((item) => this.getPictureObject(item)),
    };
  }
  /**
   * Retorna los productos para mostrarlos
   * en LeadViewer
   */
  getProductObject(productType, product) {

    return {
      productType: productType,
      id: product.detalle_producto,
      purchaseType: product.tipo_compra,
      plan: product.plan,
      quantity: product.cantidad,
      data: product.datos,
      minut: product.minutos,
      credit: product.credito,
      friendGroup: product.grupo_amigo,
      type: product.tipo,
      line: product.linea,
      imei: product.imei_cpe,
      enodeb: product.enodeb,
      procedure: product.procedimiento,
      service: product.servicio,
    };
  }
  /**
   * Retorna el historial de estados
   * para mostrarlo en LeadEdition
   */
  getStatusRecord(statusRecord) {
    
    return {
      additionalInformation: statusRecord.informacion_adicional,
      seller: statusRecord.vendedor,
      reasonForClosing: statusRecord.motivo_cierre,
      status: statusRecord.estado,
      updateDate: statusRecord.fecha_cambio,
    }
  }
  /**
   * Retorna objeto foto para
   * ser mostrado en LeadViewer
   */
  getPictureObject(picture) {
    
    return {
      creationDate: picture.fecha_creacion,
      picture: picture.foto_url,
      pictureTypeName: picture.tipo_foto,
      distance: picture.distancia_a_lead,
    }
  }
  /**
   * Retorna objeto lead para ser mostrado
   * en ActionRegistration
   */
  getALeadForSavingAnAction(lead) {

    return {
      lead: {
        information:{
          name: lead.informacion_contacto.nombre,
          enterpriseName: lead.informacion_contacto.empresa,
          nit: lead.informacion_contacto.nit,
          owner: lead.informacion_contacto.persona_contacto,
          identityNumber: lead.informacion_contacto.cedula_identidad,
          extension: lead.informacion_contacto.extension,
          foreignIdentityNumber: lead.informacion_contacto.cedula_extranjera,
          nationality: lead.informacion_contacto.nacionalidad,
          phone: lead.informacion_contacto.telefono_contacto,
          email: lead.informacion_contacto.correo_electronico,
          firstReferencePhone: lead.informacion_contacto.numero_referencia_1,
          secondReferencePhone: lead.informacion_contacto.numero_referencia_2,
          city: lead.informacion_contacto.ciudad,
          note: lead.informacion_contacto.observacion,
          chanel: lead.informacion_contacto.canal,
          leadNumber: lead.informacion_contacto.numero,
          numberOfPurchaseProcess: lead.informacion_contacto.numero_proceso_venta,
          status: lead.informacion_contacto.estado,
          creationDate: lead.informacion_contacto.fecha_creacion,
          createdBy: lead.informacion_contacto.creado_por,
          source: lead.informacion_contacto.origen,
        },
        products: lead.productos.postpago.map((item) => this.getProductObject('postpago', item))
          .concat(lead.productos.lte_fijo.map((item) => this.getProductObject('lte', item)))
          .concat(lead.productos.telefonia_publica.map((item) => this.getProductObject('telefonía pública', item)))
          .concat(lead.productos.vpn.map((item) => this.getProductObject('vpn', item)))
          .concat(lead.productos.bolsas.map((item) => this.getProductObject('bolsas', item)))
          .concat(lead.productos.otros.map((item) => this.getProductObject('otros', item))),
      },
      configurationData: {
        durations: this.translateConfigurationDataKeys(lead.registrar_accion.duraciones),
        actions: this.translateConfigurationDataKeys(lead.registrar_accion.acciones),  
      },
    };
  }
  /**
   * Retorna el body para guardar
   * una acción
   */
  getBodyForSavingAnAction(action) {
    
    return {
      fecha: action.date || null,
      hora: action.time || null,
      duracion: action.duration || '',
      accion: action.action || '',
      observacion: action.note || '',	
    };
  }
  /**
   * Retorna el body para obtener acciones
   * por fecha
   */
  getBodyForGettingActionsByDate(date) {
    
    return {
      fecha: date,
    };
  }
  /**
   * Retorna objetos action a ser mostrado en la vista
   * Appointment
   */
  getActionObjectsForAppointmentView(data) {

    return data.length !== 0
    ? {
        [moment(data[0].fecha_timestamp).format('YYYY-MM-DD')]: data.map((item) =>
          this.getActionObjectForAppointmentView(item)
        )
      }
    : {};
  }
  /**
   * Retorna objeto action a ser mostrado en la
   * vista Appointment
   */
  getActionObjectForAppointmentView(data) {
    
    return {
      id: data.id,
      initTime: data.hora_inicial,
      endTime: data.hora_final,
      editable: data.puede_editar,
      action: data.accion,
    };
  }
  /**
   * Retorna objeto con los datos a mostrar
   * en el Home
   */
  getHomeDataObject(data) {
    
    return {
      pendingAssignments: data.cant_asignaciones_pendientes,
      observedLeads: data.cant_observados,
      notifications: data.cant_notificaciones_no_vistas,
    }
  }
  /**
   * Retorna array de subestados, ademas la cantidad de
   * leads por sub estados
   */
  getLeadsBySubStatus(data) {

    return data.map((item) => ({
      id: item.id,
      name: item.nombre,
      quantity: item.cantidad,
    }));
  }
  /**
   * Retorna objeto con la acción a
   * editar para ser mostrada en 
   * ActionEdition
   */
  getAnActionForEditionObject(data) {

    return {
      information:{
        name: data.informacion_accion.informacion_contacto.nombre,
        enterpriseName: data.informacion_accion.informacion_contacto.empresa,
        nit: data.informacion_accion.informacion_contacto.nit,
        owner: data.informacion_accion.informacion_contacto.persona_contacto,
        identityNumber: data.informacion_accion.informacion_contacto.cedula_identidad,
        extension: data.informacion_accion.informacion_contacto.extension,
        foreignIdentityNumber: data.informacion_accion.informacion_contacto.cedula_extranjera,
        nationality: data.informacion_accion.informacion_contacto.nacionalidad,
        phone: data.informacion_accion.informacion_contacto.telefono_contacto,
        email: data.informacion_accion.informacion_contacto.correo_electronico,
        firstReferencePhone: data.informacion_accion.informacion_contacto.numero_referencia_1,
        secondReferencePhone: data.informacion_accion.informacion_contacto.numero_referencia_2,
        city: data.informacion_accion.informacion_contacto.ciudad,
        note: data.informacion_accion.informacion_contacto.observacion,
        chanel: data.informacion_accion.informacion_contacto.canal,
        leadNumber: data.informacion_accion.informacion_contacto.numero,
        numberOfPurchaseProcess: data.informacion_accion.informacion_contacto.numero_proceso_venta,
        status: data.informacion_accion.informacion_contacto.estado,
        creationDate: data.informacion_accion.informacion_contacto.fecha_creacion,
        createdBy: data.informacion_accion.informacion_contacto.creado_por,
        source: data.informacion_accion.informacion_contacto.origen,
      },
      products: data.informacion_accion.productos.postpago.map((item) => this.getProductObject('postpago', item))
        .concat(data.informacion_accion.productos.lte_fijo.map((item) => this.getProductObject('lte', item)))
        .concat(data.informacion_accion.productos.telefonia_publica.map((item) => this.getProductObject('telefonía pública', item)))
        .concat(data.informacion_accion.productos.vpn.map((item) => this.getProductObject('vpn', item)))
        .concat(data.informacion_accion.productos.bolsas.map((item) => this.getProductObject('bolsas', item)))
        .concat(data.informacion_accion.productos.otros.map((item) => this.getProductObject('otros', item))),
      action: data.objeto_accion && {
        time: data.objeto_accion.hora,
        durationId: data.objeto_accion.duracion && data.objeto_accion.duracion.id,
        durationName: data.objeto_accion.duracion && data.objeto_accion.duracion.nombre,
        date: data.objeto_accion.fecha,
        note: data.objeto_accion.observacion,
        actionId: data.objeto_accion.accion && data.objeto_accion.accion.id,
        actionName: data.objeto_accion.accion && data.objeto_accion.accion.nombre,
      },
      configurationData: {
        durations: this.translateConfigurationDataKeys(data.informacion_accion.editar_accion.duraciones),
        actions: this.translateConfigurationDataKeys(data.informacion_accion.editar_accion.acciones),
        actionStatus: data.informacion_accion.editar_accion.estados.map((item) => ({
          id: item.id,
          name: item.nombre,
          answers: [{ id: '', name: 'Seleccione una respuesta' }].concat(item.respuestas.map((item) => ({
            id: item.id,
            name: item.nombre,
            reasons: this.translateConfigurationDataKeys(item.motivos),
            viewToShow: item.info_guardar,
          }))),
        }))
      }
    }
  }
  /**
   * Retorna el body para actualizar una acción
   */
  getBodyForUpdateAnAction(data) {
    
    return {
      accion:{
    		fecha: data.date || null,
    		hora: data.time || null,
    		duracion: data.duration || '',
    		accion: data.action || '',
    		observacion: data.note || '',
    		estado: data.status || '',
    		respuesta: data.answer || '',
      },
    	motivo_cierre:{
    		motivo: data.reason || '',
    		justificacion: data.justification || '',
    	},
    	venta_exitosa:{
    		numero_proceso_venta: data.numberOfPurchaseProcess || '',
    	},
    	siguiente_accion:{
    		fecha: data.newDate || null,
    		hora: data.newTime || null,
    		duracion: data.newDuration || '',
    		accion: data.newAction || '',
    		observacion: data.newNote || '',
    	}
    }
  }
  /**
   * Retorna la lista de leads pendientes para mostrarlos
   * en PendingAssignment
   */
  getPendingLeadObjects(leads) {
    
    return leads.map((item) => {

      return {
        id: item.id,
        creationDate: item.fecha_creacion,
        number: item.numero,
        name: item.nombre,
        phone: item.telefono_contacto,
        products: item.productos,
      };
    })
  }
  /**
   * Retorna un lead pendiente
   * para ser visto en ApprobationOrRejection
   */
  getAPendingLeadObject(lead) {
    
    return {
      information:{
        name: lead.informacion_contacto.nombre,
        enterpriseName: lead.informacion_contacto.empresa,
        nit: lead.informacion_contacto.nit,
        owner: lead.informacion_contacto.persona_contacto,
        identityNumber: lead.informacion_contacto.cedula_identidad,
        extension: lead.informacion_contacto.extension,
        foreignIdentityNumber: lead.informacion_contacto.cedula_extranjero,
        nationality: lead.informacion_contacto.nacionalidad,
        phone: lead.informacion_contacto.telefono_contacto,
        email: lead.informacion_contacto.correo_electronico,
        firstReferencePhone: lead.informacion_contacto.numero_referencia_1,
        secondReferencePhone: lead.informacion_contacto.numero_referencia_2,
        city: lead.informacion_contacto.ciudad,
        note: lead.informacion_contacto.observacion,
        chanel: lead.informacion_contacto.canal,
        leadNumber: lead.informacion_contacto.numero,
        numberOfPurchaseProcess: lead.informacion_contacto.numero_proceso_venta,
        status: lead.informacion_contacto.estado,
        creationDate: lead.informacion_contacto.fecha_creacion,
        createdBy: lead.informacion_contacto.creado_por,
        source: lead.informacion_contacto.origen,
      },
      products: lead.productos.postpago.map((item) => this.getProductObject('postpago', item))
        .concat(lead.productos.lte_fijo.map((item) => this.getProductObject('lte', item)))
        .concat(lead.productos.telefonia_publica.map((item) => this.getProductObject('telefonía pública', item)))
        .concat(lead.productos.vpn.map((item) => this.getProductObject('vpn', item)))
        .concat(lead.productos.bolsas.map((item) => this.getProductObject('bolsas', item)))
        .concat(lead.productos.otros.map((item) => this.getProductObject('otros', item))),
    };
  }
  /**
   * Retorna el body para rechazar un lead pendiente de asignacion
   */
  getBodyForRejectingAPendingLead(data) {
    
    return {
      razon: data,
    };
  }
  /**
   * Retorna la lista de leads observados para mostrarlos
   * en ObservedLead
   */
  getObservedLeadObjects(leads) {

    return leads.map((item) => {

      return {
        id: item.lead_id,
        observationDate: item.fecha_observacion,
        number: item.numero,
        name: item.nombre,
        observationId: item.observacion_id,
        phone: item.telefono,
        justification: item.justificacion,
      };
    })
  }
  /**
   * Retorna la lista de notificaciones para mostrarlas
   * en la vista Notification
   */
  getNotificationObjects(notifications) {

    return notifications.map((item) => ({
      id: item.id,
      message: item.contenido,
      date: item.fecha,
      seen: item.visto,
      type: item.tipo,
      leadId: item.extra.lead.id,
      leadNumber: item.extra.lead.numero,
    }));
  }
}