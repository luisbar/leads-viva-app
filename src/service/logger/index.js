/**
 * Wrapper para chraslytics
 */
export default class Logger {
  
  constructor(logger) {
    this.logger = logger;
  }
  /**
   * Envía el error a crashlytics
   * @param  {string} message el mensaje de error
   */
  recordError(message) {
    this.logger.recordError(new Date().getTime(), message);
  }
  /**
   * Envía información relevante de un error a crashlytics
   * @param  {string} message mensaje acerca del error
   */
  log(message) {
    this.logger.log(message);
  }
}