/***********************
 * Node modules import *
 ***********************/
import firebase from 'react-native-firebase';
import Realm from 'realm';
/******************
 * Project import *
 ******************/
import PushNotification from './pushNotification/index.js';
import Database from './database/index.js';
import Requester from './requester/index.js';
import Logger from './logger/index.js';

import Session from './database/model/session.js';
/*************
 * Constants *
 *************/
const session = new Session();

export const database = new Database(new Realm(
{ schema:
  [
    session,
  ],
  schemaVersion: 6,
}));

export const pushNotification = new PushNotification(firebase.messaging(), firebase.notifications());
export const requester = new Requester();
export const logger = new Logger(firebase.crashlytics());
