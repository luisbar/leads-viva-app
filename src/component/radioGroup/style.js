/******************
 * Project import *
 ******************/
import { radioLabel } from '../../color.js';

export default function(props) {
  
  return {
    
    radioGroupContainer: {
      marginHorizontal: 15,
      marginBottom: 5,
    },
    radioGroupLabel: {
      color: radioLabel,
    },
    radioGroup: {
      borderRadius: 5,
      paddingHorizontal: 3,
      paddingVertical: 5,
    },
    advice: {
      color: props.errorColor,
      left: 0,
      fontSize: 13,
      width: '90%',
    },
  };
};