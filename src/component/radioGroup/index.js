/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import { Radio, Text, ListItem, Body } from 'native-base';
import { View } from 'react-native';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
/**
 * It renders a radio group
 */
class RadioGroup extends PureComponent {
  
  constructor(props) {
    super(props);
    //To set the listener here for improving the performance
    this._renderItem = this.renderItem.bind(this);
    this._onItemSelected = (itemId) => this.onItemPressed.bind(this, itemId);
    //State
    this.state = {
      itemSelected: this.props.defaultValue,
      advice: null,
    };
  }

  render() {

    return (
      <View style={style(this.props).radioGroupContainer}>
        <Text style={style(this.props).radioGroupLabel}>
          {this.props.title}
        </Text>
        
        <View
          style={style(this.props).radioGroup}>
          {this.props.items.map(this._renderItem)}
        </View>
        <Text style={style(this.props).advice}>{this.state.advice}</Text>
      </View>
    );
  }
  /**
   * It renders radio buttons in order to populate
   * the radio group
   * @param  {object} item data about a radio button
   * @param  {[type]} index index of a radio button
   */
  renderItem(item, index) {
    
    return (
      <ListItem
        button
        last
        key={index}
        onPress={this._onItemSelected(item.id)}>
      
        <Radio
          selected={item.id === this.state.itemSelected}
          onPress={this._onItemSelected(item.id)}/>
        
        <Body>
          <Text>{item.name}</Text>
        </Body>
      </ListItem>
    );
  }
  /**
   * Listener that is triggered when an user pressed
   * over a radio button
   * @param  {number} itemId id of the pressed radio button
   */
  onItemPressed(itemId) {
    if (this.state.itemSelected !== itemId)
      this.setState({
        itemSelected: itemId,
      }, () => 
        this.props.onItemPressed(this.state.itemSelected)
      );
  }
  /**
   * It checks if errors props has data and update the
   * advice value of the state
   */
  componentDidUpdate(prevProps, prevState) {
    let advice = '';

    if (this.props.errors)
      this.props.errors.map((error) => {
        if (error)
          advice += error + '\n';
      });

    this.setState({ advice: advice });
  }
}

RadioGroup.propTypes = {
  items: PropTypes.array,//Array that represents radio buttons to be rendered
  defaultValue: PropTypes.number,//Item selected initially
  title: PropTypes.string,//Label of the radio group
  onItemPressed: PropTypes.func,//Listener that is triggered when a radio button is pressed
  errorColor: PropTypes.string,//Color of error message
  errors: PropTypes.array,//Errors to show
};

RadioGroup.defaultProps = {
  items: [
    { id: 0, name: 'opcion 1' },
    { id: 1, name: 'opcion 2' },
  ],
  defaultValue: 1,
  title: 'Title',
};

export default RadioGroup;