/******************
 * Project import *
 ******************/
import {
  drawerBackground,
  drawerOddItemBackground,
  drawerPairItemBackground,
  drawerTextColor,
  drawerIconBackground,
  drawerFooter,
  drawerFooterText,
} from '../../color.js';

export default {
  
  container: {
    backgroundColor: drawerBackground,
  },
  oddItem: {
    height: 60,
    backgroundColor: drawerOddItemBackground,
  },
  pairItem: {
    height: 60,
    backgroundColor: drawerPairItemBackground,
  },
  leftCol: {
    width: 28,
    height: 28,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: drawerIconBackground,
    borderRadius: 28,
    marginLeft: 10,
    overflow: 'hidden',//Para ios es necesario
  },
  rightCol: {
    flexDirection: 'column',
    justifyContent: 'center',
  },
  text: {
    color: drawerTextColor,
  },
  footer: {
    backgroundColor: drawerFooter,
  },
  footerText: {
    flex: 1,
    color: drawerFooterText,
    textAlign: 'center',
  },
};