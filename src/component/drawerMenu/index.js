/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Container, Content, Text, Grid, Col, Button, StyleProvider, Footer } from 'native-base';
import MCIIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../../scene/basisComponent.js';
import { drawerOddIcon, drawerPairIcon } from '../../color.js';
import { logger } from '../../service/index.js';
import { executeSignOut } from '../../data/session/action.js';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import {
  appointment,
  configuration,
  login,
  home,
} from '../../config.js';
/**
 * Renderiza el menu del drawer
 */
class DrawerMenu extends BasisComponent {

  constructor(props) {
    super(props);
    //State
    this.state = {
      options: [
        { id: 0, option: '', icon: 'account' },
        { id: 1, option: 'Home', icon: 'home', scene: home, title: 'Home' },
        { id: 2, option: 'Agenda', icon: 'calendar', scene: appointment, title: 'Agenda' },
        { id: 3, option: 'Cambiar contraseña', icon: 'lock', scene: configuration, title: 'Cambiar contraseña' },
        { id: 4, option: 'Cerrar sesión', icon: 'logout-variant', scene: 'logout' },
      ],
    };
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onItemClick = (item) => this.onItemClick.bind(this, item.scene, item.title);
  }
  /**
   * Verifica si hay sesión activa, por que desde esta clase
   * se invoca al metodo para cerrar sesión
   */
  componentWillReceiveProps(nextProps) {
    this.getCurrentScreenId()
    .then((screen) => {
      if (!nextProps.session &&
          !nextProps.statusOfSigningOut.signOutIsBeingExecuted &&
          screen.screenId !== 'screenInstanceID2')//Verifica que el identificador no sea de la scene main
        this.resetTo(login);
    })
    .catch((error) => {
      logger.recordError(String(error));
      logger.log('DrawerMenu: error al obtener el id de la vista actual')
    })
    //Para poner el nombre de usuario en el drawer
    if (nextProps.session && !this.state.options[0].option) {
      let aux = this.state.options;
      aux[0].option = nextProps.session.username;
      
      this.setState({ options: aux });
    };
    //Para borrar el nombre de usuario del drawer cuando se cierra sesion
    if (this.props.statusOfSigningOut.signOutIsBeingExecuted &&
      !nextProps.statusOfSigningOut.signOutIsBeingExecuted) {
      
      let aux = this.state.options;
      aux[0].option = '';
      
      this.setState({ options: aux });
    }
  }

  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container
          style={style.container}>
          <Content>
            {
              this.state.options.map((item, index) => this.renderItem(item, index))
            }
          </Content>
          <Footer style={style.footer}>
            <Text style={style.footerText}>{'Copyright © 2018 Viva. All rights reserved.'}</Text>
          </Footer>
        </Container>
      </StyleProvider>
    );
  }
  /**
   * Renderiza item del drawer
   * @param  {json} item datos correspondientes al item
   * @return {reactComponent} item del drawer
   */
  renderItem(item, index) {

    return (
      <Button
        key={index}
        style={item.id % 2 === 0 ? style.pairItem : style.oddItem}
        onPress={this._onItemClick(item)}>

        <Grid>
          <Col
            style={style.leftCol}>
            <MCIIcon name={item.icon} size={25} color={item.id % 2 === 0 ? drawerPairIcon : drawerOddIcon}/>
          </Col>

          <Col
            style={style.rightCol}>
            <Text style={style.text}>{item.option}</Text>
          </Col>
        </Grid>
      </Button>
    )
  }
  /**
   * Verifica si hubo un error al ejecutar
   * el metodo executeSignOut
   */
  componentDidUpdate(prevProps, prevState) {
    if (this.props.statusOfSigningOut.wasAnError)
      this.showErrorMessage('Hubo un problema al cerrar sesión');
  }
  /**
   * Escuchador que se dispara cuando se presiona un
   * item del drawer
   * @param  {string} scene identificador de la vista a mostrar
   * @param  {[type]} title titulo que se mostrará en el toolbar
   * @param  {object} context contexto
   */
  onItemClick(scene, title, context) {
    if (scene === 'logout')
      this.props.executeSignOut();
    else if (scene === home)
      this.resetTo(scene);
    else if (scene)
      this.push(scene);

    this.closeMenu();
  }
  /**
   * Cierra el navigation drawer
   */
  closeMenu() {
    this.props.navigator.toggleDrawer({
      to: 'closed',
      side: 'left',
      animated: true,
    });
  };
}

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  statusOfSigningOut: state.dataReducer.sessionReducer.statusOfSigningOut,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeSignOut: executeSignOut,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DrawerMenu);
