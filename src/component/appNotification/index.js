/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import { Thumbnail, Button } from 'native-base';
import { View, Text } from 'react-native';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import Router from '../../router.js';
import { notification } from '../../config.js';
/**
 * Renderiza las notificaciones cuando la app
 * esta corriendo
 */
class AppNotification extends PureComponent {
  
  constructor(props) {
    super(props);
    this._onAppNotificationPressed = this.onAppNotificationPressed.bind(this);
  }

  render() {

    return (
      <Button
        style={style.container}
        onPress={this._onAppNotificationPressed}>
        <Thumbnail
          style={style.thumbnail}
          source={require('../../image/app-icon.png')}/>
        
        <View style={style.dataContainer}>
          <Text
            numberOfLines={1}
            style={style.title}>
            {this.props.title}
          </Text>
          
          <Text
            numberOfLines={1}
            style={style.body}>
            {this.props.body}
          </Text>
        </View>
      </Button>
    );
  }
  /**
   * Escuchador que se dispara cuando un usuario
   * presiona sobre una AppNotification
   */
  onAppNotificationPressed() {
    Router.getInstance().push(notification);
  }
}

AppNotification.propTypes = {
  title: PropTypes.string,//Titulo de la notificacion
  body: PropTypes.string,//Body de la notificacion
};

export default AppNotification;