/******************
 * Project import *
 ******************/
import {
  appNotificationContainer,
  appNotificationTitle,
  appNotificationBody,
} from '../../color.js';

export default {
  
  container: {
    maxHeight: 66,
    minHeight: 66,
    backgroundColor: appNotificationContainer,
    flexDirection: 'row',
  },
  thumbnail: {
    margin: 5,
  },
  dataContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    width: '90%',
  },
  title: {
    color: appNotificationTitle,
    fontFamily: 'Museo700',
    maxWidth: '88%',
  },
  body: {
    color: appNotificationBody,
    fontFamily: 'Museo300',
    maxWidth: '88%',
  },
};