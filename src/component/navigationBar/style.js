export default function (props) {

  return {

      toolBar: props.toolBarStyle,
      left: {
        flex: 0,
        marginRight: 15,
      },
      right: {
        flex: 0,
      },
      title: props.titleStyle,
      titleDefaultStyle: {
        fontFamily: 'Museo700',
      },
      rightButtonIcon: props.rightButtonIconStyle,
      badge: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        height: 30,
        width: 30,
        zIndex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30
      },
      badgeText: {
        fontSize: 8,
      },
      searchBar: props.searchBarStyle,
      searchBarIcon: {
        color: props.searchBarIconColor,
      },
  };
};
