/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import { Header, Left, Body, Right, Item, Input, Title, Button, Icon, Badge, Text } from 'native-base';
import { View } from 'react-native';
import IIcon from 'react-native-vector-icons/Ionicons';
import Progress from 'react-native-progress/Bar';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
/**
 * It renders a navigator bar
 */
class NavigationBar extends PureComponent {

  render() {

    return (
      <View>
        <Header
          style={style(this.props).toolBar}
          androidStatusBarColor={this.props.toolBarStyle.backgroundColor}>
          {this.renderLeftSide()}

          <Body>
            <Title style={[style(this.props).title, style(this.props).titleDefaultStyle]}>{this.props.title}</Title>
          </Body>

          {this.renderRightButtons()}
        </Header>

        {this.renderSearchBar()}
        {this.renderProgressBar()}
      </View>
    );
  }
  /**
   * It renders the left side of the toolBar
   */
  renderLeftSide() {

    return (
      this.props.leftButtonVisible &&
      <Left
        style={style(this.props).left}>
        <Button
          transparent
          onPress={this.props.onPressLeftButton}>

          <IIcon
            name={this.props.iconLeftButton}
            color={this.props.colorIconLeftButton}
            size={30}/>
        </Button>
      </Left>
    );
  }
  /**
   * It renders the right buttons of the toolBar
   */
  renderRightButtons() {

    return (
      <Right
        style={style(this.props).right}>
      {
        this.props.rightButtonsVisible &&
        this.props.rightButtons.map((item, index) =>
          <Button
            key={index}
            transparent
            onPress={item.onPressRightButton}>
            {
              item.badge && item.badgeText
              ? <Badge
                  style={style(this.props).badge}>
                  <Text numberOfLines={1} style={style(this.props).badgeText}>{item.badgeText}</Text>
                </Badge>
              : null
            }
            <IIcon
              style={style(item).rightButtonIcon}
              name={item.iconRightButton}
              color={item.colorIconRightButton}
              size={30}/>
          </Button>
        )
      }
      </Right>
    );
  }
  /**
   * It render the search bar of the navigation bar
   */
  renderSearchBar() {

    return (
      this.props.searchBarVisible &&
      <Header
        searchBar
        rounded
        style={style(this.props).searchBar}>

        <Item>
          <Icon
            style={style(this.props).searchBarIcon}
            name={'ios-search'}/>
          <Input
            placeholder={'Buscar'}
            onChangeText={this.props.onSearch}
            defaultValue={this.props.defaultSearchValue}/>
        </Item>
      </Header>
    );
  }
  /**
   * It renders a progress bar
   */
  renderProgressBar() {

    return (
      this.props.progressBarVisible &&
      <Progress
        indeterminate={true}
        width={null}
        height={3}
        unfilledColor={this.props.progressBarBackgroundColor}
        color={this.props.progressBarColor}
        borderRadius={0}
        useNativeDriver={true}/>
    );
  }
}

NavigationBar.propTypes = {
  toolBarStyle: PropTypes.object,//Style for toolBar
  leftButtonVisible: PropTypes.bool,//Flag for showing the left button or not
  onPressLeftButton: PropTypes.func,//Listener that is triggered when an user has pressed the left button
  iconLeftButton: PropTypes.string,//Icon for the left button
  colorIconLeftButton: PropTypes.string,//Color of the icon for the left button
  title: PropTypes.string,//Title for showing on the toolBar
  titleStyle: PropTypes.object,//Style for the title
  rightButtonsVisible: PropTypes.bool,//Flag for showing the right buttons or not
  rightButtons: PropTypes.array,//Array with properties for showing buttons on the right side
  searchBarVisible: PropTypes.bool,//Flag for showing a search bar or not
  searchBarStyle: PropTypes.object,//Style for the search bar
  searchBarIconColor: PropTypes.string,//Icon for the search bar
  onSearch: PropTypes.func,//Listener that is triggered when an user has written on the search input
  defaultSearchValue: PropTypes.string,//Default value for the search input
  progressBarVisible: PropTypes.bool,//Flag for showing the progress bar
  progressBarBackgroundColor: PropTypes.string,//Background color of the progress bar
  progressBarColor: PropTypes.string,//Color of the progress bar
};

NavigationBar.defaultProps = {
  toolBarStyle: {
    backgroundColor: '#bfe43d',
    borderColor: '#bfe43d',
  },
  leftButtonVisible: false,
  onPressLeftButton: () => console.warn('leftButton'),
  iconLeftButton: 'md-menu',
  colorIconLeftButton: '#5D3C9E',
  title: 'Titulo',
  titleStyle: {
    color: '#5D3C9E',
  },
  rightButtonsVisible: false,
  rightButtons: [
    {
      onPressRightButton: () => console.warn('1'),
      iconRightButton: 'ios-funnel',
      colorIconRightButton: '#5D3C9E',
      colorIconRightButton: '#5D3C9E',
    },
    {
      onPressRightButton: () => console.warn('2'),
      iconRightButton: 'ios-search',
      colorIconRightButton: '#5D3C9E',
      badge: true,
      badgeText: 5,
    },
  ],
  searchBarVisible: false,
  searchBarStyle: {
    backgroundColor: '#bfe43d',
    borderColor: '#bfe43d',
  },
  searchBarIconColor: '#5D3C9E',
  onSearch: (text) => console.warn(text),
  progressBarVisible: false,
  progressBarBackgroundColor: '#FFFFFF',
  progressBarColor: '#AE603A',
}

export default NavigationBar;
