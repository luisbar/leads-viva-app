/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Content } from 'native-base';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import BasisComponent from '../../scene/basisComponent.js';
import ProgressScreen from '../progressScreen/index.js';
import FormWithAllInputs from '../formWithAllInputs/index.js';
import {
  inputLabelColor,
  inputTextColor,
  inputPlaceholderColor,
  inputBackgroundColor,
  pickerColor,
  pickerLabelColor,
  pickerBackgroundColor,
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
} from '../../color.js';
/*************
 * Constants *
 *************/
const TYPE_INPUT = 'type';
const PHONE_INPUT = 'phone';
const FIRST_REFERENCE_PHONE_INPUT = 'firstReferencePhone';
const SECOND_REFERENCE_PHONE_INPUT = 'secondReferencePhone';
const EMAIL_INPUT = 'email';
const SOURCE_INPUT = 'source';
const CITY_INPUT = 'city';
const CHANEL_INPUT = 'chanel';
const NOTE_INPUT = 'note';
//Natural
const NAME_INPUT = 'name';
const DOCUMENT_TYPE_INPUT = 'documentType';
//Natural->Cedula de identidad
const IDENTITY_NUMBER_INPUT = 'identityNumber';
const EXTENSION_INPUT = 'extension';
//Natural->Cedula extranjera o pasaporte
const FOREIGN_IDENTITY_NUMBER_INPUT = 'foreignIdentityNumber';
const NATIONALITY_INPUT = 'nationality';
//Jurídico
const ENTERPRISE_NAME_INPUT = 'enterpriseName';
const NIT_INPUT = 'nit';
const OWNER_INPUT = 'owner';
//Tipo de lead
const PERSON = 'persona';
const ENTERPRISE = 'empresa';
//Tipo de documento
const NONE = '';
const IDENTITY_CARD = 'ci_nacional';
const FOREIGN_IDENTITY_CARD = 'ci_extranjero';
const PASSPORT = 'pasaporte';
/**
 * Renderiza formulario para registrar, editar y mostrar datos del
 * cliente o lead
 */
class InformationForm extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._reference = (form) => this.props.reference(form);
    this._onTypeChanged = this.onTypeChanged.bind(this);
    this._onDocumentTypeChanged = this.onDocumentTypeChanged.bind(this);
    this._onExtensionChanged = this.onExtensionChanged.bind(this);
    this._onNationalityChanged = this.onNationalityChanged.bind(this);
    this._onSourceChanged = this.onSourceChanged.bind(this);
    this._onCityChanged = this.onCityChanged.bind(this);
    this._onChanelChanged = this.onChanelChanged.bind(this);
    //Variables globales para ver que tipo de lead y que tipo de documento seleccionó el usuario
    this.idOfLeadTypeSelected;
    this.idOfDocumentTypeSelected;
    this.idOfExtensionSelected;
    this.idOfNationalitySelected;
    this.idOfSourceSelected;
    this.idOfCitySelected;
    this.idOfChanelSelected;
  }
  
  componentWillReceiveProps(nextProps) {
    const previousProps = this.props;
    //Setea el valor por defecto de todos los combos,
    //siempre y cuando cumpla las condiciones, si es que se va a guardar un lead
    if (nextProps.configurationData && !this.idOfLeadTypeSelected && !nextProps.lead) {
      this.idOfLeadTypeSelected = nextProps.configurationData.typeInputItems[0].id;
      this.idOfExtensionSelected = nextProps.configurationData.extensionInputItems[0].id;
      this.idOfNationalitySelected = nextProps.configurationData.nationalityInputItems[0].id;
      this.idOfSourceSelected = nextProps.configurationData.sourceInputItems[0].id;
      this.idOfCitySelected = nextProps.configurationData.cityInputItems[0].id;
      this.idOfChanelSelected = nextProps.configurationData.chanelInputItems[0].id;
      this.idOfDocumentTypeSelected = NONE;
    }
    //Si se va a modificar un lead guardado
    if (nextProps.lead && !this.idOfLeadTypeSelected) {
      this.idOfLeadTypeSelected = nextProps.lead.type.id;
      this.idOfDocumentTypeSelected = nextProps.lead.documentType &&
        nextProps.configurationData.documentTypeInputItems.filter((item) => item.id === nextProps.lead.documentType.id)[0].id;
      //Dropdowns
      this.idOfExtensionSelected = nextProps.lead.extension
        ? nextProps.configurationData.extensionInputItems.filter((item) => item.id === nextProps.lead.extension.id)[0].id
        : nextProps.configurationData.extensionInputItems[0].id;
      this.idOfNationalitySelected = nextProps.lead.nationality
        ? nextProps.configurationData.nationalityInputItems.filter((item) => item.id === nextProps.lead.nationality.id)[0].id
        : nextProps.configurationData.nationalityInputItems[0].id;
      this.idOfSourceSelected = nextProps.lead.source
        ? nextProps.configurationData.sourceInputItems.filter((item) => item.id === nextProps.lead.source.id)[0].id
        : nextProps.configurationData.sourceInputItems[0].id;
    }
    
    this.getFieldsByTypeOfLeadOrDocumentType(previousProps);
  }

  render() {

    return (
      <Content>
      {
        this.props.showProgressScreen
        ? this.renderProgressScreen()
        : this.renderForm()
      }
      </Content>
    );
  }
  /**
   * Renderiza el FormWithAllInputs
   */
  renderForm() {
    
    return (
      this.props.configurationData &&//Si se pudo obtener la configuración para el formulario de registro de leads
      <FormWithAllInputs
        ref={'form'}
        fields={this.state.fields}
        onFieldsOk={this.onFieldsOk.bind(this)}
        onEmptyFields={this.onEmptyFields.bind(this)}/>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {
    
    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbarAndTabBar={true}/>
    );
  }
  /**
   * Actualiza el campo inputValues del state
   */
  componentDidMount() {
    this.getFieldsByTypeOfLeadOrDocumentType(this.props);
  }
  /**
   * Invoca metodo para validar el formulario
   */
  excuteValidateForm() {
    this.refs.form.validateForm();
  }
  /**
  * Escuchador que se dispara despues de invocar al metodo validateForm
  * del componente FormWithAllInputs, siempre y cuando el formulario
  * tenga los campos requeridos con datos
  * @param  {object} inputs contiene el valor de cada input del formulario
  */
  onFieldsOk(inputs) {
    this.props.onValidatedForm(this.getInputValues(inputs));
  }
  /**
   * Escuchador que se dispara despues de invocar al metodo validateForm
   * del componente FormWithAllInputs, siempre y cuando el formulario
   * tenga campos requeridos vacios
   * @param  {object} inputs contiene el valor de cada input del formulario
   */
  onEmptyFields(inputs) {
    this.props.onValidatedForm(this.getInputValues(inputs));
  }
  /**
   * Retorna json formateado para enviar a la api
   */
  getInputValues(inputs) {
    
    return {
      type: this.idOfLeadTypeSelected,
      name: (inputs[NAME_INPUT] && inputs[NAME_INPUT].value) || '',
      documentType: this.idOfDocumentTypeSelected,
      enterpriseName: (inputs[ENTERPRISE_NAME_INPUT] && inputs[ENTERPRISE_NAME_INPUT].value) || '',
      nit: (inputs[NIT_INPUT] && inputs[NIT_INPUT].value) || '',
      owner: (inputs[OWNER_INPUT] && inputs[OWNER_INPUT].value) || '',
      identityNumber: (inputs[IDENTITY_NUMBER_INPUT] && inputs[IDENTITY_NUMBER_INPUT].value) || '',
      extension: this.idOfExtensionSelected,
      foreignIdentityNumber: (inputs[FOREIGN_IDENTITY_NUMBER_INPUT] && inputs[FOREIGN_IDENTITY_NUMBER_INPUT].value) || '',
      nationality: this.idOfNationalitySelected,
      phone: (inputs[PHONE_INPUT] && inputs[PHONE_INPUT].value) || '',
      email: (inputs[EMAIL_INPUT] && inputs[EMAIL_INPUT].value) || '',
      firstReferencePhone: (inputs[FIRST_REFERENCE_PHONE_INPUT] && inputs[FIRST_REFERENCE_PHONE_INPUT].value) || '',
      secondReferencePhone: (inputs[SECOND_REFERENCE_PHONE_INPUT] && inputs[SECOND_REFERENCE_PHONE_INPUT].value) || '',
      city: this.idOfCitySelected,
      source: this.idOfSourceSelected,
      note: (inputs[NOTE_INPUT] && inputs[NOTE_INPUT].value) || '',
      chanel: this.idOfChanelSelected,
    };
  }
  /**
   * Retorna los fields de acuerdo a que tipo de lead
   * y/o tipo de documento ha sido seleccionado
   */
  getFieldsByTypeOfLeadOrDocumentType(props) {
    if (props.configurationData &&//Si ya se tiene la configuración del form
      this.idOfDocumentTypeSelected)//Si tiene seleccionado algun tipo de documento
      this.updateFieldsByDocumentType(this.idOfDocumentTypeSelected);
    else if (props.configurationData &&//Si ya se tiene la configuración del form
      !this.idOfDocumentTypeSelected)//Si no tiene seleccionado algun tipo de documento
      this.updateFieldsByLeadType(this.idOfLeadTypeSelected);
  }
  /**
   * Escuchador que se dispara cuando se selecciona
   * otro item del TYPE_INPUT
   */
  onTypeChanged(newTypeSelected, index) {
    this.idOfLeadTypeSelected = this.props.configurationData.typeInputItems[index].id;
    this.idOfDocumentTypeSelected = NONE;
    this.updateFieldsByLeadType(this.idOfLeadTypeSelected);  
  }
  /**
   * Actualiza los fields del form por id de tipo de lead
   */
  updateFieldsByLeadType(leadTypeId) {
    
    switch (leadTypeId) {
      
      case PERSON://Natural
        this.setState({ fields: this.getInputsForNaturalLead() })
        break;
      
      case ENTERPRISE://Jurídico
        this.setState({ fields: this.getInputsForLegalLead() })
        break;
    }
  }
  /**
   * Escuchador que se dispara cuando se selecciona
   * otro item del DOCUMENT_TYPE_INPUT
   */
  onDocumentTypeChanged(newDocumentTypeSelected, index) {
    this.idOfDocumentTypeSelected = this.props.configurationData.documentTypeInputItems[index].id;
    this.updateFieldsByDocumentType(this.idOfDocumentTypeSelected);
  }
  /**
   * Actualiza los fields del form por id de documento
   */
  updateFieldsByDocumentType(documentTypeId) {
    
    switch (documentTypeId) {
      
      case NONE://Ninguno
        this.setState({ fields: this.getInputsForNaturalLead() })
        break;
        
      case IDENTITY_CARD://Cedula de identidad
        this.setState({ fields: this.getInputsForLeadWithIdentityCard() })
        break;
        
      case FOREIGN_IDENTITY_CARD:
      case PASSPORT://Cedula de extranjero o pasaporte
        this.setState({ fields: this.getInputsForLeadWithForeignIdentityCard() })
        break;
    }
  }
  /**
   * Escuchador que se dispara cuando se selecciona
   * otro item del EXTENSION_INPUT
   */
  onExtensionChanged(newTypeSelected, index) {
    this.idOfExtensionSelected = this.props.configurationData.extensionInputItems[index].id;
  }
  /**
   * Escuchador que se dispara cuando se selecciona
   * otro item del NATIONALITY_INPUT
   */
  onNationalityChanged(newTypeSelected, index) {
    this.idOfNationalitySelected = this.props.configurationData.nationalityInputItems[index].id;
  }
  /**
   * Escuchador que se dispara cuando se selecciona
   * otro item del SOURCE_INPUT
   */
  onSourceChanged(newTypeSelected, index) {
    this.idOfSourceSelected = this.props.configurationData.sourceInputItems[index].id;
  }
  /**
   * Escuchador que se dispara cuando se selecciona
   * otro item del CITY_INPUT
   */
  onCityChanged(newTypeSelected, index) {
    this.idOfCitySelected = this.props.configurationData.cityInputItems[index].id;
  }
  /**
   * Escuchador que se dispara cuando se selecciona
   * otro item del CHANEL_INPUT
   */
  onChanelChanged(newTypeSelected, index) {
    this.idOfChanelSelected = this.props.configurationData.chanelInputItems[index].id;
  }
  /**
   * Retorna los inputs para cuando el item seleccionado del TYPE_INPUT
   * es igual a Natural
   */
  getInputsForNaturalLead() {
    
    return [
      this.getTypeInput()
    ]
    .concat(this.getNameAndDocumentTypeInputs(PHONE_INPUT))
    .concat(this.getOtherUnchangedInputs());
  }
  /**
   * Retorna los inputs para cuando el item seleccionado del TYPE_INPUT
   * es igual a Jurídico
   */
  getInputsForLegalLead() {
    
    return [
      this.getTypeInput(),
      {
        id: ENTERPRISE_NAME_INPUT,
        type: 'editableInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Nombre de la empresa(*)',
        placeholder: 'Escriba aquí',
        minLength: 1,
        maxLength: 200,
        keyboardType: 'default',
        returnKeyType: 'next',
        editable: true,
        onSubmitEditing: NIT_INPUT,
        visible: true,
        isRequired: false,
        errors: this.props.fieldErrors && this.props.fieldErrors.enterpriseName,
        defaultValue: this.props.lead ? this.props.lead.enterpriseName : '',
      },
      {
        id: NIT_INPUT,
        type: 'editableInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'NIT(*)',
        placeholder: 'Escriba aquí',
        minLength: 1,
        maxLength: 30,
        keyboardType: 'numeric',
        returnKeyType: 'next',
        editable: true,
        onSubmitEditing: OWNER_INPUT,
        visible: true,
        isRequired: false,
        errors: this.props.fieldErrors && this.props.fieldErrors.nit,
        defaultValue: this.props.lead ? this.props.lead.nit : '',
      },
      {
        id: OWNER_INPUT,
        type: 'editableInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Persona de contacto(*)',
        placeholder: 'Escriba aquí',
        minLength: 1,
        maxLength: 100,
        keyboardType: 'default',
        returnKeyType: 'next',
        editable: true,
        onSubmitEditing: PHONE_INPUT,
        visible: true,
        isRequired: false,
        errors: this.props.fieldErrors && this.props.fieldErrors.owner,
        defaultValue: this.props.lead ? this.props.lead.owner : '',
      },
    ].concat(this.getOtherUnchangedInputs());
  }
  /**
   * Retorna los inputs para cuando el item seleccionado del DOCUMENT_TYPE_INPUT
   * es igual a cedula de identidad
   */
  getInputsForLeadWithIdentityCard() {
    
    return [
      this.getTypeInput(),
    ]
    .concat(this.getNameAndDocumentTypeInputs(IDENTITY_NUMBER_INPUT))
    .concat([
      {
        id: IDENTITY_NUMBER_INPUT,
        type: 'editableInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Cedula de identidad(*)',
        placeholder: 'Escriba aquí',
        minLength: 1,
        maxLength: 15,
        keyboardType: 'numeric',
        returnKeyType: 'next',
        editable: true,
        onSubmitEditing: PHONE_INPUT,
        visible: true,
        isRequired: false,
        errors: this.props.fieldErrors && this.props.fieldErrors.identityNumber,
        defaultValue: this.props.lead ? this.props.lead.identityNumber : '',
      },
      {
        id: EXTENSION_INPUT,
        type: 'dropdown',
        colorPickerAndroid: pickerColor,
        labelColor: pickerLabelColor,
        pickerBackgroundColor: pickerBackgroundColor,
        label: 'Extensión(*)',
        items: this.props.configurationData.extensionInputItems.map((item) => item.name),
        onItemSelected: this._onExtensionChanged,
        visible: true,
        defaultValue: this.props.lead && this.props.lead.extension ? this.props.lead.extension.name : '',
      },
    ])
    .concat(this.getOtherUnchangedInputs());
  }
  /**
   * Retorna los inputs para cuando el item seleccionado del DOCUMENT_TYPE_INPUT
   * es igual a cedula extranjero o pasaporte
   */
  getInputsForLeadWithForeignIdentityCard() {
    
    return [
      this.getTypeInput(),
    ]
    .concat(this.getNameAndDocumentTypeInputs(FOREIGN_IDENTITY_NUMBER_INPUT))
    .concat([
      {
        id: FOREIGN_IDENTITY_NUMBER_INPUT,
        type: 'editableInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'CI Extranjero/Pasaporte(*)',
        placeholder: 'Escriba aquí',
        minLength: 1,
        maxLength: 15,
        keyboardType: 'default',
        returnKeyType: 'next',
        editable: true,
        onSubmitEditing: PHONE_INPUT,
        visible: true,
        isRequired: false,
        errors: this.props.fieldErrors && this.props.fieldErrors.foreignIdentityNumber,
        defaultValue: this.props.lead ? this.props.lead.foreignIdentityNumber : '',
      },
      {
        id: NATIONALITY_INPUT,
        type: 'dropdown',
        colorPickerAndroid: pickerColor,
        labelColor: pickerLabelColor,
        pickerBackgroundColor: pickerBackgroundColor,
        label: 'Nacionalidad(*)',
        items: this.props.configurationData.nationalityInputItems.map((item) => item.name),
        onItemSelected: this._onNationalityChanged,
        visible: true,
        defaultValue: this.props.lead && this.props.lead.nationality ? this.props.lead.nationality.name : '',
      },
    ])
    .concat(this.getOtherUnchangedInputs());
  }
  /**
   * Retorna json que representa el TYPE_INPUT
   */
  getTypeInput() {
    
    return {
      id: TYPE_INPUT,
      type: 'dropdown',
      colorPickerAndroid: pickerColor,
      labelColor: pickerLabelColor,
      pickerBackgroundColor: pickerBackgroundColor,
      label: 'Tipo(*)',
      items: this.props.configurationData.typeInputItems.map((item) => item.name),
      onItemSelected: this._onTypeChanged,
      visible: this.props.lead ? false : true,
    };
  }
  /**
   * Retorna json que representa los inputs NAME_INPUT
   * y DOCUMENT_TYPE_INPUT
   */
  getNameAndDocumentTypeInputs(onSubmitEditing) {
    
    return [
      {
        id: NAME_INPUT,
        type: 'editableInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Nombre(*)',
        placeholder: 'Escriba aquí',
        minLength: 1,
        maxLength: 200,
        keyboardType: 'default',
        returnKeyType: 'next',
        editable: true,
        onSubmitEditing: onSubmitEditing,
        visible: true,
        isRequired: false,
        errors: this.props.fieldErrors && this.props.fieldErrors.name,
        defaultValue: this.props.lead ? this.props.lead.name : '',
      },
      {
        id: DOCUMENT_TYPE_INPUT,
        type: 'dropdown',
        colorPickerAndroid: pickerColor,
        labelColor: pickerLabelColor,
        pickerBackgroundColor: pickerBackgroundColor,
        label: 'Tipo de documento',
        items: this.props.configurationData.documentTypeInputItems.map((item) => item.name),
        onItemSelected: this._onDocumentTypeChanged,
        visible: true,
        defaultValue: this.props.lead && this.props.lead.documentType ? this.props.lead.documentType.name : '',
      },
    ];
  }
  /**
   * Retorna los inputs que siempre estan visibles
   */
  getOtherUnchangedInputs() {
    
    return [
      {
        id: PHONE_INPUT,
        type: 'editableInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Teléfono de contácto(*)',
        placeholder: 'Escriba aquí',
        minLength: 1,
        maxLength: 8,
        keyboardType: 'phone-pad',
        returnKeyType: 'next',
        editable: true,
        onSubmitEditing: FIRST_REFERENCE_PHONE_INPUT,
        visible: true,
        isRequired: false,
        errors: this.props.fieldErrors && this.props.fieldErrors.phone,
        defaultValue: this.props.lead ? this.props.lead.phone : '',
      },
      {
        id: FIRST_REFERENCE_PHONE_INPUT,
        type: 'editableInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Número de referencia 1',
        placeholder: 'Escriba aquí',
        minLength: 1,
        maxLength: 8,
        keyboardType: 'phone-pad',
        returnKeyType: 'next',
        editable: true,
        onSubmitEditing: SECOND_REFERENCE_PHONE_INPUT,
        visible: true,
        isRequired: false,
        errors: this.props.fieldErrors && this.props.fieldErrors.firstReferencePhone,
        defaultValue: this.props.lead ? this.props.lead.firstReferencePhone : '',
      },
      {
        id: SECOND_REFERENCE_PHONE_INPUT,
        type: 'editableInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Número de referencia 2',
        placeholder: 'Escriba aquí',
        minLength: 1,
        maxLength: 8,
        keyboardType: 'phone-pad',
        returnKeyType: 'next',
        editable: true,
        onSubmitEditing: EMAIL_INPUT,
        visible: true,
        isRequired: false,
        errors: this.props.fieldErrors && this.props.fieldErrors.secondReferencePhone,
        defaultValue: this.props.lead ? this.props.lead.secondReferencePhone : '',
      },
      {
        id: EMAIL_INPUT,
        type: 'editableInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Correo',
        placeholder: 'Escriba aquí',
        minLength: 1,
        maxLength: 150,
        keyboardType: 'email-address',
        returnKeyType: 'next',
        editable: true,
        onSubmitEditing: NOTE_INPUT,
        visible: true,
        isRequired: false,
        errors: this.props.fieldErrors && this.props.fieldErrors.email,
        defaultValue: this.props.lead ? this.props.lead.email : '',
      },
      {
        id: SOURCE_INPUT,
        type: 'dropdown',
        colorPickerAndroid: pickerColor,
        labelColor: pickerLabelColor,
        pickerBackgroundColor: pickerBackgroundColor,
        label: 'Origen(*)',
        items: this.props.configurationData.sourceInputItems.map((item) => item.name),
        onItemSelected: this._onSourceChanged,
        visible: true,
        defaultValue: this.props.lead && this.props.lead.source ? this.props.lead.source.name : '',
      },
      {
        id: CITY_INPUT,
        type: 'dropdown',
        colorPickerAndroid: pickerColor,
        labelColor: pickerLabelColor,
        pickerBackgroundColor: pickerBackgroundColor,
        label: 'Ciudad(*)',
        items: this.props.configurationData.cityInputItems.map((item) => item.name),
        onItemSelected: this._onCityChanged,
        visible: this.props.lead ? false : true,
      },
      {
        id: CHANEL_INPUT,
        type: 'dropdown',
        colorPickerAndroid: pickerColor,
        labelColor: pickerLabelColor,
        pickerBackgroundColor: pickerBackgroundColor,
        label: 'Canal(*)',
        items: this.props.configurationData.chanelInputItems.map((item) => item.name),
        onItemSelected: this._onChanelChanged,
        visible: this.props.lead ? false : true,
      },
      {
        id: NOTE_INPUT,
        type: 'multilineEditableInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Observación',
        placeholder: 'Escriba aquí',
        multiline: true,
        minLength: 1,
        maxLength: 500,
        keyboardType: 'default',
        returnKeyType: 'done',
        editable: true,
        visible: true,
        isRequired: false,
        errors: this.props.fieldErrors && this.props.fieldErrors.note,
        defaultValue: this.props.lead ? this.props.lead.note : '',
      },
    ]
  }
}

InformationForm.PropTypes = {
  onValidatedForm: PropTypes.func.isRequired,//Escuchador que se dispara cuando se valida el formulario,
                                             //es decir se invoca al metodo validateForm del componente FormWithAllInputs
  showProgressScreen: PropTypes.bool.isRequired,//Bandera para saber si mostrar o no el ProgressScreen
  configurationData: PropTypes.object,//Datos de configuración del form
  fieldErrors: PropTypes.object,//Objeto que tiene los errores que se mostraran en los inputs del form
  lead: PropTypes.object,//Objeto que contiene datos de un lead a ser editado
};

export default InformationForm;