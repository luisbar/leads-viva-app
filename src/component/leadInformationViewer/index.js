/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import { Content, Title, Text } from 'native-base';
import { View } from 'react-native';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import ProgressScreen from '../progressScreen/index.js';
import {
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
} from '../../color.js';
/**
 * Renderiza la información acerca de un lead
 */
class LeadInformationViewer extends PureComponent {
  
  constructor(props) {
    super(props);
    //Se bindea aqui para mejorar el performance
    this._renderItem = this.renderItem.bind(this);
    //Variable para saber cual estilo utilizar al renderizar un item
    this.darkBackground;
  }

  render() {
    
    return (
      <Content>
        {
          this.props.showProgressScreen
          ? this.renderProgressScreen()
          : this.renderContent()
        }
      </Content>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {
    
    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbarAndTabBar={true}/>
    );
  }
  /**
   * Renderiza el contenido principal
   */
  renderContent() {
    
    return (
      this.props.leadInformation &&
      <View>
        {this.renderItem('Nro. lead', this.props.leadInformation.leadNumber)}
        {this.renderItem('Nombre empresa', this.props.leadInformation.enterpriseName)}
        {this.renderItem('NIT', this.props.leadInformation.nit)}
        {this.renderItem('Persona de contacto', this.props.leadInformation.owner)}
        {this.renderItem('Nombre', this.props.leadInformation.name)}
        {this.renderItem('Cedula de identidad', this.props.leadInformation.identityNumber
          ? `${this.props.leadInformation.identityNumber} ${this.props.leadInformation.extension}`
          : null)}
        {this.renderItem('Cedula extranjero', this.props.leadInformation.foreignIdentityNumber)}
        {this.renderItem('Nacionalidad', this.props.leadInformation.nationality)}
        {this.renderItem('Teléfono de contacto', this.props.leadInformation.phone)}
        {this.renderItem('Correo', this.props.leadInformation.email)}
        {this.renderItem('Número de referencia 1', this.props.leadInformation.firstReferencePhone)}
        {this.renderItem('Número de referencia 2', this.props.leadInformation.secondReferencePhone)}
        {this.renderItem('Ciudad', this.props.leadInformation.city)}
        {this.renderItem('Canal', this.props.leadInformation.chanel)}
        {this.renderItem('Estado', this.props.leadInformation.status)}
        {this.renderItem('Numero de proceso de venta', this.props.leadInformation.numberOfPurchaseProcess)}
        {this.renderItem('Dirección', this.props.leadInformation.address)}
        {this.renderItem('Observación', this.props.leadInformation.note)}
        {this.renderItem('Creado por', this.props.leadInformation.createdBy)}
        {this.renderItem('Origen', this.props.leadInformation.source)}
        {this.renderItem('Canal de creación', this.props.leadInformation.creationChanel)}
      </View>
    );
  }
  /**
   * Renderiza los datos acerca de un lead
   */
  renderItem(label, data) {
    //Variable para saber cual estilo utilizar al renderizar un item
    this.darkBackground = data ? !this.darkBackground : this.darkBackground;
    
    return (
      data
      ? <View
          style={this.darkBackground ? style.listItemWithDarkBackground : style.listItem}>
          
          <Title
            style={style.listItemLeftSideText}
            numberOfLines={2}>
            {label}
          </Title>
          
          <Text
            style={style.listItemRightSideText}>
            {data}
          </Text>
        </View>
      : null
    );
  }
}

LeadInformationViewer.propTypes = {
  leadInformation: PropTypes.object,//Información acerca de un lead
  showProgressScreen: PropTypes.bool.isRequired,//Bandera para saber si mostrar o no el progress screen
};

export default LeadInformationViewer;