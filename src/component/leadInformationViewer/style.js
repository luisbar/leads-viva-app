/******************
 * Project import *
 ******************/
import {
  leadInformationViewerItemBackground,
  leadInformationViewerItemLeftSideText,
  leadInformationViewerItemRightSideText,
} from '../../color.js';
 
export default {

  listItem: {
    flexDirection: 'row',
    padding: 10,
  },
  listItemWithDarkBackground: {
    backgroundColor: leadInformationViewerItemBackground,
    flexDirection: 'row',
    padding: 10,
  },
  listItemLeftSideText: {
    color: leadInformationViewerItemLeftSideText,
    flex: 1,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  listItemRightSideText: {
    color: leadInformationViewerItemRightSideText,
    flex: 1,
    textAlign: 'left',
    textAlignVertical: 'center',
  },
}