/***********************
 * Node modules import *
 ***********************/
import React from 'react';

import { Container, StyleProvider, Tabs } from 'native-base';
import { MenuProvider } from 'react-native-popup-menu';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import BasisComponent from '../../scene/basisComponent.js';
import NavigationBar from '../navigationBar/index.js';
import style from './style.js';
import { toolbarButtonColor } from '../../color.js';
import Router from '../../router.js';
/**
 * Renderiza el view pager
 */
class ViewPager extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onPageSelected = this.onPageSelected.bind(this);
    this._pop = this.pop.bind(this);
    //State
    this.state = {
      rightButtons: [{
        onPressRightButton: this.props.onSave,
        iconRightButton: 'md-checkmark',
        colorIconRightButton: toolbarButtonColor,
      }],
    };
  }

  render() {

    return (
      <MenuProvider>
        <StyleProvider style={getTheme(material)}>
           <Container>
             <NavigationBar
               toolBarStyle={style.toolbar}
               leftButtonVisible={true}
               onPressLeftButton={this._pop}
               iconLeftButton={'md-arrow-back'}
               colorIconLeftButton={toolbarButtonColor}
               title={this.props.title}
               titleStyle={style.title}
               rightButtonsVisible={this.props.showRightButtons}
               rightButtons={this.state.rightButtons}/>
              <Tabs
                ref={'pager'}
                locked={true}
                initialPage={0}
                tabBarPosition={'bottom'}
                tabBarUnderlineStyle={style.tabBarUnderlineStyle}
                onChangeTab={this._onPageSelected}
                prerenderingSiblingsNumber={3}>
                {this.props.children}
              </Tabs>
           </Container>
        </StyleProvider>
      </MenuProvider>
    );
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona
   * un tab diferente al actual
   */
  onPageSelected({i}) {
    //Si esta en el tab de mapa y no hay ningun product restringido agregado a la lista de productos
    //, muestra el boton para abrir la vista para buscar una dirección
    if (i === 2 && this.props.showButtonForOpenPlaceView)
      this.setState({
        rightButtons: [
          {
            onPressRightButton: this.props.onOpenPlaceView,
            iconRightButton: 'md-search',
            colorIconRightButton: toolbarButtonColor,
          },
          {
            onPressRightButton: this.props.onSave,
            iconRightButton: 'md-checkmark',
            colorIconRightButton: toolbarButtonColor,
          },
        ]
      });
    else
      this.setState({
        rightButtons: [{
            onPressRightButton: this.props.onSave,
            iconRightButton: 'md-checkmark',
            colorIconRightButton: toolbarButtonColor,
        }]
      });
  }
  /**
   * Metodo que va a la pagina indicada por parametro
   */
  setPage(page) {
    this.refs.pager.goToPage(page);
  }
  /**
   * Va hacia la vista de atras
   */
  pop() {
    //Remueve la vista actual de la pila
    Router.getInstance().navigator.pop();
  }
}

ViewPager.PropTypes = {
  title: PropTypes.string,//Titulo que aparece en el NavigationBar
  onSave: PropTypes.func.isRequired,//Escuchador que se dispara cuando se presiona el boton para guardar los datos de las tres paginas
  onOpenPlaceView: PropTypes.func.isRequired,//Escuchador que se dispara cuando se presiona el boton para buscar una dirección
  showRightButtons: PropTypes.bool,//Bandera para saber si se muestra o no el boton para buscar una dirección con google place
  showButtonForOpenPlaceView: PropTypes.bool,//Bandera para saber si se muestra el boton para abrir el componente place
  leftButtonIconName: PropTypes.string,//Nombre del icono que se mostrara al lado izquierdo del toolbar, en base
                                       //a ese nombre se elegirá la acción;
};

export default ViewPager;