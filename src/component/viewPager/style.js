/******************
 * Project import *
 ******************/
import {
  toolbarBackgroundColor,
  toolbarBorderColor,
  toolbarTextColor,
  tabUnderlineColor,
} from '../../color.js';

export default {
  
  toolbar: {
    backgroundColor: toolbarBackgroundColor,
    borderColor: toolbarBorderColor,
  },
  title: {
    color: toolbarTextColor,
  },
  tabBarUnderlineStyle: {
    backgroundColor: tabUnderlineColor,
  },
}