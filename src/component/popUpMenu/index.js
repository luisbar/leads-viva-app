/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import { Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu';
import { View } from 'react-native';
import { Text } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
/**
 * It renders a popUpMenu, its parent must use MenuProvider
 * tag
 */
class PopUpMenu extends PureComponent {
  
  constructor(props) {
    super(props);
  
    //To bind the listeners here for improving the performance
    this._renderItem = this.renderItem.bind(this);
  }

  render() {

    return (
      <Menu>
        <MenuTrigger>
          <Icon
            name={'dots-vertical'}
            size={30}
            color={this.props.menuIconColor}/>
        </MenuTrigger>

        <MenuOptions
          customStyles={style(this.props).menuContainer}>
          {this.props.items.map(this._renderItem)}
        </MenuOptions>
      </Menu>
    );
  }
  /**
   * It renders items of the popUpMenu
   */
  renderItem(item, index) {

    return (
      <MenuOption
        key={index}
        onSelect={item.itemPressed}>

        <View style={style(this.props).itemContainer}>
          <Icon name={item.icon} size={25} color={item.iconColor}/>
          <Text numberOfLines={1} style={style(item).itemText}>{item.text}</Text>
        </View>
      </MenuOption>
    );
  }
}

PopUpMenu.propTypes = {
  menuIconColor: PropTypes.string,//Icon color of the icon for opening the menu
  menuContainerStyle: PropTypes.object,//Object for styling the menu container
  items: PropTypes.array,//Items array in order to populate the menu with options
};

PopUpMenu.defaultProps = {
  menuIconColor: '#EFADEF',
  menuContainerStyle: {
    borderRadius: 10,
    backgroundColor: '#89362A',
  },
  items: [
    {
      itemPressed: () => alert(1),
      icon: 'eye',
      iconColor: '#FFFFFF',
      text: 'Ver lead',
      textColor: '#FFFFFF',
    },
    {
      itemPressed: () => alert(2),
      icon: 'pencil-box-outline',
      iconColor: '#FFFFFF',
      text: 'Editar lead',
      textColor: '#FFFFFF',
    },
  ]
}

export default PopUpMenu;
