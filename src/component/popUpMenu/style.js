export default function (props) {

  return {
    menuContainer: {
      optionsContainer: props.menuContainerStyle,
    },
    itemContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center',
    },
    itemText: {
      color: props.textColor,
      marginLeft: 5,
      fontFamily: 'Museo300',
    },
  };
}
