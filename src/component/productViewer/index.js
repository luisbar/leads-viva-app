/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import { Content, Title, Text } from 'native-base';
import { FlatList, View } from 'react-native';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import ProgressScreen from '../progressScreen/index.js';
import {
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
} from '../../color.js';
/**
 * Renderiza los productos de un lead
 */
class ProductViewer extends PureComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._renderItem = this.renderItem.bind(this);
  }

  render() {

    return (
      <Content>
        {
          this.props.showProgressScreen
          ? this.renderProgressScreen()
          : this.renderProductList()
        }
      </Content>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {
    
    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbarAndTabBar={true}/>
    );
  }
  /**
   * Renderiza la lista de productos
   */
  renderProductList() {
    
    return this.props.products &&
    <FlatList
      style={style.list}
      data={this.props.products}
      ListHeaderComponent={this.renderHeader()}
      renderItem={this._renderItem}
      ListFooterComponent={this.renderFooter()}/>
  }
  /**
   * Renderiza los items de la lista de productos
   */
  renderItem({item, index}) {
    
    return (
      <View
        style={style.listItem}
        key={index}>
        {this.renderItemHeader(item.productType)}
        {this.renderSubItem('Tipo de compra', item.purchaseType)}
        {this.renderSubItem('Plan', item.plan)}
        {this.renderSubItem('Cantidad', item.quantity)}
        {this.renderSubItem('Datos', item.data)}
        {this.renderSubItem('Minutos', item.minut)}
        {this.renderSubItem('Crédito', item.credit)}
        {this.renderSubItem('Grupo amigo', item.friendGroup)}
        {this.renderSubItem('Tipo', item.type)}
        {this.renderSubItem('Linea', item.line)}
        {this.renderSubItem('IMEI', item.imei)}
        {this.renderSubItem('Enodeb', item.enodeb)}
        {this.renderSubItem('Procedimiento', item.procedure)}
        {this.renderSubItem('Servicio', item.service)}
      </View>
    );
  }
  /**
   * Renderiza el header de cada item de la lista
   * de productos
   */
  renderItemHeader(label) {
    
    return (
      <View style={style.headerOfItemOfList}>
        <Title style={style.textOfheaderOfItemOfList}>
          {label.toUpperCase()}
        </Title>
      </View>
    );
  }
  /**
   * Renderiza los sub items de la lista de productos
   * @param  {string} label label del subitem
   * @param  {string} data dato a mostrar acerca del producto
   */
  renderSubItem(label, data) {
    
    return (
      data
      ? <View>
          {this.renderItemContentSeparator()}
          <View style={style.subItemOfList}>
            <View style={style.leftSubItemOfList}>
              <Title
                style={style.textOfLeftSubItemOfList}
                numberOfLines={2}>
                {label}
              </Title>
            </View>
            
            <View style={style.rightSubItemOfList}>
              <Text style={style.textOfRightSubItemOfList}>{data}</Text>
            </View>
          </View>
        </View>
      : null
    );
  }
  /**
   * Renderiza el separador de cada subitem de la lista
   * de productos
   */
  renderItemContentSeparator() {
    
    return (
      <View style={style.separatoraOfSubItemOfList}/>
    );
  }
  /**
   * Renderiza el header de la lista de productos
   */
  renderHeader() {
    
    return (
      <View style={style.listHeader}/>
    );
  }
  /**
   * Renderiza el footer de la lista de productos
   */
  renderFooter() {
    
    return (
      <View style={style.listFooter}/>
    );
  }
}

ProductViewer.propTypes = {
  products: PropTypes.array,//Productos de un lead
  showProgressScreen: PropTypes.bool.isRequired,//Bandera para saber si mostrar o no el progress screen
};

export default ProductViewer;