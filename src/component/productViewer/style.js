/******************
 * Project import *
 ******************/
import {
  productListItemBackground,
  productLeftSubItemOfList,
  productTextOfLeftSubItemOfList,
  productRightSubItemOfList,
  productTextOfRightSubItemOfList,
  productTextOfheaderOfItemOfList,
  productSeparatoraOfSubItemOfList,
} from '../../color.js';

export default {
  
  list: {
    flex: 1,
    paddingRight: 5,
    paddingLeft: 5,
  },
  listHeader: {
    marginTop: 5,
  },
  listFooter: {
    marginBottom: 5,
  },
  listItem: {
    flexDirection: 'column',
    elevation: 3,
    borderRadius: 20,
    backgroundColor: productListItemBackground,
    margin: 5,
    overflow: 'hidden',
  },
  subItemOfList: {
    flexDirection: 'row',
  },
  leftSubItemOfList: {
    backgroundColor: productLeftSubItemOfList,
    justifyContent: 'center',
    flex: 1,
  },
  textOfLeftSubItemOfList: {
    color: productTextOfLeftSubItemOfList,
    padding: 10,
  },
  rightSubItemOfList: {
    backgroundColor: productRightSubItemOfList,
    justifyContent: 'center',
    flex: 1.3,
  },
  textOfRightSubItemOfList: {
    color: productTextOfRightSubItemOfList,
    padding: 10,
  },
  headerOfItemOfList: {
    height: 50,
    justifyContent: 'center',
  },
  textOfheaderOfItemOfList: {
    color: productTextOfheaderOfItemOfList,
  },
  separatoraOfSubItemOfList: {
    height: 1,
    backgroundColor: productSeparatoraOfSubItemOfList,
  },
}