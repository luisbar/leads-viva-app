/******************
 * Project import *
 ******************/
import { progressModalBackground, progressModalText } from '../../color.js';

export default {
  
  container: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    width: '80%',
    height: 150,
    margin: 10,
    backgroundColor: progressModalBackground,
    borderRadius: 10,
    justifyContent: 'center',
    elevation: 3,
  },
  text: {
    color: progressModalText,
    textAlign: 'center',
  },
};