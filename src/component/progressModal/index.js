/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import { Container, Text, Spinner, StyleProvider } from 'native-base';
import { View } from 'react-native';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import style from './style.js';
import { progressModalSpinner } from '../../color.js';
/**
 * Renderiza un modal de progreso, para mostrar
 * cuando se esta realizando un proceso
 * largo
 */
class ProgressModal extends PureComponent {

  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container style={style.container}>
          <View style={style.content}>
            <Spinner color={progressModalSpinner}/>
            <Text style={style.text}>{this.props.text}</Text>
          </View>
        </Container>
      </StyleProvider>
    );
  }
}

PropTypes.propTypes = {
  text: PropTypes.string,//Texto a mostrar
};

PropTypes.defaultProps = {
  text: 'Cargando...'
};

export default ProgressModal;