export default {
  
  form: {
    margin: 0,
  },
  switchContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  labelSwitch: {
    color: '#FFFFFF',
    marginLeft: 15,
    marginRight: 15,
  },
  clearDateButton: {
    position: 'absolute',
    right: 15,
    top: 25,
  },
  clearDateButtonIcon: {
    color: '#5C349E',
  },
  text: {
    color: '#5C349E',
    textAlign: 'center',
    marginHorizontal: 10,
    marginVertical: 5,
  },
  title: {
    color: '#5C349E',
    textAlign: 'center',
    marginHorizontal: 10,
    marginVertical: 5,
  },
};