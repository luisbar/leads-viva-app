/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import { Form, Text, Button, Icon, Title } from 'native-base';
import { View, Keyboard } from 'react-native';
import DatePicker from 'react-native-modal-datetime-picker';
import Switch from 'react-native-switch-pro';

import moment from 'moment';
import 'moment/locale/es';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import InputWithValidator from '../inputWithValidator/index.js';
import RadioGroup from '../radioGroup/index.js';
import Dropdown from '../dropdown/index.js';
import style from './style.js';
/*************
 * Constants *
 *************/
const NON_EDITABLE_INPUT = 'nonEditableInput';
const EDITABLE_INPUT = 'editableInput';
const MULTILINE_EDITABLE_INPUT = 'multilineEditableInput';
const PASSWORD_INPUT = 'passwordInput';
const DATE_INPUT = 'dateInput';
const TIME_INPUT = 'timeInput';
const DROPDOWN = 'dropdown';
const SWITCH = 'switch';
const RADIO = 'radio';
const TITLE = 'title';
const TEXT = 'text';
/**
 * It renders a form, and the form can render all the next types of input:
 * NON_EDITABLE_INPUT
 * EDITABLE_INPUT
 * MULTILINE_EDITABLE_INPUT
 * PASSWORD_INPUT
 * DATE_INPUT
 * DROPDOWN
 * SWITCH
 * RADIO
 * TITLE
 * TEXT
 */
class FormWithAllInputs extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      datePickerVisible: false,//Flag in order to show the DatePicker
      dateInputPressed: null,//Id of the dateInput pressed in order to set it the date selected from DatePicker
      timePickerVisible: false,//Flag in order to show the TimePicker
      timeInputPressed: null,//Id of the timeInput pressed in order to set it the time selected from TimePicker
      inputValues: this.getInputValues(this.props.fields),//Object for saving the state and value of all inputs
    };
    //To bind the listeners here for improving the performance
    this._onHandleInputValue = this.updateValue.bind(this);
    this._onConfirmDate = this.onConfirmDate.bind(this);
    this._onHideDatePicker = this.onHideDatePicker.bind(this);
    this._onShowDatePicker = (field) => this.onShowDatePicker.bind(this, field);
    this._onConfirmTime = this.onConfirmTime.bind(this);
    this._onHideTimePicker = this.onHideTimePicker.bind(this);
    this._onShowTimePicker = (field) => this.onShowTimePicker.bind(this, field);
    this._onItemSelected = (field) => this.onItemSelected.bind(this, field);
    this._onSwitchPressed = (field) => this.onSwitchPressed.bind(this, field);
    this._onClearDateInput = (fieldId) => this.onClearDateInput.bind(this, fieldId);
    this._onItemPressed = (field) => this.onItemPressed.bind(this, field);
  }
  
  componentWillReceiveProps(nextProps) {
    //To update the inputValues object in the state
    this.updateInputValues(nextProps.fields);
  }

  render() {

    return (
      <Form
        style={style.form}>
        {
          this.props.fields.map((field) => field.visible && this.renderField(field))//If visible prop is true, so, the field is rendered
        }
        <DatePicker
          cancelTextIOS={this.props.cancelTextIOS}
          cancelTextStyle={this.props.cancelTextStyle}
          confirmTextIOS={this.props.confirmTextIOS}
          confirmTextStyle={this.props.confirmTextStyle}
          titleIOS={this.props.titleIOS}
          isVisible={this.state.datePickerVisible}
          onConfirm={this._onConfirmDate}
          minimumDate={this.props.minimumDate}
          onCancel={this._onHideDatePicker}/>
          
        <DatePicker
          cancelTextIOS={this.props.cancelTextIOS}
          cancelTextStyle={this.props.cancelTextStyle}
          confirmTextIOS={this.props.confirmTextIOS}
          confirmTextStyle={this.props.confirmTextStyle}
          titleIOS={this.props.titleIOS}
          isVisible={this.state.timePickerVisible}
          onConfirm={this._onConfirmTime}
          minimumDate={this.props.minimumDate}
          onCancel={this._onHideTimePicker}
          mode={'time'}
          is24Hour={false}/>
      </Form>
    );
  }
  /**
   * It renders a field according its type
   */
  renderField(field) {
    switch (field.type) {

      case NON_EDITABLE_INPUT:
        return (
          <InputWithValidator
            key={field.id}
            id={field.id}
            ref={field.id}
            type={field.type}
            isRequired={field.isRequired}
            value={field.value}
            inputTextColor={field.inputTextColor}
            labelTextColor={field.labelTextColor}
            placeholderTextColor={field.placeholderTextColor}
            backgroundInputColor={field.backgroundInputColor}
            label={field.label}
            minLength={field.minLength}
            maxLength={field.maxLength}
            editable={field.editable}
            autoCapitalize={field.autoCapitalize}
            onHandleInputValue={this._onHandleInputValue}/>
        );

      case EDITABLE_INPUT:
        return (
          <InputWithValidator
            key={field.id}
            id={field.id}
            ref={field.id}
            isRequired={field.isRequired}
            value={this.state.inputValues[field.id].value}
            inputTextColor={field.inputTextColor}
            labelTextColor={field.labelTextColor}
            placeholderTextColor={field.placeholderTextColor}
            backgroundInputColor={field.backgroundInputColor}
            label={field.label}
            placeholder={field.placeholder}
            minLength={field.minLength}
            maxLength={field.maxLength}
            keyboardType={field.keyboardType}
            returnKeyType={field.returnKeyType}
            autoFocus={field.autoFocus}
            editable={field.editable}
            autoCapitalize={field.autoCapitalize}
            errors={field.errors}
            onSubmitEditing={field.onSubmitEditing
              ? () => this.refs[field.onSubmitEditing].refs.input._root.focus()
              : Keyboard.dismiss}
            onHandleInputValue={this._onHandleInputValue}/>
        );

      case PASSWORD_INPUT:
        return (
          <InputWithValidator
            key={field.id}
            id={field.id}
            ref={field.id}
            isRequired={field.isRequired}
            inputTextColor={field.inputTextColor}
            labelTextColor={field.labelTextColor}
            placeholderTextColor={field.placeholderTextColor}
            backgroundInputColor={field.backgroundInputColor}
            label={field.label}
            placeholder={field.placeholder}
            minLength={field.minLength}
            maxLength={field.maxLength}
            keyboardType={field.keyboardType}
            returnKeyType={field.returnKeyType}
            editable={field.editable}
            autoCapitalize={field.autoCapitalize}
            errors={field.errors}
            secureTextEntry={field.secureTextEntry}
            onSubmitEditing={field.onSubmitEditing
              ? () => this.refs[field.onSubmitEditing].refs.input._root.focus()
              : Keyboard.dismiss}
            onHandleInputValue={this._onHandleInputValue}/>
        );

      case MULTILINE_EDITABLE_INPUT:
        return (
          <InputWithValidator
            key={field.id}
            id={field.id}
            ref={field.id}
            isRequired={field.isRequired}
            value={this.state.inputValues[field.id].value}
            inputTextColor={field.inputTextColor}
            labelTextColor={field.labelTextColor}
            placeholderTextColor={field.placeholderTextColor}
            backgroundInputColor={field.backgroundInputColor}
            label={field.label}
            placeholder={field.placeholder}
            multiline={field.multiline}
            minLength={field.minLength}
            maxLength={field.maxLength}
            keyboardType={field.keyboardType}
            returnKeyType={field.returnKeyType}
            editable={field.editable}
            autoCapitalize={field.autoCapitalize}
            errors={field.errors}
            blurOnSubmit={field.blurOnSubmit}
            onSubmitEditing={field.onSubmitEditing
              ? () => this.refs[field.onSubmitEditing].refs.input._root.focus()
              : Keyboard.dismiss}
            onHandleInputValue={this._onHandleInputValue}/>
        );

      case DATE_INPUT:
        return (
          <View key={field.id}>
            <InputWithValidator
              key={field.id}
              id={field.id}
              ref={field.id}
              isRequired={field.isRequired}
              inputTextColor={field.inputTextColor}
              labelTextColor={field.labelTextColor}
              placeholderTextColor={field.placeholderTextColor}
              backgroundInputColor={field.backgroundInputColor}
              label={field.label}
              placeholder={field.placeholder}
              minLength={field.minLength}
              maxLength={field.maxLength}
              editable={field.editable}
              autoCapitalize={field.autoCapitalize}
              errors={field.errors}
              value={this.state.inputValues[field.id].value}
              onPress={this._onShowDatePicker(field)}
              onHandleInputValue={this._onHandleInputValue}/>
              
            <Button
              transparent
              style={style.clearDateButton}
              onPress={this._onClearDateInput(field.id)}>
              <Icon
                size={30}
                name={'md-close'}
                style={style.clearDateButtonIcon}/>
            </Button>
          </View>
        );
        
      case TIME_INPUT:
        return (
          <InputWithValidator
            key={field.id}
            id={field.id}
            ref={field.id}
            isRequired={field.isRequired}
            inputTextColor={field.inputTextColor}
            labelTextColor={field.labelTextColor}
            placeholderTextColor={field.placeholderTextColor}
            backgroundInputColor={field.backgroundInputColor}
            label={field.label}
            placeholder={field.placeholder}
            minLength={field.minLength}
            maxLength={field.maxLength}
            editable={field.editable}
            autoCapitalize={field.autoCapitalize}
            errors={field.errors}
            value={this.state.inputValues[field.id].value}
            onPress={this._onShowTimePicker(field)}
            onHandleInputValue={this._onHandleInputValue}/>
        );

      case DROPDOWN:
        return (
          <Dropdown
            key={field.id}
            ref={field.id}
            colorPickerAndroid={field.colorPickerAndroid}
            pickerBackgroundColor={field.pickerBackgroundColor}
            labelColor={field.labelColor}
            iconColor={field.iconColor}
            titleColor={field.titleColor}
            label={field.label}
            iosTextHeader={field.iosTextHeader}
            itemSelected={this.state.inputValues[field.id].value}
            items={field.items}
            onValueChange={this._onItemSelected(field)}
            errors={field.errors}
            errorColor={field.errorColor}/>
        );

      case SWITCH:
        return (
          <View
            key={field.id}
            style={style.switchContainer}>

            <Text style={style.labelSwitch}>{field.label}</Text>

            <Switch
              ref={field.id}
              value={this.state.inputValues[field.id].value}
              defaultValue={field.defaultValue}
              disabled={field.disabled}
              circleColorActive={field.circleColorActive}
              circleColorInactive={field.circleColorInactive}
              backgroundActive={field.backgroundActive}
              backgroundInactive={field.backgroundInactive}
              onSyncPress={this._onSwitchPressed(field)}/>
          </View>
        );
        
      case RADIO:
        return (
          <RadioGroup
            items={field.items}
            defaultValue={field.value}
            title={field.title}
            onItemPressed={this._onItemPressed(field)}
            errors={field.errors}
            errorColor={field.errorColor}/>
        );
        
      case TEXT:
        return (
          <Text style={style.text}>{field.text}</Text>
        );
        
      case TITLE:
        return (
          <Title style={style.title}>{field.title}</Title>
        );
    }
  }
  /**
   * It returns an inputValues object for initialize
   * the state
   */
  getInputValues(fields) {
    let inputValues = {};

    //Add an object with state and value properties for each input
    //into inputValues
    fields.map((field) => inputValues[field.id] = {
      state: field.type == NON_EDITABLE_INPUT ||
       field.type == DROPDOWN ||
       field.type == SWITCH ||
       field.type == DATE_INPUT ||
       field.type == TIME_INPUT ||
       (field.type == RADIO && field.defaultValue) ||
       (field.type ==  MULTILINE_EDITABLE_INPUT && field.defaultValue) ||
       (field.type ==  EDITABLE_INPUT && field.defaultValue)//These fields has a default value
          ? true
          : false,
      value: field.type == SWITCH ||
        field.type == EDITABLE_INPUT ||
        field.type == MULTILINE_EDITABLE_INPUT ||
        field.type == DATE_INPUT ||
        field.type == TIME_INPUT ||
        field.type == RADIO
          ? field.defaultValue
          : field.type == DROPDOWN && !field.defaultValue
          ? field.items[0]
          : field.type == DROPDOWN && field.defaultValue
          ? field.defaultValue
          : field.value,//For non editable input
    });
    
    return inputValues;
  }
  /**
   * It adds new inputs to inputValues object and mantain
   * the old inputs
   */
  updateInputValues(fields) {
    let inputValues = {};

    fields.map((field) => {
      if (this.state.inputValues[field.id])//If there is the field 
        inputValues[field.id] = this.state.inputValues[field.id]
      else
        inputValues[field.id] = {
          state: field.type == NON_EDITABLE_INPUT ||
           field.type == DROPDOWN ||
           field.type == SWITCH ||
           field.type == DATE_INPUT ||
           field.type == TIME_INPUT ||
           field.type == RADIO ||
           (field.type ==  MULTILINE_EDITABLE_INPUT && field.defaultValue) ||
           (field.type ==  EDITABLE_INPUT && field.defaultValue)//These fields has a default value
              ? true
              : false,
          value: field.type == SWITCH ||
            field.type == EDITABLE_INPUT ||
            field.type == MULTILINE_EDITABLE_INPUT ||
            field.type == DATE_INPUT ||
            field.type == TIME_INPUT ||
            field.type == RADIO
              ? field.defaultValue
              : field.type == DROPDOWN && !field.defaultValue
              ? field.items[0]
              : field.type == DROPDOWN && field.defaultValue
              ? field.defaultValue
              : field.value,//For non editable input
      };
    });
    //Update the state
    this.setState({
      inputValues: inputValues,
    });
  }
  /**
   * Listener that is triggered when an user pressed a dateInput
   */
  onShowDatePicker(field) {
    this.setState({
      datePickerVisible: true,//To show the DatePicker
      dateInputPressed: field.id,//It saves the id of the dateInput pressed
    });
  }
  /**
   * Listener that is triggered when an user pressed the cancel button of the DatePicker
   */
  onHideDatePicker() {
    this.setState({
      datePickerVisible: false,//To hide the DatePicker
    });
  }
  /**
   * Listener that is triggered when an user pressed the accept button
   * of the DatePicker
   */
  onConfirmDate(date) {
    const dateSelected = moment(date).format('DD/MM/YYYY');
    //To hide the DatePicker
    this.setState({
      datePickerVisible: false,
    });
    //To trigger the onConfirmDate
    const field = this.props.fields.filter((field) => field.id == this.state.dateInputPressed )[0];//To get the field pressed
    //To update the dateInput appearance if the date input is a required input
    this.refs[field.id].handleInputValue(dateSelected);
    //It updates the state and value about the date input
    this.updateValue(field.id, true, dateSelected)
    .then(() => 
      //It triggers an event on the view that uses the date picker
      field.onConfirmDate(dateSelected)
    );
  }
  /**
   * Listener that is triggered when an user pressed a timeInput
   */
  onShowTimePicker(field) {
    this.setState({
      timePickerVisible: true,//To show the TimePicker
      timeInputPressed: field.id,//It saves the id of the timeInput pressed
    });
  }
  /**
   * Listener that is triggered when an user pressed the cancel button of the TimePicker
   */
  onHideTimePicker() {
    this.setState({
      timePickerVisible: false,//To hide the TimePicker
    });
  }
  /**
   * Listener that is triggered when an user pressed the accept button
   * of the TimePicker
   */
  onConfirmTime(time) {
    const timeSelected = moment(time).format('h:mm a');
    //To hide the TimePicker
    this.setState({
      timePickerVisible: false,
    });
    //To trigger the onConfirmTime
    const field = this.props.fields.filter((field) => field.id == this.state.timeInputPressed )[0];//To get the field pressed
    //To update the timeInput appearance if the time input is a required input
    this.refs[field.id].handleInputValue(timeSelected);
    //It updates the state and value about the time input
    this.updateValue(field.id, true, timeSelected)
    .then(() => 
      //It triggers an event on the view that uses the time picker
      field.onConfirmTime(timeSelected)
    );
  }
  /**
   * Listener that is triggered when an user has selected an item
   * of the dropdown
   */
  onItemSelected(field, item, index) {
    //It updates the state and value about the dropdown
    this.updateValue(field.id, true, item)
    .then(() => {
      //It triggers an event on the view that uses the dropdown
      if (field.onItemSelected)
        field.onItemSelected(item, index)
    });
    
  }
  /**
   * Listener that is triggered when an user pressed
   * the switch
   */
  onSwitchPressed(field) {
    //It updates the state and value about the switch
    this.updateValue(field.id, true, !this.state.inputValues[field.id].value)//To save the value about the switch pressed in the inputValues
    .then(() =>
      //It triggers an event on the view that uses the switch
      field.onSwitchPressed(this.state.inputValues[field.id].value)
    );
  }
  /**
   * Listener that is triggered when an user
   * pressed the button for clearing the date input
   */
  onClearDateInput(fieldId) {
    //To clean the date input
    this.refs[fieldId].refs.input._root.clear();
    //To update the value of dateInput on state object
    this.setState({
      [this.state.inputValues[fieldId]]: Object.assign(this.state.inputValues[fieldId], {
        value: undefined,
      })
    });
  }
  /**
   * Listener that is triggered when an user pressed a radio button
   * from the radioGroup
   */
  onItemPressed(field, radioButtonId) {
    //It updates the state and value about the radioGroup
    this.updateValue(field.id, true, radioButtonId)
    .then(() => {
      //It triggers an event on the view that uses the radioGroup
      if (field.onItemPressed)
        field.onItemPressed(radioButtonId);
    });
  }
  /**
   * It updates the value of each input in the inputValues object
   */
  updateValue(id, state, value) {

    return new Promise((resolve, reject) => {
      this.setState({
        inputValues: Object.assign({}, this.state.inputValues, {
          [id]: {
            state: state,
            value: value,
          },
        }),
      }, resolve);
    })
  }
  /**
   * It validates all inputs and triggers the onEmptyFields listener when there is
   * an empty field or more, and the onFieldsOk when all inputs are ok
   */
  validateForm() {
    if (this.thereIsAnEmptyField())
      this.props.onEmptyFields(this.state.inputValues);
    else
      this.props.onFieldsOk(this.state.inputValues);
  }
  /**
   * It checks if a required field is empty
   */
  thereIsAnEmptyField() {
    let flag;

    Object.keys(this.state.inputValues).map((key) => {
      if (!flag && !this.state.inputValues[key].state)//If true, a field is empty
        flag = true;

      if (this.refs[key] && this.refs[key].handleInputValue)//To update the input appearance
        this.refs[key].handleInputValue(this.state.inputValues[key].value);
    });

    return flag;
  }
}

FormWithAllInputs.propTypes = {
  fields: PropTypes.array.isRequired,//Array that contains all fields to be renderized
  onEmptyFields: PropTypes.func.isRequired,//Listener that is triggered when there is an empty input
  onFieldsOk: PropTypes.func.isRequired,//Listener that is triggered when all inputs are ok
  cancelTextIOS: PropTypes.string,//Label for the cancel button of the DatePicker
  cancelTextStyle: PropTypes.object,//Style for the cancel button of the DatePicker
  confirmTextIOS: PropTypes.string,//Label for the confirm button of the DatePicker
  confirmTextStyle: PropTypes.object,//Style for the cancel button of the DatePicker
  titleIOS: PropTypes.string,//Title for the DatePicker
  minimumDate: PropTypes.object,//Minimun date to be showed by the DatePicker
};

FormWithAllInputs.defaultProps = {
  fields: [
    {
      id: 'nonEditableInput',
      type: 'nonEditableInput',
      isRequired: false,
      value: 'dato',
      inputTextColor: '#000000',
      labelTextColor: '#000000',
      placeholderTextColor: '#BEBEBE',
      label: 'Input no editable:',
      minLength: 1,
      maxLength: 10,
      editable: false,
      visible: true,
    },
    {
      id: 'editableInput',
      type: 'editableInput',
      inputTextColor: '#000000',
      labelTextColor: '#000000',
      placeholderTextColor: '#BEBEBE',
      label: 'Editable input:',
      placeholder: 'Escriba aquí',
      minLength: 1,
      maxLength: 10,
      keyboardType: 'default',
      returnKeyType: 'next',
      autoFocus: true,
      editable: true,
      onSubmitEditing: 'editableInputWithDefaultValue',
      visible: true,
    },
    {
      id: 'editableInputWithDefaultValue',
      type: 'editableInput',
      defaultValue: 'default',
      inputTextColor: '#000000',
      labelTextColor: '#000000',
      placeholderTextColor: '#BEBEBE',
      label: 'Default value:',
      placeholder: 'Escriba aquí',
      minLength: 1,
      maxLength: 10,
      keyboardType: 'default',
      returnKeyType: 'next',
      autoFocus: true,
      editable: true,
      onSubmitEditing: 'editableInputNotRequired',
      visible: true,
    },
    {
      id: 'editableInputNotRequired',
      type: 'editableInput',
      isRequired: false,
      inputTextColor: '#000000',
      labelTextColor: '#000000',
      placeholderTextColor: '#BEBEBE',
      label: 'Not required:',
      placeholder: 'Escriba aquí',
      minLength: 1,
      maxLength: 10,
      keyboardType: 'default',
      returnKeyType: 'next',
      editable: true,
      onSubmitEditing: 'passwordInput',
      visible: true,
    },
    {
      id: 'passwordInput',
      type: 'passwordInput',
      inputTextColor: '#000000',
      labelTextColor: '#000000',
      placeholderTextColor: '#BEBEBE',
      label: 'Password input:',
      placeholder: 'Escriba aquí',
      minLength: 1,
      maxLength: 10,
      keyboardType: 'default',
      returnKeyType: 'next',
      editable: true,
      secureTextEntry: true,
      onSubmitEditing: 'multilineEditableInput',
      visible: true,
    },
    {
      id: 'multilineEditableInput',
      type: 'multilineEditableInput',
      inputTextColor: '#000000',
      labelTextColor: '#000000',
      placeholderTextColor: '#BEBEBE',
      label: 'Multiline editable input:',
      placeholder: 'Escriba aquí',
      multiline: true,
      minLength: 1,
      maxLength: 10,
      keyboardType: 'default',
      returnKeyType: 'done',
      editable: true,
      visible: true,
    },
    {
      id: 'dateInput',
      type: 'dateInput',
      inputTextColor: '#000000',
      labelTextColor: '#000000',
      placeholderTextColor: '#BEBEBE',
      label: 'Date input:',
      placeholder: 'Presione aquí',
      minLength: 1,
      maxLength: 10,
      editable: false,
      onConfirmDate: (date) => console.warn(date),
      visible: true,
    },
    {
      id: 'dropdown',
      type: 'dropdown',
      colorPickerAndroid: '#000',
      labelColor: '#000',
      iconColor: '#000',
      titleColor: '#000',
      label: 'Items:',
      iosTextHeader: 'Seleccione item',
      items: ['Item 1', 'Item 2', 'Item 3'],
      onItemSelected: (item) => console.warn(item),
      visible: true,
    },
    {
      id: 'switch',
      type: 'switch',
      defaultValue: false,
      disabled: false,
      circleColorActive: '#FFFFFF',
      circleColorInactive: '#424242',
      backgroundActive: '#A000AD',
      backgroundInactive: '#BEBEBE',
      label: 'Switch:',
      onSwitchPressed: (value) => console.warn(value),
      visible: true,
    },
  ],
  cancelTextIOS: 'Cancelar',
  cancelTextStyle: { color: '#000' },
  confirmTextIOS: 'Aceptar',
  confirmTextStyle: { color: '#000' },
  titleIOS: 'Seleccione fecha',
};

export default FormWithAllInputs;
