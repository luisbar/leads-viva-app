/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Content, Button, Text } from 'native-base';
import { View } from 'react-native';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import BasisComponent from '../../scene/basisComponent.js';
import style from './style.js';
import ProgressScreen from '../progressScreen/index.js';
import {
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
} from '../../color.js';
/*************
 * Constants *
 *************/
const POST_PAID = 'postPaid';
const LTE = 'lte';
const PUBLIC_TELEPHONY = 'publicTelephony';
const VPN = 'vpn';
const BAG = 'bag';
const OTHER = 'other';
/**
 * Renderiza los productos
 */
class ProductSelection extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onProductPressed = (product) => () => this.props.onProductPressed(product);
    //Lista de productos a renderizar
    this.products = [
      { id: POST_PAID, label: 'POSTPAGO' },
      { id: LTE, label: 'LTE FIJO' },
      { id: PUBLIC_TELEPHONY, label: 'TELEFONÍA PÚBLICA' },
      { id: VPN, label: 'VPN' },
      { id: BAG, label: 'BOLSAS' },
      { id: OTHER, label: 'OTROS' },
    ];
  }

  render() {

    return (
      <Content contentContainerStyle={style.contentContainerStyle}>
        {
          this.props.showProgressScreen
          ? this.renderProgressScreen()
          : this.renderProducts()
        }
      </Content>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {
    
    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbarAndTabBar={true}/>
    );
  }
  /**
   * Renderiza la lista de productos
   */
  renderProducts() {
    
    return (
      //Si se pudo obtener los datos de configuración para el registro de leads
      this.props.showProducts &&
      <View style={style.container}>
        <View style={style.productButtonsContainer}>
        {
          this.products.map((item, index) => this.renderProductButton(item))
        }
        </View>
      </View>
    );
  }
  /**
   * Renderiza boton por cada producto
   */
  renderProductButton(product) {

    return (
      <Button
        key={product.id}
        style={this.props.errorsOnPlansAdded && this.props.errorsOnPlansAdded[product.id]
          ? style.productButtonWithError
          : this.props.products && this.props.products[product.id] && this.props.products[product.id].length !== 0
          ? style.productButtonWithData
          : style.productButton}
        onPress={this._onProductPressed(product)}>
        <Text style={style.productButtonText}>{product.label}</Text>
      </Button>
    );
  }
}

ProductSelection.propTypes = {
  onProductPressed: PropTypes.func.isRequired,//Escuchador que se dispara cuando el usuario presiona un producto
  showProgressScreen: PropTypes.bool.isRequired,//Bandera para saber si se muestra o no el ProgressScreen
  showProducts: PropTypes.bool.isRequired,//Bandera para saber si se muestra o no los productos
  errorsOnPlansAdded: PropTypes.object,//Objeto que contiene los errores por producto y de cada input de cada plan agregado
  products: PropTypes.object,//Objeto que contiene los productos agregados, se utiliza para marcar los buttons de verde cuando hay productos agregados
};

export default ProductSelection;