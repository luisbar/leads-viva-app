/******************
 * Project import *
 ******************/
import {
  productButtonBackgroundColor,
  productButtonBackgroundColorWithError,
  productButtonBackgroundColorWithData,
  productButtonTextColor
} from '../../color.js';

export default {
  
  contentContainerStyle: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  productButtonsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    marginRight: 10,
    marginLeft: 10,
  },
  productButton: {
    width: '47%',
    height: 80,
    borderRadius: 25,
    marginVertical: 5,
    backgroundColor: productButtonBackgroundColor,
    justifyContent: 'center',
  },
  productButtonWithError: {
    width: '47%',
    height: 80,
    borderRadius: 25,
    marginVertical: 5,
    backgroundColor: productButtonBackgroundColorWithError,
    justifyContent: 'center',
  },
  productButtonWithData: {
    width: '47%',
    height: 80,
    borderRadius: 25,
    marginVertical: 5,
    backgroundColor: productButtonBackgroundColorWithData,
    justifyContent: 'center',
  },
  productButtonText: {
    color: productButtonTextColor,
    textAlign: 'center',
  },
};