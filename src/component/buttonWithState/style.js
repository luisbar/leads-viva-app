export default function (props) {

  return {
    
    button: {
      backgroundColor: props.buttonBackgroundColor,
    },
    text: {
      color: props.buttonTextColor,
    },
  };
};
