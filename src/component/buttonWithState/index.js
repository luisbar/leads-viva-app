/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import { Button, Text, Spinner } from 'native-base';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style';
/**
 * It renders a button with an spinner inside of it
 */
class ButtonWithState extends PureComponent {

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <Button
        block={this.props.block}
        rounded={this.props.rounded}
        disabled={this.props.disabled}
        style={style(this.props).button}
        onPress={this.props.onPress}>

        {this.renderSpinner()}
        {
          !this.props.disabled &&
          <Text
            style={style(this.props).text}>
            {this.props.text}
          </Text>
        }
      </Button>
    );
  }
  /**
   * It renders the spinner when this.props.disabled is true
   */
  renderSpinner() {
    
    return (
        this.props.disabled && <Spinner color={this.props.spinnerColor}/>
    );
  }
}

ButtonWithState.propTypes = {
  block: PropTypes.bool,//If true, button take all width of its container
  rounded: PropTypes.bool,//To show a button with rounded corners
  disabled: PropTypes.bool,//For disabling the button
  text: PropTypes.string,//A text that appears on the button
  buttonBackgroundColor: PropTypes.string,//The background color of the button
  buttonTextColor: PropTypes.string,//The text color of the button
  spinnerColor: PropTypes.string,//The color of the spinner
  onPress: PropTypes.func,//Listener that is triggered when an user presses the button
}

ButtonWithState.defaultProps = {
  block: false,
  rounded: false,
  disabled: false,
  text: 'Button',
  buttonBackgroundColor: '#000000',
  buttonTextColor: '#FFFFFF',
  spinnerColor: '#643443',
}

export default ButtonWithState;