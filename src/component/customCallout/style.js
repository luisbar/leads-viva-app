/******************
 * Project import *
 ******************/
import {
  calloutBackgroundColor
} from '../../color.js';

export default {
  
  container: {
    flex: 1,
    minWidth: 160,
    borderRadius: 5,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: calloutBackgroundColor,
    padding: 2,
  },
  title: {
    fontWeight: 'bold',
  },
  latitudeContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  labelLatitude: {
    fontWeight: 'bold',
  },
  latitude: {
    flex: 1,
  },
  longitudeContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  labelLongitude: {
    fontWeight: 'bold',
  },
  longitude: {
    flex: 1,
  },
};