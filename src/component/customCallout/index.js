/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { Text } from 'native-base';
import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
/**
 * Renderiza un callout personalizado
 */
class CustomCallout extends PureComponent {

  render() {

    return (
      <View style={style.container}>
        <Text style={style.title}>{'Información'}</Text>
        
        <View style={style.latitudeContainer}>
          <Text style={style.labelLatitude}>{'Latitud: '}</Text>
          <Text style={style.latitude} numberOfLines={1}>{this.props.latitude}</Text>
        </View>
        
        <View style={style.longitudeContainer}>
          <Text style={style.labelLongitude}>{'Longitud: '}</Text>
          <Text style={style.longitude} numberOfLines={1}>{this.props.longitude}</Text>
        </View>

        {
          this.props.availabilityOfServiceIsBeingFetched
          ? <Text numberOfLines={1}>{'Cargando...'}</Text>
          : <Text numberOfLines={1}>{this.props.description}</Text>
        }
      </View>
    );
  }
}

CustomCallout.propTypes = {
  latitude: PropTypes.node.isRequired,//Latitud que se mostrara en el callout
  longitude: PropTypes.node.isRequired,//Longitud que se mostrara en el callout
  description: PropTypes.string,//Descripción de la disponibilidad del servicio
  availabilityOfServiceIsBeingFetched: PropTypes.bool,//Flag para saber que se esta obteniendo la disponibilidad del servicio
};

export default CustomCallout;