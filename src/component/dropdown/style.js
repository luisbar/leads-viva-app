/***********************
 * Node modules import *
 ***********************/
import { Dimensions } from 'react-native';
/*************
 * Constants *
 *************/
const SCREEN_WIDTH = Dimensions.get('screen').width;
const HORIZONTAL_MARGIN_OF_PICKER = 30;

export default function(props) {

  return {
    
    container: {
      marginRight: 15,
      borderBottomWidth: 0,
      height: 75,
    },
    pickerContainer: {
      borderRadius: 5,
      backgroundColor: props.pickerBackgroundColor,
      height: 48.7,
    },
    pickerAndroid: {
      color: props.colorPickerAndroid,
      minWidth: SCREEN_WIDTH - HORIZONTAL_MARGIN_OF_PICKER,
      maxWidth: SCREEN_WIDTH - HORIZONTAL_MARGIN_OF_PICKER,
      flex: 1,
    },
    pickerIos: {
      minWidth: 130,
      maxWidth: 130,
    },
    label: {
      color: props.labelColor,
      top: 0,
    },
    icon: {
      color: props.iconColor,
    },
    title: {
      color: props.titleColor,
    },
    titleContainer: {
      flex: 3,
    },
    advice: {
      color: props.errorColor,
      left: 19,
      fontSize: 13,
      width: '90%',
    },
  };
};
