/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import { Platform, View } from 'react-native';
import { Picker, Item, Label, Header, Left, Button, Icon, Body, Title, Right, Text } from 'native-base';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
/**
 * It renders a dropdown
 */
class Dropdown extends PureComponent {
  
  constructor(props) {
    super(props);
  
    //To bind the listeners here for improving the performance
    this._renderHeader = this.renderHeader.bind(this);
    //State
    this.state = {
      advice: null,
    };
  }

  render() {

    return (
      <View>
        <Item
          stackedLabel
          style={style(this.props).container}>

          <Label style={style(this.props).label}>
            {this.props.label}
          </Label>

          <View style={style(this.props).pickerContainer}>
            <Picker
              style={Platform.OS === 'ios' ? style(this.props).pickerIos : style(this.props).pickerAndroid}
              iosHeader={this.props.iosTextHeader}
              mode={'dialog'}
              selectedValue={this.props.itemSelected}
              onValueChange={this.props.onValueChange}
              renderHeader={this._renderHeader}>

              {this.renderItems()}
            </Picker>
          </View>
        </Item>
        <Text style={style(this.props).advice}>{this.state.advice}</Text>
      </View>
    );
  }
  /**
   * It renders the header of the dialog for seleting an item
   */
  renderHeader(backAction) {
    
    return (
      <Header>

        <Left>
          <Button
            transparent
            onPress={backAction}>
            <Icon
              style={style(this.props).icon}
              name={'arrow-back'}/>
          </Button>
        </Left>

        <Body
          style={style(this.props).titleContainer}>
          <Title
            style={style(this.props).title}>
            {this.props.iosTextHeader}
          </Title>
        </Body>

        <Right/>
      </Header>
    );
  }
  /**
   * It renders the items that the user can select
   */
  renderItems() {
    
    return (
      this.props.items.map((item, index) =>
        <Item
          key={index}
          label={item}
          value={item}/>
      )
    );
  }
  /**
   * It checks if errors props has data and update the
   * advice value of the state
   */
  componentDidUpdate(prevProps, prevState) {
    let advice = '';

    if (this.props.errors)
      this.props.errors.map((error) => {
        if (error)
          advice += error + '\n';
      });

    this.setState({ advice: advice });
  }
}

Dropdown.propTypes = {
  pickerBackgroundColor: PropTypes.string,
  colorPickerAndroid: PropTypes.string,
  labelColor: PropTypes.string,//Color of label to the left side of dropdown
  iconColor: PropTypes.string,//Color of icon for going back (ios)
  titleColor: PropTypes.string,//Color of the title located on the toolbar (ios)
  label: PropTypes.string,//Text of the label located to the left side of dropdown
  iosTextHeader: PropTypes.string,//Text of the toolbar (ios)
  itemSelected: PropTypes.node,//Item selected by default
  onValueChange: PropTypes.func.isRequired,//Listener that is triggered when the user selected a new item
  items: PropTypes.array,//Items for showing into the dropdown
  errorColor: PropTypes.string,//Color of error message
  errors: PropTypes.array,//Errors to show
};

Dropdown.defaultProps = {
  pickerBackgroundColor: '#FAFAFA',
  colorPickerAndroid: '#FFFFFF',
  labelColor: '#FFFFFF',
  iconColor: '#FFFFFF',
  titleColor: '#FFFFFF',
  label: 'Items',
  iosTextHeader: 'Seleccione item',
  itemSelected: 'Item 1',
  items: ['Item 1', 'Item 2', 'Item 3'],
};

export default Dropdown;
