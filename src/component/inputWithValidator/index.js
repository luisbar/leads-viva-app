/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';

import { View } from 'react-native';
import { Item, Label, Icon, Input, Text } from 'native-base';

import isEmail from 'validator/lib/isEmail';
import isNumeric from 'validator/lib/isNumeric';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style';
/**
 * keyboardTypes
 */
const EMAIL_ADDRESS = 'email-address';
const PASSWORD = 'password';
const PHONE = 'phone-pad';
const DEFAULT = 'default';
const NUMERIC = 'numeric';
/**
 * Return keyTypes
 */
const DONE = 'done';
const GO = 'go';
const NEXT = 'next';
const SEARCH = 'search';
const SEND = 'send';
/**
 * It renders an input that validates the text introduced and trigger
 * an event that return the complete text and a flag for knowing if the text
 * introduced is right or wrong according keyboardType
 */
export default class InputWithValidator extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      icon: 'md-close-circle',//Icon that is shown to the right side of the input
      iconOpacity: 0,//Opacity of the icon shown to the right side
      iconColor: this.props.dangerColor,//Color of icon shown to the right side
      advice: undefined,//Advice shown on the bottom side of input
    };
    //To bind the listeners here for improving the performance
    this._onChangeText = this.handleInputValue.bind(this);
  }

  render() {

    return (
      <View>
        <Item
          stackedLabel
          onPress={this.props.onPress}
          style={style(this.props, this.state).item}>

          <Label
            style={style(this.props, this.state).label}>
            {this.props.label}
          </Label>

          <Input
            ref={'input'}
            style={[
              style(this.props, this.state).input,
              this.props.multiline && style(this.props, this.state).multilineInput//If is multiline, Another style is added
            ]}
            placeholderTextColor={this.props.placeholderTextColor}
            placeholder={this.props.placeholder}
            keyboardType={this.props.keyboardType}
            secureTextEntry={this.props.secureTextEntry}
            maxLength={this.props.maxLength}
            value={this.props.value}
            multiline={this.props.multiline}
            editable={this.props.editable}
            autoCapitalize={this.props.autoCapitalize}
            defaultValue={this.props.defaultValue}
            blurOnSubmit={this.props.blurOnSubmit}
            returnKeyType={this.props.returnKeyType}
            onSubmitEditing={this.props.onSubmitEditing}
            autoFocus={this.props.autoFocus}
            onChangeText={this._onChangeText}/>
        </Item>
        <Icon
          style={style(this.props, this.state).icon}
          name={this.state.icon}/>

        <Text style={style(this.props, this.state).advice}>
          {this.state.advice}
        </Text>
      </View>
    );
  }
  /**
   * It checks if errors props has data, this props is used
   * when the validation is from backend, and the input is
   * not required
   */
  componentDidUpdate(prevProps, prevState) {
    if (!this.props.isRequired) {
      let advice = '';

      this.props.errors.map((error) => {
        if (error)
          advice += error + '\n';

        return advice;
      });

      this.updateStateFromCloud(advice, advice ? 1 : 0);
    }
  }
  /**
   * Listener that is triggered when an user introduced text into
   * the input, in addition, it triggers the event onHandleInputValue
   * that has been sent as a prop
   */
  handleInputValue(value) {

    if (this.props.isRequired) {//If the input is required, it will be validated
      const isValid = this.checkKeyboardType(value);

      this.updateStateFromLocal(isValid);
      this.props.onHandleInputValue(this.props.id, isValid, value);
    } else
      this.props.onHandleInputValue(this.props.id, true, value);
  }
  /**
   * It checks if the text introduced is right according the keyboardType
   */
  checkKeyboardType(value) {

    switch (this.props.keyboardType) {

      case EMAIL_ADDRESS:
        return value && isEmail(value);

      case PASSWORD:
        return value &&
                value.length >= this.props.minLength &&
                /([a-z]+\d+)|(\d+[a-z]+)/ig.test(value);

      case NUMERIC:
        return value && value.length >= this.props.minLength && isNumeric(value);

      case PHONE:
      case DEFAULT:
        return value && value.length >= this.props.minLength;
    }
  }
  /**
   * It updates the state according the isValid parameter
   */
  updateStateFromLocal(isValid) {

    this.setState({
      icon: isValid ? 'md-checkmark-circle' : 'md-close-circle',
      iconOpacity: 1,
      iconColor: isValid ? this.props.successColor : this.props.dangerColor,
      advice: isValid ? undefined : this.getErrorMessage(),
    });
  }
  /**
   * It update the state according the params
   */
  updateStateFromCloud(advice, iconOpacity) {

    this.setState({
      icon: 'md-close-circle',
      iconOpacity: iconOpacity,
      iconColor: this.props.dangerColor,
      advice: advice,
    });
  }
  /**
   * It returns an error message if the text introduced is wrong,
   * the message is showing according keyboardType
   */
  getErrorMessage() {

    switch (this.props.keyboardType) {

      case EMAIL_ADDRESS:
        return 'Correo inválido';

      case PASSWORD:
        return 'Contraseña inválida';

      case PHONE:
        return 'Teléfono inválido';

      case NUMERIC:
        return 'Número inválido';

      case DEFAULT:
        return 'Texto muy corto';
    }
  }
}

InputWithValidator.propTypes = {
  id: PropTypes.node.isRequired,//Id for the input
  isRequired: PropTypes.bool,//Flag for knowing if the input is required
  value: PropTypes.string,//Text for showing into the input
  inputTextColor: PropTypes.string,//Color of input text
  labelTextColor: PropTypes.string,//Color of label about input text
  backgroundInputColor: PropTypes.string,//backgroundColor of the input
  placeholderTextColor: PropTypes.string,//Color of placeholder
  label: PropTypes.string,//Label about text input
  placeholder: PropTypes.string,//Placeholder
  multiline: PropTypes.bool,//If input is multiline or not
  minLength: function(props, propName, componentName) {//Minimum quantity of chars introduced into the input
    if (props.minLength <= 0)
      return new Error(
        `Invalid prop ${propName} supplied to ${componentName} with id=${props.id}`
      );
  },
  maxLength: function(props, propName, componentName) {//Maximun quantity of chars introduced into the input
    if (props.minLength <= 0)
      return new Error(
        `Invalid prop ${propName} supplied to ${componentName} with id=${props.id}`
      );
  },
  keyboardType: PropTypes.oneOf([//Types of keyboard
    EMAIL_ADDRESS,
    PASSWORD,
    PHONE,
    DEFAULT,
    NUMERIC
  ]),
  secureTextEntry: PropTypes.bool,//If the input is for password or not
  returnKeyType: PropTypes.oneOf([//Types of return key
    DONE,
    GO,
    NEXT,
    SEARCH,
    SEND
  ]),
  autoFocus: PropTypes.bool,//For putting the focus over the input
  editable: PropTypes.bool,//If the user could introduced text into the input or not
  autoCapitalize: PropTypes.oneOf([//Types of keyboard
    'characters',
    'words',
    'sentences',
    'none',
  ]),
  defaultValue: PropTypes.string,//For putting a initial value
  errors: PropTypes.array,//Errors message which will be displayed
  blurOnSubmit: PropTypes.bool,//If true after submitting the method onSubmitEditing is triggered
  dangerColor: PropTypes.string,//Danger color for showing the error message when the validation fail
  successColor: PropTypes.string,//Success color for showing the success message when the validation is ok
  onPress: PropTypes.func,//Listener that is triggered when an user pressed the input e.g. when the input is no editable you can show a dialog or filling the input
  onSubmitEditing: PropTypes.any,//Listener that is triggered when an user pressed the returnKey or ref to the next field
  onHandleInputValue: PropTypes.func.isRequired,//Listener that is triggered when an user writes or erases text from input
};

InputWithValidator.defaultProps = {
  isRequired: true,
  inputTextColor: '#000000',
  labelTextColor: '#000000',
  backgroundInputColor: '#FAFAFA',
  placeholderTextColor: '#424242',
  label: 'Label',
  placeholder: 'Escriba aquí',
  multiline: false,
  minLength: 1,
  maxLength: 5,
  keyboardType: DEFAULT,
  secureTextEntry: false,
  returnKeyType: NEXT,
  autoFocus: false,
  editable: true,
  autoCapitalize: 'sentences',
  defaultValue: '',
  errors: [],
  blurOnSubmit: true,
  dangerColor: '#FF8888',
  successColor: '#008249',
}
