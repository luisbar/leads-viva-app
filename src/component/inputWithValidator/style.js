export default function(props, state) {

  return {

    item: {
      height: props.multiline ? 110 : 75,
      marginRight: 15,
      borderBottomWidth: 0,
    },
    label: {
      color: props.labelTextColor,
      top: 0,
    },
    input: {
      color: props.inputTextColor,
      flex: -1,
      width: '100%',
      paddingRight: 0,
      borderRadius: 5,
      backgroundColor: props.backgroundInputColor,
    },
    multilineInput: {
      color: props.inputTextColor,
      minHeight: 80,
    },
    icon: {
      position: 'absolute',
      right: 10,
      top: 15,
      opacity: state.iconOpacity,
      color: state.iconColor,
    },
    advice: {
      color: props.dangerColor,
      left: 19,
      fontSize: 13,
      width: '90%',
    },
  };
}
