/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { View, Image, PermissionsAndroid } from 'react-native';
import { Content, Fab, Text, List, ListItem, Thumbnail, Body, Right } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Lightbox from 'react-native-lightbox';

import moment from 'moment';
import 'moment/locale/es';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../../scene/basisComponent.js';
import PopUpMenu from '../popUpMenu/index.js';
import ProgressScreen from '../progressScreen/index.js';
import {
  pictureFabButtonIconColor,
  pictureListItemIconColor,
  popupMenuItemIconColor,
  popupMenuItemTextColor,
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
} from '../../color.js';
/**
 * Renderiza la vista para tomar las fotos
 */
class Picture extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._renderItem = this.renderItem.bind(this);
    this._removePicture = (index) => this.removePicture.bind(this, index);
    this._renderContent = (item) => this.renderContent.bind(this, item);
    this._onOpenCamera = this.onOpenCamera.bind(this);
    this._onScroll = this.onScroll.bind(this);
    //State
    this.state = {
      pictures: this.props.pictures,
      cameraButtonVisible: true,
    };
  }
  
  componentWillReceiveProps(nextProps) {
    this.setState({ pictures: nextProps.pictures });
  }

  render() {
    
    return (
      <Content contentContainerStyle={style.contentContainerStyle}>
        {
          this.props.showProgressScreen
          ? this.renderProgressScreen()
          : this.renderPictureList()
        }
      </Content>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {

    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbarAndTabBar={true}/>
    );
  }
  /**
   * Renderiza la lista de fotos tomadas
   */
  renderPictureList() {

    return (
      //Si se obtuvo los datos de configuración para registrar leads
      this.props.showPictures &&
      <View style={style.container}>
        <List
          onScroll={this._onScroll}
          dataArray={this.state.pictures}
          renderRow={this._renderItem}/>
        
        {
          this.state.cameraButtonVisible &&
          <Fab
            position={'bottomRight'}
            style={style.cameraButton}
            onPress={this._onOpenCamera}>
          
            <Icon
              name={'add-a-photo'}
              size={20}
              color={pictureFabButtonIconColor}/>
          </Fab>
        }
      </View>
    );
  }
  /**
   * Renderiza los items de la lista de fotos tomadas
   */
  renderItem(item, index) {

    return (
      <ListItem>
        <Lightbox
          swipeToDismiss={false}
          renderContent={this._renderContent(item)}>
          <Thumbnail
            square
            size={80}
            source={{ uri: item.picture }}/>
        </Lightbox>
        
        <Body>
          <Text style={style.listItemPictureType}>{item.pictureTypeName}</Text>
          {
            this.props.maximumDistance && (item.distance || item.distance === 0)
            ? <Text style={item.distance > this.props.maximumDistance ? style.distanceText : null}>{`Esta foto fue tomada a ${item.distance} metro(s) del lead\n`}</Text>
            : item.distance || item.distance === 0
            ? <Text>{`Esta foto fue tomada a ${item.distance} metro(s) del lead\n`}</Text>
            : null
          }
          <Text>{moment(item.date).format('DD/MM/YYYY h:mm A')}</Text>
        </Body>
        
        <Right>
          <PopUpMenu
            menuIconColor={pictureListItemIconColor}
            menuContainerStyle={style.popUpMenu}
            items={this.getPopUpMenuItems(item)}/>
        </Right>
      </ListItem>
    );
  }
  /**
   * Renderiza el contenido del Lightbox
   */
  renderContent(item) {

    return (
      <Image
        style={style.imageLightBox}
        source={{ uri: item.picture }}
        resizeMode={'center'}/>
    );
  }
  /**
   * Retorna las opciones del popUpMenu
   */
  getPopUpMenuItems(item) {
    
    return [{
      itemPressed: this._removePicture(item.pictureTypeId),
      icon: 'delete',
      iconColor: popupMenuItemIconColor,
      text: 'Eliminar',
      textColor: popupMenuItemTextColor,
    }];
  }
  /**
   * Escuchador que se dispara cuando el usuario
   * presiona la opción eliminar del popUpMenu
   */
  removePicture(type) {
    const newState = this.state.pictures.filter((item) => item.pictureTypeId !== type);
    
    this.setState({ pictures: newState }, () => this.props.onRemovePicture(this.state.pictures));
  }
  /**
   * Escuchador que se dispara cuando se presiona
   * el float button para abrir la camara
   */
  onOpenCamera() {
    const permission = PermissionsAndroid.PERMISSIONS.CAMERA;
    
    PermissionsAndroid.check(permission)
    .then((isPermissionGranted) => isPermissionGranted || PermissionsAndroid.request(permission))
    .then((data) => {
      if (typeof data === 'boolean')//Si anteriormente se concedió permiso
        this.props.onOpenCamera();
      else if (data === PermissionsAndroid.RESULTS.GRANTED)//Si recientemente se concedió permiso
        this.props.onOpenCamera();
      else
        this.showErrorMessage('Debe conceder permiso a la aplicación para poder utilizar la camara')
    })
  }
  /**
   * Escuchador que se dispara cuando se hace scroll
   * al List
   */
  onScroll(event) {
    const currentOffset = event.nativeEvent.contentOffset.y;
    const direction = currentOffset > this.previousOffset ? 'down' : 'up';
    this.previousOffset = currentOffset;

    if (this.state.upButtonVisible !== direction)
      this.setState({ cameraButtonVisible: direction === 'up' });
  }
}

Picture.propTypes = {
  onOpenCamera: PropTypes.func.isRequired,//Escuchador que es dispara cuando se presiona el float button para abrir la camara
  pictures: PropTypes.array,//Array de fotos tomadas
  onRemovePicture: PropTypes.func.isRequired,//Escuchador que se dispara cuando el usuario elimina una foto de la lista de fotos
  showPictures: PropTypes.bool,//Bandera para saber si se muestra o no la lista de fotos
  showProgressScreen: PropTypes.bool.isRequired,//Bandera para saber si se muestra o no el ProgressScreen
  maximumDistance: PropTypes.number,//Maxima distancia de una foto al lead 
};

export default Picture;