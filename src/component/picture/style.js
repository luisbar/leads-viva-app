/******************
 * Project import *
 ******************/
import {
  pictureFabButtonBackgroundColor,
  pictureTypeBackgroundColor,
  pictureTypeTextColor,
  popupMenuBackgroundColor,
  pictureDistanceText,
} from '../../color.js';

export default {
  
  contentContainerStyle: {
    flex: 1,
  },
  container: {
    flex: 1,
  },
  cameraButton: {
    backgroundColor: pictureFabButtonBackgroundColor,
  },
  listItemPictureType: {
    maxWidth: 100,
    marginBottom: 10,
    backgroundColor: pictureTypeBackgroundColor,
    color: pictureTypeTextColor,
    borderRadius: 5,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  distanceText: {
    color: pictureDistanceText,
  },
  popUpMenu: {
    borderRadius: 10,
    backgroundColor: popupMenuBackgroundColor,
  },
  imageLightBox: {
    flex: 1,
  },
}