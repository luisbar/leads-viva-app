/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import { StyleProvider, Container, Text, Title, Button, Form } from 'native-base';
import { View } from 'react-native';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import InputWithValidator from '../inputWithValidator/index.js';
import {
  inputTextColor,
  inputLabelColor,
  inputPlaceholderColor,
  inputBackgroundColor,
  confirmModalBackground,
  confirmModalTitle,
  confirmModalNote,
  confirmModalRightButton,
  confirmModalRightButtonText,
  confirmModalLeftButton,
  confirmModalLeftButtonText,
} from '../../color.js';
/**
 * It renders a confirmation modal
 */
class ConfirmModal extends PureComponent {

  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container
          style={style(this.props).mainContainer}>
          <View
            style={style(this.props).secondaryContainer}>
            
            <Title
              style={style(this.props).title}>{this.props.title}</Title>
            
            <Text style={style(this.props).note}>{this.props.note}</Text>
            {this.renderInput()}
            {this.renderButtons()}
          </View>
        </Container>
      </StyleProvider>
    );
  }
  /**
   * It render an input for introducing some text
   */
  renderInput() {
    
    return (
      this.props.showNoteInput && 
      <Form>
        <InputWithValidator
          id={'note'}
          inputTextColor={this.props.inputTextColor}
          labelTextColor={this.props.labelTextColor}
          placeholderTextColor={this.props.placeholderTextColor}
          backgroundInputColor={this.props.backgroundInputColor}
          label={this.props.inputLabel}
          placeholder={'Escriba aquí'}
          multiline={true}
          minLength={1}
          maxLength={500}
          keyboardType={'default'}
          returnKeyType={'done'}
          editable={true}
          visible={true}
          isRequired={false}
          onHandleInputValue={this.props.onHandleInputValue}/>
      </Form>
    );
  }
  /**
   * It renders the buttons
   */
  renderButtons() {
    
    return (
      <View
        style={style(this.props).buttonsContainer}>
        
        <Button
          onPress={this.props.onLeftButtonPressed}
          style={style(this.props).leftButton}>

          <Text style={style(this.props).leftButtonText}>{this.props.leftButtonText}</Text>
        </Button>
        
        <Button
          onPress={this.props.onRightButtonPressed}
          style={style(this.props).rightButton}>

          <Text style={style(this.props).rightButtonText}>{this.props.rightButtonText}</Text>
        </Button>
      </View>
    );
  }
}

ConfirmModal.PropTypes = {
  backgroundColor: PropTypes.string,//Backgroundcolor of the modal
  titleColor: PropTypes.string,//Text color of the title of the modal
  noteColor: PropTypes.string,//Text color of the note of the modal
  inputTextColor: PropTypes.string,//Text color of the input in the modal
  labelTextColor: PropTypes.string,//Label color of the input in the modal
  placeholderTextColor: PropTypes.string,//Placeholder color of the input in the modal
  backgroundInputColor: PropTypes.string,//Backgroundcolor of the input in the modal
  leftButtonBackgroundColor: PropTypes.string,//Backgroundcolor of the left button in the modal
  leftButtonTextColor: PropTypes.string,//Text color of the left button in the modal
  rightButtonBackgroundColor: PropTypes.string,//Backgroundcolor of the right button in the modal
  rightButtonTextColor: PropTypes.string,//Text color of the right button in the modal
  showNoteInput: PropTypes.bool,//Flag in order to show the note input or not
  title: PropTypes.string,//Title of the modal
  note: PropTypes.string,//Note of the modal
  inputLabel: PropTypes.string,//Label of the input note in the modal
  leftButtonText: PropTypes.string,//Text of the left button in the modal
  rightButtonText: PropTypes.string,//Text of the right button in the modal
  onLeftButtonPressed: PropTypes.func,//Listener that is triggered when the left button was pressed
  onRightButtonPressed: PropTypes.func,//Listener that is triggered when the right button was pressed
  onHandleInputValue: PropTypes.func,//Listener that is triggered when an user writes on note input
};

ConfirmModal.defaultProps = {
  backgroundColor: confirmModalBackground,
  titleColor: confirmModalTitle,
  noteColor: confirmModalNote,
  inputTextColor: inputTextColor,
  labelTextColor: inputLabelColor,
  placeholderTextColor: inputPlaceholderColor,
  backgroundInputColor: inputBackgroundColor,
  leftButtonBackgroundColor: confirmModalLeftButton,
  leftButtonTextColor: confirmModalLeftButtonText,
  rightButtonBackgroundColor: confirmModalRightButton,
  rightButtonTextColor: confirmModalRightButtonText,
  showNoteInput: true,
  title: 'Confirmación',
  note: '¿Esta seguro que desea cerrar el lead?',
  inputLabel: 'Razón',
  leftButtonText: 'No',
  rightButtonText: 'Si',
  onLeftButtonPressed: () => alert('left'),
  onRightButtonPressed: () => alert('right'),
  onHandleInputValue: () => { },
};

export default ConfirmModal;