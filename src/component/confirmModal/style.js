/***********************
 * Node modules import *
 ***********************/
import { Dimensions } from 'react-native';
/*************
 * Constants *
 *************/
const SCREEN_WIDTH = Dimensions.get('screen').width;
const HORIZONTAL_MARGIN_OF_SECONDARY_CONTAINER = 30;

export default function(props) {
  
  return {
    
    mainContainer: {
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },
    secondaryContainer: {
      width: SCREEN_WIDTH - HORIZONTAL_MARGIN_OF_SECONDARY_CONTAINER,
      backgroundColor: props.backgroundColor,
      flexDirection: 'column',
      borderRadius: 10,
      padding: 10,
    },
    title: {
      marginBottom: 15,
      color: props.titleColor,
    },
    note: {
      marginBottom: 10,
      color: props.noteColor,
      textAlign: 'center',
    },
    buttonsContainer: { 
      flexDirection: 'row',
      justifyContent: 'center',
      marginTop: 10,
    },
    leftButton: {
      marginRight: 10,
      backgroundColor: props.leftButtonBackgroundColor,
      minWidth: 50,
    },
    leftButtonText: {
      color: props.leftButtonTextColor,
      textAlign: 'center',
    },
    rightButton: {
      backgroundColor: props.rightButtonBackgroundColor,
      minWidth: 50,
    },
    rightButtonText: {
      color: props.rightButtonTextColor,
      textAlign: 'center',
    },
  };
};