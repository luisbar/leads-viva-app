/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import { Body, Text, Spinner } from 'native-base';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
/**
 * It renders a progress screen
 */
class ProgressScreen extends PureComponent {

  render() {

    return (
      <Body
        style={style(this.props).container}>

        <Spinner color={this.props.spinnerColor}/>
        <Text style={style(this.props).text}>{this.props.progressText}</Text>
      </Body>
    );
  }
}

ProgressScreen.propTypes = {
  withToolbar: PropTypes.bool,//Flag for knowing if the view has toolbar or not
  withBiggerToolbar: PropTypes.bool,//Flag for knowing if the view has a bigger toolbar or not
  withToolbarAndTabBar: PropTypes.bool,//Flag for knowing if then view has a toolbar and tabbar
  progressText: PropTypes.string,//Text for showing on the progress screen
  backgroundColor: PropTypes.string,//Background color of the progress screen
  textColor: PropTypes.string,//Text color of the text on the progress screen
  spinnerColor: PropTypes.string//Spinner color of the spinner on the progress screen
};

ProgressScreen.defaultProps = {
  withToolbar: false,
  withBiggerToolbar: false,
  withToolbarAndTabBar: false,
  progressText: 'Cargando...',
  backgroundColor: '#FFFFFF',
  textColor: '#000000',
  spinnerColor: '#279afe',
};

export default ProgressScreen;