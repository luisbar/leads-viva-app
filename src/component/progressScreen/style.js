/***********************
 * Node modules import *
 ***********************/
import { Dimensions, Platform, StatusBar } from 'react-native';
/*************
 * Constants *
 *************/
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const STATUS_BAR_HEIGHT = StatusBar.currentHeight;
const IS_IPHONE_X = Platform.OS === 'ios' && SCREEN_HEIGHT === 812 && SCREEN_WIDTH === 375;
const TOOL_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 56;
const TAB_BAR_HEIGHT = 49.3;

export default function(props) {

  return {

    container: {
      backgroundColor: props.backgroundColor,
      justifyContent: 'center',
      width: SCREEN_WIDTH,
      height: SCREEN_HEIGHT - (props.withToolbar
                              ? TOOL_BAR_HEIGHT + STATUS_BAR_HEIGHT
                              : props.withBiggerToolbar
                              ? (TOOL_BAR_HEIGHT * 2) + STATUS_BAR_HEIGHT
                              : props.withToolbarAndTabBar
                              ? TOOL_BAR_HEIGHT + STATUS_BAR_HEIGHT + TAB_BAR_HEIGHT
                              : STATUS_BAR_HEIGHT),//If withToolbar or withBiggerToolbar is true, so, height is reduced
    },
    text: {
      color: props.textColor,
    }
  };
};
