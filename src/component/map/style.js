/***********************
 * Node modules import *
 ***********************/
import { Dimensions } from 'react-native';
/******************
 * Project import *
 ******************/
import { gpsButtonBackgroundColor } from '../../color.js';
/*************
 * Constants *
 *************/
const SCREEN_WIDTH = Dimensions.get('screen').width;
const SCREEN_HEIGHT = Dimensions.get('screen').height;
const MAP_MARGIN = 15;

export default {
  
  map: {
    width: SCREEN_WIDTH - (MAP_MARGIN * 2),
    height: SCREEN_HEIGHT / 2.3,
    margin: MAP_MARGIN,
  },
  markerImage: {
    alignSelf: 'center',
  },
  gpsButton: {
    backgroundColor: gpsButtonBackgroundColor,
  },
};