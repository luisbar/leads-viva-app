/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Content, Form, Fab } from 'native-base';
import { PermissionsAndroid, View, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import MapView, { Marker } from 'react-native-maps';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../../scene/basisComponent.js';
import InputWithValidator from '../inputWithValidator/index.js';
import ProgressScreen from '../progressScreen/index.js';
import CustomCallout from '../customCallout/index.js';
import { logger } from '../../service/index.js';
import {
  gpsButtonIconColor,
  inputTextColor,
  inputLabelColor,
  inputPlaceholderColor,
  inputBackgroundColor,
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
} from '../../color.js';
/**
 * Renderiza el mapa
 */
class Map extends BasisComponent {

  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onAddOrUpdateDraggableMarker = this.onAddOrUpdateDraggableMarker.bind(this);
    this._checkIfLocationPermissionHasBeenGranted = this.checkIfLocationPermissionHasBeenGranted.bind(this);
    this._onDragEnd = this.onDragEnd.bind(this);
    this._onMapReady = this.onMapReady.bind(this);
    //State
    this.state = {
      markerKey: null,//Para que el marker se actualize cuando cambian las props y state
      markerCoordinate: null,//Ubicación del marker
      calloutDescription: null,//Descripción si hay o no cobertura
      isMapReady: false,
    };
  }
  /**
   * Verifica si se paso la ubicación como props,
   * si es asi se pone el marker en la ubicación dada,
   * ademas actualiza el calloutDescription cuando
   * se ejecuta el metodo executeGetAvailabilityOfService
   * y muestra mensaje de error cuando no pudo
   * recuperar la disponibilidad del servicio
   */
  componentWillReceiveProps(nextProps) {
    const { location, statusOfGettingAvailabilityOfService, availabilityOfService } = nextProps;

    if (this.state.isMapReady &&
      this.props.location !== location &&
      location.latitude &&
      location.longitude) {

      this.addOrUpdateDraggableMarker(location.latitude, location.longitude);
      this.refs.map.animateToRegion(this.getRegion(location.latitude, location.longitude));
      this.props.onNewLocation(location.latitude, location.longitude);
    }
    //Actualiza la disponibilidad del servicio
    if (statusOfGettingAvailabilityOfService.availabilityOfServiceIsBeingFetched)
      this.setState({
        markerKey: new Date().getTime(),
        calloutDescription: availabilityOfService,
      });
    //Muestra mensaje de error si no se pudo obtener la disponibilidad del servicio
    if (statusOfGettingAvailabilityOfService.availabilityOfServiceIsBeingFetched && 
        statusOfGettingAvailabilityOfService.wasAnError)
      this.showErrorMessage('No se pudo obtener la cobertura del servicio')
  }
  
  render() {

    return (
      <Content>
      {
        this.props.showProgressScreen
        ? this.renderProgressScreen()
        : this.renderInputMapAndButton()
      }
      </Content>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {
    
    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbarAndTabBar={true}/>
    );
  }
  /**
   * Renderiza el input, el mapa y el float button
   */
  renderInputMapAndButton() {
    
    return (
      //Si se cargo los datos de configuración para guardar los leads
      this.props.showMap &&
      <View>
        {this.renderAddressInput()}
        {this.renderMap()}
        {this.renderGpsButton()}
      </View>
    );
  }
  /**
   * Renderiza el input para introducir la dirección
   */
  renderAddressInput() {
    
    return (
      <Form>
        <InputWithValidator
          id={'address'}
          backgroundInputColor={inputBackgroundColor}
          inputTextColor={inputTextColor}
          labelTextColor={inputLabelColor}
          placeholderTextColor={inputPlaceholderColor}
          label={'Dirección'}
          placeholder={'Escriba aquí'}
          multiline={true}
          minLength={1}
          maxLength={200}
          keyboardType={'default'}
          returnKeyType={'done'}
          editable={true}
          isRequired={false}
          defaultValue={this.props.address}
          onHandleInputValue={this.props.onNewAddress}/>
      </Form>
    );
  }
  /**
   * Renderiza el mapa
   */
  renderMap() {
    
    return (
      <MapView
        ref={'map'}
        style={style.map}
        showsUserLocation={false}
        showsMyLocationButton={false}
        zoomControlEnabled={true}
        initialRegion={this.getInitialRegion()}
        onMapReady={this._onMapReady}
        onPress={this.props.markerIsDraggable && this._onAddOrUpdateDraggableMarker}>
        {
          this.state.markerCoordinate &&
          <Marker
            key={this.state.markerKey}//Para que el callout se actualize
            draggable={this.props.markerIsDraggable}
            coordinate={this.state.markerCoordinate}
            onDragEnd={this._onDragEnd}>
            
            <CustomCallout
              latitude={this.state.markerCoordinate.latitude}
              longitude={this.state.markerCoordinate.longitude}
              description={this.state.calloutDescription}
              availabilityOfServiceIsBeingFetched={this.props.statusOfGettingAvailabilityOfService.availabilityOfServiceIsBeingFetched}/>
            
            <Image
              style={style.markerImage}
              source={require('../../image/marker.png')}/>
          </Marker>
        }
      </MapView>
    );
  }
  /**
   * Renderiza el boton para obtener la ubicación actual
   */
  renderGpsButton() {
    
    return (
      <Fab
        position={'bottomRight'}
        style={style.gpsButton}
        onPress={this._checkIfLocationPermissionHasBeenGranted}>
        
        <Icon name={'gps-fixed'} size={20} color={gpsButtonIconColor}/>
      </Fab>
    );
  }
  /**
   * Escuchador que se dispara cuando el mapa
   * ha sido renderizado, este escuchador
   * se implemento cuando se va a editar un lead,
   * por que da error en el componentWillReceiveProps
   */
  onMapReady() {
    if (this.props.location && this.props.location.latitude && this.props.location.longitude) {
      this.addOrUpdateDraggableMarker(this.props.location.latitude, this.props.location.longitude);
      this.refs.map.animateToRegion(this.getRegion(this.props.location.latitude, this.props.location.longitude));
      this.props.onNewLocation(this.props.location.latitude, this.props.location.longitude);
    }
    
    this.setState({ isMapReady: true });
  }
  /**
   * Escuchador que se dispara cuando un usuario presiona sobre
   * el mapa, al presionar se agrega un marker sobre la region
   * presionada
   */
  onAddOrUpdateDraggableMarker(event) {
    const coordinate = event.nativeEvent.coordinate;    
    this.addOrUpdateDraggableMarker(coordinate.latitude, coordinate.longitude);
    
    this.props.onNewLocation(coordinate.latitude, coordinate.longitude);
  }
  /**
   * Escuchador que se dispara cuando se arrastra el marker
   * @param  {object} event objeto que contiene las coordenadas
   */
  onDragEnd(event) {
    const coordinate = event.nativeEvent.coordinate;   
    this.addOrUpdateDraggableMarker(coordinate.latitude, coordinate.longitude);
    
    this.props.onNewLocation(coordinate.latitude, coordinate.longitude);
  }
  /**
   * Añade marker al mapa con las coordenadas especificadas. o actualiza
   * el marker que ya esta en el mapa
   * @param {number} latitude latitud
   * @param {number} longitude longitud
   * @param {string} description texto que muestra si hay o no cobertura
   */
  addOrUpdateDraggableMarker(latitude, longitude, description) {
    //Solicita la cobertura del servicio en la ubicación actual
    this.props.executeGetAvailabilityOfService(
      latitude,
      longitude,
      this.props.accessToken,
      this.props.refreshToken
    );
    //Agrega el marker al mapa
    this.setState({
      markerKey: new Date().getTime(),
      markerCoordinate: { latitude: latitude, longitude: longitude },
      calloutDescription: this.state.calloutDescription,
    });
  }
  /**
   * Retorna region de inicio
   */
  getInitialRegion() {
    
    return this.getRegion(-17.7841546, -63.181788);
  }
  /**
   * Verifica si el usuario ha concedido permiso para utilizar el gps,
   * si es así, se obtiene la ubicación actual
   */
  async checkIfLocationPermissionHasBeenGranted() {
    //Verifica si la app tiene permiso para utilizar el gps
    let permission = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
    //Si tiene permiso obtiene la ubicación
    if (permission) {
      this.showInformativeMessage('Obteniendo ubicación');
      this.requestCurrentLocation();
    } else {
      //Sino solicita permiso
      permission = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
        'title': 'Permiso de gps',
        'message': 'La aplicación necesita permiso al gps para poder obtener tu ubicación'
      });
      //Si concede permiso obtiene la ubicación
      if (permission === PermissionsAndroid.RESULTS.GRANTED) {
        this.showInformativeMessage('Obteniendo ubicación');
        this.requestCurrentLocation();
      } else
        this.showErrorMessage('Debe conceder permiso a la aplicación para utilizar el gps');
    }
  }
  /**
   * Obtiene la ubicación actual y se centra el mapa en
   * dicha ubicación
   */
  requestCurrentLocation() {
    this.getCurrentPosition({ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 })
    .then(({ coords }) => this.getRegion(coords.latitude, coords.longitude))
    .then((region) => {
      this.addOrUpdateDraggableMarker(region.latitude, region.longitude);//Pone el marker en el mapa
      this.refs.map.animateToRegion(region);//Mueve el mapa donde esta el marker
      this.props.onNewLocation(region.latitude, region.longitude);
    })
    .catch((error) => {
      if (error.code === 2)
        this.showErrorMessage('Debe habilitar el gps');
      else {
        this.showErrorMessage('Hubo un problema obteniendo la ubicación');
        logger.recordError(String(error));
        logger.log('Map: error al obtener la ubicación');
      }
    });
  }
  /**
   * Obtiene la ubicación actual del usuario
   * @param  {object} options opciones para obtener la ubicación
   */
  getCurrentPosition(options) {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resolve, reject, options);
    });
  }
  /**
   * Devuelve objeto region
   */
  getRegion(latitude, longitude) {
    
    return {
      latitude: latitude,
      longitude: longitude,
      latitudeDelta: 0.005,
      longitudeDelta: 0.005
    };
  }
  /**
   * Devuelve la cobertura
   */
  getAvailabilityOfService() {
    
    return this.state.calloutDescription;
  }
}

Map.propTypes = {
  onNewLocation: PropTypes.func.isRequired,//Escuchador que se dispara cuando se termina de arrastrar el marker
  onNewAddress: PropTypes.func.isRequired,//Escuchador que se dispara cuando el usuario escribe en el campo para la dirección
  address: PropTypes.string,//Dirección para setear en el campo dirección
  location: PropTypes.object,//Objeto que contiene la latitud y longitud
  markerIsDraggable: PropTypes.bool,//Bandera para saber si se puede mover el marker
  executeGetAvailabilityOfService: PropTypes.func,//Funcion para obtener la disponibilidad del servicio en la ubicación actual
  statusOfGettingAvailabilityOfService: PropTypes.object,//Objeto que contiene el estado de obtención de la disponibilidad del servicio
  availabilityOfService: PropTypes.string,//Mensaje que muestra la disponibilidad del servicio
  accessToken: PropTypes.string,
  refreshToken: PropTypes.string,
  showMap: PropTypes.bool,//Bandera para saber si se muestar o no el mapa
  showProgressScreen: PropTypes.bool,//Bandera para saber si se muestra o no el ProgressScreen
};

export default Map;