//Scene ids
export const login = 'leads.viva.Login';
export const home = 'leads.viva.Home';
export const leadsList = 'leads.viva.LeadsList';
export const appointment = 'leads.viva.Appointment';
export const configuration = 'leads.viva.Configuration';
export const leadRegistration = 'leads.viva.LeadRegistration';
export const leadEdition = 'leads.viva.LeadEdition';
export const observedLead = 'leads.viva.ObservedLead';
export const pendingAssignment = 'leads.viva.PendingAssignment';
export const actionsRecord = 'leads.viva.ActionsRecord';
export const comment = 'leads.viva.Comment';
export const leadViewer = 'leads.viva.LeadViewer';
export const actionRegistration = 'leads.viva.ActionRegistration';
export const filter = 'leads.viva.Filter';
export const leadStatus = 'leads.viva.LeadStatus';
export const leadSubStatus = 'leads.viva.LeadSubStatus';
export const actionEdition = 'leads.viva.ActionEdition';
export const actionViewer = 'leads.viva.ActionViewer';
export const approbationOrRejection = 'leads.viva.ApprobationOrRejection';
export const camera = 'leads.viva.Camera';
export const product = 'leads.viva.Product';
export const notification = 'leads.viva.Notification';
export const place = 'leads.viva.Place';

export const confirmModal = 'leads.viva.ConfirmModal';
export const progressModal = 'leads.viva.ProgressModal';

export const appNotification = 'leads.viva.AppNotification';
//Endpoints
export const MAIN_URI = 'http://leads.ipublicity.org/';
export const API_VERSION = 'api-rest-v1/';
export const MAIN_URI_PLUS_API_VERSION = MAIN_URI + API_VERSION;
export const SIGN_IN_URI = `${MAIN_URI_PLUS_API_VERSION}autentificacion/token/`;
export const CHANGE_PASSWORD_URI = `${MAIN_URI_PLUS_API_VERSION}usuario/cambiar-contrasena/`;
export const CONFIGURATION_DATA_URI = `${MAIN_URI_PLUS_API_VERSION}leads/registrar-lead/`;
export const SERVICE_AVAILABLE_URI = `${MAIN_URI_PLUS_API_VERSION}mapas/cobertura/`
export const SAVE_LEAD_URI = `${MAIN_URI_PLUS_API_VERSION}leads/registrar-lead/`;
export const LEADS_URI = `${MAIN_URI_PLUS_API_VERSION}leads/lista-leads-vendedor/`;
export const GET_A_LEAD_URI = `${MAIN_URI_PLUS_API_VERSION}leads/editar-lead/leadId`;
export const UPDATE_LEAD_URI = `${MAIN_URI_PLUS_API_VERSION}leads/editar-lead/leadId/`;
export const ACTIONS_RECORD_URI = `${MAIN_URI_PLUS_API_VERSION}agenda/historial-acciones/leadId/`;
export const COMMENTS_URI = `${MAIN_URI_PLUS_API_VERSION}comentarios/leadId/`;
export const SAVE_COMMENT_URI = `${MAIN_URI_PLUS_API_VERSION}comentarios/leadId/`;
export const GET_A_LEAD_WITH_STATUS_RECORD_URI = `${MAIN_URI_PLUS_API_VERSION}leads/ver-lead/leadId/`;
export const GET_A_LEAD_FOR_SAVING_AN_ACTION_URI = `${MAIN_URI_PLUS_API_VERSION}agenda/registrar-accion/leadId/`;
export const SAVE_AN_ACTION_URI = `${MAIN_URI_PLUS_API_VERSION}agenda/registrar-accion/leadId/`;
export const GET_ACTIONS_BY_DATE_URI = `${MAIN_URI_PLUS_API_VERSION}agenda/obtener-acciones/`;
export const GET_HOME_DATA_URI = `${MAIN_URI_PLUS_API_VERSION}leads/inicio-vendedor/`;
export const LEADS_BY_SUB_STATUS_URI = `${MAIN_URI_PLUS_API_VERSION}leads/cantidad-leads-estados/status/`;
export const ACTION_FOR_EDITION_URI = `${MAIN_URI_PLUS_API_VERSION}agenda/editar-accion/actionId/`;
export const UPDATE_AN_ACTION_URI = `${MAIN_URI_PLUS_API_VERSION}agenda/editar-accion/actionId/`;
export const GET_AN_ACTION_URI = `${MAIN_URI_PLUS_API_VERSION}agenda/ver-accion/actionId/`;
export const GET_PENDING_LEADS_URI = `${MAIN_URI_PLUS_API_VERSION}asignacion/asignaciones-pendientes/page/`;
export const GET_LEAD_TO_REJECT_URI = `${MAIN_URI_PLUS_API_VERSION}asignacion/rechazar-lead/leadId/`;
export const GET_LEAD_TO_APPROVE_URI = `${MAIN_URI_PLUS_API_VERSION}asignacion/aprobar-lead/leadId/`;
export const APPROVE_A_PENDING_LEAD_URI = `${MAIN_URI_PLUS_API_VERSION}asignacion/aprobar-lead/leadId/`;
export const REJECT_A_PENDING_LEAD_URI = `${MAIN_URI_PLUS_API_VERSION}asignacion/rechazar-lead/leadId/`;
export const GET_OBSERVED_LEADS_URI = `${MAIN_URI_PLUS_API_VERSION}validacion-lead/leads-observados/page/`;
export const RE_OPEN_A_LEAD_URI = `${MAIN_URI_PLUS_API_VERSION}validacion-lead/reaperturar-lead/observationId/`;
export const GET_NOTIFICATIONS_URI = `${MAIN_URI_PLUS_API_VERSION}mensaje/mis-notificaciones/page/`;
export const SEE_A_NOTIFICATION_URI = `${MAIN_URI_PLUS_API_VERSION}mensaje/notificacion-poner-en-visto/notificationId/`;

export const CHAT_AVATAR_URI = `${MAIN_URI}/static/img/usuario_comentarios.png`;
//Keys
export const GRANT_TYPE_ONE = 'password';
export const GRANT_TYPE_TWO = 'refresh_token';
export const CLIENT_ID = 'tu17kFb2BJAkCgk7rUr77MwF51EZIsjPCGxHGI5c';
export const CLIENT_SECRET = 'aXEziJnAzLu6MDxR9gsWNdqqPd07pBUk9RQGDlBz17Mwg4EOCurnvdErw30KB02TbU6DKxTPEXTwN8ARRtpUJxIIUdBL0G9H3JFIGvOYWOsmubW6CQQs6LpsKTZjzThl';
//Version
export const appVersion = '0.0.2';
