/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Container, Button, Text, StyleProvider, Title } from 'native-base';
import { View } from'react-native';
import { Agenda, LocaleConfig } from 'react-native-calendars';
import Icon from 'react-native-vector-icons/Ionicons';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import moment from 'moment';
import 'moment/locale/es';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../basisComponent.js';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import { actionEdition, actionViewer } from '../../config.js';
import { executeGetActionsByDate } from '../../data/action/action.js';
import { NavigationBar, ProgressScreen } from '../../component/index.js';
import {
  toolbarButtonColor,
  agendaKnobColor,
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
} from '../../color.js';
/*************
 * Constants *
 *************/
const MONTH_NAMES = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
const MONTH_NAMES_SHORT = ['Ene.','Feb.','Mar.','Abr.','May.','Jun.','Jul.','Ago.','Sep.','Oct.','Nov.','Dic.'];
const DAY_NAMES = ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
const DAY_NAMES_SHORT = ['Dom.','Lun.','Mar.','Mier.','Ju.','Vi.','Sab.'];
/**
 * Renderiza la vista de la agenda
 */
class Appointment extends BasisComponent {

  constructor(props) {
    super(props);
    //Se bindea aquí para mejorar el performance
    this._pop = this.pop.bind(this);
    this._renderItem = this.renderItem.bind(this);
    this._rowHasChanged = this.rowHasChanged.bind(this);
    this._renderKnob = this.renderKnob.bind(this);
    this._onDayPress = this.onDayPress.bind(this);
    this._onPressAction = (actionId, editableAction) => this.onPressAction.bind(this, actionId, editableAction);
    this._onActionEdited = this.onActionEdited.bind(this);
    this._renderEmptyData = this.renderEmptyData.bind(this);
    //Setea la configuración de la agenda para español
    LocaleConfig.locales['es'] = {
      monthNames: MONTH_NAMES,
      monthNamesShort: MONTH_NAMES_SHORT,
      dayNames: DAY_NAMES,
      dayNamesShort: DAY_NAMES_SHORT,
    };
    //Setea el idioma de la agenda a español
    LocaleConfig.defaultLocale = 'es';
    //Variable para saber si se edito una acción
    this.actionEdited;
    //Escuchador del navigator
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  
  componentWillReceiveProps(nextProps) {
    if (nextProps.statusOfGettingActionsByDate.wasAnError)
      this.showErrorMessage(
        nextProps.statusOfGettingActionsByDate.error &&
        nextProps.statusOfGettingActionsByDate.error.errorMessage ||
        'Hubo un problema al obtener las acciones para la fecha seleccionada'
      );
  }

  render() {
    
    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <View style={style.container}>
            <NavigationBar
              toolBarStyle={style.toolbar}
              leftButtonVisible={true}
              onPressLeftButton={this._pop}
              iconLeftButton={'md-arrow-back'}
              colorIconLeftButton={toolbarButtonColor}
              title={'Agenda'}
              titleStyle={style.title}/>
              
            <Agenda
              ref={'agenda'}
              items={this.props.actionsByDate}
              selected={moment(new Date()).format('YYYY-MM-DD')}
              renderItem={this._renderItem}
              rowHasChanged={this._rowHasChanged}
              renderEmptyData={this._renderEmptyData}
              theme={style.calendar}
              pastScrollRange={5}
              futureScrollRange={5}
              renderKnob={this._renderKnob}
              onDayPress={this._onDayPress}/>
          </View>
        </Container>
      </StyleProvider>
    );
  }
  /**
   * Renderiza el item el cual contiene los datos de una acción
   * @param  {object} item datos acerca del acción
   */
  renderItem(item) {

    return (
      <Button
        transparent
        block
        style={style.dateItem}
        onPress={this._onPressAction(item.id, item.editable)}>
        
        <View style={style.dateItemDataContainer}>
          <View style={item.editable ? style.editable : style.nonEditable}/>
          <View>
            <Title style={style.time}>{`${item.initTime}-${item.endTime}`}</Title>
            <Text numberOfLines={null} style={style.eventDescription}>{item.action}</Text>
          </View>
        </View>
      </Button>
    );
  }
  /**
   * Renderiza el icono para mostrar la lista
   * de meses
   */
  renderKnob() {
    
    return (
      <Icon name={'ios-arrow-down'} size={30} color={agendaKnobColor}/>
    );
  }
  /**
   * Renderiza componente para reemplazar al
   * activity indicator de la agenda
   */
  renderEmptyData() {
    
    return (
      this.props.statusOfGettingActionsByDate.actionsByDateAreBeingFetching
      ? <ProgressScreen
          backgroundColor={progressScreenNestedBackgroundColor}
          textColor={progressScreenTextColor}
          spinnerColor={progressScreenSpinnerColor}
          withBiggerToolbar={true}/>
      : <View style={style.emptyActionsContainer}>
          <Text style={style.emptyActionsText}>{'No hay acciones para la fecha seleccionada'}</Text>
        </View>
    );
  }
  /**
   * Obtiene las acciones de la fecha actual
   */
  componentDidMount() {
    this.props.executeGetActionsByDate(
      moment(new Date()).format('DD/MM/YYYY'),
      this.props.session.accessToken,
      this.props.session.refreshToken
    )
  }
  /**
   * Para mejorar el performance
   */
  rowHasChanged(r1, r2) {

    return r1.name !== r2.name;
  }
  /**
   * Esuchador que se dispara cuando un usuario
   * presiona sobre un día
   */
  onDayPress(dayPressed) {

    this.props.executeGetActionsByDate(
      moment(dayPressed.timestamp).add(1, 'days').format('DD/MM/YYYY'),
      this.props.session.accessToken,
      this.props.session.refreshToken
    )
  }
  /**
   * Esuchador que se dispara cuando un usuario
   * presiona sobre un evento o acción de la
   * agenda
   * @param  {string} actionId identificador de la acción
   * @param  {bool} editableAction bandera para saber si se puede editar la acción o no
   */
  onPressAction(actionId, editableAction) {
    if (editableAction)
      this.push(actionEdition, {
        actionId: actionId,
        onActionEdited: this._onActionEdited,
      });
    else
      this.push(actionViewer, {
        actionId: actionId,
      });
  }
  /**
   * Escuchador del navigator que se dispara cuando
   * esta vista va a ser visible y cuando se va 
   * a ocultar
   */
  onNavigatorEvent(event) {
    switch(event.id) {
      
      case 'willAppear':
        if (this.actionEdited) {
          this.refs.agenda.chooseDay(moment(new Date()).format('YYYY-MM-DD'));
          this.actionEdited = false;
        }
        break;
    }
  }
  /**
   * Escuchador que se dispara cuando se edito
   * una acción
   */
  onActionEdited() {
    this.actionEdited = true;
  }
}

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  actionsByDate: state.dataReducer.actionReducer.actionsByDate,
  statusOfGettingActionsByDate: state.dataReducer.actionReducer.statusOfGettingActionsByDate,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetActionsByDate: executeGetActionsByDate,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Appointment);