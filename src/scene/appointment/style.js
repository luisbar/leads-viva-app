/******************
 * Project import *
 ******************/
import {
  toolbarBackgroundColor,
  toolbarBorderColor,
  toolbarTextColor,
  calendarBackground,
  textSectionTitleColor,
  selectedDayBackgroundColor,
  selectedDayTextColor,
  todayTextColor,
  dayTextColor,
  textDisabledColor,
  dotColor,
  selectedDotColor,
  agendaKnobColor,
  monthTextColor,
  backgroundColor,
  agendaDayNumColor,
  agendaDayTextColor,
  agendaTodayColor,
  eventDescriptionText,
  agendaEmptyActionsText,
  eventEditable,
  eventNonEditable,
  eventContainer,
  eventTime,
} from '../../color.js';

export default {
  
  container: {
    flex: 1,
  },
  toolbar: {
    backgroundColor: toolbarBackgroundColor,
    borderColor: toolbarBorderColor,
  },
  title: {
    color: toolbarTextColor,
  },
  calendar: {
    calendarBackground: calendarBackground,//Color de fondo del calendario
    textSectionTitleColor: textSectionTitleColor,//Color de los labels de los dias (do, lu, etc)
    selectedDayBackgroundColor: selectedDayBackgroundColor,//Color de fondo de la fecha actualmente seleccionada
    selectedDayTextColor: selectedDayTextColor,//Color del label de la fecha actualmente seleccionada
    todayTextColor: todayTextColor,//Color del label del dia de la fecha actual
    dayTextColor: dayTextColor,//Color de los labels de los dias (1, 2, etc)
    textDisabledColor: textDisabledColor,//Color de los labels de los dias que esta desabilitados (1, 2, etc)
    dotColor: dotColor,//Color del indicador de que hay un evento en esa fecha
    selectedDotColor: selectedDotColor,//Color del indicador de que hay un evento en esa fecha, cuando esta seleccionado
    agendaKnobColor: agendaKnobColor,//Color del boton para mostrar la lista de calendarios
    monthTextColor: monthTextColor,//Color de los labels de los meses de las lista de calendarios
    textDayFontFamily: 'Museo300',
    textMonthFontFamily: 'Museo300',
    textDayHeaderFontFamily: 'Museo300',
    textDayFontSize: 16,
    textMonthFontSize: 16,
    textDayHeaderFontSize: 16,
    backgroundColor: backgroundColor,//Color de fondo de la agenda
    agendaDayNumColor: agendaDayNumColor,//Color de los labels de los dias de la agenda (1, 2, etc)
    agendaDayTextColor: agendaDayTextColor,//Color de los labels de los dias de la agenda (do, lun, etc) 
    agendaTodayColor: agendaTodayColor,//Color del label de la fecha actual de la agenda
  },
  emptyActionsContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  emptyActionsText: {
    textAlign: 'center',
    color: agendaEmptyActionsText,
  },
  dateItem: {
    backgroundColor: eventContainer,
    height: 'auto',
    marginTop: 15,
    marginRight: 10,
    marginBottom: 5,
    elevation: 2,
    padding: 0,
    borderRadius: 5,
  },
  dateItemDataContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  editable: {
    flex: 1,
    maxWidth: 10,
    backgroundColor: eventEditable,
    borderRadius: 5,
    marginHorizontal: 5,
  },
  nonEditable: {
    flex: 1,
    maxWidth: 10,
    backgroundColor: eventNonEditable,
    borderRadius: 5,
    marginHorizontal: 5,
  },
  time: {
    color: eventTime,
    textAlign: 'left',
    fontSize: 14
  },
  eventDescription: {
    color: eventDescriptionText,
    paddingLeft: 0,
    paddingRight: 0,
    maxWidth: '90%'
  },
}