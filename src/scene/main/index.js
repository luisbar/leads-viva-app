/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Container, Content, StyleProvider } from 'native-base';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
/******************
 * Project import *
 ******************/
import BasisComponent from '../basisComponent.js';
import { ProgressScreen } from '../../component/index.js';
import { executeGetSession } from '../../data/session/action.js';
import { progressScreenBackgroundColor, progressScreenSpinnerColor, progressScreenTextColor } from '../../color.js';
import Icon from '../../icon.js';
import Router from '../../router.js';
import { login, home } from '../../config.js';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import { logger, pushNotification } from '../../service/index.js';
/**
 * Renderiza la vista de inicio, ademas verifica si hay una sesión
 * activa y carga los iconos
 */
class Main extends BasisComponent {

  constructor(props) {
    super(props);
    //Desabilita el drawer
    this.props.navigator.setDrawerEnabled({
      side: 'left',
      enabled: false
    });
    //Crea un singleton para las rutas
    new Router(this.props.navigator);
    //Variables de la clase
    this.icon = new Icon();
  }
  /**
   * Si hay una sesión activa, la vista de home
   * sera desplegada, caso contrario el login sera desplegado
   */
  componentWillReceiveProps(nextProps) {
    pushNotification.onReceiveNotification();
    pushNotification.onNotificationPressed();
    pushNotification.onAppOpenedByPressingANotification()
    .then((wasOpenedByPressingANotification) => {
      if (nextProps.session)
        this.resetTo(home, {
          appWasOpenedByPressingANotification: wasOpenedByPressingANotification ? true : false,
        });
      else if (!nextProps.session &&
        !nextProps.statusOfGettingSession.wasAnError)
        Router.getInstance().resetTo(login);
    })
  }

  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <Content>
            <ProgressScreen
              backgroundColor={progressScreenBackgroundColor}
              textColor={progressScreenTextColor}
              spinnerColor={progressScreenSpinnerColor}/>
          </Content>
        </Container>
      </StyleProvider>
    );
  }
  /**
   * Invoca metodo para cargar los iconos,
   * luego verifica si ya hay una sesión activa
   */
  componentDidMount() {
    this.icon.populateIcons()
    .then(() => this.props.executeGetSession())
    .catch((error) => {
      this.showErrorMessage('No se pudo cargar los iconos');
      logger.recordError(String(error));
      logger.log('Main: error al cargar los iconos');
    });
  }
  /**
   * Verifica si hubo un error al ejecutar
   * el metodo executeGetSession
   */
  componentDidUpdate(prevProps, prevState) {
    if (this.props.statusOfGettingSession.wasAnError)
      this.showErrorMessage('Hubo un problema al obtener la sesión');
  }
}

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  statusOfGettingSession: state.dataReducer.sessionReducer.statusOfGettingSession,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetSession: executeGetSession,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Main);
