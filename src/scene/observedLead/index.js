/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { StyleProvider, Container, Content, Text, Fab, Button, Spinner } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { MenuProvider } from 'react-native-popup-menu';
import { View, FlatList, RefreshControl } from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../basisComponent.js';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import { leadViewer, actionsRecord, confirmModal } from '../../config.js';
import { NavigationBar, ProgressScreen, PopUpMenu } from '../../component/index.js';
import { executeGetObservedLeads, executeGetMoreObservedLeads, executeReOpenALead } from '../../data/lead/action.js';
import {
  toolbarButtonColor,
  popupMenuIconColor,
  popupMenuItemIconColor,
  popupMenuItemTextColor,
  progressScreenNestedBackgroundColor,
  progressScreenSpinnerColor,
  progressScreenTextColor,
  upButtonIconColor,
  refreshBackgroundColor,
  refreshColor,
  footerSpinnerColor,
} from '../../color.js';
/**
 * Renderiza vista para ver los leads observados
 */
class ObservedLead extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._pop = this.pop.bind(this);
    this._renderItem = this.renderItem.bind(this);
    this._onRefresh = this.executeGetObservedLeads.bind(this);
    this._handleLoadMore = this.handleLoadMore.bind(this);
    this._upButtonPressed = this.upButtonPressed.bind(this);
    this._executeGetMoreLeads = this.executeGetMoreObservedLeads.bind(this);
    this._reOpenALead = (observationId, leadNumber, leadName) => this.reOpenALead.bind(this, observationId, leadNumber, leadName);
    this._seeLead = (leadId) => this.seeLead.bind(this, leadId);
    this._seeActionsRecord = (leadId) => this.seeActionsRecord.bind(this, leadId);
    this._onScroll = this.onScroll.bind(this);
    //State
    this.state = {
      page: 1,//Pagina actual
      upButtonVisible: true,//Bandera para ocultar y mostrar el float button
    };
  }

  /**
   * Verifica si hubo un error al ejecutar
   * los metodos executeGetObservedLeads, executeGetMoreObservedLeads
   * y executeReOpenALead
   */
  componentWillReceiveProps(nextProps) {
    
    this.showOrDismissProgressModal(nextProps);
    this.showErrorMessageForReOpeningALead(nextProps);
    this.showSuccessMessageForReOpeningALead(this.props, nextProps);
    
    this.showErrorForGettingObservedLeads(nextProps);
    this.showErrorForGettingMoreObservedLeads(nextProps);
  }
  
  render() {

    return (
      <MenuProvider>
        <StyleProvider style={getTheme(material)}>
          <Container>
            <NavigationBar
              toolBarStyle={style.toolbar}
              leftButtonVisible={true}
              onPressLeftButton={this._pop}
              iconLeftButton={'md-arrow-back'}
              colorIconLeftButton={toolbarButtonColor}
              title={'Lead observados'}
              titleStyle={style.title}/>
            <Content contentContainerStyle={style.content}>
              {
                this.props.statusOfGettingObservedLeads.observedLeadsAreBeingFetching
                ? this.renderProgressScreen()
                : this.renderContent()
              }
            </Content>
          </Container>
        </StyleProvider>
      </MenuProvider>
    );
  }
  /**
   * Renderiza la vista de progreso mientras
   * se obtienen los leads observados
   */
  renderProgressScreen() {

    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbar={true}/>
    );
  }
  /**
   * Renderiza el contenido principal
   */
  renderContent() {
    
    return (
      <View style={style.secondaryContent}>
        {this.renderList()}
        {this.renderFab()}
      </View>
    );
  }
  /**
   * Renderiza la lista de leads observados
   */
  renderList() {
    
    return (
      this.props.observedLeads.length !== 0
      ? <View style={style.listContainer}>
          <FlatList
            style={style.list}
            ref={'flatList'}
            keyExtractor={item => item.id}
            data={this.props.observedLeads}
            ListHeaderComponent={this.renderHeader()}
            renderItem={this._renderItem}
            ListFooterComponent={this.renderFooter()}
            removeClippedSubviews={true}//Remueve los items que no estan a la vista
            initialNumToRender={10}//Cuantos elementos renderizar en el lote inicial, estos elementos nunca van a ser desmontados por que se los necesita para el metodo scrollToTop
            maxToRenderPerBatch={10}//Cantidad maxima de items a renderizar por lote
            windowSize={40}//Determina el maximo de items que se renderizaran fuera del area visible, en este caso, 10 arriba del area visible y 10 abajo del area visible
            onScroll={this._onScroll}
            refreshControl={
              <RefreshControl
                refreshing={this.props.statusOfGettingObservedLeads.observedLeadsAreBeingFetching}
                onRefresh={this._onRefresh}
                colors={[refreshColor]}
                progressBackgroundColor={refreshBackgroundColor}/>
             }/>
         </View>
      : <View style={style.empty}>
          <Text style={style.emptyText}>
            {'No hay información para mostrar'}
          </Text>
        </View>
    );
  }
  /**
   * Renderiza el header de la lista de leads observados
   */
  renderHeader() {

    return (<View style={style.listHeader}/>);
  }
  /**
   * Renderiza los items de la lista de leads observados
   * @param  {object} item objeto con los datos del lead observado
   */
  renderItem({item, index}) {

    return (
      <View
        key={item.id} style={(index + 1) % 2 === 0
          ? style.listItemPair
          : style.listItemOdd}>
        <View style={style.listItemDataContainer}>

          <View style={style.listItemIdAndStateContainer}>
            <Text
              numberOfLines={1}
              style={style.listItemId}>
              {item.number}
            </Text>

            <Text
              numberOfLines={1}
              style={[style.listItemText, style.listItemTextMinorWidth]}>
              {item.name}
            </Text>
          </View>

          <Text
            numberOfLines={1}
            style={style.listItemText}>
            {item.phone}
          </Text>
          
          <Text
            numberOfLines={1}
            style={style.listItemText}>
            {`Observado en ${item.observationDate}`}
          </Text>
          
          <View style={style.listItemJustificationContainer}>
            <Text style={style.listItemJustificationText}>
              {item.justification}
            </Text>
          </View>
        </View>
        {this.renderPopUpMenu(item.id, item.observationId, item.number, item.name)}
      </View>
    );
  }
  /**
   * Renderiza el footer de la lista de leads
   */
  renderFooter() {

    return (
      <View
        style={this.props.observedLeads.length % 2 === 0
          ? style.listPairFooter
          : style.listOddFooter}>
          {this.renderFooterButton()}
          {this.renderFooterSpinner()}
      </View>
    );
  }
  /**
   * Renderiza el boton del footer para obtener mas leads
   * observados
   */
  renderFooterButton() {
    
    return (
      !this.props.statusOfGettingMoreObservedLeads.moreObservedLeadsAreBeingFetching &&
      this.state.page < this.props.totalOfPagesOfObservedLeads &&
      <Button
        transparent
        full
        onPress={this._handleLoadMore}>
        <Text style={style.listFooterButton}>{'VER MAS'}</Text>
      </Button>
    );
  }
  /**
   * Renderiza el spinner que va en el footer,
   * el cual se muestra cuando se esta trayendo mas leads
   * observados
   */
  renderFooterSpinner() {
    
    return (
      this.props.statusOfGettingMoreObservedLeads.moreObservedLeadsAreBeingFetching &&
      <Spinner color={footerSpinnerColor}/>
    );
  }
  /**
   * Renderiza el float button para ir hacia arriba
   */
  renderFab() {
    
    return(
      this.props.observedLeads.length !== 0 && this.state.upButtonVisible &&
      <Fab
        position={'bottomRight'}
        style={style.upButton}
        onPress={this._upButtonPressed}>

        <Icon name={'keyboard-arrow-up'} size={20} color={upButtonIconColor}/>
      </Fab>
    );
  }
  /**
   * Renderiza el PopUpMenu para cada item
   */
  renderPopUpMenu(leadId, observationId, leadNumber, leadName) {
    let itemsMenu = [];
    itemsMenu.push(this.getPopPupMenuItem(this._reOpenALead(observationId, leadNumber, leadName), 'folder-open', 'Reaperturar lead'));
    itemsMenu.push(this.getPopPupMenuItem(this._seeLead(leadId), 'eye', 'Ver lead'));
    itemsMenu.push(this.getPopPupMenuItem(this._seeActionsRecord(leadId), 'clock', 'Historial de acciones'));

    return (
      <PopUpMenu
        menuIconColor={popupMenuIconColor}
        menuContainerStyle={style.popUpMenu}
        items={itemsMenu}/>
    );
  }
  /**
   * Invoca metodo para obtener los leads desde la api
   */
  componentDidMount() {
    this.executeGetObservedLeads();
  }
  /**
   * Escuchador que se dispara cuando el usuario
   * presiona el boton de traer mas leads observados
   */
   handleLoadMore() {
    this.setState({
      page: this.state.page + 1,
    }, this._executeGetMoreLeads);
   };
  /**
   * Escuchador que se dispara cuando el usuario presiona el
   * fab button para ir hacia arriba
   */
  upButtonPressed() {
    this.refs.flatList.scrollToItem({
      animated: true,
      item: this.props.observedLeads[0],
    });
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona
   * la opción de reaperturar lead del PopUpMenu
   */
  reOpenALead(observationId, leadNumber, leadName) {
    this.showTransparentModal(confirmModal, {
      onLeftButtonPressed: this.dismissModal.bind(this),
      onRightButtonPressed: this.onConfirmReOpening.bind(this, observationId),
      title: 'Confirmación',
      note: `¿Estás seguro de reaperturar el lead ${leadNumber} ${leadName}?`,
      showNoteInput: false,
    });
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona
   * la opción de ver lead del PopUpMenu
   */
  seeLead(leadId) {
    //Renderiza la vista para ver un lead
    this.push(leadViewer, {
      leadId: leadId,
    });
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona
   * la opción de historial de acciones del PopUpMenu
   */
  seeActionsRecord(leadId) {
    //Renderiza la vista para ver el historial de
    //acciones
    this.push(actionsRecord, {
      leadId: leadId,
    });
  }
  /**
   * Invoca a metodo para obtener los leads
   * observados
   */
  executeGetObservedLeads() {
    this.setState({
      page: 1,
    }, () => 
      this.props.executeGetObservedLeads(
        this.state.page,
        this.props.session.accessToken,
        this.props.session.refreshToken
      )
    );
  }
  /**
   * Invoca metodo para traer mas leads
   * observados
   */
  executeGetMoreObservedLeads() {
    this.props.executeGetMoreObservedLeads(
      this.state.page,
      this.props.session.accessToken,
      this.props.session.refreshToken
    );
  }
  /**
   * Retorna un objeto json que representa un item
   * del PopUpMenu
   * @param  {function} method metodo que se ejecutara cuando se presione el item
   * @param  {string} icon icono que estará en el item del PopUpMenu
   * @param  {string} text texto que estará en el item del PopUpMenu
   */
  getPopPupMenuItem(method, icon, text) {
    
    return {
      itemPressed: method,
      icon: icon,
      iconColor: popupMenuItemIconColor,
      text: text,
      textColor: popupMenuItemTextColor,
    };
  }
  /**
   * Escuchador que se confirmo la reapertura de un
   * lead observado
   */
  onConfirmReOpening(observationId) {
    this.dismissModal();
    this.props.executeReOpenALead(
      observationId,
      this.props.session.accessToken,
      this.props.session.refreshToken
    );
  }
  /**
   * Escuchador que se dispara cuando se hace scroll
   * al FlatList
   */
  onScroll(event) {
    const currentOffset = event.nativeEvent.contentOffset.y;
    const direction = currentOffset > this.previousOffset ? 'down' : 'up';
    this.previousOffset = currentOffset;

    if (this.state.upButtonVisible !== direction)
      this.setState({ upButtonVisible: direction === 'up' });
  }
  /**
   * Muestra u oculta el ProgressModal
   */
  showOrDismissProgressModal(nextProps) {
    if (nextProps.statusOfReOpeningALead.reOpeningALeadIsBeingExecuted)
      this.showProgressModal('Reaperturando lead...');
    else
      this.dismissModal();
  }
  /**
   * Muestra errores cuando se ejecuta el
   * metodo executeReOpenALead, si hubo problemas
   */
  showErrorMessageForReOpeningALead(nextProps) {
    if (nextProps.statusOfReOpeningALead.wasAnError &&
        nextProps.statusOfReOpeningALead.reOpeningALeadIsBeingExecuted)//Si hubo error
      this.showErrorMessage(
        nextProps.statusOfReOpeningALead.error &&
        nextProps.statusOfReOpeningALead.error.errorMessage ||
        'Hubo un problema al reaperturar el lead'
      )
  }
  /**
   * Muestra mensaje de exito si el metodo
   * executeReOpenALead se ejecuto exitosamente
   */
  showSuccessMessageForReOpeningALead(previousProps, nextProps) {
    if (!previousProps.statusOfReOpeningALead.wasAnError &&
        !nextProps.statusOfReOpeningALead.reOpeningALeadIsBeingExecuted &&
        !nextProps.statusOfGettingObservedLeads.observedLeadsAreBeingFetching &&
        !previousProps.statusOfGettingObservedLeads.observedLeadsAreBeingFetching &&
        !nextProps.statusOfGettingMoreObservedLeads.moreObservedLeadsAreBeingFetching &&
        !previousProps.statusOfGettingMoreObservedLeads.moreObservedLeadsAreBeingFetching) {//Si no hubo error

      this.showSuccessMessage('El lead fue reaperturado exitosamente');
      this.executeGetObservedLeads();
    }
  }
  /**
   * Muestra mensaje de error despues de que se ejecuta
   * el metodo executeGetObservedLeads, si es que hubo error
   */
  showErrorForGettingObservedLeads(nextProps) {
    if (nextProps.statusOfGettingObservedLeads.wasAnError &&
        nextProps.statusOfGettingObservedLeads.observedLeadsAreBeingFetching)
      this.showErrorMessage(
        nextProps.statusOfGettingObservedLeads.error &&
        nextProps.statusOfGettingObservedLeads.error.errorMessage ||
        'Hubo un problema al obtener los leads observados'
      );
  }
  /**
   * Muestra mensaje de error despues de que se ejecuta
   * el metodo executeGetMoreObservedLeads, si es que hubo error
   */
  showErrorForGettingMoreObservedLeads(nextProps) {
    if (nextProps.statusOfGettingMoreObservedLeads.wasAnError &&
        nextProps.statusOfGettingMoreObservedLeads.moreObservedLeadsAreBeingFetching) {
      this.setState({ page: this.state.page - 1 });
      this.showErrorMessage(
        nextProps.statusOfGettingMoreObservedLeads.error &&
        nextProps.statusOfGettingMoreObservedLeads.error.errorMessage ||
        'Hubo un problema al obtener mas leads observados'
      );
    }
  }
}

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  observedLeads: state.dataReducer.leadReducer.observedLeads,
  totalOfPagesOfObservedLeads: state.dataReducer.leadReducer.totalOfPagesOfObservedLeads,
  statusOfGettingObservedLeads: state.dataReducer.leadReducer.statusOfGettingObservedLeads,
  statusOfGettingMoreObservedLeads: state.dataReducer.leadReducer.statusOfGettingMoreObservedLeads,
  statusOfReOpeningALead: state.dataReducer.leadReducer.statusOfReOpeningALead,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetObservedLeads: executeGetObservedLeads,
  executeGetMoreObservedLeads: executeGetMoreObservedLeads,
  executeReOpenALead: executeReOpenALead,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ObservedLead);