/******************
 * Project import *
 ******************/
import {
  toolbarBackgroundColor,
  toolbarBorderColor,
  toolbarTextColor,
  listItemOddBackgroundColor,
  listItemPairBackgroundColor,
  listItemIdBackgroundColor,
  listItemIdTextColor,
  listItemJustificationContainer,
  listItemJustificationText,
  emptyListText,
  listItemTextColor,
  popupMenuBackgroundColor,
  upButtonBackgroundColor,
  footerButtonTextColor,
} from '../../color.js';

export default {
  
  toolbar: {
    backgroundColor: toolbarBackgroundColor,
    borderColor: toolbarBorderColor,
  },
  title: {
    color: toolbarTextColor,
  },
  content: {
    flex: 1,
  },
  secondaryContent: {
    flex: 1,
  },
  listContainer: {
    flex: 1,
  },
  list: {
    marginLeft: 10,
    marginRight: 10,
  },
  listHeader: {
    backgroundColor: listItemOddBackgroundColor,
    height: 10,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: 10,
  },
  listPairFooter: {
    backgroundColor: listItemPairBackgroundColor,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    marginBottom: 10,
    minHeight: 10,
  },
  listOddFooter: {
    backgroundColor: listItemOddBackgroundColor,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    marginBottom: 10,
    minHeight: 10,
  },
  listFooterButton: {
    color: footerButtonTextColor,
  },
  listItemPair: {
    backgroundColor: listItemPairBackgroundColor,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  listItemOdd: {
    backgroundColor: listItemOddBackgroundColor,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  listItemDataContainer: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    minWidth: '90%',
    maxWidth: '90%'
  },
  listItemIdAndStateContainer: {
    flexDirection: 'row',
  },
  listItemId: {
    maxHeight: 23,
    marginLeft: 10,
    marginTop: 5,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: listItemIdBackgroundColor,
    color: listItemIdTextColor,
    borderRadius: 5,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  listItemText: {
    maxWidth: '100%',
    color: listItemTextColor,
    marginTop: 5,
    marginLeft: 10,
    marginBottom: 5,
  },
  listItemTextMinorWidth: {
    maxWidth: '80%',
  },
  listItemJustificationContainer: {
    width: '100%',
    alignSelf: 'flex-start',
    flexWrap: 'wrap',
    flexDirection: 'row',
    padding: 10,
    marginBottom: 10,
    marginLeft: 10,
    backgroundColor: listItemJustificationContainer,
    borderRadius: 10,
  },
  listItemJustificationText: {
    color: listItemJustificationText,
  },
  popUpMenu: {
    borderRadius: 10,
    backgroundColor: popupMenuBackgroundColor,
    width: 'auto',
  },
  upButton: {
    backgroundColor: upButtonBackgroundColor,
  },
  empty: {
    flex: 1,
    justifyContent: 'center',
  },
  emptyText: {
    color: emptyListText,
    textAlign: 'center',
  }
}