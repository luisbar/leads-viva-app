/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Button, Text, Container, StyleProvider } from 'native-base';
import { View, PermissionsAndroid } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { TagSelect } from 'react-native-tag-select';

import ImageResizer from 'react-native-image-resizer';

import geolib from 'geolib';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../basisComponent.js';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import { NavigationBar } from '../../component/index.js';
import { toolbarButtonColor } from '../../color.js';
import { logger } from '../../service/index.js';
import { confirmModal } from '../../config.js';
/**
 * Renderiza la camara
 */
class Camera extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Referencia de componentes
    this.camera;
    this.tagSelect;
    //Se bindea los escuchadores aquí para mejorar el performance
    this._cameraRef = (reference) => this.camera = reference;
    this._tagRef = (reference) => this.tagSelect = reference;
    this._onClose = this.onClose.bind(this);
    this._takePicture = this.takePicture.bind(this);
    this._getPicture = this.getPicture.bind(this);
    this._checkIfLocationPermissionHasBeenGranted = this.checkIfLocationPermissionHasBeenGranted.bind(this);
    //tipos de fotos
    this.pictureTypes = this.props.pictureTypes.map((item) => {
      
      return {
        id: item.id,
        label: item.name,
        calculateDistance: item.calculateDistance,
      };
    })
    //State
    this.state = {
      latitude: this.props.latitude,
      longitude: this.props.longitude,
      isTakingPicture: false,
    };
  }

  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <View style={style.container}>
            <NavigationBar
              toolBarStyle={style.toolbar}
              leftButtonVisible={true}
              onPressLeftButton={this._onClose}
              iconLeftButton={'md-arrow-back'}
              colorIconLeftButton={toolbarButtonColor}
              title={'Camara'}
              titleStyle={style.title}/>
            
            <View style={style.tagsContainer}>
              <TagSelect
                data={this.pictureTypes}
                ref={this._tagRef}
                itemStyle={style.tagItem}
                itemLabelStyle={style.tagItemLabel}
                itemStyleSelected={style.tagItemSelected}
                itemLabelStyleSelected={style.tagItemLabelSelected}
                max={1}
                onMaxError={() => console.log('nothing')}/>
            </View>

            <RNCamera
              ref={this._cameraRef}
              style={style.camera}
              type={RNCamera.Constants.Type.back}
              flashMode={RNCamera.Constants.FlashMode.auto}
              fixOrientation={true}
              permissionDialogTitle={'Permiso para utilizar la camara'}
              permissionDialogMessage={'Esta aplicación necesita acceso a la camara para funcionar correctamente'}/>
            
            {
              !this.state.isTakingPicture &&
              <Button
                onPress={this._takePicture}
                style={style.cameraButton}>
                <Text style={style.cameraButtonText}>{'Capturar'}</Text>
              </Button>
            }
          </View>
        </Container>
      </StyleProvider>
    );
  }
  /**
   * Escuchador que se dispara cuado el usuario
   * presiona el boton para cerrar la camara
   */
  onClose() {
    this.pop();
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona
   * el boton para tomar una foto
   */
  takePicture() {
    if (!this.camera)
      return this.showInformativeMessage('No se pudo tomar la foto');
      
    if (this.tagSelect.totalSelected === 0 )
      return this.showInformativeMessage('Debe seleccionar un tipo de foto');
     
    this.setState({ isTakingPicture: true }, !this.state.latitude || !this.state.longitude
    ? this._checkIfLocationPermissionHasBeenGranted
    : this._getPicture);
  };
  /**
   * Verifica si el usuario ha concedido permiso para utilizar el gps,
   * si es así, se obtiene la ubicación actual
   */
  async checkIfLocationPermissionHasBeenGranted() {
    const permission = PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION;
    //Verifica si la app tiene permiso para utilizar el gps
    PermissionsAndroid.check(permission)
    .then((isPermissionGranted) => isPermissionGranted || PermissionsAndroid.request(permission))
    .then((data) => {
      //Si tiene permiso obtiene la ubicación
      if (typeof data === 'boolean') {//Si anteriormente se concedió permiso 
        this.showInformativeMessage('Obteniendo ubicación', );
        this.requestCurrentLocation();
      } else if (data === PermissionsAndroid.RESULTS.GRANTED) {//Si recientemente se concedió permiso
        this.showInformativeMessage('Obteniendo ubicación');
        this.requestCurrentLocation();
      } else {
        this.setState({ isTakingPicture: false },() => {
          this.showErrorMessage('Debe conceder permiso a la aplicación para utilizar el gps')
        });
      }
    })
  }
  /**
   * Obtiene la ubicación actual
   */
  requestCurrentLocation() {
    this.getCurrentPosition({ enableHighAccuracy: true, timeout: 10000, maximumAge: 1000 })
    .then(({coords}) => this.setState({ latitude: coords.latitude, longitude: coords.longitude }, this._getPicture))
    .catch((error) => {
      this.setState({ isTakingPicture: false }, () => {
        if (error.code === 2)
          this.showErrorMessage('Debe habilitar el gps');
        else {
          this.showErrorMessage('Hubo un problema obteniendo la ubicación');
          logger.recordError(String(error));
          logger.log('Camera: error al obtener la ubicación');
        }
      });
    });
  }
  /**
   * Obtiene la ubicación actual del usuario
   * @param  {object} options opciones para obtener la ubicación
   */
  getCurrentPosition(options) {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resolve, reject, options);
    });
  }
  /**
   * Metodo que obtiene la foto
   */
  getPicture() {
    //Opciones de la foto  
    const options = { quality: 1.0, base64: true };
    //Obtengo la distancia
    const distance = this.getDistanceInMeters();
    //Se toma la foto e.g. photo: { base64: , uri: , height: , width: }
    this.camera.takePictureAsync(options)
    .then((photo) =>       // Restablece el tamaño de la imagen
      ImageResizer.createResizedImage(photo.uri,
        1010,//Width
        1347,//Height
        'JPEG',
        70,//Calidad = 0-100
        0,//Rotation = 0-10
        null)//Donde será almacenada, sí es nulo entonces es almacenada en la cache
    )
    .then((photoResized) => {//Se actualiza el state
      if (this.props.maximumDistance &&
          this.props.maximumDistance.distanceInMeters &&
          this.tagSelect.itemsSelected[0].calculateDistance) {//Si hay que calcular distancia
        //Si la distancia de la ubicación de la foto con la ubicacion del lead es mayor a la permitida
        if (this.props.maximumDistance &&
            distance > this.props.maximumDistance.distanceInMeters)
          
          this.showTransparentModal(confirmModal, {
            onLeftButtonPressed: this.onCloseConfirmModalAndCamera.bind(this),
            onRightButtonPressed: this.onPictureTaken.bind(
              this,
              `data:image/${photoResized.uri.split('.').pop()};base64,${photoResized.base64}`,
              this.tagSelect.itemsSelected[0].id,
              this.tagSelect.itemsSelected[0].label,
              new Date().getTime(),
              this.state.latitude,
              this.state.longitude,
              distance
            ),
            title: 'Confirmación',
            note: `La distancia entre la foto de ${this.tagSelect.itemsSelected[0].id === 1 ? 'la' : 'el'} ${this.tagSelect.itemsSelected[0].label.toLowerCase()} y la ubicación excede los ${this.props.maximumDistance.distanceInMeters} metros permitidos, ¿Desea guardar de todas formas?`,
            showNoteInput: false,
          });
        else//Si la distancia de la ubicación de la foto con la ubicacion del lead es menor igual a la permitida
        
          this.onPictureTaken(
            `data:image/${photoResized.uri.split('.').pop()};base64,${photoResized.base64}`,
            this.tagSelect.itemsSelected[0].id,
            this.tagSelect.itemsSelected[0].label,
            new Date().getTime(),
            this.state.latitude,
            this.state.longitude,
            distance
          );
      } else//Si no hay que calcular distancia
        
        this.onPictureTaken(
          `data:image/${photoResized.uri.split('.').pop()};base64,${photoResized.base64}`,
          this.tagSelect.itemsSelected[0].id,
          this.tagSelect.itemsSelected[0].label,
          new Date().getTime(),
          this.state.latitude,
          this.state.longitude,
          distance
        );
    })
    .catch((error) => {
      this.setState({ isTakingPicture: false }, () => {
        this.showErrorMessage('No se pudo tomar la foto');
        logger.recordError(String(error));
        logger.log('Camera: error al tomar la foto');
      })
    });
  }
  /**
   * Retorna la distancia en metros con la ubicación del
   * domicilio del cliente y la ubicacion de la foto que se esta tomando
   */
  getDistanceInMeters() {
    
    return geolib.getDistance(
      this.getTruncatedCoordinate({ latitude: this.props.leadLatitude, longitude: this.props.leadLongitude }),
      this.getTruncatedCoordinate({ latitude: this.state.latitude, longitude: this.state.longitude })
    );
  }
  /**
   * Dispara el escuchador onPictureTaken pasado como props
   */
  onPictureTaken(
    picture,
    pictureTypeId,
    pictureTypeName,
    date,
    latitude,
    longitude,
    distance
  ) {
    this.props.onPictureTaken({
        picture: picture,
        pictureTypeId: pictureTypeId,
        pictureTypeName: pictureTypeName,
        date: date,
        latitude: latitude,
        longitude: longitude,
        distance: distance,
      }, 
      latitude,
      longitude
    );
    this.dismissModal();
    this.pop();
  }
  /**
   * Cierrar el ConfirmModal y la Camara
   */
  onCloseConfirmModalAndCamera() {
    this.dismissModal();
    this.pop();
  }
  /**
   * Redondea la latitud y longitud
   */
  getTruncatedCoordinate(coordinate) {

    return {
      latitude: parseFloat(this.truncate(coordinate.latitude)),
      longitude: parseFloat(this.truncate(coordinate.longitude))
    };
  }
  /**
   * Retorna un float truncado
   */
  truncate(floatToTruncate) {
    let floatSplited = String(floatToTruncate).split('.');
    let decimalsTruncated = floatSplited[1].substring(0, this.props.maxDecimalQuantityOfLatlng);
    
    return `${floatSplited[0]}.${decimalsTruncated}`;
  }
}

Camera.propTypes = {
  onPictureTaken: PropTypes.func.isRequired,//Escuchador que es disparado cuando se persiona el boton para tomar una foto
  pictureTypes: PropTypes.array.isRequired,//Array de los tipos de fotos disponibles
  pictures: PropTypes.array.isRequired,//Array de fotos tomadas
  latitude: PropTypes.number,//Latitud previamente capturada por esta clase
  longitude: PropTypes.number,//Longitud previamente capturada por esta clase
  maximumDistance: PropTypes.object,//Objeto que contiene la maxima distancia de la foto tomada a la ubicación del lead
  leadLatitude: PropTypes.number,//Latitud del lead
  leadLongitude: PropTypes.number,//Longitude del lead
  maxDecimalQuantityOfLatlng: PropTypes.number,//Numero maximo de decimales para la latitud y longitud
}

export default Camera;