/******************
 * Project import *
 ******************/
import {
  toolbarBackgroundColor,
  toolbarBorderColor,
  toolbarTextColor,
  tagsContainerBackground,
  tagBackgroundColor,
  tagLabelColor,
  selectedTagBackgroundColor,
  selectedTagLabelColor,
  cameraButtonBackgroundColor,
  cameraButtonTextColor,
} from '../../color.js';

export default {
  
  toolbar: {
    backgroundColor: toolbarBackgroundColor,
    borderColor: toolbarBorderColor,
  },
  title: {
    color: toolbarTextColor,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  tagsContainer: {
    backgroundColor: tagsContainerBackground,
  },
  tagItem: {
    backgroundColor: tagBackgroundColor,
  },
  tagItemLabel: {
    color: tagLabelColor,
    fontFamily: 'Museo300',
  },
  tagItemSelected: {
    backgroundColor: selectedTagBackgroundColor,
  },
  tagItemLabelSelected: {
    color: selectedTagLabelColor,
  },
  camera: {
    flex: 1,
  },
  cameraButton: {
    padding: 15,
    alignSelf: 'center',
    position: 'absolute',
    bottom: 15,
    backgroundColor: cameraButtonBackgroundColor,
    borderRadius: 20,
  },
  cameraButtonText: {
    color: cameraButtonTextColor,
  },
}