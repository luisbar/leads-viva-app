/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Container, Content, StyleProvider } from 'native-base';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
/******************
 * Project import *
 ******************/
import BasisComponent from '../basisComponent.js';
import { NavigationBar, FormWithAllInputs } from '../../component/index.js';
import { executeChangePassword } from '../../data/session/action.js';
import style from './style.js';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import {
  toolbarButtonColor,
  toolbarButtonColorWithBackground,
  inputTextColor,
  inputLabelColor,
  inputPlaceholderColor,
  inputBackgroundColor,
} from '../../color.js';
/*************
 * Constants *
 *************/
const CURRENT_PASSWORD = 'currentPassword';
const NEW_PASSWORD = 'newPassword';
const CONFIRMATION_OF_NEW_PASSWORD = 'confirmationOfNewPassword';
/**
 * Renderiza la vista para cambiar la contraseña
 */
class Configuration extends BasisComponent {

  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._pop = this.pop.bind(this);
    this._onChangePassword = this.onChangePassword.bind(this);
    this._onFieldsOk = this.onFieldsOk.bind(this);
    this._onEmptyFields = this.onEmptyFields.bind(this);
    //State
    this.state = {
      currentPasswordInputErrors: [],
      newPasswordInputErrors: [],
      confirmationOfNewPasswordInputErrors: [],
    };
    //Array de botones que se renderizaran en el navigation bar
    this.rightButtons = [
      {
        onPressRightButton: this._onChangePassword,
        iconRightButton: 'md-checkmark',
        colorIconRightButton: toolbarButtonColorWithBackground,
        rightButtonIconStyle: style.rightButtonsIcons,
      },
    ];
  }

  /**
   * Verifica si hubo un error al ejecutar
   * el metodo executeChangePassword para mostrar un mensaje de error
   * y si no hubo error para mostrar un mensaje de exito
   */
  componentWillReceiveProps(nextProps) {
    this.showErrorMessageForChangingPassword(nextProps);
    this.showSuccessMessageForChangingPassword(this.props, nextProps);
  }

  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <NavigationBar
            toolBarStyle={style.toolbar}
            leftButtonVisible={true}
            onPressLeftButton={this._pop}
            iconLeftButton={'md-arrow-back'}
            colorIconLeftButton={toolbarButtonColor}
            title={'Cambiar contraseña'}
            titleStyle={style.title}
            rightButtonsVisible={true}
            rightButtons={this.rightButtons}/>
          <Content>
            <FormWithAllInputs
              ref={'form'}
              fields={this.getFields()}
              onFieldsOk={this._onFieldsOk}
              onEmptyFields={this._onEmptyFields}/>
          </Content>
        </Container>
      </StyleProvider>
    );
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona
   * el boton para confirmar el cambio de la contraseña
   */
  onChangePassword() {
    this.refs.form.validateForm();
    this.showInformativeMessage('La contraseña esta siendo cambiada, espere un momento por favor.');
  }
  /**
   * Escuchador que se dispara despues de ejecutar el metodo
   * validateForm del componente FormWithAllInputs,
   * siempre y cuando los campos requeridos cumplan
   * con la validación
   * @param  {object} inputs objeto que contiene el valor
   * y estado de todos los inputs
   */
  onFieldsOk(inputs) {
    this.props.executeChangePassword(
      inputs[CURRENT_PASSWORD].value,
      inputs[NEW_PASSWORD].value,
      inputs[CONFIRMATION_OF_NEW_PASSWORD].value,
      this.props.session.accessToken, this.props.session.refreshToken);
  }
  /**
   * Escuchador que se dispara despues de ejecutar el metodo
   * validateForm del componente FormWithAllInputs,
   * siempre y cuando los campos requeridos no cumplan con la validación
   */
  onEmptyFields(inputs) {
    this.props.executeChangePassword(
      inputs[CURRENT_PASSWORD].value,
      inputs[NEW_PASSWORD].value,
      inputs[CONFIRMATION_OF_NEW_PASSWORD].value,
      this.props.session.accessToken, this.props.session.refreshToken);
  }
  /**
   * Retorna un json array, cada elemento representa un input del form
   */
  getFields() {
    return [
      {
        id: CURRENT_PASSWORD,
        isRequired: false,
        type: 'passwordInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Contraseña actual',
        placeholder: '***********',
        minLength: 1,
        maxLength: 128,
        keyboardType: 'password',
        secureTextEntry: true,
        returnKeyType: 'next',
        editable: true,
        autoCapitalize: 'none',
        errors: this.state.currentPasswordInputErrors,
        onSubmitEditing: NEW_PASSWORD,
        visible: true,
      },
      {
        id: NEW_PASSWORD,
        isRequired: false,
        type: 'passwordInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Nueva contraseña',
        placeholder: '***********',
        minLength: 1,
        maxLength: 128,
        keyboardType: 'password',
        secureTextEntry: true,
        returnKeyType: 'next',
        editable: true,
        autoCapitalize: 'none',
        errors: this.state.newPasswordInputErrors,
        onSubmitEditing: CONFIRMATION_OF_NEW_PASSWORD,
        visible: true,
      },
      {
        id: CONFIRMATION_OF_NEW_PASSWORD,
        isRequired: false,
        type: 'passwordInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Repetir contraseña',
        placeholder: '***********',
        minLength: 1,
        maxLength: 128,
        keyboardType: 'password',
        secureTextEntry: true,
        returnKeyType: 'done',
        editable: true,
        autoCapitalize: 'none',
        errors: this.state.confirmationOfNewPasswordInputErrors,
        visible: true,
      },
    ];
  }
  /**
   * Muestra los errores despues de ejecutar
   * el metodo executeChangePassword, si es que
   * hubo errores
   */
  showErrorMessageForChangingPassword(nextProps) {
    if (nextProps.session &&//Para que no se muestre el snackbar cuando se cierra sesión, ya que se modifica el objeto sesión
        nextProps.statusOfChangingPassword.wasAnError &&
        nextProps.statusOfChangingPassword.changePasswordIsBeingExecuted) {//Si hubo error
      //Muestra mensaje de error
      this.showErrorMessage(
        nextProps.statusOfChangingPassword.error &&
        nextProps.statusOfChangingPassword.error.errorMessage ||
        'Hubo un problema al cambiar la contraseña'
      );
      //Setea los errores a los inputs
      if (nextProps.statusOfChangingPassword.error.fieldErrors)
        this.setState({
          currentPasswordInputErrors: nextProps.statusOfChangingPassword.error.fieldErrors.currentPassword,
          newPasswordInputErrors: nextProps.statusOfChangingPassword.error.fieldErrors.newPassword,
          confirmationOfNewPasswordInputErrors: nextProps.statusOfChangingPassword.error.fieldErrors.confirmationOfNewPassword,
        });
    }
  }
  /**
   * Muestra mensaje de exito despues de
   * ejecutar el metodo executeChangePassword, si todo
   * salio bien
   */
  showSuccessMessageForChangingPassword(previousProps, nextProps) {
    if (nextProps.session &&//Para que no se muestre el snackbar cuando se cierra sesión, ya que se modifica el objeto sesión
        !previousProps.statusOfChangingPassword.wasAnError &&
        !nextProps.statusOfChangingPassword.changePasswordIsBeingExecuted) {//Si no hubo error

      this.showSuccessMessage('La contraseña fue cambiada exitosamente');
      //Limpia los errores de los inputs
      this.setState({
        currentPasswordInputErrors: [],
        newPasswordInputErrors: [],
        confirmationOfNewPasswordInputErrors: [],
      });
    }
  }
}

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  statusOfChangingPassword: state.dataReducer.sessionReducer.statusOfChangingPassword,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeChangePassword: executeChangePassword,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Configuration);
