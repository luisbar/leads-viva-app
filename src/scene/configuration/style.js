/******************
 * Project import *
 ******************/
import {
  toolbarBackgroundColor,
  toolbarBorderColor,
  toolbarTextColor,
  toolbarButtonBackgroundColor,
} from '../../color.js';

export default {
  
  toolbar: {
    backgroundColor: toolbarBackgroundColor,
    borderColor: toolbarBorderColor,
  },
  title: {
    color: toolbarTextColor,
  },
  rightButtonsIcons: {
    backgroundColor: toolbarButtonBackgroundColor,
    borderRadius: 20,
    width: 35,
    height: 35,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
}