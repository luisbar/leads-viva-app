/***********************
 * Node modules import *
 ***********************/
import { Dimensions } from 'react-native';
/******************
 * Project import *
 ******************/
import {
  commentBubbleBackground,
  commentBubbleMessageText,
} from '../../../../color.js';
/*************
 * Constants *
 *************/
const SCREEN_WIDTH = Dimensions.get('screen').width;
 
export default {
  
  bubble: {
    backgroundColor: commentBubbleBackground,
    borderRadius: 10,
    flex: 1,
    minHeight: 50,
    maxWidth:  SCREEN_WIDTH / 1.7,
    padding: 5,
    marginBottom: 10,
  },
  bubbleMessage: {
    color: commentBubbleMessageText,
    marginVertical: 5,
  },
  bubbleDate: {
    textAlign: 'right',
  },
}