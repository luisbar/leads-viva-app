/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Text } from 'native-base';
import { View } from 'react-native';
import { Bubble } from 'react-native-gifted-chat'

import moment from 'moment';
import 'moment/locale/es';
/******************
 * Project import *
 ******************/
import style from './style.js';
/**
 * Renderiza los contenedores de los mensajes
 * del chat
 */
class ChatBubble extends Bubble {

  render() {

    return (
      <View
        style={style.bubble}>
        
        <Text 
          note
          numberOfLines={1}>
          {this.props.currentMessage.user.name}
        </Text>
        
        <Text
          style={style.bubbleMessage}>
          {this.props.currentMessage.text}
        </Text>
        
        <Text
          note
          numberOfLines={1}
          style={style.bubbleDate}>
          {moment(this.props.currentMessage.createdAt).fromNow()}
        </Text>
      </View>
    );
  }
}

export default ChatBubble;