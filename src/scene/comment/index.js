/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Container, Header, Text, Icon, StyleProvider } from 'native-base';
import { View } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import { GiftedChat, Send } from 'react-native-gifted-chat'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../basisComponent.js';
import ChatBubble from './component/chatBubble/index.js';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import { CHAT_AVATAR_URI } from '../../config.js';
import { NavigationBar, ProgressScreen } from '../../component/index.js';
import { executeGetComments, executeSaveComment } from '../../data/comment/action.js';
import {
  toolbarButtonColor,
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
} from '../../color.js';
/**
 * Muestra los comentarios de un lead
 */
class Comment extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onClose = this.onClose.bind(this);
    this._onSaveComment = this.onSaveComment.bind(this);
    this._renderProgressScreen = this.renderProgressScreen.bind(this);
    this._renderBubble = this.renderBubble.bind(this);
    this._renderSendButton = this.renderSendButton.bind(this);
  }
  /**
   * Muestra los mensajes de erro, si es que hubo error
   */
  componentWillReceiveProps(nextProps) {
    const previousProps = this.props;
    
    if (nextProps.statusOfGettingComments.wasAnError &&
        nextProps.statusOfGettingComments.commentsAreBeingFetching)//Si hubo error
      this.showErrorMessage(
        nextProps.statusOfGettingComments.error &&
        nextProps.statusOfGettingComments.error.errorMessage ||
        'No se pudo obtener los comentarios, intente nuevamente'
      );
      
    if (nextProps.statusOfSavingComment.wasAnError &&
        nextProps.statusOfSavingComment.commentIsBeingSaving)//Si hubo error
      this.showErrorMessage(
        nextProps.statusOfSavingComment.error &&
        nextProps.statusOfSavingComment.error.errorMessage ||
        'No se pudo guardar el comentario, intente nuevamente'
      );
  }

  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          {this.renderNavigationBar()}
          {this.renderSubHeader()}
          {this.renderContent()}
        </Container>
      </StyleProvider>
    );
  }
  /**
   * Renderiza el navigation bar
   */
  renderNavigationBar() {
    
    return (
      <NavigationBar
        toolBarStyle={style.toolbar}
        leftButtonVisible={true}
        onPressLeftButton={this._onClose}
        iconLeftButton={'md-arrow-back'}
        colorIconLeftButton={toolbarButtonColor}
        title={'Comentarios'}
        titleStyle={style.title}/>
    );
  }
  /**
   * Renderiza el sub header, es donde se muestra
   * información del lead
   */
  renderSubHeader() {
    
    return (
      this.props.lead &&
      <Header
        style={style.subHeader}>
        <Text style={style.subHeaderText}>{`Comentarios de #${this.props.lead.number}`}</Text>
        <Text style={style.subHeaderText}>{this.props.lead.name}</Text>
      </Header>
    );
  }
  /**
   * Renderiza el contenido principal
   */
  renderContent() {
    
    return (
      <View style={style.content}>
      {
        this.props.statusOfGettingComments.commentsAreBeingFetching
        ? this.renderProgressScreen()
        : this.renderGiftedChat()
      }  
      </View>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {

    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbar={true}/>
    );
  }
  /**
   * Renderiza el contenido de la vista de comentarios
   */
  renderGiftedChat() {
    
    return (
      <GiftedChat
        messages={this.props.comments}
        locale={'es'}
        onSend={this._onSaveComment}
        placeholder={'Nuevo comentario'}
        showUserAvatar={true}
        showAvatarForEveryMessage={true}
        renderLoading={this._renderProgressScreen}
        renderBubble={this._renderBubble}
        renderSend={this._renderSendButton}
        textInputProps={style.textInputGiftedChat}
        renderAvatarOnTop={true}
        user={this.getUser()}/>
    );
  }
  /**
   * Renderiza el contenedor
   * de los mensajes del GiftedChat
   */
  renderBubble(props) {
    
    return (
      <ChatBubble {...props}/>
    );
  }
  /**
   * Renderiza el boton para enviar
   * el mensaje
   */
  renderSendButton(props) {
    
    return (
      <Send
      {...props}>
        <View style={style.sendButton}>
          <Icon
            style={style.sendButtonIcon}
            name={'md-send'}
            size={30}/>
        </View>
      </Send>
    );
  }
  /**
   * Obtiene los comentarios
   */
  componentDidMount() {
    this.props.executeGetComments(
      this.props.leadId,
      this.props.session.accessToken,
      this.props.session.refreshToken
    );
  }
  /**
   * Escuchador que se dispara cuando se presiona
   * el boton para cerrar la vista
   */
  onClose() {
    this.pop();
  }
  /**
   * Escuchador que se dispara cuando el usuario
   * presiona el boton para guardar un
   * comentario
   */
  onSaveComment(message) {
    this.props.executeSaveComment(
      this.props.leadId,
      message[0].text,
      this.props.session.accessToken,
      this.props.session.refreshToken
    );
  }
  /**
   * Retorna los datos del usuario
   * logueado
   */
  getUser() {
    
    return {
      _id: this.props.session.username,
      name: this.props.session.username,
      avatar: CHAT_AVATAR_URI,
    };
  }
}

Comment.propTypes = {
  leadId: PropTypes.string.isRequired,//Id del lead del cual se quiere obtener los comentarios
};

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  comments: state.dataReducer.commentReducer.comments,
  lead: state.dataReducer.commentReducer.lead,
  statusOfGettingComments: state.dataReducer.commentReducer.statusOfGettingComments,
  statusOfSavingComment: state.dataReducer.commentReducer.statusOfSavingComment,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetComments: executeGetComments,
  executeSaveComment: executeSaveComment,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Comment);