/******************
 * Project import *
 ******************/
import {
  toolbarBackgroundColor,
  toolbarBorderColor,
  toolbarTextColor,
  commentSubHeaderBackground,
  commentSubHeaderText,
  commentSendButtonIcon,
} from '../../color.js';

export default {
  
  toolbar: {
    backgroundColor: toolbarBackgroundColor,
    borderColor: toolbarBorderColor,
  },
  title: {
    color: toolbarTextColor,
  },
  subHeader: {
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: commentSubHeaderBackground,
  },
  subHeaderText: {
    color: commentSubHeaderText,
    textAlign: 'center',
  },
  content: {
    flex: 1,
  },
  textInputGiftedChat: { 
    fontFamily: 'Museo300',
  },
  sendButton: {
    marginRight: 10,
    marginBottom: 10,
  },
  sendButtonIcon: {
    color: commentSendButtonIcon,
  },
};