/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Container, Content, Text, Spinner, Label, StyleProvider } from 'native-base';
import { View } from 'react-native';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import BasisComponent from '../basisComponent.js';
import style from './style.js';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import { executeGetConfigurationData } from '../../data/configuration/action.js';
import { executeGetLeads } from '../../data/lead/action.js';
import { NavigationBar, FormWithAllInputs, ProgressScreen } from '../../component/index.js';
import {
  toolbarButtonColor,
  toolbarButtonColorWithBackground,
  inputTextColor,
  inputLabelColor,
  inputPlaceholderColor,
  inputBackgroundColor,
  multiSelectSpinner,
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
} from '../../color.js';
/*************
 * Constants *
 *************/
const INITIAL_DATE_INPUT = 'initialDate';
const END_DATE_INPUT = 'endDate';
/**
 * Renderiza modal para filtrar los leads
 */
class Filter extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea aqui para mejorar el performance
    this._onCloseView = this.onCloseView.bind(this);
    this._onFilter = this.onFilter.bind(this);
    this._onSelectedItemsChange = this.onSelectedItemsChange.bind(this);
    this._onFieldsOk = this.onFieldsOk.bind(this);
    this._onEmptyFields = this.onEmptyFields.bind(this);
    this._onConfirmDate = () => {};
    //Array de botones que se renderizaran en el navigation bar
    this.rightButtons = [
      {
        onPressRightButton: this._onFilter,
        iconRightButton: 'md-checkmark',
        colorIconRightButton: toolbarButtonColorWithBackground,
        rightButtonIconStyle: style.rightButtonsIcons,
      },
    ];
    //State
    this.state = {
      selectedItems: this.props.selectedItems,
    };
    //Variable donde se guardaran los items del multiSelect
    this.multiSelectItems = [];
    //Variable donde se guardaran las fechas para pasarlas a LeadsList
    this.initialDate = this.props.initialDate;
    this.endDate = this.props.endDate;
  }
  
  componentWillReceiveProps(nextProps) {
    //Mapea objeto para poblar el multiSelect
    if (nextProps.configurationData && this.multiSelectItems.length === 0)
      this.multiSelectItems.push({
        name: 'Origenes',
        id: 0,
        children: nextProps.configurationData.sourceInputItems
      })
      
    this.showErrorForGettingConfigurationData(nextProps);
    this.showErrorMessageForGettingLeads(nextProps);
    this.goLeadListIfToGetLeadsHasBeenExecutedsuccessfully(this.props, nextProps);
    this.showOrDismissProgressModal(nextProps);
  }

  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <NavigationBar
            toolBarStyle={style.toolbar}
            leftButtonVisible={true}
            onPressLeftButton={this._onCloseView}
            iconLeftButton={'md-arrow-back'}
            colorIconLeftButton={toolbarButtonColor}
            title={'Filtro avanzado'}
            titleStyle={style.title}
            rightButtonsVisible={this.props.configurationData ? true : false}
            rightButtons={this.rightButtons}/>
          <Content>
            {
              this.props.statusOfGettingConfigurationData.configurationDataIsBeingFetching
              ? this.renderProgressScreen()
              : this.renderMainContent()
            }
          </Content>
        </Container>
      </StyleProvider>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {
    
    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbar={true}/>
    );
  }
  /**
   * Renderiza el contenido principal
   */
  renderMainContent() {
    
    return (
      this.props.configurationData &&
      <View>
        {this.renderMultiselect()}
        {this.renderForm()}
      </View>
    );
  }
  /**
   * Renderiza el multiSelect
   */
  renderMultiselect() {
    
    return (
      <View style={style.multiSelectContainer}>
        <Label
          style={style.multiSelectLabel}>
          {'Origenes'}
        </Label>

        <SectionedMultiSelect
          showDropDowns={false}
          readOnlyHeadings={true}
          showCancelButton={false}
          showRemoveAll={true}
          modalAnimationType={'slide'}
          items={this.multiSelectItems} 
          uniqueKey={'id'}
          subKey={'children'}
          displayKey={'name'}
          selectText={'Seleccione origen(es)'}
          confirmText={'ACEPTAR'}
          selectedText={'seleccionado(s)'}
          searchPlaceholderText={'Buscar origen(es)'}
          removeAllText={'Eliminar todos'}
          noResultsComponent={this.renderNoResults()}
          loadingComponent={this.renderSpinner()}
          itemFontFamily={style.fontFamilyOfMultiselectModal}
          subItemFontFamily={style.fontFamilyOfMultiselectModal}
          searchTextFontFamily={style.fontFamilyOfMultiselectModal}
          confirmFontFamily={style.fontFamilyOfMultiselectModal}
          onSelectedItemsChange={this._onSelectedItemsChange}
          selectedItems={this.state.selectedItems}
          colors={style.multiSelectColors}
          styles={style.multiSelect}/>
      </View>
    );
  }
  /**
   * Renderiza componente text que se mostrará
   * en el multiSelect cuando no hayan
   * coincidencias
   */
  renderNoResults() {
    
    return (
      <Text style={style.multiSelectTextError}>{'No hay coincidencias'}</Text>
    );
  }
  /**
   * Renderiza spinner del multiSelect
   */
  renderSpinner() {
    
    return (
      <Spinner color={multiSelectSpinner}/>
    );
  }
  /**
   * Renderiza el form
   */
  renderForm() {
    
    return (
      <FormWithAllInputs
        ref={'form'}
        fields={this.getFields()}
        onFieldsOk={this._onFieldsOk}
        onEmptyFields={this._onEmptyFields}/>
    );
  }
  /**
   * Invoca metodo para obtener los datos de configuración
   * para poder filtrar los leads
   */
  componentDidMount() {
    this.props.executeGetConfigurationData(
      this.props.session.accessToken,
      this.props.session.refreshToken,
    );
  }
  /**
   * Escuchador que se dispara cuando se presiona
   * el boton para cerrar la vista
   */
  onCloseView() {
    this.props.onFilterViewWillBeClosed();
    this.pop();
  }
  /**
   * Escuchador que se dispara cuando el usuario
   * presiona el check button del toolbar
   */
  onFilter() {
    this.refs.form.validateForm();
  }
  /**
   * Escuchador que se dispara cuando selecciona
   * o quita un nuevo item del componente multiSelect
   */
  onSelectedItemsChange(selectedItems) {
    this.setState({ selectedItems: selectedItems })
  }
  /**
   * Escuchador que se dispara despues de ejecutar
   * el metodo validateForm del componente FormWithAllInputs,
   * siempre y cuando todos los campos requeridos esten
   * correctos
   * @param  {object} inputs objeto que contiene el valor de todos los inputs del form
   */
  onFieldsOk(inputs) {
    this.afterValidateForm(inputs);
  }
  /**
   * Escuchador que se dispara despues de ejecutar
   * el metodo validateForm del componente FormWithAllInputs,
   * siempre y cuando algún campo requerido este
   * incorrecto
   * @param  {object} inputs objeto que contiene el valor de todos los inputs del form
   */
  onEmptyFields(inputs) {
    this.afterValidateForm(inputs);
  }
  /**
   * Metodo que se ejecuta despues de ejecutar
   * el validateForm del form
   */
  afterValidateForm(inputs) {
    this.initialDate = inputs.initialDate.value;
    this.endDate = inputs.endDate.value;
    
    this.props.executeGetLeads(
      1,
      this.props.state,
      this.props.search,
      this.state.selectedItems,
      this.initialDate,
      this.endDate,
      this.props.session.accessToken,
      this.props.session.refreshToken
    );
  }
  /**
   * Retorna un objeto json que representa
   * todos los inputs que tendra el FormWithAllInputs
   */
  getFields() {
    
    return [
      {
        id: INITIAL_DATE_INPUT,
        type: 'dateInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Creado entre',
        placeholder: 'Presione aquí',
        minLength: 1,
        maxLength: 10,
        editable: false,
        visible: true,
        isRequired: false,
        onConfirmDate: this._onConfirmDate,
        defaultValue: this.props.initialDate,
      },
      {
        id: END_DATE_INPUT,
        type: 'dateInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Al',
        placeholder: 'Presione aquí',
        minLength: 1,
        maxLength: 10,
        editable: false,
        visible: true,
        isRequired: false,
        onConfirmDate: this._onConfirmDate,
        defaultValue: this.props.endDate,
      },
    ]
  }
  /**
   * Muestra errores cuando se ejecuta
   * el metodo executeGetConfigurationData,
   * si es que los hay
   */
  showErrorForGettingConfigurationData(nextProps) {
    if (nextProps.statusOfGettingConfigurationData.wasAnError)
      this.showErrorMessage(
        nextProps.statusOfGettingConfigurationData.error &&
        nextProps.statusOfGettingConfigurationData.error.errorMessage ||
        'Hubo un problema al obtener los datos de configuración para filtrar leads'
      );
  }
  /**
   * Muestra errores cuando se ejecuta el
   * metodo executeGetLeads, si es que los hay
   */
  showErrorMessageForGettingLeads(nextProps) {
    if (nextProps.statusOfGettingLeads.wasAnError)//Si hubo error
      this.showErrorMessage(
        nextProps.statusOfGettingLeads.error &&
        nextProps.statusOfGettingLeads.error.errorMessage ||
        'Hubo un problema al obtener los leads'
      )
  }
  /**
   * Muestra mensaje de exito si el metodo
   * executeGetLeads se ejecuto exitosamente
   */
  goLeadListIfToGetLeadsHasBeenExecutedsuccessfully(previousProps, nextProps) {
    if (!previousProps.statusOfGettingLeads.wasAnError &&
        !nextProps.statusOfGettingLeads.leadsAreBeingFetching &&
        !nextProps.statusOfGettingConfigurationData.configurationDataIsBeingFetching &&
        !previousProps.statusOfGettingConfigurationData.configurationDataIsBeingFetching)//Si no hubo error
      this.props.onLeadFiltered(this.state.selectedItems, this.initialDate, this.endDate);
  }
  /**
   * Muestra u oculta el ProgressModal
   */
  showOrDismissProgressModal(nextProps) {
    if (nextProps.statusOfGettingLeads.leadsAreBeingFetching)
      this.showProgressModal('Filtrando leads...');
    else
      this.dismissModal();
  }
}

Filter.propTypes = {
  selectedItems: PropTypes.array,//Items que se seleccionaron anteriormente del multiSelect
  initialDate: PropTypes.string,//Fecha inicial anteriormente seleccionada
  endDate: PropTypes.string,//Fecha final anteriormente seleccionada
  state: PropTypes.string,//Estado de lead a filtrar
  search: PropTypes.string,//Dato introducido en el campo de busqueda en LeadsList
  onLeadFiltered: PropTypes.func,//Escuchador que es disparado cuando el filtrado de lead ha sido exitoso
  onFilterViewWillBeClosed: PropTypes.func,//Escuchador que se dispara para saber que esta vista será cerrada
};

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  statusOfGettingConfigurationData: state.dataReducer.configurationReducer.statusOfGettingConfigurationData,
  configurationData: state.dataReducer.configurationReducer.configurationData,
  statusOfGettingLeads: state.dataReducer.leadReducer.statusOfGettingLeads,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetConfigurationData: executeGetConfigurationData,
  executeGetLeads: executeGetLeads,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Filter);