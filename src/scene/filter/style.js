/******************
 * Project import *
 ******************/
import {
  toolbarBackgroundColor,
  toolbarBorderColor,
  toolbarTextColor,
  toolbarButtonBackgroundColor,
  multiSelectDropDownBackground,
  multiSelectDropDownText,
  multiSelectChipBackground,
  multiSelectChipText,
  multiSelectChipIcon,
  multiSelectPrimaryColor,
  multiSelectSuccessColor,
  multiSelectTextColor,
  multiSelectSubTextColor,
  multiSelectPlaceholderTextColor,
  multiSelectLabel,
  multiSelectTextError,
} from '../../color.js';

export default {
  
  toolbar: {
    backgroundColor: toolbarBackgroundColor,
    borderColor: toolbarBorderColor,
  },
  title: {
    color: toolbarTextColor,
  },
  rightButtonsIcons: {
    backgroundColor: toolbarButtonBackgroundColor,
    borderRadius: 20,
    width: 35,
    height: 35,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  multiSelectContainer: {
    marginHorizontal: 15,
    marginTop: 15
  },
  multiSelect: { 
    selectToggle: {
      backgroundColor: multiSelectDropDownBackground,
      borderRadius: 5,
      paddingRight: 10,
      paddingLeft: 3,
      minHeight: 50,
      height: 50,
      marginBottom: 12,
    },
    selectToggleText: {
      fontFamily: 'Museo300',
      color: multiSelectDropDownText,
    },
    chipContainer: {
      backgroundColor: multiSelectChipBackground,
      borderWidth: 0,
    },
    chipText: {
      color: multiSelectChipText,
      fontFamily: 'Museo300',
    },
    chipIcon: {
      color: multiSelectChipIcon,
    },
    itemText: {
      fontFamily: 'Museo700',
    }
  },
  multiSelectColors: {
    primary: multiSelectPrimaryColor,
    success: multiSelectSuccessColor,
    text: multiSelectTextColor,
    subText: multiSelectSubTextColor,
    searchPlaceholderTextColor: multiSelectPlaceholderTextColor,
  },
  multiSelectLabel: {
    color: multiSelectLabel,
    top: 0,
    fontSize: 15,
    marginBottom: 3,
  },
  multiSelectTextError: {
    textAlign: 'center',
    fontFamily: 'Museo300',
    color: multiSelectTextError,
  },
  fontFamilyOfMultiselectModal: {
    fontFamily: 'Museo300',
  },
}