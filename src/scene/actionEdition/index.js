/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Tab, TabHeading, Text } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../basisComponent.js';
import ActionForm from './component/actionForm/index.js';
import { tabIconColor } from '../../color.js';
import { executeGetAnActionForEdition, executeUpdateAnAction } from '../../data/action/action.js';
import { ViewPager, LeadInformationViewer, ProductViewer } from '../../component/index.js';
/**
 * Renderiza la vista para editar una acción
 */
class ActionEdition extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onSave = this.onSave.bind(this);
    this._openPlaceView = () => {};
    this._onValidateForm = this.onValidateForm.bind(this);
    //State
    this.state = {
      fieldErrors: {},
    };
  }
  
  /**
   * Verifica si hubo un error en todas
   * las acciones
   */
  componentWillReceiveProps(nextProps) {
    const previousProps = this.props;
    
    this.showErrorForGettigAnActionForEdition(nextProps);
    this.showErrorMessageForUpdatingAnAction(nextProps);
    this.showSuccessMessageForUpdatingAnAction(previousProps, nextProps);
    this.showOrDismissProgressModal(nextProps);
  }

  render() {

    return (
      <ViewPager
        ref={'viewPager'}
        title={'Editar acción'}
        onSave={this._onSave}
        onOpenPlaceView={this._openPlaceView}
        showRightButtons={this.props.actionForEdition && !this.props.statusOfGettingAnActionForEdition.actionForEditionIsBeingFetching
          ? true
          : false}
        showButtonForOpenPlaceView={false}
        leftButtonIconName={'close'}>

        <Tab
          heading={this.renderHeading('pencil-box-outline', 'Editar')}>
          
          <ActionForm
            ref={'actionForm'}
            actionForEdition={this.props.actionForEdition && this.props.actionForEdition.action}
            configurationData={this.props.actionForEdition && this.props.actionForEdition.configurationData}
            onValidateForm={this._onValidateForm}
            showProgressScreen={this.props.statusOfGettingAnActionForEdition.actionForEditionIsBeingFetching}
            fieldErrors={this.state.fieldErrors}/>
        </Tab>
        
        <Tab
          heading={this.renderHeading('information-outline', 'Información')}>
          
          <LeadInformationViewer
            leadInformation={this.props.actionForEdition && this.props.actionForEdition.information}
            showProgressScreen={this.props.statusOfGettingAnActionForEdition.actionForEditionIsBeingFetching}/>
        </Tab>
        
        <Tab
          heading={this.renderHeading('shopping', 'Producto')}>
          
          <ProductViewer
            products={this.props.actionForEdition && this.props.actionForEdition.products}
            showProgressScreen={this.props.statusOfGettingAnActionForEdition.actionForEditionIsBeingFetching}/>
        </Tab>
      </ViewPager>
    );
  }
  /**
   * Renderiza los headings de cada tab
   * @param  {string} icon nombre del icono
   * @param  {string} text texto que va en el tab
   */
  renderHeading(icon, text) {
    
    return (
      <TabHeading style={style.heading}>
        <Text>{text}</Text>
        <Icon size={20} name={icon} color={tabIconColor}/>
      </TabHeading>
    );    
  }
  /**
   * Invoca metodo para obtener la acción
   * a editar
   */
  componentDidMount() {
    this.props.executeGetAnActionForEdition(
      this.props.actionId,
      this.props.session.accessToken,
      this.props.session.refreshToken,
    );
  }
  /**
   * Escuchador que se dispara cuando un usuario
   * presiona el checkbutton del toolbar
   */
  onSave() {
    this.refs.actionForm.executeValidateForm();
  }
  /**
   * Escuchador que se dispara despues de ejecutar el metodo
   * executeValidateForm del ActionForm
   * @param  {object} inputs objeto que contiene los valores de los inputs de ActionForm
   */
  onValidateForm(inputs) {
    this.props.executeUpdateAnAction(
      this.props.actionId,
      inputs,
      this.props.session.accessToken,
      this.props.session.refreshToken
    );
  }
  /**
   * Muestra errores cuando se ejecuta
   * el metodo executeGetAnActionForEdition,
   * si es que los hay
   */
  showErrorForGettigAnActionForEdition(nextProps) {
    if (nextProps.statusOfGettingAnActionForEdition.wasAnError)
      this.showErrorMessage(
        (nextProps.statusOfGettingAnActionForEdition.error &&
        nextProps.statusOfGettingAnActionForEdition.error.errorMessage) ||
        'Hubo un problema al obtener la acción a editar'
      );
  }
  /**
   * Muestra errores cuando se ejecuta el
   * metodo executeUpdateAnAction, si es que los hay
   */
  showErrorMessageForUpdatingAnAction(nextProps) {
    if (nextProps.statusOfUpdatingAnAction.wasAnError &&
        nextProps.statusOfUpdatingAnAction.updatingAnActionIsBeingExecuted) {//Si hubo error
      this.showErrorMessage(
        nextProps.statusOfUpdatingAnAction.error &&
        nextProps.statusOfUpdatingAnAction.error.errorMessage ||
        'Hubo un problema al actualizar la acción'
      );
      //Para que se muestren los errores en los inputs
      if (nextProps.statusOfUpdatingAnAction.error.fieldErrors)
        this.setState({ fieldErrors: nextProps.statusOfUpdatingAnAction.error.fieldErrors });
    }
  }
  /**
   * Muestra mensaje de exito si el metodo
   * executeUpdateAnAction se ejecuto exitosamente
   */
  showSuccessMessageForUpdatingAnAction(previousProps, nextProps) {
    if (!previousProps.statusOfUpdatingAnAction.wasAnError &&
        !nextProps.statusOfUpdatingAnAction.updatingAnActionIsBeingExecuted &&
        !nextProps.statusOfGettingAnActionForEdition.actionForEditionIsBeingFetching &&
        !previousProps.statusOfGettingAnActionForEdition.actionForEditionIsBeingFetching) {//Si no hubo error

      this.showSuccessMessage('La acción fue actualizada exitosamente');
      //Para que se borren los errores en los inputs
      this.setState({ fieldErrors: {} });
      this.props.onActionEdited();
      this.pop();
    }
  }
  /**
   * Muestra u oculta el ProgressModal
   */
  showOrDismissProgressModal(nextProps) {
    if (nextProps.statusOfUpdatingAnAction.updatingAnActionIsBeingExecuted)
      this.showProgressModal('Actualizando acción...');
    else
      this.dismissModal();
  }
}

ActionEdition.propTypes = {
  actionId: PropTypes.string.isRequired,//Id de la acción que se quiere editar
  onActionEdited: PropTypes.func,//Esuchador que se dispara cuando acción fue editada exitosamente
};

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  statusOfGettingAnActionForEdition: state.dataReducer.actionReducer.statusOfGettingAnActionForEdition,
  actionForEdition: state.dataReducer.actionReducer.actionForEdition,
  statusOfUpdatingAnAction: state.dataReducer.actionReducer.statusOfUpdatingAnAction,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetAnActionForEdition: executeGetAnActionForEdition,
  executeUpdateAnAction: executeUpdateAnAction,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ActionEdition);