/******************
 * Project import *
 ******************/
import { tabHeadingBackgroundColor } from '../../color.js';

export default {
  
  heading: {
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: tabHeadingBackgroundColor,
  },
};