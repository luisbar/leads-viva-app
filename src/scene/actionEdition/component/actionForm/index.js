/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Content } from 'native-base';

import PropTypes from 'prop-types';

import moment from 'moment';
import 'moment/locale/es';
/******************
 * Project import *
 ******************/
import BasisComponent from '../../../basisComponent.js';
import { FormWithAllInputs, ProgressScreen } from '../../../../component/index.js';
import {
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
  inputTextColor,
  inputLabelColor,
  inputPlaceholderColor,
  inputBackgroundColor,
  pickerColor,
  pickerLabelColor,
  pickerBackgroundColor,
  pickerErrorColor,
  radioErrorColor,
} from '../../../../color.js';
/*************
 * Constants *
 *************/
const DATE_INPUT = 'date';
const TIME_INPUT = 'time';
const DURATION_INPUT = 'duration';
const ACTION_INPUT = 'action';
const NOTE_INPUT = 'note';
const STATUS_INPUT = 'status';

const ANSWER_INPUT = 'answer';

const REASON_INPUT = 'reason';
const JUSTIFICATION_INPUT = 'justification';

const NEW_DATE_INPUT = 'newDate';
const NEW_TIME_INPUT = 'newTime';
const NEW_DURATION_INPUT = 'newDuration';
const NEW_ACTION_INPUT = 'newAction';
const NEW_NOTE_INPUT = 'newNote';

const NUMBER_OF_PURCHASE_PROCESS_INPUT = 'numberOfPurchaseProcess';
//Flags para saber que campos mostrar y cuales ocultar
const CONTACTED = 'contactado';
const NOT_CONTACTED = 'no_contactado';

const NEXT_ACTION = 'siguiente_accion';
const NUMBER_OF_PURCHASE_PROCESS = 'numero_proceso_venta';
const REASON = 'motivo_cierre';
/**
 * Renderiza formulario para registrar una acción
 */
class ActionForm extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onFieldsOk = this.onFieldsOk.bind(this);
    this._onEmptyFields = this.onEmptyFields.bind(this);
    this._onConfirmDate = () => {};
    this._onConfirmTime = () => {};
    this._onDurationChanged = this.onDurationChanged.bind(this);
    this._onActionChanged = this.onActionChanged.bind(this);
    this._onStatusChanged = this.onStatusChanged.bind(this);
    this._onAnswerChanged = this.onAnswerChanged.bind(this);
    this._onReasonChanged = this.onReasonChanged.bind(this);
    this._onNewDurationChanged = this.onNewDurationChanged.bind(this);
    this._onNewActionChanged = this.onNewActionChanged.bind(this);
    this._onConfirmNewDate = () => {};
    this._onConfirmNewTime = () => {};
    //Ids de la opción seleccionada de los dropdowns
    this.idOfDurationSelected;
    this.idOfActionSelected;
    this.idOfStatusSelected;
    this.idOfAnswerSelected;
    this.idOfNewDurationSelected;
    this.idOfNewActionSelected;
    
    this.viewToShow;
    //State
    this.state = {
      fields: [],
    };
  }
  
  componentWillReceiveProps(nextProps) {
    //Setea el id del item seleccionado de los combos
    if (nextProps.configurationData && !this.idOfDurationSelected) {
      this.idOfDurationSelected = nextProps.actionForEdition.durationId;
      this.idOfActionSelected = nextProps.actionForEdition.actionId;
    }
    
    this.setState({
      fields: this.getFields(nextProps)
    });
  }

  render() {

    return (
      <Content>
        {
          this.props.showProgressScreen
          ? this.renderProgressScreen()
          : this.renderForm()
        }
      </Content>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {
    
    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbarAndTabBar={true}/>
    );
  }
  /**
   * Renderiza el formulario
   */
  renderForm() {
    
    return (
      this.props.configurationData &&
      <FormWithAllInputs
        ref={'form'}
        fields={this.state.fields}
        onFieldsOk={this._onFieldsOk}
        onEmptyFields={this._onEmptyFields}
        minimumDate={moment(new Date(), 'DD-MM-YYYY').toDate()}/>
    );
  }
  
  componentDidMount() {
    this.setState({
      fields: this.getFields(this.props),
    });
  }
  /**
   * Escuchador cuando el usuario cambia el
   * valor del DURATION_INPUT
   */
  onDurationChanged(newDuration, index) {
    this.idOfDurationSelected = this.props.configurationData.durations[index].id;
  }
  /**
   * Escuchador cuando el usuario cambia el
   * valor del ACTION_INPUT
   */
  onActionChanged(newAction, index) {
    this.idOfActionSelected = this.props.configurationData.actions[index].id;
  }
  /**
   * Invoca metodo para validar el formulario
   */
  executeValidateForm() {
    this.refs.form.validateForm();
  }
  /**
   * Escuchador que se dispara despues de ejecutar
   * el metodo validateForm del componente FormWithAllInputs,
   * siempre y cuando todos los campos requeridos esten
   * correctos
   * @param  {object} inputs objeto que contiene el valor de todos los inputs del form
   */
  onFieldsOk(inputs) {
    this.props.onValidateForm(this.getInputValues(inputs));
  }
  /**
   * Escuchador que se dispara despues de ejecutar
   * el metodo validateForm del componente FormWithAllInputs,
   * siempre y cuando algún campo requerido este
   * incorrecto
   * @param  {object} inputs objeto que contiene el valor de todos los inputs del form
   */
  onEmptyFields(inputs) {
    this.props.onValidateForm(this.getInputValues(inputs));
  }
  /**
   * Escuchador que se dispara cuando un usuario
   * presiona sobre un radio button del radio
   * group
   */
  onStatusChanged(statusId) {
    this.viewToShow = '';
    this.idOfAnswerSelected = '';
    this.idOfReasonSelected = '';
    this.idOfNewDurationSelected = '';
    this.idOfNewActionSelected = '';
    this.idOfStatusSelected = statusId;
    this.setState({
      fields: this.getFields(this.props),
    });
  }
  /**
   * Escuchador cuando el usuario cambia el
   * valor del ANSWER_INPUT
   */
  onAnswerChanged(newAnswer, index) {
    this.idOfReasonSelected = '';
    this.idOfNewDurationSelected = '';
    this.idOfNewActionSelected = '';
    const answers = this.props.configurationData.actionStatus.filter((item) => item.id === this.idOfStatusSelected)[0].answers;
    this.idOfAnswerSelected = answers[index].id;
    this.viewToShow = answers.filter((item) => item.id === this.idOfAnswerSelected)[0].viewToShow;

    this.setState({
      fields: this.getFields(this.props),
    });
  }
  /**
   * Escuchador cuando el usuario cambia el
   * valor del REASON_INPUT
   */
  onReasonChanged(newReason, index) {
    const answers = this.props.configurationData.actionStatus.filter((item) => item.id === this.idOfStatusSelected)[0].answers;
    const reasons = answers.filter((item) => item.id === this.idOfAnswerSelected)[0].reasons;
    this.idOfReasonSelected = reasons[index].id;
  }
  /**
   * Escuchador cuando el usuario cambia el
   * valor del NEW_DURATION_INPUT
   */
  onNewDurationChanged(newDuration, index) {
    this.idOfNewDurationSelected = this.props.configurationData.durations[index].id;
  }
  /**
   * Escuchador cuando el usuario cambia el
   * valor del NEW_ACTION_INPUT
   */
  onNewActionChanged(newAction, index) {
    this.idOfNewActionSelected= this.props.configurationData.actions[index].id;
  }
  /**
   * Retorna json formateado para enviar a la api
   */
  getInputValues(inputs) {

    return {
      date: inputs[DATE_INPUT] && inputs[DATE_INPUT].value,
      time: inputs[TIME_INPUT] && inputs[TIME_INPUT].value,
      duration: this.idOfDurationSelected,
      action: this.idOfActionSelected,
      note: inputs[NOTE_INPUT] && inputs[NOTE_INPUT].value,
      status: inputs[STATUS_INPUT] && inputs[STATUS_INPUT].value,
      answer: this.idOfAnswerSelected,
      numberOfPurchaseProcess: inputs[NUMBER_OF_PURCHASE_PROCESS_INPUT] && inputs[NUMBER_OF_PURCHASE_PROCESS_INPUT].value,
      reason: this.idOfReasonSelected,
      justification: inputs[JUSTIFICATION_INPUT] && inputs[JUSTIFICATION_INPUT].value,
      newDate: inputs[NEW_DATE_INPUT] && inputs[NEW_DATE_INPUT].value,
      newTime: inputs[NEW_TIME_INPUT] && inputs[NEW_TIME_INPUT].value,
      newDuration: this.idOfNewDurationSelected,
      newAction: this.idOfNewActionSelected,
      newNote: inputs[NEW_NOTE_INPUT] && inputs[NEW_NOTE_INPUT].value,
    };
  }
  /**
   * Retorna objeto json que representa los inputs
   * del FormWithAllInputs
   */
  getFields(props) {
    if (props.configurationData) {
      if (this.idOfStatusSelected) {//Si ha seleccionado un estado

        switch (this.viewToShow) {//Si ha seleccionado una respuesta
          case REASON:
            return this.getFieldsByAnswerOfTypeReason(props);
          
          case NEXT_ACTION:
            return this.getFieldsByAnswerOfTypeNextAction(props);
          
          case NUMBER_OF_PURCHASE_PROCESS:
            return this.getFieldsByAnswerOfTypeNumberOfPurchaseProcess(props);
            
          default:
            return this.getFieldsWithStatusSelected(props);
        }
        
      } else//Si no ha seleccionado estado
        return this.getDefaultFields(props);
    }
  }
  /**
   * Retorna un objeto json que representa
   * todos los inputs por default de una acción
   */
  getDefaultFields(props) {
    
    return [
      {
        id: DATE_INPUT,
        type: 'dateInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Fecha',
        placeholder: 'Presione aquí',
        minLength: 1,
        maxLength: 10,
        returnKeyType: 'next',
        editable: false,
        onSubmitEditing: TIME_INPUT,
        visible: true,
        isRequired: false,
        onConfirmDate: this._onConfirmDate,
        errors: props.fieldErrors && props.fieldErrors.date,
        defaultValue: props.actionForEdition && props.actionForEdition.date,
      },
      {
        id: TIME_INPUT,
        type: 'timeInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Hora',
        placeholder: 'Presione aquí',
        minLength: 1,
        maxLength: 8,
        returnKeyType: 'next',
        editable: false,
        onSubmitEditing: NOTE_INPUT,
        visible: true,
        isRequired: false,
        onConfirmTime: this._onConfirmTime,
        errors: props.fieldErrors && props.fieldErrors.time,
        defaultValue: props.actionForEdition && props.actionForEdition.time,
      },
      {
        id: DURATION_INPUT,
        type: 'dropdown',
        colorPickerAndroid: pickerColor,
        labelColor: pickerLabelColor,
        pickerBackgroundColor: pickerBackgroundColor,
        label: 'Duración',
        items: props.configurationData.durations.map((item) => item.name),
        visible: true,
        onItemSelected: this._onDurationChanged,
        defaultValue: props.actionForEdition && props.actionForEdition.durationName,
      },
      {
        id: ACTION_INPUT,
        type: 'dropdown',
        colorPickerAndroid: pickerColor,
        labelColor: pickerLabelColor,
        pickerBackgroundColor: pickerBackgroundColor,
        label: 'Acción',
        items: props.configurationData.actions.map((item) => item.name),
        visible: true,
        onItemSelected: this._onActionChanged,
        defaultValue: props.actionForEdition && props.actionForEdition.actionName,
      },
      {
        id: NOTE_INPUT,
        type: 'multilineEditableInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Observación',
        placeholder: 'Escriba aquí',
        multiline: true,
        minLength: 1,
        maxLength: 500,
        keyboardType: 'default',
        returnKeyType: 'done',
        editable: true,
        visible: true,
        isRequired: false,
        errors: props.fieldErrors && props.fieldErrors.note,
        defaultValue: props.actionForEdition && props.actionForEdition.note,
      },
      {
        id: STATUS_INPUT,
        type: 'radio',
        items: props.configurationData.actionStatus,
        title: 'Estado',
        visible: true,
        isRequired: false,
        onItemPressed: this._onStatusChanged,
        errorColor: radioErrorColor,
        errors: props.fieldErrors && props.fieldErrors.status,
      },
    ]
  }
  /**
   * Retorna inputs cuando el usuario ha seleccionado el
   * estado de la acción
   */
  getFieldsWithStatusSelected(props) {
    const answers = props.configurationData.actionStatus.filter((item) => item.id === this.idOfStatusSelected)[0].answers;
    
    return this.getDefaultFields(props)
    .concat([
      {
        id: ANSWER_INPUT,
        type: 'dropdown',
        colorPickerAndroid: pickerColor,
        labelColor: pickerLabelColor,
        pickerBackgroundColor: pickerBackgroundColor,
        label: 'Respuesta',
        items: answers.map((item) => item.name),
        visible: true,
        onItemSelected: this._onAnswerChanged,
        errorColor: pickerErrorColor,
        errors: props.fieldErrors && props.fieldErrors.answer,
      }
    ]);
  }
  /**
   * Retorna inputs cuando el usuario ha seleccionado
   * una respuesta de tipo NUMBER_OF_PURCHASE_PROCESS
   */
  getFieldsByAnswerOfTypeNumberOfPurchaseProcess(props) {
    
    return this.getFieldsWithStatusSelected(props)
    .concat([
      {
        id: NUMBER_OF_PURCHASE_PROCESS_INPUT,
        type: 'editableInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Número de proceso de venta',
        placeholder: 'Escriba aquí',
        minLength: 1,
        maxLength: 200,
        keyboardType: 'default',
        returnKeyType: 'next',
        editable: true,
        visible: true,
        isRequired: false,
        errors: props.fieldErrors && props.fieldErrors.numberOfPurchaseProcess,
      },
    ]);
  }
  /**
   * Retorna inputs cuando el usuario ha seleccionado
   * una respuesta de tipo REASON
   */
  getFieldsByAnswerOfTypeReason(props) {
    const answers = props.configurationData.actionStatus.filter((item) => item.id === this.idOfStatusSelected)[0].answers;
    const reasons = answers.filter((item) => item.id === this.idOfAnswerSelected)[0].reasons;
    this.idOfReasonSelected = answers.filter((item) => item.id === this.idOfAnswerSelected)[0].reasons[0].id;
    
    return this.getFieldsWithStatusSelected(props)
    .concat([
      {
        id: REASON_INPUT,
        type: 'dropdown',
        colorPickerAndroid: pickerColor,
        labelColor: pickerLabelColor,
        pickerBackgroundColor: pickerBackgroundColor,
        label: 'Motivo',
        items: reasons.map((item) => item.name),
        visible: true,
        onItemSelected: this._onReasonChanged,
      },
      {
        id: JUSTIFICATION_INPUT,
        type: 'multilineEditableInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Justificación',
        placeholder: 'Escriba aquí',
        multiline: true,
        minLength: 1,
        maxLength: 500,
        keyboardType: 'default',
        returnKeyType: 'done',
        editable: true,
        visible: true,
        isRequired: false,
        errors: props.fieldErrors && props.fieldErrors.justification,
      },
    ]);
  }
  /**
   * Retorna inputs cuando el usuario ha seleccionado
   * una respuesta de tipo NEXT_ACTION
   */
  getFieldsByAnswerOfTypeNextAction(props) {
    this.idOfNewDurationSelected = props.configurationData.durations[0].id;
    this.idOfNewActionSelected = props.configurationData.actions[0].id;
    
    return this.getFieldsWithStatusSelected(props)
    .concat([
      {
        type: 'title',
        title: 'Siguiente acción',
        visible: true,
      },
      {
        id: NEW_DATE_INPUT,
        type: 'dateInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Fecha',
        placeholder: 'Presione aquí',
        minLength: 1,
        maxLength: 10,
        returnKeyType: 'next',
        editable: false,
        onSubmitEditing: NEW_TIME_INPUT,
        visible: true,
        isRequired: false,
        onConfirmDate: this._onConfirmNewDate,
        errors: props.fieldErrors && props.fieldErrors.newDate,
      },
      {
        id: NEW_TIME_INPUT,
        type: 'timeInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Hora',
        placeholder: 'Presione aquí',
        minLength: 1,
        maxLength: 8,
        returnKeyType: 'next',
        editable: false,
        onSubmitEditing: NEW_NOTE_INPUT,
        visible: true,
        isRequired: false,
        onConfirmTime: this._onConfirmNewTime,
        errors: props.fieldErrors && props.fieldErrors.newTime,
      },
      {
        id: NEW_DURATION_INPUT,
        type: 'dropdown',
        colorPickerAndroid: pickerColor,
        labelColor: pickerLabelColor,
        pickerBackgroundColor: pickerBackgroundColor,
        label: 'Duración',
        items: props.configurationData.durations.map((item) => item.name),
        visible: true,
        onItemSelected: this._onNewDurationChanged,
      },
      {
        id: NEW_ACTION_INPUT,
        type: 'dropdown',
        colorPickerAndroid: pickerColor,
        labelColor: pickerLabelColor,
        pickerBackgroundColor: pickerBackgroundColor,
        label: 'Acción',
        items: props.configurationData.actions.map((item) => item.name),
        visible: true,
        onItemSelected: this._onNewActionChanged,
      },
      {
        id: NEW_NOTE_INPUT,
        type: 'multilineEditableInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Observación',
        placeholder: 'Escriba aquí',
        multiline: true,
        minLength: 1,
        maxLength: 500,
        keyboardType: 'default',
        returnKeyType: 'done',
        editable: true,
        visible: true,
        isRequired: false,
        errors: props.fieldErrors && props.fieldErrors.newNote,
      },
    ]);
  }
}

ActionForm.propTypes = {
  actionForEdition: PropTypes.object,//Acción a editar
  showProgressScreen: PropTypes.bool,//Bandera para saber si se muestra o no el ProgressScreen
  configurationData: PropTypes.object,//Objeto que contiene los datos de configuración para registrar una acción
  onValidateForm: PropTypes.func.isRequired,//Escuchador que se dispara despues de ejecutar el metodo validateForm del componente FormWithAllInputs
  fieldErrors: PropTypes.object,//Objeto que contiene los errores de cada input
};

export default ActionForm;