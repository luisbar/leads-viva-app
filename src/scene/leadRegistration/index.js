/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Tab, TabHeading, Text } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import geolib from 'geolib';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../basisComponent.js';
import { executeGetConfigurationData, executeGetAvailabilityOfService } from '../../data/configuration/action.js';
import { executeSaveLead } from '../../data/lead/action.js';
import { ViewPager, InformationForm, ProductSelection, Map, Picture } from '../../component/index.js';
import { product, place, camera } from '../../config.js';
import { tabIconColor } from '../../color.js';
/*************
 * Constants *
 *************/
const FORM = 0;
const PRODUCT = 1;
const MAP = 2;
/**
 * Renderiza la vista para registrar un lead
 */
class LeadRegistration extends BasisComponent {

  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onSave = this.onSave.bind(this);
    this._openPlaceView = this.onOpenPlaceView.bind(this);
    this._onValidateForm = this.onValidatedForm.bind(this);
    this._onProductPressed = this.onProductPressed.bind(this);
    this._onNewLocation = this.onNewLocation.bind(this);
    this._onNewAddress = this.onNewAddress.bind(this);
    this._onOpenCamera = this.onOpenCamera.bind(this);
    this._onAddProducts = this.onAddProducts.bind(this);
    this._onPlaceSelected = this.onPlaceSelected.bind(this);
    this._onRemovePicture = this.onRemovePicture.bind(this);
    this._onPictureTaken = this.onPictureTaken.bind(this);
    //Variables para guardar datos
    this.information;//Información del cliente o lead
    this.location = {
      latitude: '',
      longitude: '',
      address: '',
      availabilityOfService: '',
    };
    //State
    this.state = {
      location: null,//Para actualizar la ubicación del componente map cuando se selecciona un lugar de la lista del componente Place
      thereIsNotARestrictedProduct: true,//Bandera para saber si en la lista de productos agregados hay alguno restringido
      fieldErrors: {},//Objeto donde se guardan los errores
      pictures: [],//Array donde se guardaran las fotos
      pictureLatitude: null,//Latitud para pasarle como props a camera
      pictureLongitude: null,//Longitud para pasarle como props a camera
      products: {//Productos añadidos
        postPaid: [],
        lte: [],
        publicTelephony: [],
        vpn: [],
        bag: [],
        other: [],
      },
    };
  }
  /**
   * Verifica si hubo un error en todas
   * las acciones
   */
  componentWillReceiveProps(nextProps) {
    this.showOrDismissProgressModal(nextProps);
    this.showErrorForGettingConfigurationData(nextProps);
    this.showErrorMessageForSavingALead(nextProps);
    this.showSuccessMessageForSavingLead(this.props, nextProps);
  }

  render() {

    return (
      <ViewPager
        ref={'viewPager'}
        title={'Registrar lead'}
        onSave={this._onSave}
        onOpenPlaceView={this._openPlaceView}
        showRightButtons={this.props.configurationData && !this.props.statusOfSavingALead.savingLeadIsBeingExecuted
          ? true
          : false}
        showButtonForOpenPlaceView={this.state.thereIsNotARestrictedProduct}
        leftButtonIconName={'drawer'}>

        <Tab
          heading={this.renderHeading('information-outline', 'Información')}>
          
          <InformationForm
            ref={'informationForm'}
            onValidatedForm={this._onValidateForm}
            showProgressScreen={this.props.statusOfGettingConfigurationData.configurationDataIsBeingFetching}
            configurationData={this.props.configurationData}
            fieldErrors={this.state.fieldErrors}/>
        </Tab>
        
        <Tab
          heading={this.renderHeading('shopping', 'Producto')}>
          
          <ProductSelection
            onProductPressed={this._onProductPressed}
            showProgressScreen={this.props.statusOfGettingConfigurationData.configurationDataIsBeingFetching}
            showProducts={this.props.configurationData ? true : false}
            errorsOnPlansAdded={this.state.fieldErrors && this.state.fieldErrors.errorsOnPlansAdded}
            products={this.state.products}/>
        </Tab>
        
        <Tab
          heading={this.renderHeading('map-marker', 'Ubicación')}>
          
          <Map
            ref={'map'}
            onNewLocation={this._onNewLocation}
            onNewAddress={this._onNewAddress}
            location={this.state.location}
            markerIsDraggable={this.state.thereIsNotARestrictedProduct}
            executeGetAvailabilityOfService={this.props.executeGetAvailabilityOfService}
            statusOfGettingAvailabilityOfService={this.props.statusOfGettingAvailabilityOfService}
            availabilityOfService={this.props.availabilityOfService}
            accessToken={this.props.session && this.props.session.accessToken}
            refreshToken={this.props.session && this.props.session.refreshToken}
            showProgressScreen={this.props.statusOfGettingConfigurationData.configurationDataIsBeingFetching}
            showMap={this.props.configurationData ? true : false}/>
        </Tab>

        <Tab
          heading={this.renderHeading('camera', 'Foto')}>
          
          <Picture
            onOpenCamera={this._onOpenCamera}
            pictures={this.state.pictures}
            onRemovePicture={this._onRemovePicture}
            showProgressScreen={this.props.statusOfGettingConfigurationData.configurationDataIsBeingFetching}
            showPictures={this.props.configurationData ? true : false}
            maximumDistance={this.props.configurationData &&
              this.props.configurationData.maximumDistance &&
              this.props.configurationData.maximumDistance.distanceInMeters}/>
        </Tab>

      </ViewPager>
    );
  }
  /**
   * Renderiza los headings de cada tab
   * @param  {string} icon nombre del icono
   * @param  {string} text text que va en el tab
   */
  renderHeading(icon, text) {
    
    return (
      <TabHeading style={style.heading}>
        <Text>{text}</Text>
        <Icon size={20} name={icon} color={tabIconColor}/>
      </TabHeading>
    );    
  }
  /**
   * Invoca metodo para obtener la configuración del formulario
   * para registrar leads
   */
  componentDidMount() {
    this.props.executeGetConfigurationData(
      this.props.session.accessToken,
      this.props.session.refreshToken,
    );
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona el boton para
   * guardar los datos de componente view pager
   */
  onSave() {
    this.refs.informationForm.excuteValidateForm();
  }
  /**
   * Escuchador que se dispara cuando se invoca el metodo validateForm
   * del componente InformationForm
   * @param  {object} inputs contiene el valor de cada input del formulario de información
   */
  onValidatedForm(inputs) {
    this.showInformativeMessage('El lead sera guardado, aguarde un momento por favor');
    this.information = inputs;
    this.location.availabilityOfService = this.refs.map.getAvailabilityOfService();

    this.props.executeSaveLead({
      information: this.information,
      products: this.state.products,
      location: this.location,
      pictures: this.state.pictures,
    }, this.props.session.accessToken, this.props.session.refreshToken);
  }
  /**
   * Escuchador que es disparado cuando un usuario
   * presiona un producto del componente ProductSelection
   */
  onProductPressed(productPressed) {
    this.push(
      product,
      {
        onAddProducts: this._onAddProducts,
        productPressed: productPressed,
        plansAdded: this.state.products,
        configurationData: this.props.configurationData,
        errorsOnPlansAdded: this.state.fieldErrors && this.state.fieldErrors.errorsOnPlansAdded,
      }
    );
  }
  /**
   * Escuchador que es disparado cuando se presiona el
   * check button del modal de producto
   */
  onAddProducts(products) {
    this.setState({ products: products },
    () => {
      if (this.thereIsNotARestrictedProduct() !== this.state.thereIsNotARestrictedProduct)
        this.setState({ thereIsNotARestrictedProduct: this.thereIsNotARestrictedProduct() })
    });
  }
  /**
   * Escuchador que se dispara cuando el usuario
   * cambia la posición del marker
   * @param  {object} latlng continene la latitud y longitud
   */
  onNewLocation(latitude, longitude) {
    this.location.latitude = latitude;
    this.location.longitude = longitude;
    this.updatePictureDistances();
  }
  /**
   * Escuchador que se dispara cuando el usuario escribe en el campo dirección
   * de la página donde se introduce los datos de ubicación del cliente
   * @param  {number} id id del input
   * @param  {bool} state si esta vacio o no
   * @param  {string} value texto del input
   */
  onNewAddress(id, state, value) {
    this.location.address = value;
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona
   * sobre el boton para abrir el modal para buscar una
   * dirección
   */
  onOpenPlaceView() {
    this.push(place, {
      onPlaceSelected: this._onPlaceSelected,
    });
  }
  /**
   * Escuchador que se dispara cuando se presiona
   * un item de la lista de sugerencias del
   * componente Place
   */
  onPlaceSelected(address, latitude, longitude) {
    this.location.latitude = latitude;
    this.location.longitude = longitude;
    this.updatePictureDistances();

    this.setState({
      location: {
        latitude: latitude,
        longitude: longitude,
      },
    });
  }
  /**
   * Escuchador que se dispara cuando el usuario
   * elimina una foto de la lista de fotos
   * que se encuentra en el componente picture
   */
  onRemovePicture(pictures) {
    this.setState({ pictures: pictures });
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona
   * sobre el float button para tomar una foto que esta en
   * el componente picture
   */
  onOpenCamera() {
    if (this.location.latitude && this.location.longitude) {
      this.push(camera, {
        onPictureTaken: this._onPictureTaken,
        pictureTypes: this.props.configurationData.pictureTypes,
        pictures: this.state.pictures,
        latitude: this.state.pictureLatitude,
        longitude: this.state.pictureLongitude,
        maximumDistance: this.props.configurationData.maximumDistance,
        leadLatitude: this.location.latitude,
        leadLongitude: this.location.longitude,
        maxDecimalQuantityOfLatlng: this.props.session.maxDecimalQuantityOfLatlng,
      });
    } else {
      this.refs.viewPager.setPage(MAP);
      this.showErrorMessage('Debe establecer la ubicación en el mapa para poder utilizar la camara');
    }
  }
  /**
   * Escuchador que se dispara cuando el usuario
   * presiona en el check button del camera
   */
  onPictureTaken(picture, latitude, longitude) {
    let pictures = this.state.pictures.filter((item) => item.pictureTypeId !== picture.pictureTypeId);
    this.setState({
      pictures: pictures.concat([picture]),
      pictureLatitude: latitude,
      pictureLongitude: longitude,
    });
  }
  /**
   * Verifica si en los productos agregados hay alguno
   * restringido
   */
  thereIsNotARestrictedProduct() {
    const restrictedProducts = this.props.configurationData.restriction.restrictedProducts;
    const products = Object.keys(this.state.products);

    for (var productName of products) {//Navego todos los productos
      if (restrictedProducts.indexOf(productName)  !== -1)//Si el producto es restringido
        for (var plan of this.state.products[productName]) {//Navego la lista de planes agregados
          const properties = Object.keys(plan);

          for (var property of properties) {//Navego las propiedades del plan
            if (property !== 'price' && plan[property])//Verifico si al menos un campo de los planes
              return false;                             //agregados es distinto de vacio, a excepción de price
          }
        }
    }

    return true;
  }
  /**
   * Metodo que verifica en que pagina del viewPager
   * esta el error
   */
  whereAreErrors(fieldErrors) {

    if (fieldErrors) {

      if ((Object.keys(fieldErrors).length === 3 &&
        fieldErrors.latitude &&
        fieldErrors.longitude &&
        fieldErrors.availabilityOfService) ||
        (Object.keys(fieldErrors).length === 1 &&
        fieldErrors.availabilityOfService))
        return MAP;

      if (fieldErrors.name ||
        fieldErrors.identityNumber ||
        fieldErrors.foreignIdentityNumber ||
        fieldErrors.owner ||
        fieldErrors.enterpriseName ||
        fieldErrors.nit ||
        fieldErrors.phone ||
        fieldErrors.email ||
        fieldErrors.firstReferencePhone ||
        fieldErrors.secondReferencePhone ||
        fieldErrors.note
      )
        return FORM;

      return PRODUCT;
    }

    return FORM;
  }
  /**
   * Muestra errores cuando se ejecuta
   * el metodo executeGetConfigurationData,
   * si es que los hay
   */
  showErrorForGettingConfigurationData(nextProps) {
    if (nextProps.statusOfGettingConfigurationData.wasAnError)
      this.showErrorMessage(
        nextProps.statusOfGettingConfigurationData.error &&
        nextProps.statusOfGettingConfigurationData.error.errorMessage ||
        'Hubo un problema al obtener la configuración del formulario para registrar leads'
      );
  }
  /**
   * Muestra errores cuando se ejecuta el
   * metodo executeSaveLead, si es que los hay
   */
  showErrorMessageForSavingALead(nextProps) {
    if (nextProps.session &&//Para que no se muestre el snackbar cuando se cierra sesión, ya que se modifica el objeto sesión
        nextProps.statusOfSavingALead.wasAnError &&
        nextProps.statusOfSavingALead.savingLeadIsBeingExecuted) {//Si hubo error
      //Obtiene el index de la pagina donde hay error
      const pageWithError = this.whereAreErrors(nextProps.statusOfSavingALead.error.fieldErrors);
      //Muestra el mensaje de error
      this.showErrorMessage(
        pageWithError === MAP
        ? 'Debe establecer la ubicación en el mapa y verificar que se obtenga la cobertura'
        : nextProps.statusOfSavingALead.error && nextProps.statusOfSavingALead.error.errorCode == 413
        ? 'La petición excedió el tamaño máximo'
        : nextProps.statusOfSavingALead.error && nextProps.statusOfSavingALead.error.errorMessage
        ? nextProps.statusOfSavingALead.error.errorMessage
        : 'Hubo un problema al guardar el lead')
      //Para que se muestren los errores en los inputs
      if (nextProps.statusOfSavingALead.error.fieldErrors)
        this.setState({ fieldErrors: nextProps.statusOfSavingALead.error.fieldErrors });
      //Va a la pagina con errores
      this.refs.viewPager.setPage(pageWithError);
    }
  }
  /**
   * Muestra mensaje de exito si el metodo
   * executeSaveLead se ejecuto exitosamente
   */
  showSuccessMessageForSavingLead(previousProps, nextProps) {
    if (nextProps.session &&//Para que no se muestre el snackbar cuando se cierra sesión, ya que se modifica el objeto sesión
        !previousProps.statusOfSavingALead.wasAnError &&
        !nextProps.statusOfSavingALead.savingLeadIsBeingExecuted &&
        !nextProps.statusOfGettingConfigurationData.configurationDataIsBeingFetching &&
        !previousProps.statusOfGettingConfigurationData.configurationDataIsBeingFetching &&
        !nextProps.statusOfGettingAvailabilityOfService.availabilityOfServiceIsBeingFetched &&
        !previousProps.statusOfGettingAvailabilityOfService.availabilityOfServiceIsBeingFetched) {//Si no hubo error

      this.showSuccessMessage('El lead fue guardado exitosamente');
      //Para que se borren los errores en los inputs
      this.setState({ fieldErrors: nextProps.statusOfSavingALead.error.fieldErrors });
      this.pop();
    }
  }
  /**
   * Muestra u oculta el ProgressModal
   */
  showOrDismissProgressModal(nextProps) {
    if (nextProps.statusOfSavingALead.savingLeadIsBeingExecuted)
      this.showProgressModal('Guardando lead...');
    else
      this.dismissModal();
  }
  /**
   * Actualiza la distancia de las fotos tomadas
   */
  updatePictureDistances() {
    
    this.setState({
      pictures: this.state.pictures.map((pictureTaken) => Object.assign({}, pictureTaken, {
        distance: this.getDistanceInMeters(pictureTaken.latitude, pictureTaken.longitude)
      }))
    });
  }
  /**
   * Retorna la distancia en metros, desde la ubicación del
   * domicilio del cliente a la ubicacion de la foto tomada
   */
  getDistanceInMeters(pictureLatitude, pictureLongitude) {
    
    return geolib.getDistance(
      this.getTruncatedCoordinate({ latitude: this.location.latitude, longitude: this.location.longitude }),
      this.getTruncatedCoordinate({ latitude: pictureLatitude, longitude: pictureLongitude })
    );
  }
  /**
   * Redondea la latitud y longitud
   */
  getTruncatedCoordinate(coordinate) {

    return {
      latitude: parseFloat(this.truncate(coordinate.latitude)),
      longitude: parseFloat(this.truncate(coordinate.longitude))
    };
  }
  /**
   * Retorna un float truncado
   */
  truncate(floatToTruncate) {
    let floatSplited = String(floatToTruncate).split('.');
    let decimalsTruncated = floatSplited[1].substring(0, this.props.maxDecimalQuantityOfLatlng);
    
    return `${floatSplited[0]}.${decimalsTruncated}`;
  }
}

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  statusOfGettingConfigurationData: state.dataReducer.configurationReducer.statusOfGettingConfigurationData,
  configurationData: state.dataReducer.configurationReducer.configurationData,
  statusOfGettingAvailabilityOfService: state.dataReducer.configurationReducer.statusOfGettingAvailabilityOfService,
  availabilityOfService: state.dataReducer.configurationReducer.availabilityOfService,
  statusOfSavingALead: state.dataReducer.leadReducer.statusOfSavingALead,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetConfigurationData: executeGetConfigurationData,
  executeSaveLead: executeSaveLead,
  executeGetAvailabilityOfService: executeGetAvailabilityOfService,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LeadRegistration);
