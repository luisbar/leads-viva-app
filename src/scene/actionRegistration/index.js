/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Tab, TabHeading, Text } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../basisComponent.js';
import ActionForm from './component/actionForm/index.js';
import { executeGetALeadForSavingAnAction } from '../../data/lead/action.js';
import { executeSaveAnAction } from '../../data/action/action.js';
import { ViewPager, LeadInformationViewer, ProductViewer } from '../../component/index.js';
import { tabIconColor } from '../../color.js';
/**
 * Renderiza la vista para registrar una acción
 */
class ActionRegistration extends BasisComponent {

  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onSave = this.onSave.bind(this);
    this._onValidateForm = this.onValidateForm.bind(this);
    //State
    this.state = {
      fieldErrors: {},
    };
  }
  
  componentWillReceiveProps(nextProps) {
    this.showErrorForGettingALeadForSavingAnAction(nextProps);
    this.showErrorMessageForSavingAnAction(nextProps);
    this.showSuccessMessageForSavingAnAction(this.props, nextProps);
    this.showOrDismissProgressModal(nextProps);
  }
  
  render() {

    return (
      <ViewPager
        title={'Registrar acción'}
        onSave={this._onSave}
        showRightButtons={this.props.configurationDataForSavingAnAction && !this.props.statusOfSavingAnAction.savingAnActionIsBeingExecuted
          ? true
          : false}
        showButtonForOpenPlaceView={false}
        leftButtonIconName={'close'}>
      
        <Tab
          heading={this.renderHeading('pencil-box-outline', 'Registrar')}>
          
          <ActionForm
            ref={'actionForm'}
            configurationDataForSavingAnAction={this.props.configurationDataForSavingAnAction}
            onValidateForm={this._onValidateForm}
            showProgressScreen={this.props.statusOfGettingALeadForSavingAnAction.leadForSavingAnActionIsBeingFetching}
            fieldErrors={this.state.fieldErrors}/>
        </Tab>

        <Tab
          heading={this.renderHeading('information-outline', 'Información')}>
          
          <LeadInformationViewer
            leadInformation={this.props.leadForSavingAnAction && this.props.leadForSavingAnAction.information}
            showProgressScreen={this.props.statusOfGettingALeadForSavingAnAction.leadForSavingAnActionIsBeingFetching}/>
        </Tab>
        
        <Tab
          heading={this.renderHeading('shopping', 'Producto')}>
          
          <ProductViewer
            products={this.props.leadForSavingAnAction && this.props.leadForSavingAnAction.products}
            showProgressScreen={this.props.statusOfGettingALeadForSavingAnAction.leadForSavingAnActionIsBeingFetching}/>
        </Tab>
      </ViewPager>
    );
  }
  /**
   * Renderiza los headings de cada tab
   * @param  {string} icon nombre del icono
   * @param  {string} text texto que va en el tab
   */
  renderHeading(icon, text) {
    
    return (
      <TabHeading style={style.heading}>
        <Text>{text}</Text>
        <Icon size={20} name={icon} color={tabIconColor}/>
      </TabHeading>
    );    
  }
  /**
   * Invoca metodo para obtener el lead en el
   * cual se registrará la acción
   */
  componentDidMount() {
    this.props.executeGetALeadForSavingAnAction(
      this.props.leadId,
      this.props.session.accessToken,
      this.props.session.refreshToken,
    );
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona el boton para
   * guardar los datos de componente view pager
   */
  onSave() {
    this.refs.actionForm.executeValidateForm();
  }
  /**
   * Escuchador que se dispara despues de ejecutar
   * el metodo executeValidateForm del componente
   * ActionForm
   * @param  {object} inputs objeto que contiene el valor de todos los inputs del form
   */
  onValidateForm(inputs) {
    this.props.executeSaveAnAction(
      this.props.leadId,
      inputs,
      this.props.session.accessToken,
      this.props.session.refreshToken,
    );
  }
  /**
   * Muestra errores cuando se ejecuta
   * el metodo executeGetALeadForSavingAnAction,
   * si es que los hay
   */
  showErrorForGettingALeadForSavingAnAction(nextProps) {
    if (nextProps.statusOfGettingALeadForSavingAnAction.wasAnError)
      this.showErrorMessage(
        nextProps.statusOfGettingALeadForSavingAnAction.error &&
        nextProps.statusOfGettingALeadForSavingAnAction.error.errorMessage ||
        'Hubo un problema al obtener los datos de configuración para guardar una acción'
      );
  }
  /**
   * Muestra errores cuando se ejecuta el
   * metodo executeSaveAnAction, si es que los hay
   */
  showErrorMessageForSavingAnAction(nextProps) {
    if (nextProps.session &&//Para que no se muestre el snackbar cuando se cierra sesión, ya que se modifica el objeto sesión
        nextProps.statusOfSavingAnAction.wasAnError &&
        nextProps.statusOfSavingAnAction.savingAnActionIsBeingExecuted) {//Si hubo error
      //Muestra el mensaje de error
      this.showErrorMessage(
        nextProps.statusOfSavingAnAction.error &&
        nextProps.statusOfSavingAnAction.error.errorMessage ||
        'Hubo un problema al guardar la acción'
      )
      //Para que se muestren los errores en los inputs
      if (nextProps.statusOfSavingAnAction.error.fieldErrors)
        this.setState({ fieldErrors: nextProps.statusOfSavingAnAction.error.fieldErrors });
    }
  }
  /**
   * Muestra mensaje de exito si el metodo
   * executeSaveLead se ejecuto exitosamente
   */
  showSuccessMessageForSavingAnAction(previousProps, nextProps) {
    if (nextProps.session &&//Para que no se muestre el snackbar cuando se cierra sesión, ya que se modifica el objeto sesión
        !previousProps.statusOfSavingAnAction.wasAnError &&
        !nextProps.statusOfSavingAnAction.savingAnActionIsBeingExecuted &&
        !nextProps.statusOfGettingALeadForSavingAnAction.leadForSavingAnActionIsBeingFetching &&
        !previousProps.statusOfGettingALeadForSavingAnAction.leadForSavingAnActionIsBeingFetching) {//Si no hubo error

      this.showSuccessMessage('La acción fue guardada exitosamente');
      //Para que se borren los errores en los inputs
      this.setState({ fieldErrors: nextProps.statusOfSavingAnAction.error.fieldErrors });
      this.props.onActionRegistered();
      this.pop();
    }
  }
  /**
   * Muestra u oculta el ProgressModal
   */
  showOrDismissProgressModal(nextProps) {
    if (nextProps.statusOfSavingAnAction.savingAnActionIsBeingExecuted)
      this.showProgressModal('Guardando accion...');
    else
      this.dismissModal();
  }
}

ActionRegistration.propTypes = {
  leadId: PropTypes.string.isRequired,//Id del lead en el cual se registrará una acción
  onActionRegistered: PropTypes.func,//Escuchador que se dispara cuando se registro una accion exitosamente
};

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  statusOfGettingALeadForSavingAnAction: state.dataReducer.leadReducer.statusOfGettingALeadForSavingAnAction,
  leadForSavingAnAction: state.dataReducer.leadReducer.leadForSavingAnAction,
  configurationDataForSavingAnAction: state.dataReducer.leadReducer.configurationDataForSavingAnAction,
  statusOfSavingAnAction: state.dataReducer.actionReducer.statusOfSavingAnAction,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetALeadForSavingAnAction: executeGetALeadForSavingAnAction,
  executeSaveAnAction: executeSaveAnAction,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ActionRegistration);