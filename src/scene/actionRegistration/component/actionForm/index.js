/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Content } from 'native-base';

import PropTypes from 'prop-types';

import moment from 'moment';
import 'moment/locale/es';
/******************
 * Project import *
 ******************/
import BasisComponent from '../../../basisComponent.js';
import { FormWithAllInputs, ProgressScreen } from '../../../../component/index.js';
import {
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
  inputTextColor,
  inputLabelColor,
  inputPlaceholderColor,
  inputBackgroundColor,
  pickerColor,
  pickerLabelColor,
  pickerBackgroundColor,
} from '../../../../color.js';
/*************
 * Constants *
 *************/
const DATE_INPUT = 'date';
const TIME_INPUT = 'time';
const DURATION_INPUT = 'duration';
const ACTION_INPUT = 'action';
const NOTE_INPUT = 'note';
/**
 * Renderiza formulario para registrar una acción
 */
class ActionForm extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onFieldsOk = this.onFieldsOk.bind(this);
    this._onEmptyFields = this.onEmptyFields.bind(this);
    this._onConfirmDate = () => {};
    this._onConfirmTime = () => {};
    this._onDurationChanged = this.onDurationChanged.bind(this);
    this._onActionChanged = this.onActionChanged.bind(this);
    //Ids de la opción seleccionada de los dropdowns
    this.idOfDurationSelected;
    this.idOfActionSelected;
  }
  
  componentWillReceiveProps(nextProps) {
    //Setea el valor por defecto de los combos
    if (nextProps.configurationDataForSavingAnAction && !this.idOfDurationSelected) {
      this.idOfDurationSelected = nextProps.configurationDataForSavingAnAction.durations[0].id;
      this.idOfActionSelected = nextProps.configurationDataForSavingAnAction.actions[0].id;
    }
  }

  render() {

    return (
      <Content>
        {
          this.props.showProgressScreen
          ? this.renderProgressScreen()
          : this.renderForm()
        }
      </Content>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {
    
    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbarAndTabBar={true}/>
    );
  }
  /**
   * Renderiza el formulario
   */
  renderForm() {
    
    return (
      this.props.configurationDataForSavingAnAction &&
      <FormWithAllInputs
        ref={'form'}
        fields={this.getFields()}
        onFieldsOk={this._onFieldsOk}
        onEmptyFields={this._onEmptyFields}
        minimumDate={moment(new Date(), 'DD-MM-YYYY').toDate()}/>
    );
  }
  /**
   * Escuchador cuando el usuario cambia el
   * valor del DURATION_INPUT
   */
  onDurationChanged(newDuration, index) {
    this.idOfDurationSelected = this.props.configurationDataForSavingAnAction.durations[index].id;
  }
  /**
   * Escuchador cuando el usuario cambia el
   * valor del ACTION_INPUT
   */
  onActionChanged(newAction, index) {
    this.idOfActionSelected = this.props.configurationDataForSavingAnAction.actions[index].id;
  }
  /**
   * Invoca metodo para validar el formulario
   */
  executeValidateForm() {
    this.refs.form.validateForm();
  }
  /**
   * Escuchador que se dispara despues de ejecutar
   * el metodo validateForm del componente FormWithAllInputs,
   * siempre y cuando todos los campos requeridos esten
   * correctos
   * @param  {object} inputs objeto que contiene el valor de todos los inputs del form
   */
  onFieldsOk(inputs) {
    this.props.onValidateForm(this.getInputValues(inputs));
  }
  /**
   * Escuchador que se dispara despues de ejecutar
   * el metodo validateForm del componente FormWithAllInputs,
   * siempre y cuando algún campo requerido este
   * incorrecto
   * @param  {object} inputs objeto que contiene el valor de todos los inputs del form
   */
  onEmptyFields(inputs) {
    this.props.onValidateForm(this.getInputValues(inputs));
  }
  /**
   * Retorna json formateado para enviar a la api
   */
  getInputValues(inputs) {
    
    return {
      date: (inputs[DATE_INPUT] && inputs[DATE_INPUT].value) || '',
      time: (inputs[TIME_INPUT] && inputs[TIME_INPUT].value) || '',
      duration: this.idOfDurationSelected,
      action: this.idOfActionSelected,
      note: (inputs[NOTE_INPUT] && inputs[NOTE_INPUT].value) || '',
    };
  }
  /**
   * Retorna un objeto json que representa
   * todos los inputs que tendra el FormWithAllInputs
   */
  getFields() {
    
    return [
      {
        id: DATE_INPUT,
        type: 'dateInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Fecha',
        placeholder: 'Presione aquí',
        minLength: 1,
        maxLength: 10,
        returnKeyType: 'next',
        editable: false,
        onSubmitEditing: TIME_INPUT,
        visible: true,
        isRequired: false,
        onConfirmDate: this._onConfirmDate,
        errors: this.props.fieldErrors && this.props.fieldErrors.date,
      },
      {
        id: TIME_INPUT,
        type: 'timeInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Hora',
        placeholder: 'Presione aquí',
        minLength: 1,
        maxLength: 8,
        returnKeyType: 'next',
        editable: false,
        onSubmitEditing: NOTE_INPUT,
        visible: true,
        isRequired: false,
        onConfirmTime: this._onConfirmTime,
        errors: this.props.fieldErrors && this.props.fieldErrors.time,
      },
      {
        id: DURATION_INPUT,
        type: 'dropdown',
        colorPickerAndroid: pickerColor,
        labelColor: pickerLabelColor,
        pickerBackgroundColor: pickerBackgroundColor,
        label: 'Duración',
        items: this.props.configurationDataForSavingAnAction.durations.map((item) => item.name),
        visible: true,
        onItemSelected: this._onDurationChanged,
      },
      {
        id: ACTION_INPUT,
        type: 'dropdown',
        colorPickerAndroid: pickerColor,
        labelColor: pickerLabelColor,
        pickerBackgroundColor: pickerBackgroundColor,
        label: 'Acción',
        items: this.props.configurationDataForSavingAnAction.actions.map((item) => item.name),
        visible: true,
        onItemSelected: this._onActionChanged,
      },
      {
        id: NOTE_INPUT,
        type: 'multilineEditableInput',
        inputTextColor: inputTextColor,
        labelTextColor: inputLabelColor,
        placeholderTextColor: inputPlaceholderColor,
        backgroundInputColor: inputBackgroundColor,
        label: 'Observación',
        placeholder: 'Escriba aquí',
        multiline: true,
        minLength: 1,
        maxLength: 500,
        keyboardType: 'default',
        returnKeyType: 'done',
        editable: true,
        visible: true,
        isRequired: false,
        errors: this.props.fieldErrors && this.props.fieldErrors.note,
      },
    ]
  }
}

ActionForm.propTypes = {
  showProgressScreen: PropTypes.bool,//Bandera para saber si se muestra o no el ProgressScreen
  configurationDataForSavingAnAction: PropTypes.object,//Objeto que contiene los datos de configuración para registrar una acción
  onValidateForm: PropTypes.func.isRequired,//Escuchador que se dispara despues de ejecutar el metodo validateForm del componente FormWithAllInputs
  fieldErrors: PropTypes.object,//Objeto que contiene los errores de cada input
};

export default ActionForm;