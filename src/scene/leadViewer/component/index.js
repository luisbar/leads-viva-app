import StatusRecordViewer from './statusRecordViewer/index.js';
import MapViewer from './mapViewer/index.js';
import PictureViewer from './pictureViewer/index.js';

module.exports = {
  StatusRecordViewer,
  MapViewer,
  PictureViewer,
};
