/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import { Content, Form } from 'native-base';
import { View, Image } from 'react-native';

import MapView, { Marker } from 'react-native-maps';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import { InputWithValidator, ProgressScreen, CustomCallout } from '../../../../component/index.js';
import {
  inputTextColor,
  inputLabelColor,
  inputPlaceholderColor,
  inputBackgroundColor,
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
} from '../../../../color.js';
/**
 * Renderiza el visor del mapa
 */
class MapViewer extends PureComponent {

  constructor(props) {
    super(props);
  }
  
  render() {

    return (
      <Content>
      {
        this.props.showProgressScreen
        ? this.renderProgressScreen()
        : this.renderInputAndMap()
      }
      </Content>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {

    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbarAndTabBar={true}/>
    );
  }
  /**
   * Renderiza el input y el mapa
   */
  renderInputAndMap() {

    return (
      <View>
        {this.renderAddressInput()}
        {this.renderMap()}
      </View>
    );
  }
  /**
   * Renderiza el input para introducir la dirección
   */
  renderAddressInput() {
    
    return (
      this.props.location &&
      <Form>
        <InputWithValidator
          id={'address'}
          backgroundInputColor={inputBackgroundColor}
          inputTextColor={inputTextColor}
          labelTextColor={inputLabelColor}
          placeholderTextColor={inputPlaceholderColor}
          label={'Dirección'}
          placeholder={''}
          multiline={true}
          minLength={1}
          maxLength={200}
          keyboardType={'default'}
          returnKeyType={'done'}
          editable={false}
          isRequired={false}
          defaultValue={this.props.location && this.props.location.address}/>
      </Form>
    );
  }
  /**
   * Renderiza el mapa
   */
  renderMap() {

    return (
      this.props.location &&
      <MapView
        ref={'map'}
        style={style.map}
        showsUserLocation={false}
        showsMyLocationButton={false}
        zoomControlEnabled={true}
        initialRegion={this.getInitialRegion()}>
          {
            this.props.location &&
            this.props.location.latlng &&
            this.props.location.latlng.latitude &&
            this.props.location.latlng.longitude
            ? <Marker
                coordinate={this.props.location.latlng}>
                
                <CustomCallout
                  latitude={this.props.location && this.props.location.latlng.latitude}
                  longitude={this.props.location && this.props.location.latlng.longitude}
                  description={this.props.location && this.props.location.availabilityOfService}/>
                
                <Image
                  style={style.markerImage}
                  source={require('../../../../image/marker.png')}/>
              </Marker>
            : null
          }
      </MapView>
    );
  }
  /**
   * Retorna la region inicial a mostrar
   */
  getInitialRegion() {
    
    return {
      latitude: this.props.location && this.props.location.latlng.latitude || -17.7841546,
      longitude: this.props.location && this.props.location.latlng.longitude || -63.181788,
      latitudeDelta: 0.005,
      longitudeDelta: 0.005,
    }
  }
}

MapViewer.propTypes = {
  location: PropTypes.object,//Objeto que contiene la latitud, longitud del lead, ademas la dirección y cobertura del servicio
  showProgressScreen: PropTypes.bool,//Bandera para saber si se muestra o no el ProgressScreen
};

export default MapViewer;