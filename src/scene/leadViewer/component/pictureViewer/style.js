/***********************
 * Node modules import *
 ***********************/
import { Dimensions, StatusBar, Platform } from 'react-native';
/******************
 * Project import *
 ******************/
import {
  popupMenuBackgroundColor,
  picturePictureTypeBackgroundColor,
  picturePictureTypeTextColor,
  pictureEmptyText
} from '../../../../color.js';
/*************
 * Constants *
 *************/
const SCREEN_HEIGHT = Dimensions.get('screen').height;
const SCREEN_WIDTH = Dimensions.get('screen').width;
const IS_IPHONE_X = Platform.OS === 'ios' && SCREEN_HEIGHT === 812 && SCREEN_WIDTH === 375;
const STATUS_BAR_HEIGHT = StatusBar.currentHeight;
const TOOL_BAR_HEIGHT = Platform.OS === 'ios' ? (IS_IPHONE_X ? 88 : 64) : 56;
const TAB_BAR_HEIGHT = 50;

export default {

  container: {
    flex: 1,
  },
  listItemPictureType: {
    maxWidth: 100,
    marginBottom: 10,
    backgroundColor: picturePictureTypeBackgroundColor,
    color: picturePictureTypeTextColor,
    borderRadius: 5,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  imageLightBox: {
    flex: 1,
  },
  empty: {
    width: '100%',
    height: SCREEN_HEIGHT - (STATUS_BAR_HEIGHT + TOOL_BAR_HEIGHT + TAB_BAR_HEIGHT),
    flexDirection: 'column',
    justifyContent: 'center',
  },
  emptyText: {
    color: pictureEmptyText,
    textAlign: 'center',
  },
}