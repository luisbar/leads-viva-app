/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import { View, Image } from 'react-native';
import { Content, Text, List, ListItem, Thumbnail, Body } from 'native-base';
import Lightbox from 'react-native-lightbox';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import ProgressScreen from '../../../../component/index.js';
import {
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
} from '../../../../color.js';
/**
 * Renderiza la vista para ver las fotos
 */
class PictureViewer extends PureComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._renderItem = this.renderItem.bind(this);
    this._renderContent = (item) => this.renderContent.bind(this, item);
  }
  
  render() {
    
    return (
      <Content>
        {
          this.props.showProgressScreen
          ? this.renderProgressScreen()
          : this.renderPictureList()
        }
      </Content>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {

    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbarAndTabBar={true}/>
    );
  }
  /**
   * Renderiza la lista de fotos
   */
  renderPictureList() {

    return (
      this.props.pictures && this.props.pictures.length !== 0
      ? <View style={style.container}>
          <List
            dataArray={this.props.pictures}
            renderRow={this._renderItem}/>
        </View>
      : <View style={style.empty}>
          <Text
            style={style.emptyText}>
            {'No hay fotos para mostrar'}
          </Text>
        </View>
    );
  }
  /**
   * Renderiza los items de la lista de fotos tomadas
   */
  renderItem(item, index) {

    return (
      <ListItem>
        <Lightbox
          swipeToDismiss={false}
          renderContent={this._renderContent(item)}>
          <Thumbnail
            square
            size={80}
            source={{ uri: item.picture }}/>
        </Lightbox>
        
        <Body>
          <Text style={style.listItemPictureType}>{item.pictureTypeName}</Text>
          {
            item.distance || item.distance === 0
            ? <Text>{`Esta foto fue tomada a ${item.distance} metro(s) del lead\n`}</Text>
            : null
          }
          <Text>{item.creationDate}</Text>
        </Body>
      </ListItem>
    );
  }
  /**
   * Renderiza el contenido del Lightbox
   */
  renderContent(item) {

    return (
      <Image
        style={style.imageLightBox}
        source={{ uri: item.picture }}
        resizeMode={'center'}/>
    );
  }
}

PictureViewer.propTypes = {
  pictures: PropTypes.array,//Array de fotos tomadas
  showProgressScreen: PropTypes.bool.isRequired,//Bandera para saber si se muestra o no el ProgressScreen
};

export default PictureViewer;