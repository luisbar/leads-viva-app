/******************
 * Project import *
 ******************/
import {
  statusRecordListHeaderBackground,
  statusRecordListHeaderItemBackground,
  statusRecordListHeaderItemLeftSideText,
  statusRecordListHeaderItemRightSideText,
  statusRecordListItemBackground,
  statusRecordLeftSubItemOfList,
  statusRecordTextOfLeftSubItemOfList,
  statusRecordRightSubItemOfList,
  statusRecordTextOfRightSubItemOfList,
  statusRecordTextOfheaderOfItemOfList,
  statusRecordSeparatoraOfSubItemOfList,
} from '../../../../color.js';

export default {
  
  list: {
    flex: 1,
    paddingRight: 5,
    paddingLeft: 5,
  },
  listHeader: {
    flexDirection: 'column',
    elevation: 3,
    borderRadius: 20,
    backgroundColor: statusRecordListHeaderBackground,
    marginLeft: 5,
    marginTop: 10,
    marginRight: 5,
    marginBottom: 5,
    overflow: 'hidden',
  },
  listFooter: {
    marginBottom: 5,
  },
  listItem: {
    flexDirection: 'column',
    elevation: 3,
    borderRadius: 20,
    backgroundColor: statusRecordListItemBackground,
    margin: 5,
    overflow: 'hidden',
  },
  listHeaderItem: {
    flexDirection: 'row',
    padding: 10,
  },
  listHeaderItemWithDarkBackground: {
    backgroundColor: statusRecordListHeaderItemBackground,
    flexDirection: 'row',
    padding: 10,
  },
  listHeaderItemLeftSideText: {
    color: statusRecordListHeaderItemLeftSideText,
    width: '50%',
    textAlign: 'center',
    textAlignVertical: 'center',
    marginRight: 10
  },
  listHeaderItemRightSideText: {
    color: statusRecordListHeaderItemRightSideText,
    width: '50%',
    textAlign: 'left',
    textAlignVertical: 'center',
  },
  subItemOfList: {
    flexDirection: 'row',
  },
  leftSubItemOfList: {
    backgroundColor: statusRecordLeftSubItemOfList,
    justifyContent: 'center',
    flex: 1,
  },
  textOfLeftSubItemOfList: {
    color: statusRecordTextOfLeftSubItemOfList,
    padding: 10,
  },
  rightSubItemOfList: {
    backgroundColor: statusRecordRightSubItemOfList,
    justifyContent: 'center',
    flex: 1.3,
  },
  textOfRightSubItemOfList: {
    color: statusRecordTextOfRightSubItemOfList,
    padding: 10,
  },
  headerOfItemOfList: {
    height: 50,
    justifyContent: 'center',
  },
  textOfheaderOfItemOfList: {
    color: statusRecordTextOfheaderOfItemOfList,
  },
  separatoraOfSubItemOfList: {
    height: 1,
    backgroundColor: statusRecordSeparatoraOfSubItemOfList,
  },
}