/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';
import { Content, Title, Text } from 'native-base';
import { View, FlatList } from 'react-native';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import { ProgressScreen } from '../../../../component/index.js';
import {
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
} from '../../../../color.js';
/**
 * Renderiza el historial
 * de estados de un lead
 */
class StatusRecordViewer extends PureComponent {

  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._renderItem = this.renderItem.bind(this);
  }

  render() {

    return (
      <Content>
        {
          this.props.showProgressScreen
          ? this.renderProgressScreen()
          : this.renderStatusList()
        }
      </Content>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {
    
    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbarAndTabBar={true}/>
    );
  }
  /**
   * Renderiza la lista de estados de un lead
   */
  renderStatusList() {
    
    return this.props.statusRecord &&
    <FlatList
      style={style.list}
      data={this.props.statusRecord.statusRecords}
      ListHeaderComponent={this.renderHeader(this.props.statusRecord)}
      renderItem={this._renderItem}
      ListFooterComponent={this.renderFooter()}/>
  }
  /**
   * Renderiza el header de la lista de estados
   * @param  {object} statusRecord información acerca del estado de un lead
   */
  renderHeader(statusRecord) {
    
    return (
      <View style={style.listHeader}>
        {this.renderHeaderItemWithDarkBackgound('Fecha de creación', statusRecord.creationDate)}
        {this.renderHeaderItem('Usuario actual', statusRecord.seller)}
      </View>
    );
  }
  /**
   * Renderiza item del header
   * @param  {string} label label del item a mostrar en el header
   * @param  {string} data dato a mostrar
   */
  renderHeaderItem(label, data) {
    
    return (
      <View style={style.listHeaderItem}>
        <Title numberOfLines={2} style={style.listHeaderItemLeftSideText}>{label}</Title>
        <Text style={style.listHeaderItemRightSideText}>{data}</Text>
      </View>
    );
  }
  /**
   * Renderiza item del header con 
   * color de fondo obscuro
   * @param  {string} label label del item a mostrar en el header
   * @param  {string} data dato a mostrar
   */
  renderHeaderItemWithDarkBackgound(label, data) {
    
    return (
      <View style={style.listHeaderItemWithDarkBackground}>
        <Title numberOfLines={2} style={style.listHeaderItemLeftSideText}>{label}</Title>
        <Text style={style.listHeaderItemRightSideText}>{data}</Text>
      </View>
    );
  }
  /**
   * Renderiza los items de la lista de estados
   * @param  {object} item  objeto estado
   * @param  {number} index index del estado
   */
  renderItem({item, index}) {
    
    return (
      <View
        style={style.listItem}
        key={index}>
        {this.renderSubItem('Estado', item.status, 0)}
        {this.renderSubItem('Fecha', item.updateDate, 1)}
        {this.renderSubItem('Vendedor', item.seller, 2)}
        {this.renderSubItem('Motivo', item.reasonForClosing, 3)}
        {this.renderSubItem('Justificación', item.additionalInformation, 4)}
      </View>
    );
  }
  /**
   * Renderiza los sub items de la lista de estados
   * @param  {string} label label del subitem
   * @param  {string} data dato a mostrar acerca del estado
   */
  renderSubItem(label, data, index) {
    
    return (
      <View>
        {index !== 0 && this.renderItemContentSeparator()}
        <View style={style.subItemOfList}>
          <View style={style.leftSubItemOfList}>
            <Title
              style={style.textOfLeftSubItemOfList}
              numberOfLines={2}>
              {label}
            </Title>
          </View>
          
          <View style={style.rightSubItemOfList}>
            <Text style={style.textOfRightSubItemOfList}>{data}</Text>
          </View>
        </View>
      </View>
    );
  }
  /**
   * Renderiza el separador de cada subitem de la lista
   * de estados
   */
  renderItemContentSeparator() {
    
    return (
      <View style={style.separatoraOfSubItemOfList}/>
    );
  }
  /**
   * Renderiza el footer de la lista de estados
   */
  renderFooter() {
    
    return (<View style={style.listFooter}/>);
  }
}

StatusRecordViewer.propTypes = {
  statusRecord: PropTypes.object,//Estados de un lead
  showProgressScreen: PropTypes.bool.isRequired,//Bandera para saber si mostrar o no el progress screen
};

export default StatusRecordViewer;