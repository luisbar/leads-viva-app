/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Tab, TabHeading, Text } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../basisComponent.js';
import { executeGetALeadWithStatusRecord } from '../../data/lead/action.js';
import { ViewPager, LeadInformationViewer, ProductViewer } from '../../component/index.js';
import { tabIconColor } from '../../color.js';
import { StatusRecordViewer, MapViewer, PictureViewer } from './component/index.js';
/**
 * Renderiza la vista para ver un lead
 * , ademas el historial de estados
 */
class LeadViewer extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onSave = () => {};
    this._openPlaceView = () => {};
  }
  
  componentWillReceiveProps(nextProps) {
    if (nextProps.statusOfGettingALeadWithStatusRecord.wasAnError)
      this.showErrorMessage(
        nextProps.statusOfGettingALeadWithStatusRecord.error &&
        nextProps.statusOfGettingALeadWithStatusRecord.error.errorMessage ||
        `Hubo un problema al obtener el lead`
      );
  }

  render() {

    return (
      <ViewPager
        ref={'viewPager'}
        title={'Ver lead'}
        onSave={this._onSave}
        onOpenPlaceView={this._openPlaceView}
        showRightButtons={false}
        showButtonForOpenPlaceView={false}
        leftButtonIconName={'close'}>

        <Tab
          heading={this.renderHeading('information-outline', 'Información')}>
          
          <LeadInformationViewer
            leadInformation={this.props.leadWithStatusRecord && this.props.leadWithStatusRecord.information}
            showProgressScreen={this.props.statusOfGettingALeadWithStatusRecord.leadWithStatusRecordIsBeingFetching}/>
        </Tab>
        
        <Tab
          heading={this.renderHeading('shopping', 'Producto')}>
          
          <ProductViewer
            products={this.props.leadWithStatusRecord && this.props.leadWithStatusRecord.products}
            showProgressScreen={this.props.statusOfGettingALeadWithStatusRecord.leadWithStatusRecordIsBeingFetching}/>
        </Tab>
        
        <Tab
          heading={this.renderHeading('history', 'Historial')}>
          
          <StatusRecordViewer
            statusRecord={this.props.leadWithStatusRecord && this.props.leadWithStatusRecord.statusRecord}
            showProgressScreen={this.props.statusOfGettingALeadWithStatusRecord.leadWithStatusRecordIsBeingFetching}/>
        </Tab>
        
        <Tab
          heading={this.renderHeading('map-marker', 'Ubicación')}>
          
          <MapViewer
            location={this.props.leadWithStatusRecord && this.props.leadWithStatusRecord.location}
            showProgressScreen={this.props.statusOfGettingALeadWithStatusRecord.leadWithStatusRecordIsBeingFetching}/>
        </Tab>
        
        <Tab
          heading={this.renderHeading('camera', 'Foto')}>
          
          <PictureViewer
            pictures={this.props.leadWithStatusRecord && this.props.leadWithStatusRecord.pictures}
            showProgressScreen={this.props.statusOfGettingALeadWithStatusRecord.leadWithStatusRecordIsBeingFetching}/>
        </Tab>
      </ViewPager>
    );
  }
  /**
   * Renderiza los headings de cada tab
   * @param  {string} icon nombre del icono
   * @param  {string} text texto que va en el tab
   */
  renderHeading(icon, text) {
    
    return (
      <TabHeading style={style.heading}>
        <Text>{text}</Text>
        <Icon size={20} name={icon} color={tabIconColor}/>
      </TabHeading>
    );    
  }
  /**
   * Invoca metodo para obtener el lead a ver,
   * mas el historial de estados
   */
  componentDidMount() {
    this.props.executeGetALeadWithStatusRecord(
      this.props.leadId,
      this.props.session.accessToken,
      this.props.session.refreshToken,
    );
  }
}

LeadViewer.propTypes = {
  leadId: PropTypes.string.isRequired,//Id del lead que se quiere ver
};

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  statusOfGettingALeadWithStatusRecord: state.dataReducer.leadReducer.statusOfGettingALeadWithStatusRecord,
  leadWithStatusRecord: state.dataReducer.leadReducer.leadWithStatusRecord,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetALeadWithStatusRecord: executeGetALeadWithStatusRecord,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LeadViewer);