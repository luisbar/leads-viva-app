/******************
 * Project import *
 ******************/
import {
  toolbarBackgroundColor,
  toolbarBorderColor,
  toolbarTextColor,
  actionsRecordListHeaderBackground,
  actionsRecordListHeaderItemBackground,
  actionsRecordListHeaderItemLeftSideText,
  actionsRecordListHeaderItemRightSideText,
  actionsRecordListItemBackground,
  actionsRecordListItemContentLeftSideBackground,
  actionsRecordListItemContentLeftSideText,
  actionsRecordListItemContentRightSideBackground,
  actionsRecordListItemContentRightSideText,
  actionsRecordListItemContentSeparator
} from '../../color.js';

export default {
  
  toolbar: {
    backgroundColor: toolbarBackgroundColor,
    borderColor: toolbarBorderColor,
  },
  title: {
    color: toolbarTextColor,
  },
  list: {
    flex: 1,
    paddingRight: 5,
    paddingLeft: 5,
  },
  listHeader: {
    flexDirection: 'column',
    elevation: 3,
    borderRadius: 20,
    backgroundColor: actionsRecordListHeaderBackground,
    marginLeft: 5,
    marginTop: 10,
    marginRight: 5,
    marginBottom: 5,
    overflow: 'hidden',
  },
  listHeaderItem: {
    flexDirection: 'row',
    padding: 10,
  },
  listHeaderItemWithDarkBackground: {
    backgroundColor: actionsRecordListHeaderItemBackground,
    flexDirection: 'row',
    padding: 10,
  },
  listHeaderItemLeftSideText: {
    color: actionsRecordListHeaderItemLeftSideText,
    flex: 1,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  listHeaderItemRightSideText: {
    color: actionsRecordListHeaderItemRightSideText,
    flex: 1,
    textAlign: 'left',
    textAlignVertical: 'center',
  },
  listFooter: {
    marginBottom: 5,
  },
  listItem: {
    flexDirection: 'column',
    elevation: 3,
    borderRadius: 20,
    backgroundColor: actionsRecordListItemBackground,
    margin: 5,
    overflow: 'hidden',
  },
  listItemContent: {
    flexDirection: 'row',
  },
  listItemContentLeftSide: {
    backgroundColor: actionsRecordListItemContentLeftSideBackground,
    justifyContent: 'center',
    flex: 1,
  },
  listItemContentLeftSideText: {
    color: actionsRecordListItemContentLeftSideText,
    padding: 10,
  },
  listItemContentRightSide: {
    backgroundColor: actionsRecordListItemContentRightSideBackground,
    justifyContent: 'center',
    flex: 1.3,
  },
  listItemContentRightSideText: {
    color: actionsRecordListItemContentRightSideText,
    padding: 10,
  },
  listItemContentSeparator: {
    height: 1,
    backgroundColor: actionsRecordListItemContentSeparator,
  },
};