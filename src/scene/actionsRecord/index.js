/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Container, Text, Title, StyleProvider } from 'native-base';
import { FlatList, View } from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../basisComponent.js';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import { NavigationBar, ProgressScreen } from '../../component/index.js';
import { executeGetActions } from '../../data/action/action.js';
import {
  toolbarButtonColor,
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
} from '../../color.js';
/**
 * Muestra el historial de acciones
 */
class ActionsRecord extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onClose = this.onClose.bind(this);
    this._renderItem = this.renderItem.bind(this);
  }
  
  componentWillReceiveProps(nextProps) {
    if (nextProps.statusOfGettingActions.wasAnError)
      this.showErrorMessage(
        nextProps.statusOfGettingActions.error &&
        nextProps.statusOfGettingActions.error.errorMessage ||
        'Hubo un problema al obtener el historial de acciones'
      );
  }

  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <NavigationBar
            toolBarStyle={style.toolbar}
            leftButtonVisible={true}
            onPressLeftButton={this._onClose}
            iconLeftButton={'md-arrow-back'}
            colorIconLeftButton={toolbarButtonColor}
            title={'Historial de acciones'}
            titleStyle={style.title}/>
          {
            this.props.statusOfGettingActions.actionsAreBeingFetching
            ? this.renderProgressScreen()
            : this.renderActionList()
          }  
        </Container>
      </StyleProvider>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {

    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbar={true}/>
    );
  }
  /**
   * Renderiza la lista de acciones
   */
  renderActionList() {
    
    return (
      this.props.actions && this.props.lead &&
      <FlatList
        style={style.list}
        data={this.props.actions}
        ListHeaderComponent={this.renderHeader(this.props.lead)}
        renderItem={this._renderItem}
        ListFooterComponent={this.renderFooter()}/>
    );
  }
  /**
   * Renderiza el header de la lista de acciones
   * @param  {object} lead información del lead
   */
  renderHeader(lead) {
    
    return (
      <View style={style.listHeader}>
        {this.renderHeaderItemWithDarkBackgound('Nro. lead', lead.number)}
        {this.renderHeaderItem('Nombre', lead.name)}
        {this.renderHeaderItemWithDarkBackgound('Canal', lead.chanel)}
        {this.renderHeaderItem('Ciudad', lead.city)}
      </View>
    );
  }
  /**
   * Renderiza item del header
   * @param  {string} label label del item a mostrar en el header
   * @param  {string} data dato a mostrar
   */
  renderHeaderItem(label, data) {
    
    return (
      <View style={style.listHeaderItem}>
        <Title style={style.listHeaderItemLeftSideText}>{label}</Title>
        <Text style={style.listHeaderItemRightSideText}>{data}</Text>
      </View>
    );
  }
  /**
   * Renderiza item del header con 
   * color de fondo obscuro
   * @param  {string} label label del item a mostrar en el header
   * @param  {string} data dato a mostrar
   */
  renderHeaderItemWithDarkBackgound(label, data) {
    
    return (
      <View style={style.listHeaderItemWithDarkBackground}>
        <Title style={style.listHeaderItemLeftSideText}>{label}</Title>
        <Text style={style.listHeaderItemRightSideText}>{data}</Text>
      </View>
    );
  }
  /**
   * Renderiza los items de la lista de acciones
   * @param  {object} item  objeto acción
   * @param  {number} index index de la acción
   */
  renderItem({item, index}) {
    
    return this.renderContainerOfItem(item, index);
  }
  /**
   * Renderiza el container de cada item de la lista
   * de acciones
   * @param  {object} item  objeto acción
   * @param  {number} index index de la acción
   */
  renderContainerOfItem(item, index) {
    
    return (
      <View
        style={style.listItem}
        key={index}>
        {this.renderSubItem('Acción', item.action, 0)}
        {this.renderSubItem('Fecha y hora', item.date, 1)}
        {this.renderSubItem('Respuesta', item.answer, 2)}
        {this.renderSubItem('Motivo de cierre', item.reason, 3)}
        {this.renderSubItem('Justificación o Nro. proceso de venta', item.justification, 4)}
      </View>
    );
  }
  /**
   * Renderiza los sub items de la lista de acciones
   * @param  {string} label label del subitem
   * @param  {string} data dato a mostrar acerca de la accion
   */
  renderSubItem(label, data, index) {
    
    return (
      <View key={index}>
        <View style={style.listItemContent}>
          <View style={style.listItemContentLeftSide}>
            <Title
              style={style.listItemContentLeftSideText}
              numberOfLines={2}>
              {label}
            </Title>
          </View>
          
          <View style={style.listItemContentRightSide}>
            <Text style={style.listItemContentRightSideText}>{data}</Text>
          </View>
        </View>
        {index !== 4 && this.renderItemContentSeparator()}
      </View>
    );
  }
  /**
   * Renderiza el separador de cada subitem de la lista
   * de acciones
   */
  renderItemContentSeparator() {
    
    return (
      <View style={style.listItemContentSeparator}/>
    );
  }
  /**
   * Renderiza el footer de la lista de acciones
   */
  renderFooter() {
    
    return (<View style={style.listFooter}/>);
  }
  /**
   * Obtiene las acciones
   */
  componentDidMount() {
    this.props.executeGetActions(
      this.props.leadId,
      this.props.session.accessToken,
      this.props.session.refreshToken
    );
  }
  /**
   * Escuchador que se dispara cuando se presiona
   * el boton para cerrar la vista
   */
  onClose() {
    //Remueve la vista actual de la pila
    this.pop();
  }
}

ActionsRecord.propTypes = {
  leadId: PropTypes.string.isRequired,//Id del lead del cual se quiere obtener las acciones
};

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  actions: state.dataReducer.actionReducer.actions,
  lead: state.dataReducer.actionReducer.lead,
  statusOfGettingActions: state.dataReducer.actionReducer.statusOfGettingActions,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetActions: executeGetActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ActionsRecord);