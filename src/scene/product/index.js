/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { View, FlatList } from 'react-native';
import { Container, Fab, Title, Text, Picker, Item, Input, Button, Header, Body, StyleProvider } from 'native-base';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import BasisComponent from '../basisComponent.js';
import style from './style.js';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import { NavigationBar } from '../../component/index.js';
import {
  toolbarButtonColor,
  productAddButtonIconColor,
  productListItemContentActionRightSideButtonIconColor,
} from '../../color.js';
/*************
 * Constants *
 *************/
//Productos
const POST_PAID = 'postPaid';
const LTE = 'lte';
const PUBLIC_TELEPHONY = 'publicTelephony';
const VPN = 'vpn';
const BAG = 'bag';
const OTHER = 'other';
/**
 * Renderiza formulario para agregar productos
 */
class Product extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onPressLeftButton = () => this.pop();
    this._onPressRightButton = this.onPressRightButton.bind(this);
    this._onPressAddButton = this.onAddItem.bind(this);
    this._renderItem = this.renderItem.bind(this);
    this._onRemoveItem = (itemIndex) => this.onRemoveItem.bind(this, itemIndex);
    this._onValueChanged = (input, itemIndex) => this.onValueChange.bind(this, input, itemIndex)
    this._onTextChange = (input, itemIndex) => this.onTextChange.bind(this, input, itemIndex)
    //State
    this.state = {
      plansAdded: this.getPlansAdded(),
      inputsPerProduct: this.getInputsByProduct(),
    };
  }

  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <NavigationBar
            hasTabs
            toolBarStyle={style.toolbar}
            leftButtonVisible={true}
            onPressLeftButton={this._onPressLeftButton}
            iconLeftButton={'md-arrow-back'}
            colorIconLeftButton={toolbarButtonColor}
            title={'Producto'}
            titleStyle={style.title}
            rightButtonsVisible={true}
            rightButtons={this.getRightButtons()}/>
          {
            this.props.productPressed.id !== 'other' &&
            <Header style={style.totalContainer}>
              <Body>
                <Text style={style.totalText}>
                  {`Sub total Bs. ${this.getSubTotal()}`}
                </Text>
              </Body>
            </Header>
          }
          {this.renderPlanList()}
          {this.renderAddButton()}
        </Container>
      </StyleProvider>
    );
  }
  /**
   * Renderiza la lista de planes que han sido
   * agregados
   */
  renderPlanList() {
    
    return (
      <View style={style.listContainer}>
        <FlatList
          style={style.list}
          data={this.state.plansAdded[this.props.productPressed.id]}
          ListHeaderComponent={this.renderHeader()}
          renderItem={this._renderItem}
          ListFooterComponent={this.renderFooter()}/>
      </View>
    );
  }
  /**
   * Renderiza el header de la lista de planes agregados
   */
  renderHeader() {
    
    return (
      <View style={style.listHeader}/>
    );
  }
  /**
   * Renderiza los items de la lista de planes agregados, los inputs de la lista de
   * planes son renderizados en base a la propiedad productPressed
   */
  renderItem({item, index}) {
    const itemIndex = index;
    
    return (
      <View style={style.listItem}>
        {this.renderItemHeader()}
        {
          this.state.inputsPerProduct[this.props.productPressed.id].map((input, inputIndex) =>
            this.renderItemContent(input, inputIndex, itemIndex)
          )
        }
        {this.renderItemActions(itemIndex)}
        {this.renderItemFooter()}
      </View>
    );
  }
  /**
   * Renderiza el header de cada item de la lista
   * de planes agregados
   */
  renderItemHeader() {
    
    return (
      <View style={style.listItemHeader}>
        <Title style={style.listItemHeaderText}>
          {this.props.productPressed.label.toUpperCase()}
        </Title>
      </View>
    );
  }
  /**
   * Renderiza el contenido de cada item de la lista
   * de planes agregados
   */
  renderItemContent(input, inputIndex, itemIndex) {
    
    return (
      <View key={inputIndex}>
        <View style={style.listItemContent}>
          <View style={style.listItemContentLeftSide}>
            <Title
              style={style.listItemContentLeftSideText}
              numberOfLines={2}>
              {input.label}
            </Title>
          </View>
          
          <View style={style.listItemContentRightSide}>
            {this.renderItemContentInputByType(input, itemIndex)}
            <Text style={style.inputError}>
              {this.getInputErrors(itemIndex, input.id)}
            </Text>
          </View>
        </View>
        {this.renderItemContentSeparator()}
      </View>
    );
  }
  /**
   * Renderiza el input del contenido de cada item de la lista
   * de planes agregados
   */
  renderItemContentInputByType(input, itemIndex) {
    
    switch (input.type) {

      case 'dropdown':
        return (
          <View style={style.inputContainer}>
            <Picker
              mode={'dialog'}
              selectedValue={this.state.plansAdded[this.props.productPressed.id][itemIndex][input.id] || input.items[0]}
              onValueChange={this._onValueChanged(input, itemIndex)}>
              {input.items.map((item, index) => <Item key={index} label={item} value={item}/>)}
            </Picker>
          </View>
        );
        
      case 'textInput':
        return (
          <View style={style.inputContainer}>
            <Input
              value={this.state.plansAdded[this.props.productPressed.id][itemIndex][input.id]}
              keyboardType={input.keyboardType}
              onChangeText={this._onTextChange(input, itemIndex)}/>            
          </View>
        );
        
      case 'text':
        return (
          <View style={style.inputContainer}>
            <Input
              value={this.state.plansAdded[this.props.productPressed.id][itemIndex][input.id]}
              editable={false}/>
          </View>
        );
    }
  }
  /**
   * Renderiza las acciones de cada item de la lista de 
   * planes agregados
   */
  renderItemActions(itemIndex) {
    
    return (
      <View>
        <View style={style.listItemContentAction}>
          <View style={style.listItemContentActionLeftSide}>
            <Title
              style={style.listItemContentActionLeftSideText}
              numberOfLines={2}>
              {'Acciones'}
            </Title>
          </View>
          
          <View style={style.listItemContentActionRightSide}>
            <Button
              style={style.listItemContentActionRightSideButton}
              onPress={this._onRemoveItem(itemIndex)}>
              <Icon
                name={'close'}
                size={30}
                color={productListItemContentActionRightSideButtonIconColor}/>
            </Button>
          </View>
        </View>
        {this.renderItemContentSeparator()}
      </View>
    );
  }
  /**
   * Renderiza el separador de cada subitem de la lista
   * de planes agregados
   */
  renderItemContentSeparator() {
    
    return (
      <View style={style.listItemContentSeparator}/>
    );
  }
  /**
   * Renderiza el footer de cada item de la lista de planes
   */
  renderItemFooter() {
    let total = 0;
    return (
      <View style={style.listItemFooter}>
        <View style={style.listItemFooterLeftSide}/>
        
        <View style={style.listItemFooterRightSide}/>
      </View>
    );
  }
  /**
   * Renderiza el footer de la lista de planes agregados
   */
  renderFooter() {
    
    return (
      <View style={style.listFooter}/>
    );
  }
  /**
   * Renderiza el boton para agregar un item
   * a la lista de planes
   */
  renderAddButton() {
    
    return (
      <Fab
        position={'bottomRight'}
        style={style.addButton}
        onPress={this._onPressAddButton}>
        
        <Icon name={'add'} size={20} color={productAddButtonIconColor}/>
      </Fab>
    );
  }
  /**
   * Escuchador que se dispara cuando se presiona
   * el check button del toolbar
   */
  onPressRightButton() {
    this.props.onAddProducts(this.state.plansAdded);
    this.pop();
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona
   * la accion de remover item de un item de la lista
   * de planes
   */
  onRemoveItem(itemIndex) {
    //Se obtiene la lista de planes por el producto seleccionado
    let aux = this.state.plansAdded[this.props.productPressed.id];
    //Se elimina el objeto por la posición
    aux.splice(itemIndex, 1);
    //Actualizamos el state
    this.setState({
      plansAdded: Object.assign({}, this.state.plansAdded, {
        [this.props.productPressed.id]: aux,
      })
    });
  }
  /**
   * Escuchador que se dispara cuando se presiona el boton
   * para agregar un item a la lista de planes
   */
  onAddItem() {
    //Se obtiene la lista de planes por el producto seleccionado
    let aux = this.state.plansAdded[this.props.productPressed.id];
    //Se agrega un nuevo objeto por el nuevo item a agregar
    aux.push(this.getInputValuesByProductPressed())
    //Actualizamos el state
    this.setState({
      plansAdded: Object.assign({}, this.state.plansAdded, {
        [this.props.productPressed.id]: aux,
      })
    });
  }
  /**
   * Escuchador que se dispara cuando el usuario
   * cambia el valor de algún picker
   */
  onValueChange(input, itemIndex, value, valueIndex) {
    const  { configurationData, productPressed } = this.props;
    const plansAdded = this.state.plansAdded;
    //Se obtiene la lista de planes por el producto seleccionado
    let aux = plansAdded[productPressed.id];
    //Se actualiza el valor del input del item por la posición del item y el identificador del input
    //siempre y cuando el id sea distinto de vacio
    aux[itemIndex][input.id] = !configurationData[productPressed.id][input.id][valueIndex].id ? '' : value;
    //Se guarda el id del item seleccionado de los pickers
    this.savePickerIndexInObject(input, aux, itemIndex, configurationData, productPressed, valueIndex);
    //Se calcula la tarifa si ya se introdujo cantidad y si el inputId es igual a plan, data, minut, credit o friendGroup
    if (aux[itemIndex].quantity && (input.id === 'plan' ||
        input.id === 'data' ||
        input.id === 'minut' ||
        input.id === 'credit' ||
        input.id === 'friendGroup')) {
      //Se obtiene el precio de los planes por el index
      const price = aux[itemIndex]['plan'] ? configurationData[productPressed.id].plan[aux[itemIndex].planIndex].price : 0;
      const priceByData = aux[itemIndex]['data'] ? configurationData[productPressed.id].data[aux[itemIndex].dataIndex].price : 0;
      const priceByMinut = aux[itemIndex]['minut'] ? configurationData[productPressed.id].minut[aux[itemIndex].minutIndex].price : 0;
      const priceByCredit = aux[itemIndex]['credit'] ? configurationData[productPressed.id].credit[aux[itemIndex].creditIndex].price : 0;
      const priceByFriendGroup = aux[itemIndex]['friendGroup'] ? configurationData[productPressed.id].friendGroup[aux[itemIndex].friendGroupIndex].price : 0;
      //Se calcula el subTotal
      const quantity = parseInt(aux[itemIndex].quantity);
      const subTotal = parseInt(price) + parseInt(priceByData) + parseInt(priceByMinut) + parseInt(priceByCredit) + parseInt(priceByFriendGroup);
      
      aux[itemIndex].price = String(subTotal * quantity);
    }
    //Actualizamos el state
    this.setState({
      plansAdded: Object.assign({}, plansAdded, {
        [productPressed.id]: aux,
      })
    });
  }
  /**
   * Se guarda el id del item seleccionado de los pickers: tipo de compra, datos, minutos, credito
   * grupo amigo, tipo, procedimiento y plan
   */
  savePickerIndexInObject(input, aux, itemIndex, configurationData, productPressed, valueIndex) {

    switch (input.id) {
      
      case 'purchaseType':
      case 'type':
      case 'procedure':
        aux[itemIndex][input.id + 'Id'] = configurationData[productPressed.id][input.id][valueIndex].id;
        break;
        
      case 'data':
        aux[itemIndex].dataIndex = valueIndex;//Se guarda el index para calcular el precio en base al data seleccionado,
                                              //guardar el index es mejor que guardar el id
        aux[itemIndex].dataId = this.props.configurationData[this.props.productPressed.id].data[valueIndex].id;
        break;
        
      case 'minut':
        aux[itemIndex].minutIndex = valueIndex;//Se guarda el index para calcular el precio en base al minut seleccionado,
                                              //guardar el index es mejor que guardar el id
        aux[itemIndex].minutId = this.props.configurationData[this.props.productPressed.id].minut[valueIndex].id;
        break;
        
      case 'credit':
        aux[itemIndex].creditIndex = valueIndex;//Se guarda el index para calcular el precio en base al credit seleccionado,
                                              //guardar el index es mejor que guardar el id
        aux[itemIndex].creditId = this.props.configurationData[this.props.productPressed.id].credit[valueIndex].id;
        break;
        
      case 'friendGroup':
        aux[itemIndex].friendGroupIndex = valueIndex;//Se guarda el index para calcular el precio en base al friendGroup seleccionado,
                                              //guardar el index es mejor que guardar el id
        aux[itemIndex].friendGroupId = this.props.configurationData[this.props.productPressed.id].friendGroup[valueIndex].id;
        break;
        
      case 'plan':
        aux[itemIndex].planIndex = valueIndex;//Se guarda el index para calcular el precio en base al plan seleccionado,
                                              //guardar el index es mejor que guardar el id
        aux[itemIndex].planId = this.props.configurationData[this.props.productPressed.id].plan[valueIndex].id;
        break;
    }
  }
  /**
   * Escuchador que se dispara cuando el usuario
   * modifica el valor de algun input
   */
   onTextChange(input, itemIndex, value) {
     const  { configurationData, productPressed } = this.props;
     const plansAdded = this.state.plansAdded;
     //Se obtiene la lista de planes por el producto seleccionado
     let aux = plansAdded[productPressed.id];
     //Se actualiza el valor del input del item por la posición del item y el identificador del input,
     //ademas se elimina los caracteres que no sean numericos
     aux[itemIndex][input.id] = input.keyboardType === 'numeric' ? value.replace(/[^0-9]/g, '') : value;
     //Calculamos la tarifa si el inputId es igual a quantity, si quantity es distinto de vacío
     //y si ya se selecciono un plan
     if (input.id === 'quantity' &&
        aux[itemIndex][input.id] &&
        (aux[itemIndex].planId ||
          aux[itemIndex].dataId ||
          aux[itemIndex].minutId ||
          aux[itemIndex].creditId ||
          aux[itemIndex].friendGroupId)
        ) {
        //Se obtiene el precio de todos los planes por el index
        const price = aux[itemIndex]['plan'] ? configurationData[productPressed.id].plan[aux[itemIndex].planIndex].price : 0;
        const priceByData = aux[itemIndex]['data'] ? configurationData[productPressed.id].data[aux[itemIndex].dataIndex].price : 0;
        const priceByMinut = aux[itemIndex]['minut'] ? configurationData[productPressed.id].minut[aux[itemIndex].minutIndex].price : 0;
        const priceByCredit = aux[itemIndex]['credit'] ? configurationData[productPressed.id].credit[aux[itemIndex].creditIndex].price : 0;
        const priceByFriendGroup = aux[itemIndex]['friendGroup'] ? configurationData[productPressed.id].friendGroup[aux[itemIndex].friendGroupIndex].price : 0;
        //Se calcula el subTotal
        const quantity = parseInt(aux[itemIndex].quantity);
        const subTotal = parseInt(price) + parseInt(priceByData) + parseInt(priceByMinut) + parseInt(priceByCredit) + parseInt(priceByFriendGroup);
        
        aux[itemIndex].price = String(subTotal * quantity);
      }
     //Actualizamos el state
     this.setState({
       plansAdded: Object.assign({}, plansAdded, {
         [productPressed.id]: aux,
       })
     });
   }
  /**
   * Retorna la lista de botones que van al lado
   * derecho del tool bar
   */
  getRightButtons() {
    
    return [
      {
        onPressRightButton: this._onPressRightButton,
        iconRightButton: 'md-checkmark',
        colorIconRightButton: toolbarButtonColor,
      },
    ];
  }
  /**
   * Copia array de planes agregados(props.plansAdded) y retorna el mismo
   * para manejarlo con el state
   */
  getPlansAdded() {
    //Copia el array donde se guardan los planes agregados por producto
    let plansAdded = JSON.parse(JSON.stringify(this.props.plansAdded));
    
    if (plansAdded[this.props.productPressed.id].length === 0)
      plansAdded[this.props.productPressed.id].push(this.getInputValuesByProductPressed());
      
    return plansAdded;
  }
  /**
   * Retorna el subtotal, el cual se renderiza
   * debajo del toolbar
   */
  getSubTotal() {
    let total = 0;
    let aux = this.state.plansAdded[this.props.productPressed.id].map((item) => total += parseInt(item.price));
    return aux.length !== 0 ? aux.pop() : 0;
  }
  /**
   * Retorna los errores por index del item
   * y por id de input
   */
  getInputErrors(itemIndex, inputId) {
    let inputErrorAdvice = '';
    const inputErrors = this.props.errorsOnPlansAdded &&
      this.props.errorsOnPlansAdded[this.props.productPressed.id] &&
      this.props.errorsOnPlansAdded[this.props.productPressed.id][itemIndex] &&
      this.props.errorsOnPlansAdded[this.props.productPressed.id][itemIndex][inputId]
      ? this.props.errorsOnPlansAdded[this.props.productPressed.id][itemIndex][inputId]
      : [];
    //Concatena los errores en la variable inputErrorAdvice
    inputErrors.map((inputError) => inputErrorAdvice += inputError + '\n');
    
    return inputErrorAdvice;
  }
  /**
   * Retorna objeto que representa el valor de los inputs que seran
   * renderizados por producto seleccionado, por cada item de la lista
   * se debe retornar un objeto
   */
  getInputValuesByProductPressed() {
    
    switch (this.props.productPressed.id) {
      
      case POST_PAID://Valor de inputs para postpago
        return {
          id: '',
          purchaseType: '',
          purchaseTypeId: '',
          plan: '',
          planIndex: '',
          planId: '',
          quantity: '',
          data: '',
          dataIndex: '',
          dataId: '',
          minut: '',
          minutIndex: '',
          minutId:'',
          credit: '',
          creditIndex: '',
          creditId: '',
          friendGroup: '',
          friendGroupIndex: '',
          friendGroupId: '',
          price: '0',
          priceByData: 0,
          priceByMinut: 0,
          priceByCredit: 0,
          priceByFriendGroup: 0,
        };
        
      case LTE://Valor de inputs para lte
        return {
          id: '',
          type: '',
          typeId: '',
          plan: '',
          planIndex: '',
          planId: '',
          quantity: '',
          line: '',
          imei: '',
          enodeb: '',
          procedure: '',
          procedureId: '',
          price: '0',
        };
        
      case PUBLIC_TELEPHONY://Valor de inputs para telefonia publica
        return {
          id: '',
          plan: '',
          planIndex: '',
          planId: '',
          quantity: '',
          line: '',
          price: '0',
        };
        
      case VPN://Valor de inputs para vpn
        return {
          id: '',
          plan: '',
          planIndex: '',
          planId: '',
          quantity: '',
          price: '0',
        };
        
      case BAG://Valor de inputs para bolsas
        return {
          id: '',
          plan: '',
          planIndex: '',
          planId: '',
          quantity: '',
          price: '0',
        };
        
      case OTHER://Valor de inputs para otros
        return {
          id: '',
          service: '',
          quantity: '',
        };
    }
  }
  /**
   * Retorna objeto que representa las propiedades de los inputs
   * que seran renderizados por producto seleccionado
   */
  getInputsByProduct() {
    //Para extraer la configuración de todos los productos
    const { postPaid, lte, publicTelephony, vpn, bag, other } = this.props.configurationData;

    return {
      postPaid: [//Inputs para postpago
        { id: 'purchaseType', label: 'Tipo compra(*)', type: 'dropdown', items: postPaid.purchaseType.map((item) => item.name), },
        { id: 'plan', label: 'Plan(*)', type: 'dropdown', items: postPaid.plan.map((item) => item.name), },
        { id: 'quantity', label: 'Cantidad(*)', type: 'textInput', keyboardType: 'numeric' },
        { id: 'data', label: 'Datos', type: 'dropdown', items: postPaid.data.map((item) => item.name), },
        { id: 'minut', label: 'Minutos', type: 'dropdown', items: postPaid.minut.map((item) => item.name), },
        { id: 'credit', label: 'Credito', type: 'dropdown', items: postPaid.credit.map((item) => item.name), },
        { id: 'friendGroup', label: 'Grupo amigo', type: 'dropdown', items: postPaid.friendGroup.map((item) => item.name), },
        { id: 'price', label: 'Tarifa', type: 'text', },
      ],
      lte: [//Inputs para lte
        { id: 'type', label: 'Tipo(*)', type: 'dropdown', items: lte.type.map((item) => item.name), },
        { id: 'plan', label: 'Plan(*)', type: 'dropdown', items: lte.plan.map((item) => item.name), },
        { id: 'quantity', label: 'Cantidad(*)', type: 'textInput', keyboardType: 'numeric', },
        { id: 'procedure', label: 'Procedimiento(*)', type: 'dropdown', items: lte.procedure.map((item) => item.name), },
        { id: 'line', label: 'Linea', type: 'textInput', keyboardType: 'numeric' },
        { id: 'imei', label: 'IMEI CPE', type: 'textInput', keyboardType: 'numeric', },
        { id: 'enodeb', label: 'Enodeb', type: 'textInput', keyboardType: 'default', },
        { id: 'price', label: 'Tarifa', type: 'text', },
      ],
      publicTelephony: [//Inputs para telefonia publica
        { id: 'plan', label: 'Plan(*)', type: 'dropdown', items: publicTelephony.plan.map((item) => item.name), },
        { id: 'quantity', label: 'Cantidad(*)', type: 'textInput', keyboardType: 'numeric', },
        { id: 'line', label: 'Linea', type: 'textInput', keyboardType: 'numeric', },
        { id: 'price', label: 'Tarifa', type: 'text', },
      ],
      vpn: [//Inputs para vpn
        { id: 'plan', label: 'Plan(*)', type: 'dropdown', items: vpn.plan.map((item) => item.name), },
        { id: 'quantity', label: 'Cantidad(*)', type: 'textInput', keyboardType: 'numeric', },
        { id: 'price', label: 'Tarifa', type: 'text', },
      ],
      bag: [//Inputs para bolsas
        { id: 'plan', label: 'Plan(*)', type: 'dropdown', items: bag.plan.map((item) => item.name), },
        { id: 'quantity', label: 'Cantidad(*)', type: 'textInput', keyboardType: 'numeric', },
        { id: 'price', label: 'Tarifa', type: 'text', },
      ],
      other: [//Inputs para otros
        { id: 'service', label: 'Servicio(*)', type: 'textInput', keyboardType: 'default', },
        { id: 'quantity', label: 'Cantidad(*)', type: 'textInput', keyboardType: 'numeric', },
      ],
    };
  }
}

Product.PropTypes = {
  onAddProducts: PropTypes.func.isRequired,//Escuchador que se dispara cuando el usuario presiona el check button del toolbar
  productPressed: PropTypes.object.isRequired,//Producto seleccionado, e.g. postpago
  plansAdded: PropTypes.array,//Array de planes que fueron agregados
  configurationData: PropTypes.object.isRequired,//Datos de configuración del form, se necesita para cargar los pickers con datos
  errorsOnPlansAdded: PropTypes.object,//Objeto que contiene los errores por producto y de cada input de cada plan agregado
};

export default Product;