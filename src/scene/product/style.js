/***********************
 * Node modules import *
 ***********************/
import { Dimensions } from 'react-native';
/******************
 * Project import *
 ******************/
import {
  toolbarBackgroundColor,
  toolbarBorderColor,
  toolbarTextColor,
  productTotalContainerBackgroundColor,
  productTotalTextColor,
  productListItemBackgroundColor,
  productListItemHeaderTextColor,
  productListItemContentLeftSideBackgroundColor,
  productListItemContentLeftSideTextColor,
  productListItemContentRightSideBackgroundColor,
  productInputContainerBackgroundColor,
  productInputErrorTextColor,
  productListItemContentActionRightSideButtonBackgroundColor,
  productListItemContentSeparator,
  productListItemFooterTextBackgroundColor,
  productListItemFooterTextColor,
  productAddButtonBackgroundColor,
} from '../../color.js';
/*************
 * Constants *
 *************/
const SCREEN_WIDTH = Dimensions.get('screen').width;

export default {
  
  toolbar: {
    backgroundColor: toolbarBackgroundColor,
    borderColor: toolbarBorderColor,
  },
  title: {
    color: toolbarTextColor,
  },
  totalContainer: {
    height: 30,
    backgroundColor: productTotalContainerBackgroundColor,
  },
  totalText: {
    color: productTotalTextColor,
    alignSelf: 'center',
  },
  listContainer: {
    flex: 1,
  },
  list: {
    paddingRight: 5,
    paddingLeft: 5,
  },
  listHeader: {
    marginTop: 5,
  },
  listFooter: {
    marginBottom: 5,
  },
  listItem: {
    flexDirection: 'column',
    elevation: 3,
    borderRadius: 20,
    backgroundColor: productListItemBackgroundColor,
    margin: 5,
    overflow: 'hidden',
  },
  listItemHeader: {
    height: 50,
    justifyContent: 'center',
  },
  listItemHeaderText: {
    color: productListItemHeaderTextColor,
  },
  listItemContent: {
    flexDirection: 'row',
  },
  listItemContentLeftSide: {
    backgroundColor: productListItemContentLeftSideBackgroundColor,
    justifyContent: 'center',
    flex: 1,
  },
  listItemContentLeftSideText: {
    color: productListItemContentLeftSideTextColor,
    padding: 10,
  },
  listItemContentRightSide: {
    backgroundColor: productListItemContentRightSideBackgroundColor,
    justifyContent: 'center',
    flex: 1.3,
  },
  inputContainer: {
    backgroundColor: productInputContainerBackgroundColor,
    borderRadius: 20,
    margin: 5,
  },
  inputError: {
    color: productInputErrorTextColor,
    left: 5,
    bottom: 5,
    fontSize: 13,
    width: '90%',
  },
  listItemContentAction: {
    flexDirection: 'row',
  },
  listItemContentActionLeftSide: {
    backgroundColor: productListItemContentLeftSideBackgroundColor,
    justifyContent: 'center',
    flex: 1,
  },
  listItemContentActionLeftSideText: {
    color: productListItemContentLeftSideTextColor,
    padding: 10,
  },
  listItemContentActionRightSide: {
    backgroundColor: productListItemContentRightSideBackgroundColor,
    flex: 1.3,
    justifyContent: 'center',
  },
  listItemContentActionRightSideButton: {
    backgroundColor: productListItemContentActionRightSideButtonBackgroundColor,
    alignSelf: 'center',
    borderRadius: 10,
    width: 35,
    height: 35,
    justifyContent: 'center',
  },
  listItemContentSeparator: {
    height: 1,
    backgroundColor: productListItemContentSeparator,
  },
  listItemFooter: {
    flexDirection: 'row',
  },
  listItemFooterLeftSide: {
    backgroundColor: productListItemContentLeftSideBackgroundColor,
    justifyContent: 'center',
    flex: 1,
    height: 50,
  },
  listItemFooterRightSide: {
    backgroundColor: productListItemContentRightSideBackgroundColor,
    justifyContent: 'center',
    flex: 1.3,
    height: 50,
  },
  addButton: {
    backgroundColor: productAddButtonBackgroundColor,
  },
}