/***********************
 * Node modules import *
 ***********************/
import React from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Container, StyleProvider } from 'native-base';
import { Animated, Keyboard } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
/******************
 * Project import *
 ******************/
import BasisComponent from '../basisComponent.js';
import style, { LOGO_NORMAL_WIDTH, LOGO_SMALLER_WIDTH, PADDING_BOTTOM_OF_CONTAINER } from './style.js';
import { LoginForm } from './component/index.js';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import { executeSignIn } from '../../data/session/action.js';
import { pushNotification } from '../../service/index.js';
import { home } from '../../config.js';
import {
  darknestPrimaryColor,
  darkerPrimaryColor,
  darkPrimaryColor,
  primaryColor,
  lightestPrimaryColor,
  lighterPrimaryColor,
  lightPrimaryColor,
} from '../../color.js';
/**
 * Renderiza el login
 */
class Login extends BasisComponent {

  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onSignIn = this.onSignIn.bind(this);
    this._keyboardDidShow = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
    this._keyboardDidHide = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this));
    //Parametros para la animación cuando se despliega el teclado
    this.keyboardHeight = new Animated.Value(PADDING_BOTTOM_OF_CONTAINER);
    this.logoWidth = new Animated.Value(LOGO_NORMAL_WIDTH);
  }
  /**
   * Va al home una vez se ejecuto el metodo executeSignIn
   */
  componentWillReceiveProps(nextProps) {
    if (nextProps.session && !nextProps.statusOfSigningIn.signInIsBeingExecuted)
      this.resetTo(home);
  }
  /**
   * Se remueve los escuchadores del teclado
   */
  componentWillUnmount() {
    this._keyboardDidShow.remove();
    this._keyboardDidHide.remove();
  }

  render() {

    return (
      <LinearGradient
        style={style(this.keyboardHeight, this.logoWidth).containerBackground}
        colors={[
          lightestPrimaryColor, lighterPrimaryColor, lightPrimaryColor,
          primaryColor,
          darkPrimaryColor, darkerPrimaryColor, darknestPrimaryColor,
        ]}>
        <StyleProvider style={getTheme(material)}>
          <Container>
            <Animated.View
              style={style(this.keyboardHeight, this.logoWidth).container}>
              <Animated.Image
                style={style(this.keyboardHeight, this.logoWidth).logo}
                source={require('../../image/logo.png')}
                resizeMode={'contain'}/>

              <LoginForm
                onSignIn={this._onSignIn}
                navigator={this.props.navigator}
                signInButtonPressed={this.props.statusOfSigningIn.signInIsBeingExecuted}
                usernameInputError={this.props.statusOfSigningIn.error.errorMessage}
                passwordInputError={this.props.statusOfSigningIn.error.errorMessage}/>
            </Animated.View>
          </Container>
        </StyleProvider>
      </LinearGradient>
    );
  }
  /**
   * Verifica si hubo un error al ejecutar
   * el metodo executeSignIn
   */
  componentDidUpdate(prevProps, prevState) {
    if (this.props.statusOfSigningIn.wasAnError)
      this.showErrorMessage(
        this.props.statusOfSigningIn.error &&
        this.props.statusOfSigningIn.error.errorMessage ||
        'Hubo un problema al iniciar sesión'
      );
  }
  /**
   * Escuchador que se dispara cuando se despliega
   * el teclado
   */
  keyboardDidShow(event) {
    this.animate(LOGO_SMALLER_WIDTH, event.endCoordinates.height + PADDING_BOTTOM_OF_CONTAINER, 1000);
  };
  /**
   * Escuchador que se dispara cuando se oculta el teclado
   */
  keyboardDidHide(event) {
    this.animate(LOGO_NORMAL_WIDTH, PADDING_BOTTOM_OF_CONTAINER, 1000);
  };
  /**
   * Ejecuta la animacion cuando se muestra o
   * oculta el teclado
   */
  animate(imageWidth, paddingBottomOfContainer, duration) {
    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        duration: duration,
        toValue: paddingBottomOfContainer,
      }),
      Animated.timing(this.logoWidth, {
        duration: duration,
        toValue: imageWidth,
      }),
    ]).start();
  }
  /**
   * Escuchador que se dispara cuando el usuario
   * presiona el boton para loguearse
   * @param  {string} username nombre de usuario
   * @param  {string} password clave del usuario
   */
  async onSignIn(username, password) {
    this.props.executeSignIn(username, password, await pushNotification.getFcmId());
  }
}

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  statusOfSigningIn: state.dataReducer.sessionReducer.statusOfSigningIn,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeSignIn: executeSignIn,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
