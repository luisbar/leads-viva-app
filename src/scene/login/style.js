/***********************
 * Node modules import *
 ***********************/
import { Dimensions, StatusBar } from 'react-native';
/*************
 * Constants *
 *************/
const STATUS_BAR_HEIGHT = StatusBar.currentHeight;
const SCREEN_WIDTH = Dimensions.get('screen').width;
const SCREEN_HEIGHT = Dimensions.get('screen').height;
export const PADDING_BOTTOM_OF_CONTAINER = (SCREEN_WIDTH + STATUS_BAR_HEIGHT) * 0.15;
const LOGO_HORIZONTAL_MARGIN = 10;
export const LOGO_NORMAL_WIDTH = SCREEN_WIDTH - (LOGO_HORIZONTAL_MARGIN * 2);
export const LOGO_SMALLER_WIDTH = 0;

export default function(keyboardHeight, logoWidth) {

  return {

    containerBackground: {
      width: SCREEN_WIDTH,
      height: SCREEN_HEIGHT - STATUS_BAR_HEIGHT,
    },
    container: {
      justifyContent: 'space-around',
      alignItems: 'center',
      flex: 1,
      paddingBottom: keyboardHeight,
    },
    logo: {
      width: logoWidth,
      marginLeft: LOGO_HORIZONTAL_MARGIN,
      marginRight: LOGO_HORIZONTAL_MARGIN,
      marginBottom: 10,
    },
  };
};
