/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Form, Body, Title, Text } from 'native-base';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
 import BasisComponent from '../../../basisComponent.js'
 import style from './style.js';
 import { InputWithValidator, ButtonWithState } from '../../../../component/index.js';
 import {
   loginButtonLabelTextColor,
   loginButtonBackgroundColor,
   inputLabelColor,
   inputTextColor,
   inputPlaceholderColor,
   inputBackgroundColor,
 } from '../../../../color.js';
/*************
 * Constants *
 *************/
const USERNAME = 'username';
const PASSWORD = 'password';
/**
 * Renderiza formulario para loguearse
 */
class LoginForm extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onHandleInputValue = this.onHandleInputValue.bind(this);
    this._onPress = this.onPress.bind(this);
  }

  render() {

    return (
      <Form
        style={style.form}>
        
        <Title style={style.title}>{'SISTEMA LEAD'}</Title>
        <Text style={style.indication}>{'Accede para comenzar tu sesión'}</Text>
        
        <InputWithValidator
          id={USERNAME}
          ref={USERNAME}
          isRequired={false}
          label={'Usuario'}
          placeholder={'nombre de usuario'}
          maxLength={150}
          keyboardType={'email-address'}
          inputTextColor={inputTextColor}
          labelTextColor={inputLabelColor}
          placeholderTextColor={inputPlaceholderColor}
          backgroundInputColor={inputBackgroundColor}
          errors={[this.props.usernameInputError]}
          onHandleInputValue={this._onHandleInputValue}
          onSubmitEditing={(event) => this.refs.password.refs.input._root.focus()}/>

        <InputWithValidator
          id={PASSWORD}
          ref={PASSWORD}
          isRequired={false}
          label={'Contraseña'}
          placeholder={'***********'}
          maxLength={128}
          keyboardType={'password'}
          secureTextEntry={true}
          returnKeyType={'done'}
          inputTextColor={inputTextColor}
          labelTextColor={inputLabelColor}
          placeholderTextColor={inputPlaceholderColor}
          backgroundInputColor={inputBackgroundColor}
          errors={[this.props.passwordInputError]}
          onHandleInputValue={this._onHandleInputValue}
          autoCapitalize={'none'}/>
        
        <Body
          style={style.buttonContainer}>
          <ButtonWithState
            text={'INGRESAR'}
            block={true}
            rounded={true}
            onPress={this._onPress}
            buttonBackgroundColor={loginButtonBackgroundColor}
            buttonTextColor={loginButtonLabelTextColor}
            disabled={this.props.signInButtonPressed}/>
        </Body>
      </Form>
    );
  }
  /**
   * Escuchador que se dispara cuando el usuario
   * modifica el texto de algun input
   * @param  {string} inputId id del input
   * @param  {bool} inputState estado del input, es decir vacio o lleno
   * @param  {string} inputValue valor contenido en el input
   */
  onHandleInputValue(inputId, inputState, inputValue) {
    this.setState({
      [inputId + 'State']: inputState,
      [inputId + 'Value']: inputValue,
    });
  }
  /**
   * Escuchador que se dispara cuando se presiona el boton de logueo
   */
  onPress() {
    this.props.onSignIn(this.state.usernameValue, this.state.passwordValue);
  }
}

LoginForm.propTypes = {
  onSignIn: PropTypes.func.isRequired,//Escuchador que se dispara cuando se presiona el boton para loguearse
  signInButtonPressed: PropTypes.bool.isRequired,//Bandera para bloquear el boton de logueo
  usernameInputError: PropTypes.string,//Mensaje que se mostrará cuando haya error con los datos del input username
  passwordInputError: PropTypes.string,//Mensaje que se mostrará cuando haya error con los datos del input password
}

export default LoginForm;