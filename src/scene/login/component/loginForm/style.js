/***********************
 * Node modules import *
 ***********************/
import { Dimensions } from 'react-native';
/******************
 * Project import *
 ******************/
import { loginFormBackground, loginTitleColor, loginIndicationColor } from '../../../../color.js';
/*************
 * Constants *
 *************/
const SCREEN_WIDTH = Dimensions.get('screen').width;
const FORM_HORIZONTAL_MARGIN = 10;

export default {

  form: {
    backgroundColor: loginFormBackground,
    borderRadius: 20,
    width: SCREEN_WIDTH - (FORM_HORIZONTAL_MARGIN * 2),
    marginLeft: FORM_HORIZONTAL_MARGIN,
    marginRight: FORM_HORIZONTAL_MARGIN,
    height: 350,
  },
  title: {
    marginTop: 10,
    color: loginTitleColor,
  },
  indication: {
    margin: 5,
    color: loginIndicationColor,
    alignSelf: 'center',
  },
  buttonContainer: {
    flex: 1,
    width: 150,
    justifyContent: 'center',
  },
};
