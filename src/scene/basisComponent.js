/***********************
 * Node modules import *
 ***********************/
import React, { PureComponent } from 'react';

import { Navigation } from 'react-native-navigation';
import { Dimensions, Platform, StatusBar, Keyboard } from 'react-native';
import Toast from 'react-native-root-toast';
/******************
 * Project import *
 ******************/
import { toastTextColor, dangerColor, infoColor, successColor } from '../color.js';
import { progressModal } from '../config.js';
import Router from '../router.js';
/**
 * Basis component which will be extended for all components, it has
 * boilerplate code
 */
class BasisComponent extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      toastWidth: this.getWidth(),
      isTheProgressModalShown: false,
    };

    this.isIphoneX = this.getPlatform() === 'ios' && this.getHeight() === 812 && this.getWidth() === 375;
    //To register a listener for knowing when orientation has changed
    Dimensions.addEventListener('change', () => this.onChageOrientation());
    //To close the Keyboard in all views
    Keyboard.dismiss();
    //Disable the drawer
    if (Router.getInstance() && Router.getInstance().navigator)
      Router.getInstance().navigator.setDrawerEnabled({
        side: 'left',
        enabled: false,
      });
    //To save the previous toast
    this.toast;
  }
  /**
   * It returs the orientation of screen
   */
  getOrientation() {
    const dim = Dimensions.get('screen');
    return dim.height >= dim.width ? 'portrait' : 'landscape';
  }
  /**
   * It return the width of screen
   */
  getWidth() {
    return Dimensions.get('screen').width;
  }
  /**
   * It return the height of screen
   */
  getHeight() {
    return Dimensions.get('screen').height;
  }
  /**
   * Listener for knowing when the orientation of a smarthpone
   * has changed
   */
  onChageOrientation() {
    if (this.state.toastWidth !== this.getWidth())
      this.setState({ toastWidth: this.getWidth() });
  }
  /**
   * It shows an error message according platform
   */
  showErrorMessage(message, modal) {
    if (Platform.OS == 'android' && !modal)
      this.showAndroidMessage(message, dangerColor);
    else
      this.showIosMessage(message, dangerColor);
  }
  /**
   * It shows an informative message according platform
   */
  showInformativeMessage(message, modal) {
    if (Platform.OS == 'android' && !modal)
      this.showAndroidMessage(message, infoColor);
    else
      this.showIosMessage(message, infoColor);
  }
  /**
   * It shows an success message according platform
   */
  showSuccessMessage(message, modal) {
    if (Platform.OS == 'android' && !modal)
      this.showAndroidMessage(message, successColor);
    else
      this.showIosMessage(message, successColor);
  }
  /**
   * It shows an error message for ios
   */
  showIosMessage(message, color) {
    Toast.show(message, {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      textColor: toastTextColor,
      backgroundColor: color,
      customStyle: {
        width: this.state.toastWidth,
      },
    });
  }
  /**
   * It shows an error message for android
   */
  showAndroidMessage(message, color) {
    if (this.toast)
      Toast.hide(this.toast);
      
    this.toast = Toast.show(message, {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      textColor: toastTextColor,
      backgroundColor: color,
      customStyle: {
        width: this.state.toastWidth,
        zIndex: 1,
      },
    });
  }
  /**
   * It returns the type of platform
   */
  getPlatform() {
    return Platform.OS;
  }
  /**
   * It opens the drawer
   */
  openDrawer() {
    Router.getInstance().navigator.toggleDrawer({
      to: 'open',
      side: 'left',
      animated: true,
    });
  }
  /**
   * It returns the height of toolbar
   */
  getToolbarHeight() {
    return this.getPlatform() === 'ios' ? (this.isIphoneX ? 88 : 64) : 56;
  }
  /**
   * It return the height of the status bar
   */
  getStatusBarHeight() {
    return StatusBar.currentHeight;
  }
  /**
   * It hides the modal
   */
  dismissModal() {
    //Si el progress modal ha sido desplegado
    if (this.state.isTheProgressModalShown)
      this.setState({ isTheProgressModalShown: false });
    //Oculta el modal
    Router.getInstance().navigator.dismissModal({
      animationType: 'slide-down',
    });
  }
  /**
   * It returns the current screen id
   */
  getCurrentScreenId() {
    return Navigation.getCurrentlyVisibleScreenId();
  }
  /**
   * To show the ProgressModal
   * @param  {string} textToShow mensaje a mostrar en el modal
   */
  showProgressModal(textToShow) {
    if (!this.state.isTheProgressModalShown)
      //Primero actualiza el estado para saber que el progressModal
      //ha sido desplegado
      this.setState({ isTheProgressModalShown: true },
      () => {
        //Se muestra el progressModal
        Router.getInstance().showTransparentModal(progressModal, {
          text: textToShow,
        });
      });
  }
  /**
   * It removes the last screen from the stack
   */
  pop() {
    Router.getInstance().navigator.pop();
  }
  /**
   * It adds a screen to the stack
   */
  push(screen, props) {
    Router.getInstance().push(screen, props);
  }
  /**
   * It removes all screens in the stack an put one
   * in it
   */
  resetTo(screen, props) {
    Router.getInstance().resetTo(screen, props);
  }
  /**
   * It shows a modal
   */
  showModal(screen, props) {
    Router.getInstance().showModal(screen, props);
  }
  /**
   * It shows a transparent modal
   */
  showTransparentModal(screen, props) {
    Router.getInstance().showTransparentModal(screen, props);
  }
}

export default BasisComponent;
