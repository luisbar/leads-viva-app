/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Container, Content, List, ListItem, Text, StyleProvider } from 'native-base';

import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
/******************
 * Project import *
 ******************/
import BasisComponent from '../basisComponent.js';
import style from './style.js';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import { NavigationBar, ProgressScreen } from '../../component/index.js';
import { executeGetMatchedPlaces } from '../../data/configuration/action.js';
import {
 toolbarButtonColor,
 searchBarIconColor,
 progressScreenNestedBackgroundColor,
 progressScreenTextColor,
 progressScreenSpinnerColor
} from '../../color.js';
/**
 * Renderiza componente que utiliza google places
 * para la busqueda de lugares en base al texto
 * introducido
 */
class Place extends BasisComponent {

  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._pop = this.pop.bind(this);
    this._searchPlace = this.searchPlace.bind(this);
    this._onSearch = this.onSearch.bind(this);
    this._renderItem = this.renderItem.bind(this);
    this._onPlaceSelected = (place) => this.onPlaceSelected.bind(this, place);
    //Array de botones que se renderizaran en el navigation bar
    this.rightButtons = [
      {
        onPressRightButton: this._searchPlace,
        iconRightButton: 'md-checkmark',
        colorIconRightButton: toolbarButtonColor,
      },
    ];
    //Variable para almacenar el texto introducido en la barra de busqueda
    this.placeToSearch = '';
    //State
    this.state = {
      suggestions: this.props.matchedPlaces,
    };
  }
  /**
   * Verifica si hubo error al buscar lugares
   * que coincidan con google place, verifica
   * que el array de lugares que coincidan
   * no este vacio y actualiza el state
   * para mostrar la lista de lugares sugeridos
   */
  componentWillReceiveProps(nextProps) {
    const { statusOfGettingMatchedPlaces, matchedPlaces } = nextProps;
    //Muestra mensaje de error
    if (statusOfGettingMatchedPlaces.wasAnError && !statusOfGettingMatchedPlaces.matchedPlacesIsBeingFetching)
      this.showErrorMessage(
        statusOfGettingMatchedPlaces.error &&
        statusOfGettingMatchedPlaces.error.errorMessage ||
        'Hubo un problema al buscar la dirección introducida'
      );
    //Muestra mensaje informativo, si el array matchedPlaces de el actual objeto props esta vacio
    //y si nextProps.statusOfGettingMatchedPlaces.matchedPlacesIsBeingFetching es falso
    if (this.props.matchedPlaces.length === 0 && !statusOfGettingMatchedPlaces.matchedPlacesIsBeingFetching)
      this.showInformativeMessage('No se encontraron lugares que coincidan con la dirección introducida');
    //Actualiza el state para mostrar la lista de sugerencias
    if (statusOfGettingMatchedPlaces.matchedPlacesIsBeingFetching)
      this.setState({ suggestions: matchedPlaces });
  }
  
  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <NavigationBar
            toolBarStyle={style.toolbar}
            leftButtonVisible={true}
            onPressLeftButton={this._pop}
            iconLeftButton={'md-arrow-back'}
            colorIconLeftButton={toolbarButtonColor}
            title={'Buscar dirección'}
            titleStyle={style.title}
            rightButtonsVisible={true}
            rightButtons={this.rightButtons}
            searchBarVisible={true}
            searchBarStyle={style.searchBar}
            searchBarIconColor={searchBarIconColor}
            onSearch={this._onSearch}/>
            <Content>
              {
                this.props.statusOfGettingMatchedPlaces.matchedPlacesIsBeingFetching
                ? this.renderProgressScreen()
                : this.renderList()
              }
            </Content>
        </Container>
      </StyleProvider>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {
    
    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withBiggerToolbar={true}/>
    );
  }
  /**
   * Renderiza la lista de sugerencias
   */
  renderList() {
    
    return (
      <List
        dataArray={this.state.suggestions}
        renderRow={this._renderItem}/>
    );
  }
  /**
   * Renderiza los items de la lista de sugerencias
   */
  renderItem(item, index) {

    return (
      <ListItem
        button
        first
        key={index}
        onPress={this._onPlaceSelected(item)}>
        <Text>{item.name}</Text>
      </ListItem>
    );
  }
  /**
   * Escuchador que se dispara cuando el usuario
   * presiona el boton para buscar la dirección
   * introducida en la barra de busqueda
   */
  searchPlace() {
    this.props.executeGetMatchedPlaces(this.placeToSearch, this.props.city);
  }
  /**
   * Escuchador que se dispara cuando el usuario introduce
   * texto en la barra de busqueda
   * @param  {string} placeToSearch texto introducido en la barra de busqueda
   */
  onSearch(placeToSearch) {
    this.placeToSearch = placeToSearch;
  }
  /**
   * Escuchador que se dispara cuando el usuario
   * presiona alguna dirección de la lista de sugerencias
   * @param  {number} place objeto que contiene toda la información del lugar
   * seleccionado
   */
  onPlaceSelected(place) {
    this.props.onPlaceSelected(place.name, place.latitude, place.longitude);
    this.pop();
  }
}

Place.propTypes = {
  onPlaceSelected: PropTypes.func.isRequired,//Escuchador que se dispara cuando el usuario presiona sobre un item de la lista de sugerencias
};

const mapStateToProps = state => ({
  city: state.dataReducer.sessionReducer.session.city,
  matchedPlaces: state.dataReducer.configurationReducer.matchedPlaces,
  statusOfGettingMatchedPlaces: state.dataReducer.configurationReducer.statusOfGettingMatchedPlaces,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetMatchedPlaces: executeGetMatchedPlaces,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Place);