/******************
 * Project import *
 ******************/
import {
  toolbarBackgroundColor,
  toolbarBorderColor,
  toolbarTextColor,
  searchBarBackgroundColor,
  searchBarBorderColor,
} from '../../color.js';

export default {
  
  toolbar: {
    backgroundColor: toolbarBackgroundColor,
    borderColor: toolbarBorderColor,
  },
  title: {
    color: toolbarTextColor,
  },
  searchBar: {
    backgroundColor: searchBarBackgroundColor,
    borderColor: searchBarBorderColor,
  },
};