/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { StyleProvider, Container, Content, Title, Text } from 'native-base';
import { View } from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../basisComponent.js';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import { executeGetAnAction } from '../../data/action/action.js';
import { NavigationBar, ProgressScreen } from '../../component/index.js';
import {
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
  toolbarButtonColor
} from '../../color.js';
/**
 * Renderiza la vista para ver una acción
 */
class ActionViewer extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea aqui para mejorar el performance
    this._pop = this.pop.bind(this);
    //Variable para saber cual estilo utilizar al renderizar un item
    this.darkBackground;
  }
  
  /**
   * Verifica si hubo un error en todas
   * las acciones
   */
  componentWillReceiveProps(nextProps) {
    const previousProps = this.props;
    
    this.showErrorForGettigAnActionForEdition(nextProps);
  }

  render() {
    
    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <NavigationBar
            toolBarStyle={style.toolbar}
            leftButtonVisible={true}
            onPressLeftButton={this._pop}
            iconLeftButton={'md-arrow-back'}
            colorIconLeftButton={toolbarButtonColor}
            title={'Ver acción'}
            titleStyle={style.title}/>
          <Content>
            {
              this.props.statusOfGettingAnAction.actionIsBeingFetching
              ? this.renderProgressScreen()
              : this.renderContent()
            }
          </Content>
        </Container>
      </StyleProvider>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {
    
    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbar={true}/>
    );
  }
  /**
   * Renderiza el contenido principal
   */
  renderContent() {
    
    return (
      this.props.action &&
      <View>
        {this.renderItem('Acción', this.props.action.action)}
        {this.renderItem('Respuesta', this.props.action.answer)}
        {this.renderItem('Fecha', this.props.action.date)}
        {this.renderItem('Observación', this.props.action.note)}
        {this.renderItem('Duración', this.props.action.duration)}
        {this.renderItem('Motivo', this.props.action.reason)}
        {this.renderItem('Estado', this.props.action.status)}
      </View>
    );
  }
  /**
   * Renderiza los datos acerca de una accion
   */
  renderItem(label, data) {
    //Variable para saber cual estilo utilizar al renderizar un item
    this.darkBackground = data ? !this.darkBackground : this.darkBackground;
    
    return (
      data
      ? <View
          style={this.darkBackground ? style.listItemWithDarkBackground : style.listItem}>
          
          <Title
            style={style.listItemLeftSideText}
            numberOfLines={2}>
            {label}
          </Title>
          
          <Text
            style={style.listItemRightSideText}>
            {data}
          </Text>
        </View>
      : null
    );
  }
  /**
   * Invoca metodo para obtener la acción
   * a editar
   */
  componentDidMount() {
    this.props.executeGetAnAction(
      this.props.actionId,
      this.props.session.accessToken,
      this.props.session.refreshToken,
    );
  }
  /**
   * Muestra errores cuando se ejecuta
   * el metodo executeGetAnAction,
   * si es que los hay
   */
  showErrorForGettigAnActionForEdition(nextProps) {
    if (nextProps.statusOfGettingAnAction.wasAnError)
      this.showErrorMessage(
        (nextProps.statusOfGettingAnAction.error &&
        nextProps.statusOfGettingAnAction.error.errorMessage) ||
        'Hubo un problema al obtener la acción a ver'
      );
  }
}

ActionViewer.propTypes = {
  actionId: PropTypes.string.isRequired,//Id de la acción que se quiere ver
};

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  statusOfGettingAnAction: state.dataReducer.actionReducer.statusOfGettingAnAction,
  action: state.dataReducer.actionReducer.action,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetAnAction: executeGetAnAction,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ActionViewer);