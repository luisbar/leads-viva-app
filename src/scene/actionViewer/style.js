/******************
 * Project import *
 ******************/
import {
  toolbarBackgroundColor,
  toolbarBorderColor,
  toolbarTextColor,
  actionViewerItemBackground,
  actionViewerItemLeftSideText,
  actionViewerItemRightSideText,
} from '../../color.js';

export default {
  
  toolbar: {
    backgroundColor: toolbarBackgroundColor,
    borderColor: toolbarBorderColor,
  },
  title: {
    color: toolbarTextColor,
  },
  listItem: {
    flexDirection: 'row',
    padding: 10,
  },
  listItemWithDarkBackground: {
    backgroundColor: actionViewerItemBackground,
    flexDirection: 'row',
    padding: 10,
  },
  listItemLeftSideText: {
    color: actionViewerItemLeftSideText,
    flex: 1,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  listItemRightSideText: {
    color: actionViewerItemRightSideText,
    flex: 1,
    textAlign: 'left',
    textAlignVertical: 'center',
  },
}