/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { StyleProvider, Container, Content, Text, Fab, Button, Spinner } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { MenuProvider } from 'react-native-popup-menu';
import { View, FlatList, RefreshControl } from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../basisComponent.js';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import Router from '../../router.js';
import { approbationOrRejection } from '../../config.js';
import { NavigationBar, ProgressScreen, PopUpMenu } from '../../component/index.js';
import { executeGetPendingLeads, executeGetMorePendingLeads } from '../../data/lead/action.js';
import {
  toolbarButtonColor,
  popupMenuIconColor,
  popupMenuItemIconColor,
  popupMenuItemTextColor,
  progressScreenNestedBackgroundColor,
  progressScreenSpinnerColor,
  progressScreenTextColor,
  upButtonIconColor,
  refreshBackgroundColor,
  refreshColor,
  footerSpinnerColor,
} from '../../color.js';
/**
 * Renderiza vista para ver los leads pendientes
 * de asignación
 */
class PendingAssignment extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._pop = this.pop.bind(this);
    this._renderItem = this.renderItem.bind(this);
    this._onRefresh = this.executeGetPendingLeads.bind(this);
    this._handleLoadMore = this.handleLoadMore.bind(this);
    this._upButtonPressed = this.upButtonPressed.bind(this);
    this._executeGetMoreLeads = this.executeGetMorePendingLeads.bind(this);
    this._approvePendingLead = (leadId) => this.approvePendingLead.bind(this, leadId);
    this._rejectPendingLead = (leadId) => this.rejectPendingLead.bind(this, leadId);
    this._renderProduct = this.renderProduct.bind(this);
    this._onLeadApprovedOrRejected = this.onLeadApprovedOrRejected.bind(this);
    this._onScroll = this.onScroll.bind(this);
    //Variable para saber si aprobo o rechazó un lead exitosamente
    this.leadApprovedOrRejected;
    //Escuchador del navigator
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    //State
    this.state = {
      page: 1,//Pagina actual
      upButtonVisible: true,//Bandera para ocultar y mostrar el float button
    };
  }

  /**
   * Verifica si hubo un error al ejecutar
   * los metodos executeGetPendingLeads y executeGetMorePendingLeads
   */
  componentWillReceiveProps(nextProps) {
    if (nextProps.statusOfGettingPendingLeads.wasAnError &&
        nextProps.statusOfGettingPendingLeads.pendingLeadsAreBeingFetching)
      this.showErrorMessage(
        nextProps.statusOfGettingPendingLeads.error &&
        nextProps.statusOfGettingPendingLeads.error.errorMessage ||
        'Hubo un problema al obtener los leads pendientes'
      );
      
    if (nextProps.statusOfGettingMorePendingLeads.wasAnError &&
        nextProps.statusOfGettingMorePendingLeads.morePendingLeadsAreBeingFetching) {
      this.setState({ page: this.state.page - 1 });
      this.showErrorMessage(
        nextProps.statusOfGettingMorePendingLeads.error &&
        nextProps.statusOfGettingMorePendingLeads.error.errorMessage ||
        'Hubo un problema al obtener mas leads pendientes'
      );
    }
  }
  
  render() {

    return (
      <MenuProvider>
        <StyleProvider style={getTheme(material)}>
          <Container>
            <NavigationBar
              toolBarStyle={style.toolbar}
              leftButtonVisible={true}
              onPressLeftButton={this._pop}
              iconLeftButton={'md-arrow-back'}
              colorIconLeftButton={toolbarButtonColor}
              title={'Asignaciones pendientes'}
              titleStyle={style.title}/>
            <Content contentContainerStyle={style.content}>
              {
                this.props.statusOfGettingPendingLeads.pendingLeadsAreBeingFetching
                ? this.renderProgressScreen()
                : this.renderContent()
              }
            </Content>
          </Container>
        </StyleProvider>
      </MenuProvider>
    );
  }
  /**
   * Renderiza la vista de progreso mientras
   * se obtienen los leads pendientes
   */
  renderProgressScreen() {

    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbar={true}/>
    );
  }
  /**
   * Renderiza el contenido principal
   */
  renderContent() {
    
    return (
      <View style={style.secondaryContent}>
        {this.renderList()}
        {this.renderFab()}
      </View>
    );
  }
  /**
   * Renderiza la lista de leads pendientes
   */
  renderList() {
    
    return (
      this.props.pendingLeads.length !== 0
      ? <View style={style.listContainer}>
          <FlatList
            style={style.list}
            ref={'flatList'}
            keyExtractor={item => item.id}
            data={this.props.pendingLeads}
            ListHeaderComponent={this.renderHeader()}
            renderItem={this._renderItem}
            ListFooterComponent={this.renderFooter()}
            removeClippedSubviews={true}//Remueve los items que no estan a la vista
            initialNumToRender={10}//Cuantos elementos renderizar en el lote inicial, estos elementos nunca van a ser desmontados por que se los necesita para el metodo scrollToTop
            maxToRenderPerBatch={10}//Cantidad maxima de items a renderizar por lote
            windowSize={40}//Determina el maximo de items que se renderizaran fuera del area visible, en este caso, 10 arriba del area visible y 10 abajo del area visible
            onScroll={this._onScroll}
            refreshControl={
              <RefreshControl
                refreshing={this.props.statusOfGettingPendingLeads.pendingLeadsAreBeingFetching}
                onRefresh={this._onRefresh}
                colors={[refreshColor]}
                progressBackgroundColor={refreshBackgroundColor}/>
             }/>
         </View>
      : <View style={style.empty}>
          <Text style={style.emptyText}>
            {'No hay información para mostrar'}
          </Text>
        </View>
    );
  }
  /**
   * Renderiza el header de la lista de leads pendientes
   */
  renderHeader() {

    return (<View style={style.listHeader}/>);
  }
  /**
   * Renderiza los items de la lista de leads pendientes
   * @param  {object} item objeto con los datos del lead pendiente
   */
  renderItem({item, index}) {

    return (
      <View
        key={item.id} style={(index + 1) % 2 === 0
          ? style.listItemPair
          : style.listItemOdd}>
        <View style={style.listItemDataContainer}>

          <View style={style.listItemIdAndStateContainer}>
            <Text
              numberOfLines={1}
              style={style.listItemId}>
              {item.number}
            </Text>

            <Text
              numberOfLines={1}
              style={[style.listItemText, style.listItemTextMinorWidth]}>
              {item.name}
            </Text>
          </View>

          <Text
            numberOfLines={1}
            style={style.listItemText}>
            {`${item.creationDate} - ${item.phone}`}
          </Text>
          
          <View style={style.listItemProductsContainer}>
            {item.products.map(this._renderProduct)}
          </View>
        </View>
        {this.renderPopUpMenu(item.id)}
      </View>
    );
  }
  /**
   * Renderiza los productos del lead
   */
  renderProduct(item, index) {
    const semicolon = index !== 0 ? ', ' : '';
    
    return (
      <Text
        key={index}
        style={style.listItemProductText}>
        {`${semicolon}${item}`}
      </Text>
    );
      
  }
  /**
   * Renderiza el footer de la lista de leads
   */
  renderFooter() {

    return (
      <View
        style={this.props.pendingLeads.length % 2 === 0
          ? style.listPairFooter
          : style.listOddFooter}>
          {this.renderFooterButton()}
          {this.renderFooterSpinner()}
      </View>
    );
  }
  /**
   * Renderiza el boton del footer para obtener mas leads
   * pendientes
   */
  renderFooterButton() {
    
    return (
      !this.props.statusOfGettingMorePendingLeads.morePendingLeadsAreBeingFetching &&
      this.state.page < this.props.totalOfPagesOfPendingLeads &&
      <Button
        transparent
        full
        onPress={this._handleLoadMore}>
        <Text style={style.listFooterButton}>{'VER MAS'}</Text>
      </Button>
    );
  }
  /**
   * Renderiza el spinner que va en el footer,
   * el cual se muestra cuando se esta trayendo mas leads
   * pendientes
   */
  renderFooterSpinner() {
    
    return (
      this.props.statusOfGettingMorePendingLeads.morePendingLeadsAreBeingFetching &&
      <Spinner color={footerSpinnerColor}/>
    );
  }
  /**
   * Renderiza el float button para ir hacia arriba
   */
  renderFab() {
    
    return(
      this.props.pendingLeads.length !== 0 && this.state.upButtonVisible &&
      <Fab
        position={'bottomRight'}
        style={style.upButton}
        onPress={this._upButtonPressed}>

        <Icon name={'keyboard-arrow-up'} size={20} color={upButtonIconColor}/>
      </Fab>
    );
  }
  /**
   * Renderiza el PopUpMenu para cada item
   */
  renderPopUpMenu(leadId) {
    let itemsMenu = [];
    itemsMenu.push(this.getPopPupMenuItem(this._approvePendingLead(leadId), 'check-circle', 'Aprobar lead'));
    itemsMenu.push(this.getPopPupMenuItem(this._rejectPendingLead(leadId), 'close-circle', 'Rechazar lead'));

    return (
      <PopUpMenu
        menuIconColor={popupMenuIconColor}
        menuContainerStyle={style.popUpMenu}
        items={itemsMenu}/>
    );
  }
  /**
   * Invoca metodo para obtener los leads desde la api
   */
  componentDidMount() {
    this.executeGetPendingLeads();
  }
  /**
   * Escuchador que se dispara cuando el usuario
   * presiona el boton de traer mas leads pendientes
   */
   handleLoadMore() {
    this.setState({
      page: this.state.page + 1,
    }, this._executeGetMoreLeads);
   };
  /**
   * Escuchador que se dispara cuando el usuario presiona el
   * fab button para ir hacia arriba
   */
  upButtonPressed() {
    this.refs.flatList.scrollToItem({
      animated: true,
      item: this.props.pendingLeads[0],
    });
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona
   * la opción de aprobar lead del PopUpMenu
   */
  approvePendingLead(leadId) {
    this.push(approbationOrRejection, {
      leadId: leadId,
      approbation: true,
      onLeadApprovedOrRejected: this._onLeadApprovedOrRejected,
    });
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona
   * la opción de rechazar lead del PopUpMenu
   */
  rejectPendingLead(leadId) {
    this.push(approbationOrRejection, {
      leadId: leadId,
      approbation: false,
      onLeadApprovedOrRejected: this._onLeadApprovedOrRejected,
    });
  }
  /**
   * Invoca a metodo para obtener los leads
   * pendientes
   */
  executeGetPendingLeads() {
    this.setState({
      page: 1,
    }, () => 
      this.props.executeGetPendingLeads(
        this.state.page,
        this.props.session.accessToken,
        this.props.session.refreshToken
      )
    );
  }
  /**
   * Invoca metodo para traer mas leads
   * pendientes
   */
  executeGetMorePendingLeads() {
    this.props.executeGetMorePendingLeads(
      this.state.page,
      this.props.session.accessToken,
      this.props.session.refreshToken
    );
  }
  /**
   * Retorna un objeto json que representa un item
   * del PopUpMenu
   * @param  {function} method metodo que se ejecutara cuando se presione el item
   * @param  {string} icon icono que estará en el item del PopUpMenu
   * @param  {string} text texto que estará en el item del PopUpMenu
   */
  getPopPupMenuItem(method, icon, text) {
    
    return {
      itemPressed: method,
      icon: icon,
      iconColor: popupMenuItemIconColor,
      text: text,
      textColor: popupMenuItemTextColor,
    };
  }
  /**
   * Escuchador del navigator que se dispara cuando
   * esta vista va a ser visible y cuando se va 
   * a ocultar
   */
  onNavigatorEvent(event) {
    switch(event.id) {
      
      case 'willAppear':
        if (this.leadApprovedOrRejected) {
          this.executeGetPendingLeads();
          this.leadApprovedOrRejected = false;
        }
        break;
    }
  }
  /**
   * Escuchador que se dispara cuando se aprobo
   * o rechazo un lead pendiente exitosamente
   */
  onLeadApprovedOrRejected() {
    this.leadApprovedOrRejected = true;
  }
  /**
   * Escuchador que se dispara cuando se hace scroll
   * al FlatList
   */
  onScroll(event) {
    const currentOffset = event.nativeEvent.contentOffset.y;
    const direction = currentOffset > this.previousOffset ? 'down' : 'up';
    this.previousOffset = currentOffset;

    if (this.state.upButtonVisible !== direction)
      this.setState({ upButtonVisible: direction === 'up' });
  }
}

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  pendingLeads: state.dataReducer.leadReducer.pendingLeads,
  totalOfPagesOfPendingLeads: state.dataReducer.leadReducer.totalOfPagesOfPendingLeads,
  statusOfGettingPendingLeads: state.dataReducer.leadReducer.statusOfGettingPendingLeads,
  statusOfGettingMorePendingLeads: state.dataReducer.leadReducer.statusOfGettingMorePendingLeads,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetPendingLeads: executeGetPendingLeads,
  executeGetMorePendingLeads: executeGetMorePendingLeads,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PendingAssignment);