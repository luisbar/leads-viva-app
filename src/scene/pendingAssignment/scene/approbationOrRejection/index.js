/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Tab, TabHeading, Text } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../../../basisComponent.js';
import { confirmModal } from '../../../../config.js';
import { tabIconColor } from '../../../../color.js';
import { ViewPager, LeadInformationViewer, ProductViewer } from '../../../../component/index.js';
import {
  executeGetAPendingLead,
  executeApproveAPendingLead,
  executeRejectAPendingLead
} from '../../../../data/lead/action.js';
/**
 * It renders a view for rejecting or approving a
 * pending lead
 */
class ApprobationOrRejection extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onApproveOrReject = this.onApproveOrReject.bind(this);
    this._openPlaceView = () => {};
    //En esta variable se guarda la justificacion cuando se quiere rechazar un lead
    this.justification;
  }

  componentWillReceiveProps(nextProps) {
    this.showOrDismissProgressModal(nextProps);
    this.showErrorForGettigAPendingLead(nextProps);
    this.showErrorMessageForApprovingOrRejectingAPendingLead(nextProps);
    this.showSuccessMessageForApprovingOrRejectingAPendingLead(this.props, nextProps);
  }

  render() {

    return (
      <ViewPager
        ref={'viewPager'}
        title={this.props.approbation ? 'Aprobar lead' : 'Rechazar lead'}
        onSave={this._onApproveOrReject}
        onOpenPlaceView={this._openPlaceView}
        showRightButtons={true}
        showButtonForOpenPlaceView={false}
        leftButtonIconName={'close'}>

        <Tab
          heading={this.renderHeading('information-outline', 'Información')}>
          
          <LeadInformationViewer
            leadInformation={this.props.pendingLead && this.props.pendingLead.information}
            showProgressScreen={this.props.statusOfGettingAPendingLead.pendingLeadIsBeingFetching}/>
        </Tab>
        
        <Tab
          heading={this.renderHeading('shopping', 'Producto')}>
          
          <ProductViewer
            products={this.props.pendingLead && this.props.pendingLead.products}
            showProgressScreen={this.props.statusOfGettingAPendingLead.pendingLeadIsBeingFetching}/>
        </Tab>
      </ViewPager>
    );
  }
  /**
   * Renderiza los headings de cada tab
   * @param  {string} icon nombre del icono
   * @param  {string} text texto que va en el tab
   */
  renderHeading(icon, text) {
    
    return (
      <TabHeading style={style.heading}>
        <Text>{text}</Text>
        <Icon size={20} name={icon} color={tabIconColor}/>
      </TabHeading>
    );    
  }
  /**
   * Invoca metodo para obtener el lead pendiente
   * a rechazar o aprobar
   */
  componentDidMount() {
    this.props.executeGetAPendingLead(
      this.props.approbation,
      this.props.leadId,
      this.props.session.accessToken,
      this.props.session.refreshToken,
    );
  }
  /**
   * Listener that is triggered when an user has pressed the
   * check button over the toolbar
   */
  onApproveOrReject() {
    if (this.props.approbation)
      this.showTransparentModal(confirmModal, {
        onLeftButtonPressed: this.dismissModal.bind(this),
        onRightButtonPressed: this.onConfirmApprobationOrRejection.bind(this),
        title: 'Confirmación',
        note: '¿Esta seguro que desea aprobar el lead?',
        showNoteInput: false,
      });
    else
      this.showTransparentModal(confirmModal, {
        onLeftButtonPressed: this.dismissModal.bind(this),
        onRightButtonPressed: this.onConfirmApprobationOrRejection.bind(this),
        title: 'Confirmación',
        note: '¿Esta seguro que desea rechazar el lead?',
        showNoteInput: true,
        inputLabel: 'Justificación',
        onHandleInputValue: this.onJustificationWritten.bind(this),
      });
  }
  /**
   * Escuchador que es dispado cuando se confirma
   * la aprobacion o rechazo de un lead pendiente
   */
  onConfirmApprobationOrRejection() {
    if (!this.props.approbation && !this.justification) {
      this.showErrorMessage('Debe introducir una justificación del por que esta rechazando el lead', true);
      return;
    }
    
    this.dismissModal();

    if (this.props.approbation)
      this.props.executeApproveAPendingLead(
        this.props.leadId,
        this.props.session.accessToken,
        this.props.session.refreshToken
      );
    else
      this.props.executeRejectAPendingLead(
        this.props.leadId,
        this.justification,
        this.props.session.accessToken,
        this.props.session.refreshToken
      );
  }
  /**
   * Escuchador que se dispara cuando un usuario
   * escribe en el campo de texto del componente ConfirmModal
   */
  onJustificationWritten(id, state, value) {
    this.justification = value;
  }
  /**
   * Muestra errores cuando se ejecuta
   * el metodo executeGetAPendingLead,
   * si es que los hay
   */
  showErrorForGettigAPendingLead(nextProps) {
    if (nextProps.statusOfGettingAPendingLead.wasAnError)
      this.showErrorMessage(
        nextProps.statusOfGettingAPendingLead.error &&
        nextProps.statusOfGettingAPendingLead.error.errorMessage ||
        `Hubo un problema al obtener el lead a ${nextProps.approbation ? 'aprobar' : 'rechazar'}`
      );
  }
  /**
   * Muestra errores cuando se ejecuta el
   * metodo executeApproveAPendingLead or executeRejectAPendingLead
   */
  showErrorMessageForApprovingOrRejectingAPendingLead(nextProps) {
    if (nextProps.statusOfApprovingOrRejectingAPendingLead.wasAnError &&
        nextProps.statusOfApprovingOrRejectingAPendingLead.approveOrRejectAPendingLeadIsBeingExecuted)//Si hubo error
      this.showErrorMessage(
        nextProps.statusOfApprovingOrRejectingAPendingLead.error &&
        nextProps.statusOfApprovingOrRejectingAPendingLead.error.errorMessage ||
        `Hubo un problema al ${nextProps.approbation ? 'aprobar' : 'rechazar'} el lead`
      )
  }
  /**
   * Muestra mensaje de exito si el metodo
   * executeApproveAPendingLead or executeRejectAPendingLead se ejecuto exitosamente
   */
  showSuccessMessageForApprovingOrRejectingAPendingLead(previousProps, nextProps) {
    if (!previousProps.statusOfApprovingOrRejectingAPendingLead.wasAnError &&
        !nextProps.statusOfApprovingOrRejectingAPendingLead.approveOrRejectAPendingLeadIsBeingExecuted &&
        !nextProps.statusOfGettingAPendingLead.pendingLeadIsBeingFetching &&
        !previousProps.statusOfGettingAPendingLead.pendingLeadIsBeingFetching) {//Si no hubo error

      this.showSuccessMessage(`El lead fue ${nextProps.approbation ? 'aprobado' : 'rechazado'} exitosamente`);
      this.props.onLeadApprovedOrRejected();
      this.pop();
    }
  }
  /**
   * Muestra u oculta el ProgressModal
   */
  showOrDismissProgressModal(nextProps) {
    if (nextProps.statusOfApprovingOrRejectingAPendingLead.approveOrRejectAPendingLeadIsBeingExecuted)
      this.showProgressModal(`${nextProps.approbation ? 'Aprobando' : 'Rechazando'} lead...`);
    else
      this.dismissModal();
  }
}

ApprobationOrRejection.propTypes = {
  leadId: PropTypes.string,
  approbation: PropTypes.bool,//Bandera para saber si se aprueba o rechaza un lead pendiente
  onLeadApprovedOrRejected: PropTypes.func,//Escuchador que se dispara cuando se aprobo o rechazo un lead exitosamente
};

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  statusOfGettingAPendingLead: state.dataReducer.leadReducer.statusOfGettingAPendingLead,
  pendingLead: state.dataReducer.leadReducer.pendingLead,
  statusOfApprovingOrRejectingAPendingLead: state.dataReducer.leadReducer.statusOfApprovingOrRejectingAPendingLead,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetAPendingLead: executeGetAPendingLead,
  executeApproveAPendingLead: executeApproveAPendingLead,
  executeRejectAPendingLead: executeRejectAPendingLead,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ApprobationOrRejection);