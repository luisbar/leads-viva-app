/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { StyleProvider, Container, Content, Text, Fab, Button, Spinner } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { View, FlatList, RefreshControl } from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../../../basisComponent.js';
import getTheme from '../../../../theme/components';
import material from '../../../../theme/variables/material';
import { NavigationBar, ProgressScreen } from '../../../../component/index.js';
import { executeGetNotifications, executeGetMoreNotifications, executeSeeANotification } from '../../../../data/notification/action.js';
import { comment, pendingAssignment, observedLead, leadsList } from '../../../../config.js';
import {
  toolbarButtonColor,
  progressScreenNestedBackgroundColor,
  progressScreenSpinnerColor,
  progressScreenTextColor,
  notificationUpButtonIconColor,
  refreshBackgroundColor,
  refreshColor,
  notificationFooterSpinnerColor,
  notificationSeenIcon,
} from '../../../../color.js';
/*************
 * Constants *
 *************/
const LEAD_COMMENT = 'comentarios_lead';
const ASSIGNED_LEAD = 'lead_asignado';
const OBSERVED_LEAD = 'lead_observado';
const TRANSFERRED_LEAD = 'lead_transferido';
const REASSIGNED_LEAD = 'lead_reasignado';
/**
 * Renderiza la lista de notificaciones
 */
class Notification extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._pop = this.pop.bind(this);
    this._renderItem = this.renderItem.bind(this);
    this._onRefresh = this.executeGetNotifications.bind(this);
    this._handleLoadMore = this.handleLoadMore.bind(this);
    this._upButtonPressed = this.upButtonPressed.bind(this);
    this._executeGetMoreNotifications = this.executeGetMoreNotifications.bind(this);
    this._onScroll = this.onScroll.bind(this);
    this._onNotificationPressed = (notificationId, leadId, leadNumber, type, wasSeen) => this.onNotificationPressed.bind(this, notificationId, leadId, leadNumber, type, wasSeen);
    //State
    this.state = {
      page: 1,//Pagina actual
      upButtonVisible: true,//Bandera para ocultar y mostrar el float button
    };
  }

  /**
   * Verifica si hubo un error al ejecutar
   * los metodos executeGetNotifications
   */
  componentWillReceiveProps(nextProps) {
    if (nextProps.statusOfGettingNotifications.wasAnError &&
        nextProps.statusOfGettingNotifications.notificationsAreBeingFetching)
      this.showErrorMessage(
        nextProps.statusOfGettingNotifications.error &&
        nextProps.statusOfGettingNotifications.error.errorMessage ||
        'Hubo un problema al obtener las notificaciones'
      );
  }
  
  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <NavigationBar
            toolBarStyle={style.toolbar}
            leftButtonVisible={true}
            onPressLeftButton={this._pop}
            iconLeftButton={'md-arrow-back'}
            colorIconLeftButton={toolbarButtonColor}
            title={'Notificaciones'}
            titleStyle={style.title}/>
          <Content contentContainerStyle={style.content}>
            {
              this.props.statusOfGettingNotifications.notificationsAreBeingFetching
              ? this.renderProgressScreen()
              : this.renderContent()
            }
          </Content>
        </Container>
      </StyleProvider>
    );
  }
  /**
   * Renderiza la vista de progreso mientras
   * se obtienen las notificaciones
   */
  renderProgressScreen() {

    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbar={true}/>
    );
  }
  /**
   * Renderiza el contenido principal
   */
  renderContent() {
    
    return (
      <View style={style.secondaryContent}>
        {this.renderList()}
        {this.renderFab()}
      </View>
    );
  }
  /**
   * Renderiza la lista de notificaciones
   */
  renderList() {
    
    return (
      this.props.notifications.length !== 0
      ? <View style={style.listContainer}>
          <FlatList
            style={style.list}
            ref={'flatList'}
            keyExtractor={item => item.id}
            data={this.props.notifications}
            ListHeaderComponent={this.renderHeader()}
            renderItem={this._renderItem}
            ListFooterComponent={this.renderFooter()}
            removeClippedSubviews={true}//Remueve los items que no estan a la vista
            initialNumToRender={10}//Cuantos elementos renderizar en el lote inicial, estos elementos nunca van a ser desmontados por que se los necesita para el metodo scrollToTop
            maxToRenderPerBatch={10}//Cantidad maxima de items a renderizar por lote
            windowSize={40}//Determina el maximo de items que se renderizaran fuera del area visible, en este caso, 10 arriba del area visible y 10 abajo del area visible
            onScroll={this._onScroll}
            refreshControl={
              <RefreshControl
                refreshing={this.props.statusOfGettingNotifications.notificationsAreBeingFetching}
                onRefresh={this._onRefresh}
                colors={[refreshColor]}
                progressBackgroundColor={refreshBackgroundColor}/>
             }/>
         </View>
      : <View style={style.empty}>
          <Text style={style.emptyText}>
            {'No hay información para mostrar'}
          </Text>
        </View>
    );
  }
  /**
   * Renderiza el header de la lista de notificaciones
   */
  renderHeader() {

    return (<View style={style.listHeader}/>);
  }
  /**
   * Renderiza los items de la lista de notificaciones
   * @param  {object} item objeto con los datos de la notificacion
   */
  renderItem({item, index}) {

    return (
      <Button
        onPress={this._onNotificationPressed(item.id, item.leadId, item.leadNumber, item.type, item.seen)}
        key={index} style={(index + 1) % 2 === 0
          ? style.listItemPair
          : style.listItemOdd}>
          <Text style={[style.listItemText, style.listItemTextBigger]}>{item.message}</Text>
          <Text style={style.listItemText}>{item.date}</Text>
          <Icon
            style={style.listItemIcon}
            name={item.seen ? 'done-all' : 'done'}
            size={25}
            color={notificationSeenIcon}/>
      </Button>
    );
  }
  /**
   * Renderiza el footer de la lista de notificaciones
   */
  renderFooter() {

    return (
      <View
        style={this.props.notifications.length % 2 === 0
          ? style.listPairFooter
          : style.listOddFooter}>
          {this.renderFooterButton()}
          {this.renderFooterSpinner()}
      </View>
    );
  }
  /**
   * Renderiza el boton del footer para obtener mas notificaciones
   */
  renderFooterButton() {
    
    return (
      !this.props.statusOfGettingMoreNotifications.moreNotificationsAreBeingFetching &&
      this.state.page < this.props.totalOfPagesOfNotifications &&
      <Button
        transparent
        full
        onPress={this._handleLoadMore}>
        <Text style={style.listFooterButton}>{'VER MAS'}</Text>
      </Button>
    );
  }
  /**
   * Renderiza el spinner que va en el footer,
   * el cual se muestra cuando se esta trayendo mas
   * notificaciones
   */
  renderFooterSpinner() {
    
    return (
      this.props.statusOfGettingMoreNotifications.moreNotificationsAreBeingFetching &&
      <Spinner color={notificationFooterSpinnerColor}/>
    );
  }
  /**
   * Renderiza el float button para ir hacia arriba
   */
  renderFab() {
    
    return(
      this.props.notifications.length !== 0 && this.state.upButtonVisible &&
      <Fab
        position={'bottomRight'}
        style={style.upButton}
        onPress={this._upButtonPressed}>

        <Icon name={'keyboard-arrow-up'} size={20} color={notificationUpButtonIconColor}/>
      </Fab>
    );
  }
  /**
   * Invoca metodo para obtener las notificaciones
   */
  componentDidMount() {
    this.executeGetNotifications();
  }
  /**
   * Escuchador que se dispara cuando el usuario
   * presiona el boton de traer mas notificaciones
   */
   handleLoadMore() {
    this.setState({
      page: this.state.page + 1,
    }, this._executeGetMoreNotifications);
   };
  /**
   * Escuchador que se dispara cuando el usuario presiona el
   * fab button para ir hacia arriba
   */
  upButtonPressed() {
    this.refs.flatList.scrollToItem({
      animated: true,
      item: this.props.notifications[0],
    });
  }
  /**
   * Invoca a metodo para obtener notificaciones
   */
  executeGetNotifications() {
    this.setState({
      page: 1,
    }, () => 
      this.props.executeGetNotifications(
        this.props.session.accessToken,
        this.props.session.refreshToken
      )
    );
  }
  /**
   * Invoca metodo para traer mas notificaciones
   */
  executeGetMoreNotifications() {
    this.props.executeGetMoreNotifications(
      this.state.page,
      this.props.session.accessToken,
      this.props.session.refreshToken
    );
  }
  /**
   * Escuchador que se dispara cuando se hace scroll
   * al FlatList
   */
  onScroll(event) {
    const currentOffset = event.nativeEvent.contentOffset.y;
    const direction = currentOffset > this.previousOffset ? 'down' : 'up';
    this.previousOffset = currentOffset;

    if (this.state.upButtonVisible !== direction)
      this.setState({ upButtonVisible: direction === 'up' });
  }
  /**
   * Escuchador que se dispara cuando se presiona sobre
   * un item de la lista de notificaciones
   */
  onNotificationPressed(notificationId, leadId, leadNumber, type, wasSeen) {
    //Se invoca metodo para poner la notificacion en vista, si es que no ha sido vista
    if (!wasSeen)
      this.props.executeSeeANotification(
        notificationId,
        this.props.session.accessToken,
        this.props.session.refreshToken
      );
    //Se renderiza la vista en base al tipo de notificacion
    switch (type) {

      case LEAD_COMMENT:
        this.push(comment, {
          leadId: leadId,
        });
        break;
        
      case ASSIGNED_LEAD:
      case TRANSFERRED_LEAD:
        this.push(pendingAssignment, null);
        break;
        
      case OBSERVED_LEAD:
        this.push(observedLead, null);
        break;
        
      case REASSIGNED_LEAD:
        this.push(leadsList, {
          leadStatus: '',
          leadStatusName: 'Resumen',
          search: String(leadNumber),
        });
        break;
    }
  }
}

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  notifications: state.dataReducer.notificationReducer.notifications,
  totalOfPagesOfNotifications: state.dataReducer.notificationReducer.totalOfPagesOfNotifications,
  statusOfGettingNotifications: state.dataReducer.notificationReducer.statusOfGettingNotifications,
  statusOfGettingMoreNotifications: state.dataReducer.notificationReducer.statusOfGettingMoreNotifications,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetNotifications: executeGetNotifications,
  executeGetMoreNotifications: executeGetMoreNotifications,
  executeSeeANotification: executeSeeANotification,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Notification);