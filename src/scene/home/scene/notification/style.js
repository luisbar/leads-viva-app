/******************
 * Project import *
 ******************/
import {
  toolbarBackgroundColor,
  toolbarBorderColor,
  toolbarTextColor,
  notificationListItemOddBackgroundColor,
  notificationListItemPairBackgroundColor,
  notificationListItemTextColor,
  notificationUpButtonBackgroundColor,
  notificationFooterButtonTextColor,
  notificationEmptyListText,
} from '../../../../color.js';

export default {

  toolbar: {
    backgroundColor: toolbarBackgroundColor,
    borderColor: toolbarBorderColor,
  },
  title: {
    color: toolbarTextColor,
  },
  content: {
    flex: 1,
  },
  secondaryContent: {
    flex: 1,
  },
  listContainer: {
    flex: 1,
  },
  list: {
    marginLeft: 10,
    marginRight: 10,
  },
  listHeader: {
    backgroundColor: notificationListItemOddBackgroundColor,
    height: 10,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: 10,
  },
  listPairFooter: {
    backgroundColor: notificationListItemPairBackgroundColor,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    marginBottom: 10,
    minHeight: 10,
  },
  listOddFooter: {
    backgroundColor: notificationListItemOddBackgroundColor,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    marginBottom: 10,
    minHeight: 10,
  },
  listFooterButton: {
    color: notificationFooterButtonTextColor,
  },
  listItemPair: {
    backgroundColor: notificationListItemPairBackgroundColor,
    flexDirection: 'column',
    alignItems: 'flex-start',
    height: 'auto',
    borderRadius: 0,
  },
  listItemOdd: {
    backgroundColor: notificationListItemOddBackgroundColor,
    flexDirection: 'column',
    alignItems: 'flex-start',
    height: 'auto',
    borderRadius: 0,
  },
  listItemText: {
    color: notificationListItemTextColor,
  },
  listItemTextBigger: {
    fontFamily: 'Museo700',
    marginBottom: 10,
  },
  listItemIcon: {
    alignSelf: 'flex-end',
    marginRight: 6,
  },
  upButton: {
    backgroundColor: notificationUpButtonBackgroundColor,
  },
  empty: {
    flex: 1,
    justifyContent: 'center',
  },
  emptyText: {
    color: notificationEmptyListText,
    textAlign: 'center',
  }
}
