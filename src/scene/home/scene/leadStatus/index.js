/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Container, Content, Grid, Row, Button, Text, Footer, StyleProvider } from 'native-base';
import { View, Image } from 'react-native';
/******************
 * Project import *
 ******************/
import style from './style.js';
import BasisComponent from '../../../basisComponent.js';
import getTheme from '../../../../theme/components';
import material from '../../../../theme/variables/material';
import { NavigationBar } from '../../../../component/index.js';
import { leadSubStatus, leadsList } from '../../../../config.js';
import { toolbarButtonColor } from '../../../../color.js';
/*************
 * Constants *
 *************/
const FIRST_BUTTON = 'asignación';
const SECOND_BUTTON = 'gestión';
const THIRD_BUTTON = 'cerrado';
const FOURTH_BUTTON = 'observado';
/**
 * Renderiza la lista de estados
 */
class LeadStatus extends BasisComponent {

  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._pop = this.pop.bind(this);
    this._goSubStatusOfLeadsScreen = (status) => this.push.bind(this, leadSubStatus, { status: status });
    this._goLeadListScreen = (status) => this.push.bind(this, leadsList, { leadStatus: status, leadStatusName: 'Resumen' });
  }

  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <NavigationBar
            toolBarStyle={style.toolbar}
            leftButtonVisible={true}
            onPressLeftButton={this._pop}
            iconLeftButton={'md-arrow-back'}
            colorIconLeftButton={toolbarButtonColor}
            title={'Estados de lead'}
            titleStyle={style.title}/>
          <Content contentContainerStyle={style.content}>
            {this.renderButtons()}
          </Content>
          <Footer style={style.footer}>
            <Image
              style={style.imageFooter}
              source={require('../../../../image/logo.png')}
              resizeMode={'center'}/>
          </Footer>
        </Container>
      </StyleProvider>
    );
  }
  /**
   * Renderiza todos los botones para ir a
   * las distintas vistas
   */
  renderButtons() {
    
    return (
      <Grid>
        <Row>
          {this.renderButton(FIRST_BUTTON, require('../../../../image/assignment.png'), this._goSubStatusOfLeadsScreen('asignacion'))}
          {this.renderButton(SECOND_BUTTON, require('../../../../image/management.png'), this._goSubStatusOfLeadsScreen('gestion'))}
        </Row>
        <Row>
          {this.renderButton(THIRD_BUTTON, require('../../../../image/closed.png'), this._goSubStatusOfLeadsScreen('cierre'))}
          {this.renderButton(FOURTH_BUTTON, require('../../../../image/observed.png'), this._goSubStatusOfLeadsScreen('observado'))}
        </Row>
        <Button
          style={style.summaryButton}
          onPress={this._goLeadListScreen('')}>
          <Text style={style.summaryButtonText}>{'resumen'}</Text>
        </Button>
      </Grid>
    );
  }
  /**
   * Renderiza los botones de leadStatus
   */
  renderButton(buttonText, buttonIcon, onPressButton){
    const dynamicStyle = this.getDynamicStyleByButton(buttonText);
    
    return (
      <View style={[style.buttonContainer, dynamicStyle]}>
        <Image
          style={style.imageButton}
          source={buttonIcon}
          resizeMode={'contain'}/>
        <Button
          style={style.button}
          onPress={onPressButton}>
          <Text style={style.text}>{buttonText}</Text>
        </Button>
      </View>
    );
  }
  /**
   * Retorna estilo dinamico en base el boton
   */
  getDynamicStyleByButton(buttonText) {
    
    return buttonText === FIRST_BUTTON
      ? style.firstButtonContainer
      : buttonText === SECOND_BUTTON
      ? style.secondButtonContainer
      : buttonText === THIRD_BUTTON
      ? style.thirdButtonContainer
      : null;
  }
}

export default LeadStatus;
