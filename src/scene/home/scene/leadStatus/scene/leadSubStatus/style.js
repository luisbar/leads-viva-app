/******************
 * Project import *
 ******************/
import {
  toolbarBackgroundColor,
  toolbarBorderColor,
  toolbarTextColor,
  subStatusContent,
  subStatusButton,
  subStatusButtonText,
  subStatusButtonBadge,
  subStatusButtonBadgeText,
  subStatusFooter,
} from '../../../../../../color.js';

export default {
  
  toolbar: {
    backgroundColor: toolbarBackgroundColor,
    borderColor: toolbarBorderColor,
  },
  title: {
    color: toolbarTextColor,
  },
  content: {
    flex: 1,
    backgroundColor: subStatusContent,
    justifyContent: 'center',
  },
  subStatusButton: {
    backgroundColor: subStatusButton,
    borderRadius: 10,
    alignSelf: 'center',
    width: '90%',
    justifyContent: 'center',
    marginVertical: 5,
  },
  subStatusButtonText: {
    color: subStatusButtonText,
    flex: 1,
    textAlign: 'center',
  },
  subStatusButtonBadge: {
    justifyContent: 'center',
    backgroundColor: subStatusButtonBadge,
    alignSelf: 'center',
    flex: -1,
    margin: 5,
  },
  subStatusButtonBadgeText: {
    color: subStatusButtonBadgeText,
  },
  footer: {
    backgroundColor: subStatusFooter,
    alignItems: 'center',
  },
  imageFooter: {
    height: 40,
  },
}