/******************
 * Project import *
 ******************/
import {
  toolbarBackgroundColor,
  toolbarBorderColor,
  toolbarTextColor,
  searchBarBackgroundColor,
  searchBarBorderColor,
  listItemOddBackgroundColor,
  listItemPairBackgroundColor,
  listItemIdBackgroundColor,
  listItemIdTextColor,
  listItemPendingStatusBackgroundColor,
  listItemContactedStatusBackgroundColor,
  listItemFallenStatusBackgroundColor,
  listItemObservedStatusBackgroundColor,
  listItemScheduledStatusBackgroundColor,
  listItemSuccessfulStatusBackgroundColor,
  listItemStoreStatusBackgroundColor,
  listItemRejectedStatusBackgroundColor,
  listItemStatusText,
  listItemTextColor,
  popupMenuBackgroundColor,
  upButtonBackgroundColor,
  footerButtonTextColor,
  emptyListText
} from '../../../../../../../../color.js';

export default {

  toolbar: {
    backgroundColor: toolbarBackgroundColor,
    borderColor: toolbarBorderColor,
  },
  title: {
    color: toolbarTextColor,
  },
  searchBar: {
    backgroundColor: searchBarBackgroundColor,
    borderColor: searchBarBorderColor,
  },
  listContainer: {
    flex: 1,
  },
  listHeader: {
    backgroundColor: listItemOddBackgroundColor,
    height: 10,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    marginTop: 10,
  },
  list: {
    marginLeft: 10,
    marginRight: 10,
  },
  listOddFooter: {
    backgroundColor: listItemOddBackgroundColor,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    marginBottom: 10,
    minHeight: 10,
  },
  listPairFooter: {
    backgroundColor: listItemPairBackgroundColor,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    marginBottom: 10,
    minHeight: 10,
  },
  listFooterButton: {
    color: footerButtonTextColor,
  },
  listItemOdd: {
    backgroundColor: listItemOddBackgroundColor,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  listItemPair: {
    backgroundColor: listItemPairBackgroundColor,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  listItemDataContainer: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    minWidth: '90%',
    maxWidth: '90%'
  },
  listItemIdAndStatusContainer: {
    flexDirection: 'row',
  },
  listItemId: {
    maxHeight: 23,
    marginLeft: 10,
    marginTop: 5,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: listItemIdBackgroundColor,
    color: listItemIdTextColor,
    borderRadius: 5,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  listItemPendingStatus: {
    backgroundColor: listItemPendingStatusBackgroundColor,
    color: listItemStatusText,
  },
  listItemContactedStatus: {
    backgroundColor: listItemContactedStatusBackgroundColor,
    color: listItemStatusText,
  },
  listItemFallenStatus: {
    backgroundColor: listItemFallenStatusBackgroundColor,
    color: listItemStatusText,
  },
  listItemObservedStatus: {
    backgroundColor: listItemObservedStatusBackgroundColor,
    color: listItemStatusText,
  },
  listItemScheduledStatus: {
    backgroundColor: listItemScheduledStatusBackgroundColor,
    color: listItemStatusText,
  },
  listItemSuccessfulStatus: {
    backgroundColor: listItemSuccessfulStatusBackgroundColor,
    color: listItemStatusText,
  },
  listItemStoreStatus: {
    backgroundColor: listItemStoreStatusBackgroundColor,
    color: listItemStatusText,
  },
  listItemRejectedStatus: {
    backgroundColor: listItemRejectedStatusBackgroundColor,
    color: listItemStatusText,
  },
  listItemText: {
    maxWidth: '100%',
    color: listItemTextColor,
    marginTop: 5,
    marginLeft: 10,
    marginBottom: 5,
  },
  listItemJustificationText: {
    fontFamily: 'Museo700',
  },
  popUpMenu: {
    borderRadius: 10,
    backgroundColor: popupMenuBackgroundColor,
    width: 'auto',
  },
  upButton: {
    backgroundColor: upButtonBackgroundColor,
  },
  empty: {
    flex: 1,
    justifyContent: 'center',
  },
  emptyText: {
    color: emptyListText,
    textAlign: 'center',
  }
};
