/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { View, FlatList, RefreshControl } from 'react-native';
import { Container, Text, Fab, Button, Spinner, StyleProvider } from 'native-base';
import { MenuProvider } from 'react-native-popup-menu';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
/******************
 * Project import *
 ******************/
import getTheme from '../../../../../../../../theme/components';
import material from '../../../../../../../../theme/variables/material';
import BasisComponent from '../../../../../../../basisComponent.js';
import style from './style.js';
import { NavigationBar, PopUpMenu, ProgressScreen } from '../../../../../../../../component/index.js';
import Router from '../../../../../../../../router.js';
import { leadEdition, actionsRecord, comment, leadViewer, actionRegistration, filter } from '../../../../../../../../config.js';
import { executeGetLeads, executeGetMoreLeads } from '../../../../../../../../data/lead/action.js';
import {
  toolbarButtonColor,
  searchBarIconColor,
  popupMenuIconColor,
  popupMenuItemIconColor,
  popupMenuItemTextColor,
  progressScreenNestedBackgroundColor,
  progressScreenSpinnerColor,
  progressScreenTextColor,
  upButtonIconColor,
  refreshBackgroundColor,
  refreshColor,
  footerSpinnerColor,
} from '../../../../../../../../color.js';
/*************
 * Constants *
 *************/
const PENDING = 'Pendiente de contacto';
const CONTACTED = 'Contactado';
const FALLEN = 'Caído';
const OBSERVED = 'Observado';
const SCHEDULED = 'Contacto programado';
const SUCCESSFUL = 'Exitoso';
const STORE = 'Pasará por tienda';
/**
 * Renderiza la lista de leads
 */
class LeadsList extends BasisComponent {

  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._onFilter = this.onFilter.bind(this);
    this._pop = this.pop.bind(this);
    this._onSearch = this.onSearch.bind(this);
    this._renderItem = this.renderItem.bind(this);
    this._onRefresh = this.executeGetLeads.bind(this);
    this._handleLoadMore = this.handleLoadMore.bind(this);
    this._upButtonPressed = this.upButtonPressed.bind(this);
    this._seeLead = (leadId) => this.seeLead.bind(this, leadId);
    this._editLead = (leadId) => this.editLead.bind(this, leadId);
    this._seeActionsRecord = (leadId) => this.seeActionsRecord.bind(this, leadId);
    this._createAction = (leadId) => this.createAction.bind(this, leadId);
    this._addComment = (leadId) => this.addComment.bind(this, leadId);
    this._executeGetMoreLeads = this.executeGetMoreLeads.bind(this);
    this._onLeadFiltered = this.onLeadFiltered.bind(this);
    this._onFilterViewWillBeClosed = this.onFilterViewWillBeClosed.bind(this);
    this._onLeadEditedOrActionRegistered = this.onLeadEditedOrActionRegistered.bind(this);
    this._onScroll = this.onScroll.bind(this);
    //Array de botones que se renderizaran en el navigation bar
    this.rightButtons = [
      {
        onPressRightButton: this._onFilter,
        iconRightButton: 'ios-funnel',
        colorIconRightButton: toolbarButtonColor,
      },
    ];
    //State
    this.state = {
      page: 1,//Pagina actual
      status: this.props.leadStatus,//Para filtrar por estado
      search: this.props.search || '',//Para filtrar por lo que se introdujo en el campo de busqueda
      sources: [],//Para filtrar por origenes
      initialDateOfCreation: null,//Para filtrar por fecha inicial de creacion
      endDateOfCreation: null,//Para filtrar por fecha final de creacion
      upButtonVisible: true,//Bandera para ocultar y mostrar el float button
    };
    //Variable para saber si la vista filter esta visible
    this.filterViewIsViewable;
    //Variable para saber si se edito un lead o se registro accion
    this.leadEditedOrActionRegistered;
    //Escuchador del navigator
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  
  /**
   * Verifica si hubo un error al ejecutar
   * los metodos executeGetLeads y executeGetMoreLeads
   */
  componentWillReceiveProps(nextProps) {
    if (!this.filterViewIsViewable &&//Si la vista de filtro no esta visible
        nextProps.statusOfGettingLeads.wasAnError &&
        nextProps.statusOfGettingLeads.leadsAreBeingFetching)
      this.showErrorMessage(
        nextProps.statusOfGettingLeads.error &&
        nextProps.statusOfGettingLeads.error.errorMessage ||
        'Hubo un problema al obtener los leads'
      );
      
    if (nextProps.statusOfGettingMoreLeads.wasAnError &&
        nextProps.statusOfGettingMoreLeads.moreLeadsAreBeingFetching) {
      this.setState({ page: this.state.page - 1 });
      this.showErrorMessage(
        nextProps.statusOfGettingMoreLeads.error &&
        nextProps.statusOfGettingMoreLeads.error.errorMessage ||
        'Hubo un problema al obtener mas leads'
      );
    }
  }

  render() {

    return (
      <MenuProvider>
        <StyleProvider style={getTheme(material)}>
        {
          this.props.statusOfGettingLeads.leadsAreBeingFetching
          ? this.renderProgressScreen()
          : this.renderContent()
        }
        </StyleProvider>
      </MenuProvider>
    );
  }
  /**
   * Renderiza la vista de progreso mientras
   * se obtienen los leads
   */
  renderProgressScreen() {

    return (
      <Container>
        <NavigationBar
          toolBarStyle={style.toolbar}
          leftButtonVisible={true}
          onPressLeftButton={this._pop}
          iconLeftButton={'md-arrow-back'}
          colorIconLeftButton={toolbarButtonColor}
          title={this.props.leadStatusName}
          titleStyle={style.title}
          rightButtonsVisible={true}
          rightButtons={this.rightButtons}
          searchBarVisible={true}
          searchBarStyle={style.searchBar}
          searchBarIconColor={searchBarIconColor}
          onSearch={this._onSearch}
          defaultSearchValue={this.state.search}/>
        <ProgressScreen
          backgroundColor={progressScreenNestedBackgroundColor}
          textColor={progressScreenTextColor}
          spinnerColor={progressScreenSpinnerColor}
          withBiggerToolbar={true}/>
      </Container>
    );
  }
  /**
   * Renderiza la lista de leads
   */
  renderContent() {

    return (
      <Container>
        <NavigationBar
          toolBarStyle={style.toolbar}
          leftButtonVisible={true}
          onPressLeftButton={this._pop}
          iconLeftButton={'md-arrow-back'}
          colorIconLeftButton={toolbarButtonColor}
          title={this.props.leadStatusName}
          titleStyle={style.title}
          rightButtonsVisible={true}
          rightButtons={this.rightButtons}
          searchBarVisible={true}
          searchBarStyle={style.searchBar}
          searchBarIconColor={searchBarIconColor}
          onSearch={this._onSearch}
          defaultSearchValue={this.state.search}/>
        {this.renderList()}
        {this.renderFab()}
      </Container>
    );
  }
  /**
   * Renderiza la lista de leads
   */
  renderList() {
    
    return (
      this.props.leads.length !== 0
      ? <View style={style.listContainer}>
          <FlatList
            style={style.list}
            ref={'flatList'}
            keyExtractor={item => item.id}
            data={this.props.leads}
            ListHeaderComponent={this.renderHeader()}
            renderItem={this._renderItem}
            ListFooterComponent={this.renderFooter()}
            removeClippedSubviews={true}//Remueve los items que no estan a la vista
            initialNumToRender={10}//Cuantos elementos renderizar en el lote inicial, estos elementos nunca van a ser desmontados por que se los necesita para el metodo scrollToTop
            maxToRenderPerBatch={10}//Cantidad maxima de items a renderizar por lote
            windowSize={40}//Determina el maximo de items que se renderizaran fuera del area visible, en este caso, 10 arriba del area visible y 10 abajo del area visible
            onScroll={this._onScroll}
            refreshControl={
              <RefreshControl
                refreshing={this.props.statusOfGettingLeads.leadsAreBeingFetching}
                onRefresh={this._onRefresh}
                colors={[refreshColor]}
                progressBackgroundColor={refreshBackgroundColor}/>
             }/>
         </View>
      : <View style={style.empty}>
          <Text style={style.emptyText}>
            {'No hay información para mostrar'}
          </Text>
        </View>
    );
  }
  /**
   * Renderiza el header de la lista de leads
   */
  renderHeader() {

    return (<View style={style.listHeader}/>);
  }
  /**
   * Renderiza los items de la lista de leads
   * @param  {object} item objeto con los datos del lead
   */
  renderItem({item, index}) {

    return (
      <View
        key={item.id} style={(index + 1) % 2 === 0
          ? style.listItemPair
          : style.listItemOdd}>
        <View style={style.listItemDataContainer}>

          <View style={style.listItemIdAndStatusContainer}>
            <Text
              numberOfLines={1}
              style={style.listItemId}>
              {item.number}
            </Text>

            <Text
              numberOfLines={1}
              style={[style.listItemId, this.getStyleByState(item.state)]}>
              {item.state}
            </Text>
          </View>

          <Text
            numberOfLines={1}
            style={style.listItemText}>
            {`${item.phone} ${item.name}`}
          </Text>

          {
            item.reasonForClousure
            ? <Text
                numberOfLines={1}
                style={style.listItemText}>
                {item.reasonForClousure}
              </Text>
            : <Text
                numberOfLines={1}
                style={style.listItemText}>
                {item.creationDate}
              </Text>
          }
          
          {
            item.numberOfPurchaseProcess
            ? <Text
                numberOfLines={1}
                style={style.listItemText}>
                {item.numberOfPurchaseProcess}
              </Text>
            : item.justification
            ? <Text
                style={[style.listItemText, style.listItemJustificationText]}>
                {item.justification}
              </Text>
            : null
          }
        </View>
        {this.renderPopUpMenu(item.id, item.actions)}
      </View>
    );
  }
  /**
   * Renderiza el footer de la lista de leads
   */
  renderFooter() {

    return (
      <View
        style={this.props.leads.length % 2 === 0
          ? style.listPairFooter
          : style.listOddFooter}>
          {this.renderFooterButton()}
          {this.renderFooterSpinner()}
      </View>
    );
  }
  /**
   * Renderiza el boton del footer para obtener mas leads
   */
  renderFooterButton() {
    
    return (
      !this.props.statusOfGettingMoreLeads.moreLeadsAreBeingFetching &&
      this.state.page < this.props.totalOfPages &&
      <Button
        transparent
        full
        onPress={this._handleLoadMore}>
        <Text style={style.listFooterButton}>{'VER MAS'}</Text>
      </Button>
    );
  }
  /**
   * Renderiza el spinner que va en el footer,
   * el cual se muestra cuando se esta trayendo mas leads
   */
  renderFooterSpinner() {
    
    return (
      this.props.statusOfGettingMoreLeads.moreLeadsAreBeingFetching &&
      <Spinner color={footerSpinnerColor}/>
    );
  }
  /**
   * Renderiza el float button para ir hacia arriba
   */
  renderFab() {
    
    return(
      this.props.leads.length !== 0 && this.state.upButtonVisible &&
      <Fab
        position={'bottomRight'}
        style={style.upButton}
        onPress={this._upButtonPressed}>

        <Icon name={'keyboard-arrow-up'} size={20} color={upButtonIconColor}/>
      </Fab>
    );
  }
  /**
   * Renderiza el PopUpMenu para cada item
   */
  renderPopUpMenu(leadId, actions) {
    let itemsMenu = [];
    
    if (actions.seeLead)
      itemsMenu.push(this.getPopPupMenuItem(this._seeLead(leadId), 'eye', 'Ver lead'));
    if (actions.editLead)
      itemsMenu.push(this.getPopPupMenuItem(this._editLead(leadId), 'pencil-box-outline', 'Editar lead'));
    if (actions.seeActionsRecord)
      itemsMenu.push(this.getPopPupMenuItem(this._seeActionsRecord(leadId), 'clock', 'Historial de acciones'));
    if (actions.createAction)
      itemsMenu.push(this.getPopPupMenuItem(this._createAction(leadId), 'file-plus', 'Crear acción'));
    if (actions.addComment)
      itemsMenu.push(this.getPopPupMenuItem(this._addComment(leadId), 'comment-text', 'Agregar comentario'));

    return (
      <PopUpMenu
        menuIconColor={popupMenuIconColor}
        menuContainerStyle={style.popUpMenu}
        items={itemsMenu}/>
    );
  }
  /**
   * Invoca metodo para obtener los leads desde la api
   */
  componentDidMount() {
    this.executeGetLeads();
  }
  /**
   * Escuchador que se dispara cuando se presiona el boton
   * para filtrar que esta en el navigation bar
   */
  onFilter() {
    //Renderiza la vista para editar un lead
    Router.getInstance().push(filter, {
      selectedItems: this.state.sources,
      initialDate: this.state.initialDateOfCreation,
      endDate: this.state.endDateOfCreation,
      state: this.state.status,
      search: this.state.search,
      onLeadFiltered: this._onLeadFiltered,
      onFilterViewWillBeClosed: this._onFilterViewWillBeClosed,
    });
    
    this.filterViewIsViewable = true;
  }
  /**
   * Escuchador que es disparado cuando el filtrado
   * de leads es exitoso
   */
  onLeadFiltered(newSources, newInitialDate, newEndDate) {
    this.filterViewIsViewable = false;
    
    //Actualiza los filtros
    this.setState({
      sources: newSources,
      initialDateOfCreation: newInitialDate,
      endDateOfCreation: newEndDate,
      page: 1,
    });
    //Cierrar la vista para filtrar
    Router.getInstance().navigator.pop(); 
  }
  /**
   * Escuchador que se dispara cuando la vista filter
   * ha sido cerrada
   */
  onFilterViewWillBeClosed() {
    this.filterViewIsViewable = false;
  }
  /**
   * Escuchador que se dispara cuando el usuario escribe
   * en el campo de texto que esta en la barra de busqueda
   */
  onSearch(text) {
    this.setState({
      search: text,
    }, () => 
      this.props.executeGetLeads(
        this.state.page,
        this.state.status,
        this.state.search,
        this.state.sources,
        this.state.initialDateOfCreation,
        this.state.endDateOfCreation,
        this.props.session.accessToken,
        this.props.session.refreshToken
      )
    );
  }
  /**
   * Escuchador que se dispara cuando el usuario
   * presiona el boton de traer mas leads
   */
   handleLoadMore() {
    this.setState({
      page: this.state.page + 1,
    }, this._executeGetMoreLeads);
   };
  /**
   * Escuchador que se dispara cuando el usuario presiona el
   * fab button para ir hacia arriba
   */
  upButtonPressed() {
    this.refs.flatList.scrollToItem({
      animated: true,
      item: this.props.leads[0],
    });
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona
   * la opción de ver lead del PopUpMenu
   */
  seeLead(leadId) {
    //Renderiza la vista para ver un lead
    Router.getInstance().push(leadViewer, {
      leadId: leadId,
    });
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona
   * la opción de editar lead del PopUpMenu
   */
  editLead(leadId) {
    //Renderiza la vista para editar un lead
    Router.getInstance().push(leadEdition, {
      leadId: leadId,
      onLeadEdited: this._onLeadEditedOrActionRegistered
    });
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona
   * la opción de historial de acciones del PopUpMenu
   */
  seeActionsRecord(leadId) {
    //Renderiza la vista para ver el historial de
    //acciones
    Router.getInstance().push(actionsRecord, {
      leadId: leadId,
    });
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona
   * la opción de crear acción del PopUpMenu
   */
  createAction(leadId) {
    //Renderiza la vista para ver el historial de
    //acciones
    Router.getInstance().push(actionRegistration, {
      leadId: leadId,
      onActionRegistered: this._onLeadEditedOrActionRegistered
    });
  }
  /**
   * Escuchador que se dispara cuando el usuario presiona
   * la opción de agregar comentario del PopUpMenu
   */
  addComment(leadId) {
    //Renderiza la vista para ver los comentarios
    //de un lead
    Router.getInstance().push(comment, {
      leadId: leadId,
    });
  }
  /**
   * Invoca a metodo para obtener los leads
   */
  executeGetLeads() {
    this.setState({
      page: 1,
    }, () => 
      this.props.executeGetLeads(
        this.state.page,
        this.state.status,
        this.state.search,
        this.state.sources,
        this.state.initialDateOfCreation,
        this.state.endDateOfCreation,
        this.props.session.accessToken,
        this.props.session.refreshToken
      )
    );
  }
  /**
   * Invoca metodo para traer mas leads
   */
  executeGetMoreLeads() {
    this.props.executeGetMoreLeads(
      this.state.page,
      this.state.status,
      this.state.search,
      this.state.sources,
      this.state.initialDateOfCreation,
      this.state.endDateOfCreation,
      this.props.session.accessToken,
      this.props.session.refreshToken
    );
  }
  /**
   * Retorna un objeto json que representa un item
   * del PopUpMenu
   * @param  {function} method metodo que se ejecutara cuando se presione el item
   * @param  {[type]} icon icono que estará en el item del PopUpMenu
   * @param  {[type]} text texto que estará en el item del PopUpMenu
   */
  getPopPupMenuItem(method, icon, text) {
    
    return {
      itemPressed: method,
      icon: icon,
      iconColor: popupMenuItemIconColor,
      text: text,
      textColor: popupMenuItemTextColor,
    };
  }
  /**
   * Escuchador del navigator que se dispara cuando
   * esta vista va a ser visible y cuando se va 
   * a ocultar
   */
  onNavigatorEvent(event) {
    switch(event.id) {
      
      case 'willAppear':
        if (this.leadEditedOrActionRegistered) {
          this.executeGetLeads();
          this.leadEditedOrActionRegistered = false;
        }
        break;
    }
  }
  /**
   * Escuchador que se dispara cuando se registro
   * una acción exitosamente o cuando se edito un lead
   * exitosamente
   */
  onLeadEditedOrActionRegistered() {
    this.leadEditedOrActionRegistered = true;
  }
  /**
   * Escuchador que se dispara cuando se hace scroll
   * al FlatList
   */
  onScroll(event) {
    const currentOffset = event.nativeEvent.contentOffset.y;
    const direction = currentOffset > this.previousOffset ? 'down' : 'up';
    this.previousOffset = currentOffset;

    if (this.state.upButtonVisible !== direction)
      this.setState({ upButtonVisible: direction === 'up' });
  }
  /**
   * Retorna estilo para el estado del lead
   * en base al leadStatus
   */
  getStyleByState(status) {
    
    return status === PENDING
      ? style.listItemPendingStatus
      : status === CONTACTED
      ? style.listItemContactedStatus
      : status === FALLEN
      ? style.listItemFallenStatus
      : status === OBSERVED
      ? style.listItemObservedStatus
      : status === SCHEDULED
      ? style.listItemScheduledStatus
      : status === SUCCESSFUL
      ? style.listItemSuccessfulStatus
      : status === STORE
      ? style.listItemStoreStatus
      : style.listItemRejectedStatus;
  }

}

LeadsList.propTypes = {
  leadStatus: PropTypes.string.isRequired,//Para filtrar los leads por el estado
  leadStatusName: PropTypes.string.isRequired,//Nombre del estado
  search: PropTypes.string,//Para filtrar los leads cuando se llama a esta vista desde la vista de notificaciones
};

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  leads: state.dataReducer.leadReducer.leads,
  totalOfPages: state.dataReducer.leadReducer.totalOfPages,
  statusOfGettingLeads: state.dataReducer.leadReducer.statusOfGettingLeads,
  statusOfGettingMoreLeads: state.dataReducer.leadReducer.statusOfGettingMoreLeads,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetLeads: executeGetLeads,
  executeGetMoreLeads: executeGetMoreLeads,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LeadsList);
