/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Container, Content, Button, Text, Badge, StyleProvider, Footer } from 'native-base';
import { Image } from 'react-native';

import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
/******************
 * Project import *
 ******************/
import style from './style.js';
import getTheme from '../../../../../../theme/components';
import material from '../../../../../../theme/variables/material';
import BasisComponent from '../../../../../basisComponent.js';
import { leadsList} from '../../../../../../config.js';
import { NavigationBar, ProgressScreen } from '../../../../../../component/index.js';
import { executeGetLeadsBySubStatus } from '../../../../../../data/configuration/action.js';
import {
 toolbarButtonColor,
 progressScreenNestedBackgroundColor,
 progressScreenSpinnerColor,
 progressScreenTextColor,
} from '../../../../../../color.js';
/*************
 * Constants *
 *************/
const ASSIGNMENT = 'asignacion';
const MANAGEMENT = 'gestion';
const CLOSED = 'cierre';
const OBSERVED = 'observado';
/**
 * Renderiza la cantidad de leads por
 * sub estado
 */
class LeadSubStatus extends BasisComponent {
  
  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._pop = this.pop.bind(this);
    this._renderSubStatus = this.renderSubStatus.bind(this);
    this._goLeadListScreen = (subStatus, subStatusName) =>
      this.push.bind(this, leadsList, { leadStatus: subStatus, leadStatusName: subStatusName });
  }
  
  componentWillReceiveProps(nextProps) {
    if (nextProps.statusOfGettingLeadsBySubStatus.wasAnError &&
        nextProps.statusOfGettingLeadsBySubStatus.leadsBySubStatusAreBeingFetching)
        this.showErrorMessage(
          nextProps.statusOfGettingLeadsBySubStatus.error &&
          nextProps.statusOfGettingLeadsBySubStatus.error.errorMessage ||
          'No se pudo obtener los estados de los leads y sus cantidades'
        )
  }


  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <NavigationBar
            toolBarStyle={style.toolbar}
            leftButtonVisible={true}
            onPressLeftButton={this._pop}
            iconLeftButton={'md-arrow-back'}
            colorIconLeftButton={toolbarButtonColor}
            title={this.getTitleByStatus()}
            titleStyle={style.title}/>
          <Content contentContainerStyle={style.content}>
            {
              this.props.statusOfGettingLeadsBySubStatus.leadsBySubStatusAreBeingFetching
              ? this.renderProgressScreen()
              : this.renderSubStatusList()
            }
          </Content>
          <Footer style={style.footer}>
            <Image
              style={style.imageFooter}
              source={require('../../../../../../image/logo.png')}
              resizeMode={'center'}/>
          </Footer>
        </Container>
      </StyleProvider>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {
    
    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbarAndTabBar={true}/>
    );
  }
  /**
   * Renderiza los sub estados de leads y las cantidades
   * de leads por sub estado
   */
  renderSubStatusList() {
    
    return (
      this.props.leadsBySubStatus &&
      this.props.leadsBySubStatus.map(this._renderSubStatus)
    );
  }
  /**
   * Renderiza sub estado
   */
  renderSubStatus(item, index) {
    
    return (
      <Button
        key={index}
        style={style.subStatusButton}
        onPress={this._goLeadListScreen(item.id, item.name)}>
        
        <Text style={style.subStatusButtonText}>{item.name}</Text>
        
        <Badge style={style.subStatusButtonBadge}>
          <Text style={style.subStatusButtonBadgeText}>
            {item.quantity}
          </Text>
        </Badge>
      </Button>
    )
  }
  /**
   * Invoca metodo que obtiene los
   * sub estados de leads y las cantidades
   * de leads por sub estado
   */
  componentDidMount() {
    this.props.executeGetLeadsBySubStatus(
      this.props.status,
      this.props.session.accessToken,
      this.props.session.refreshToken
    );
  }
  /**
   * Retorna el titulo de acuerdo al estado
   * seleccionado
   */
  getTitleByStatus() {
    
    return this.props.status === ASSIGNMENT
      ? 'Asignación'
      : this.props.status === MANAGEMENT
      ? 'Gestión'
      : this.props.status === CLOSED
      ? 'Cierre'
      : 'Observado';
  }
}

LeadSubStatus.propTypes = {
  status: PropTypes.string,//Sub estado de un lead
};

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  statusOfGettingLeadsBySubStatus: state.dataReducer.configurationReducer.statusOfGettingLeadsBySubStatus,
  leadsBySubStatus: state.dataReducer.configurationReducer.leadsBySubStatus,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetLeadsBySubStatus: executeGetLeadsBySubStatus,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LeadSubStatus);