/******************
 * Project import *
 ******************/
import {
  toolbarBackgroundColor,
  toolbarBorderColor,
  toolbarTextColor,
  leadStatusButtonContainerBorder,
  leadStatusButtonBackground,
  leadStatusButtonText,
  leadStatusSummaryButton,
  leadStatusSummaryButtonText,
  leadStatusFooter,
} from '../../../../color.js';

export default {
  toolbar: {
    backgroundColor: toolbarBackgroundColor,
    borderColor: toolbarBorderColor,
  },
  title: {
    color: toolbarTextColor,
  },
  content: {
    flex: 1,
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: leadStatusButtonContainerBorder,
  },
  firstButtonContainer: {
    borderRightWidth: 1,
    borderBottomWidth: 1,
  },
  secondButtonContainer: {
    borderBottomWidth: 1,
  },
  thirdButtonContainer: {
    borderRightWidth: 1,
  },
  imageButton: {
    width: '70%',
    height: '70%',
  },
  button: {
    alignSelf: 'center',
    backgroundColor: leadStatusButtonBackground,
    borderRadius: 10,
    marginHorizontal: 15,
    marginBottom: 15
  },
  text: {
    color: leadStatusButtonText,
    textAlign: 'center',
  },
  badge: {
    position: 'absolute',
    top: '45%',
    right: '15%',
    height: 30,
    width: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textBadge: {
    fontSize: 11,
  },
  summaryButton: {
    position: 'absolute',
    borderRadius: 10,
    backgroundColor: leadStatusSummaryButton,
    top: '48%',
    right: '35%',
  },
  summaryButtonText: {
    color: leadStatusSummaryButtonText,
  },
  footer: {
    backgroundColor: leadStatusFooter,
    alignItems: 'center',
  },
  imageFooter: {
    height: 40,
  },
};