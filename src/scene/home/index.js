/***********************
 * Node modules import *
 ***********************/
import React from 'react';
import { Container, Content, Grid, Row, Button, Text, Footer, Badge, StyleProvider } from 'native-base';
import { View, Image, RefreshControl } from 'react-native';

import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
/******************
 * Project import *
 ******************/
import style from './style.js';
import Router from '../../router.js';
import BasisComponent from '../basisComponent.js';
import getTheme from '../../theme/components';
import material from '../../theme/variables/material';
import { NavigationBar, ProgressScreen } from '../../component/index.js';
import { executeGetHomeData } from '../../data/configuration/action.js';
import {
  notification,
  leadRegistration,
  pendingAssignment,
  observedLead,
  leadStatus,
} from '../../config.js';
import {
  toolbarButtonColor,
  progressScreenNestedBackgroundColor,
  progressScreenTextColor,
  progressScreenSpinnerColor,
  refreshColor,
  refreshBackgroundColor
} from '../../color.js';
/*************
 * Constants *
 *************/
const FIRST_BUTTON = 'registrar lead';
const SECOND_BUTTON = 'asignaciones pendientes';
const THIRD_BUTTON = 'leads obervados';
const FOURTH_BUTTON = 'estados leads';
/**
 * Renderiza el home
 */
class Home extends BasisComponent {

  constructor(props) {
    super(props);
    //Se bindea los escuchadores aquí para mejorar el performance
    this._push = this.push.bind(this, notification, null);
    this._openDrawer = this.openDrawer.bind(this);
    this._goRegistrationLeadScreen = this.push.bind(this, leadRegistration, null);  
    this._goPendingAssignmentScreen = this.push.bind(this, pendingAssignment, null);  
    this._goObservedLeadScreen = this.push.bind(this, observedLead, null);  
    this._goLeadStatusScreen = this.push.bind(this, leadStatus, null);  
    this._onRefresh = () => this.props.executeGetHomeData(this.props.session.accessToken, this.props.session.refreshToken);
    //Escuchador del navigator
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }
  
  componentWillReceiveProps(nextProps) {
    if (nextProps.statusOfGettingHomeData.wasAnError &&
        nextProps.statusOfGettingHomeData.homeDataIsBeingFetching)
        this.showErrorMessage(
          nextProps.statusOfGettingHomeData.error && nextProps.statusOfGettingHomeData.error.errorMessage ||
          'No se pudo obtener la cantidad de notificaciones no leidas y de leads'
        )
  }

  render() {

    return (
      <StyleProvider style={getTheme(material)}>
        <Container>
          <NavigationBar
            toolBarStyle={style.toolbar}
            leftButtonVisible={true}
            onPressLeftButton={this._openDrawer}
            iconLeftButton={'md-menu'}
            colorIconLeftButton={toolbarButtonColor}
            title={'Home'}
            titleStyle={style.title}
            rightButtonsVisible={true}
            rightButtons={this.getRightButtons()}/>
          {
            this.props.statusOfGettingHomeData.homeDataIsBeingFetching
            ? this.renderProgressScreen()
            : this.renderButtons()
          }
          <Footer style={style.footer}>
            <Image
              style={style.imageFooter}
              source={require('../../image/logo.png')}
              resizeMode={'center'}/>
          </Footer>
        </Container>
      </StyleProvider>
    );
  }
  /**
   * Renderiza el ProgressScreen
   */
  renderProgressScreen() {
    
    return (
      <ProgressScreen
        backgroundColor={progressScreenNestedBackgroundColor}
        textColor={progressScreenTextColor}
        spinnerColor={progressScreenSpinnerColor}
        withToolbarAndTabBar={true}/>
    );
  }
  /**
   * Renderiza todos los botones para ir a
   * las distintas vistas
   */
  renderButtons() {
    
    return (
      <Content
        refreshControl={
          <RefreshControl
            refreshing={this.props.statusOfGettingHomeData.homeDataIsBeingFetching}
            onRefresh={this._onRefresh}
            colors={[refreshColor]}
            progressBackgroundColor={refreshBackgroundColor}/>
        }
        contentContainerStyle={style.content}>
        
        <Grid>
          <Row>
            {this.renderButton(FIRST_BUTTON, require('../../image/register-a-lead.png'), 0, this._goRegistrationLeadScreen)}
            {this.renderButton(SECOND_BUTTON, require('../../image/pending-assignments.png'), this.props.pendingAssignments, this._goPendingAssignmentScreen)}
          </Row>
          <Row>
            {this.renderButton(THIRD_BUTTON, require('../../image/observed-leads.png'), this.props.observedLeads, this._goObservedLeadScreen)}
            {this.renderButton(FOURTH_BUTTON, require('../../image/lead-status.png'), 0, this._goLeadStatusScreen)}
          </Row>
        </Grid>
      </Content>
    );
  }
  /**
   * Renderiza los botones del home
   */
  renderButton(buttonText, buttonIcon, buttonBadge, onPressButton){
    const dynamicStyle = this.getDynamicStyleByButton(buttonText);
    
    return (
      <View style={[style.buttonContainer, dynamicStyle]}>
        <Image
          style={style.imageButton}
          source={buttonIcon}
          resizeMode={'contain'}/>
        <Button
          style={style.button}
          onPress={onPressButton}>
          <Text style={style.text}>{buttonText}</Text>
        </Button>
        {
          buttonBadge != 0 &&
          <Badge
            style={style.badge}>
            <Text numberOfLines={1} style={style.textBadge}>{buttonBadge}</Text>
          </Badge>
        }
      </View>
    );
  }
  /**
   * Invoca metodo para obtener los datos acerca del
   * home
   */
  componentDidMount() {
    this.props.executeGetHomeData(
      this.props.session.accessToken,
      this.props.session.refreshToken,
    );
    //Si la app se abrió presionando sobre una notificacion
    //entonces se abre la vista de notificaciones
    if (this.props.appWasOpenedByPressingANotification)
      setTimeout(() => {
        this.push(notification);
      }, 1000);
  }
  /**
   * Retorna estilo dinamico en base el boton
   */
  getDynamicStyleByButton(buttonText) {
    
    return buttonText === FIRST_BUTTON
      ? style.firstButtonContainer
      : buttonText === SECOND_BUTTON
      ? style.secondButtonContainer
      : buttonText === THIRD_BUTTON
      ? style.thirdButtonContainer
      : null;
  }
  /**
   * Retorna json que representa los botones que van al
   * lado derecho del toolbar
   */
  getRightButtons() {
    
    return [
      {
        onPressRightButton: this._push,
        iconRightButton: 'md-notifications',
        colorIconRightButton: toolbarButtonColor,
        badge: true,
        badgeText: this.props.notifications,
      },
    ];
  }
  /**
   * Escuchador del navigator que se dispara cuando
   * esta vista va a ser visible y cuando se va 
   * a ocultar
   */
  onNavigatorEvent(event) {
    switch(event.id) {
      
      case 'willAppear':
        //Habilita el drawer
        Router.getInstance().navigator.setDrawerEnabled({
          side: 'left',
          enabled: true,
        });
        break;
    }
  }
}

Home.propTypes = {
  appWasOpenedByPressingANotification: PropTypes.bool,//Bandera para saber si se muestra la vista de notificaciones
};

const mapStateToProps = state => ({
  session: state.dataReducer.sessionReducer.session,
  pendingAssignments: state.dataReducer.configurationReducer.pendingAssignments,
  observedLeads: state.dataReducer.configurationReducer.observedLeads,
  notifications: state.dataReducer.configurationReducer.notifications,
  statusOfGettingHomeData: state.dataReducer.configurationReducer.statusOfGettingHomeData,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  executeGetHomeData: executeGetHomeData,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
