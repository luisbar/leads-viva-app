/******************
 * Project import *
 ******************/
import {
  toolbarBackgroundColor,
  toolbarBorderColor,
  toolbarTextColor,
  homeButtonContainerBorder,
  homeButtonBackground,
  homeButtonText,
  homeFooter,
} from '../../color.js';

export default {
  toolbar: {
    backgroundColor: toolbarBackgroundColor,
    borderColor: toolbarBorderColor,
  },
  title: {
    color: toolbarTextColor,
  },
  content: {
    flex: 1,
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: homeButtonContainerBorder,
  },
  firstButtonContainer: {
    borderRightWidth: 1,
    borderBottomWidth: 1,
  },
  secondButtonContainer: {
    borderBottomWidth: 1,
  },
  thirdButtonContainer: {
    borderRightWidth: 1,
  },
  imageButton: {
    width: '70%',
    height: '70%',
  },
  button: {
    alignSelf: 'center',
    backgroundColor: homeButtonBackground,
    borderRadius: 10,
    marginHorizontal: 15,
    marginBottom: 15
  },
  text: {
    color: homeButtonText,
    textAlign: 'center',
  },
  badge: {
    position: 'absolute',
    top: '45%',
    right: '15%',
    height: 32,
    width: 32,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
  },
  textBadge: {
    fontSize: 11,
  },
  footer: {
    backgroundColor: homeFooter,
    alignItems: 'center',
  },
  imageFooter: {
    height: 40,
  },
};