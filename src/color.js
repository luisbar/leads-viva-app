/**********************************MAIN COLORS*********************************/
export const darknestPrimaryColor = '#61761E';
export const darkerPrimaryColor = '#7E9928';
export const darkPrimaryColor = '#A6C835';
export const primaryColor = '#C0E83E';
export const lightPrimaryColor = '#D3FF44';
export const lighterPrimaryColor = '#CCFF25';
export const lightestPrimaryColor = '#DAFD68';

export const secondaryColor = '#5C349E';
export const darknestSecondaryColor = '#47267E';

export const accentColor = '#EA8F06';

export const dangerColor = '#FF8888';
export const infoColor = '#62B1F6';
export const successColor = '#AED581';
export const warningColor = '#F0AD4E';

export const lightestDisabledColor = '#E6E6E6';
export const lighterDisabledColor = '#BABABA';
export const lightDisabledColor = '#767575';
export const darkDisabledColor = '#615E5E';
export const transparent = 'rgba(0,0,0,0)';
/**********************************COMPONENTS**********************************/
/*************************************TOAST************************************/
export const toastTextColor = '#FFFFFF';
/*************************************MODAL************************************/
export const modalBackground = '#FFFFFF';
/********************************NAVIGATION BAR********************************/
export const toolbarBackgroundColor = primaryColor;
export const toolbarBorderColor = primaryColor;
export const toolbarTextColor = secondaryColor;
export const toolbarButtonColor = secondaryColor;
export const toolbarButtonColorWithBackground = '#FFFFFF';
export const toolbarButtonBackgroundColor = accentColor;
export const searchBarBackgroundColor = primaryColor;
export const searchBarBorderColor = primaryColor;
export const searchBarIconColor = secondaryColor;
export const progressBarBackgroundColor = '#FFFFFF';
export const progressBarColor = secondaryColor;
/**********************************DRAWER MENU*********************************/
export const drawerBackground = secondaryColor;
export const drawerOddItemBackground = darknestSecondaryColor;
export const drawerPairItemBackground = secondaryColor;
export const drawerOddIcon = darknestSecondaryColor;
export const drawerPairIcon = secondaryColor;
export const drawerIconBackground = '#FFFFFF';
export const drawerTextColor = '#FFFFFF';

export const drawerFooter = secondaryColor;
export const drawerFooterText = '#FFFFFF';
/*************************************INPUT************************************/
export const inputLabelColor = secondaryColor;
export const inputTextColor = '#000000';
export const inputPlaceholderColor = lightDisabledColor;
export const inputBackgroundColor = lightestDisabledColor;
/**********************************RADIO GROUP**********************************/
export const radioLabel = secondaryColor;
export const radioErrorColor = dangerColor;
/************************************PICKER************************************/
export const pickerColor = '#000000';
export const pickerLabelColor = secondaryColor;
export const pickerBackgroundColor = lightestDisabledColor;

export const pickerErrorColor = dangerColor;
/**********************************TAG SELECT**********************************/
export const tagsContainerBackground = darkPrimaryColor;
export const tagBackgroundColor = primaryColor;
export const tagLabelColor = secondaryColor;
export const selectedTagBackgroundColor = secondaryColor;
export const selectedTagLabelColor = '#FFFFFF';
/**********************************POPUP MENU**********************************/
export const popupMenuBackgroundColor = secondaryColor;
export const popupMenuIconColor = secondaryColor;
export const popupMenuItemIconColor = '#FFFFFF';
export const popupMenuItemTextColor = '#FFFFFF';
/********************************PROGRESS SCREEN*******************************/
export const progressScreenBackgroundColor = primaryColor;
export const progressScreenNestedBackgroundColor = '#FFFFFF';
export const progressScreenSpinnerColor = secondaryColor;
export const progressScreenTextColor = secondaryColor;
/*******************************REFRESH CONTROL********************************/
export const refreshBackgroundColor = primaryColor;
export const refreshColor = secondaryColor;
/*************************************LOGIN************************************/
export const loginFormBackground = '#FFFFFF';
export const loginTitleColor = secondaryColor;
export const loginIndicationColor = lightDisabledColor;
export const loginButtonLabelTextColor = '#FFFFFF';
export const loginButtonBackgroundColor = accentColor;
/*******************LEAD LIST - PENDING LEADS - OBSERVED LEADS*****************/
//list item
export const listItemOddBackgroundColor = '#D0CECE';
export const listItemPairBackgroundColor = '#E6E6E6';

export const listItemIdBackgroundColor = primaryColor;
export const listItemIdTextColor = secondaryColor;

export const listItemPendingStatusBackgroundColor = '#DB2E5F';
export const listItemContactedStatusBackgroundColor = '#00C0EF';
export const listItemFallenStatusBackgroundColor = '#DF5138';
export const listItemObservedStatusBackgroundColor = '#111111';
export const listItemScheduledStatusBackgroundColor = '#001B35';
export const listItemSuccessfulStatusBackgroundColor = '#00A35A';
export const listItemStoreStatusBackgroundColor = '#605FA8';
export const listItemRejectedStatusBackgroundColor = '#FF7A00';
export const listItemStatusText = '#FFFFFF';

export const listItemTextColor = '#616161';

export const listItemProductsContainer = secondaryColor;
export const listItemProductsText = '#FFFFFF';

export const listItemJustificationContainer = secondaryColor;
export const listItemJustificationText = '#FFFFFF';
//fab
export const upButtonBackgroundColor = accentColor;
export const upButtonIconColor = '#FFFFFF';

//list footer
export const footerSpinnerColor = secondaryColor;
export const footerButtonTextColor = secondaryColor;

export const emptyListText = secondaryColor;
/*******************************LEAD REGISTRATION*******************************/
//tab
export const tabUnderlineColor = secondaryColor;
export const tabHeadingBackgroundColor = primaryColor;
export const tabIconColor = '#FFFFFF';
/*******************************PRODUCT SELECTION*******************************/
export const productButtonBackgroundColor = secondaryColor;
export const productButtonBackgroundColorWithError = dangerColor;
export const productButtonBackgroundColorWithData = successColor;
export const productButtonTextColor = '#FFFFFF';
/************************************PRODUCT************************************/
export const productTotalContainerBackgroundColor = lightestDisabledColor;
export const productTotalTextColor = lightDisabledColor;

export const productListItemBackgroundColor = '#FFFFFF';

export const productListItemHeaderTextColor = secondaryColor;

export const productListItemContentLeftSideBackgroundColor = primaryColor;
export const productListItemContentLeftSideTextColor = secondaryColor;

export const productListItemContentRightSideBackgroundColor = '#D0CECE';

export const productInputContainerBackgroundColor = '#FFFFFF';
export const productInputErrorTextColor = dangerColor;

export const productListItemContentActionRightSideButtonBackgroundColor = '#FFFFFF';
export const productListItemContentActionRightSideButtonIconColor = darkDisabledColor;

export const productListItemContentSeparator = '#FFFFFF';

export const productAddButtonBackgroundColor = accentColor;
export const productAddButtonIconColor = '#FFFFFF';
/****************************************MAP****************************************/
export const gpsButtonBackgroundColor = accentColor;
export const gpsButtonIconColor = '#FFFFFF';

export const calloutBackgroundColor = '#FFFFFF';
/**************************************PICTURE**************************************/
export const pictureFabButtonBackgroundColor = accentColor;
export const pictureFabButtonIconColor = '#FFFFFF';

export const pictureListItemIconColor = primaryColor;
export const pictureTypeBackgroundColor = secondaryColor;
export const pictureTypeTextColor = '#FFFFFF';
export const pictureDistanceText = dangerColor;
/**************************************CAMERA***************************************/
export const cameraButtonBackgroundColor = primaryColor;
export const cameraButtonTextColor = secondaryColor;
/***********************************ACTIONS RECORD***********************************/
export const actionsRecordListHeaderBackground = '#FFFFFF';
export const actionsRecordListHeaderItemBackground = lightestDisabledColor;
export const actionsRecordListHeaderItemLeftSideText = lightDisabledColor;
export const actionsRecordListHeaderItemRightSideText = lightDisabledColor;

export const actionsRecordListItemBackground = '#FFFFFF';
export const actionsRecordListItemContentLeftSideBackground = primaryColor;
export const actionsRecordListItemContentLeftSideText = secondaryColor;
export const actionsRecordListItemContentRightSideBackground = '#D0CECE';
export const actionsRecordListItemContentRightSideText = '#616161';
export const actionsRecordListItemContentSeparator = '#FFFFFF';
/***************************************COMMENT***************************************/
export const commentBubbleBackground = lightestDisabledColor;
export const commentBubbleMessageText = lightDisabledColor;

export const commentSubHeaderBackground = darkPrimaryColor;
export const commentSubHeaderText = '#FFFFFF';
export const commentSendButtonIcon = accentColor;
/*************************************LEAD VIEWER*************************************/
export const leadInformationViewerItemBackground = lightestDisabledColor;
export const leadInformationViewerItemLeftSideText = lightDisabledColor;
export const leadInformationViewerItemRightSideText = lightDisabledColor;

export const productListItemBackground = '#FFFFFF';
export const productLeftSubItemOfList = primaryColor;
export const productTextOfLeftSubItemOfList = secondaryColor;
export const productRightSubItemOfList = '#D0CECE';
export const productTextOfRightSubItemOfList = '#616161';
export const productTextOfheaderOfItemOfList = secondaryColor;
export const productSeparatoraOfSubItemOfList = '#FFFFFF';

export const statusRecordListHeaderBackground = '#FFFFFF';
export const statusRecordListHeaderItemBackground = lightestDisabledColor;
export const statusRecordListHeaderItemLeftSideText = lightDisabledColor;
export const statusRecordListHeaderItemRightSideText = lightDisabledColor;

export const statusRecordListItemBackground = '#FFFFFF';
export const statusRecordLeftSubItemOfList = primaryColor;
export const statusRecordTextOfLeftSubItemOfList = secondaryColor;
export const statusRecordRightSubItemOfList = '#D0CECE';
export const statusRecordTextOfRightSubItemOfList = '#616161';
export const statusRecordTextOfheaderOfItemOfList = secondaryColor;
export const statusRecordSeparatoraOfSubItemOfList = '#FFFFFF';

export const picturePictureTypeBackgroundColor = secondaryColor;
export const picturePictureTypeTextColor = '#FFFFFF';
export const pictureEmptyText = secondaryColor;
/************************************PROGRESS MODAL************************************/
export const progressModalBackground = '#FFFFFF';
export const progressModalText = secondaryColor;
export const progressModalSpinner = secondaryColor;
/*************************************MULTI SELECT*************************************/
export const multiSelectDropDownBackground = lightestDisabledColor;
export const multiSelectDropDownText = '#000000';

export const multiSelectChipBackground = accentColor;
export const multiSelectChipText = '#FFFFFF';
export const multiSelectChipIcon = '#FFFFFF';

export const multiSelectPrimaryColor = primaryColor;
export const multiSelectSuccessColor = successColor;
export const multiSelectTextColor = '#000000';
export const multiSelectSubTextColor = darkDisabledColor;
export const multiSelectPlaceholderTextColor = lightDisabledColor;

export const multiSelectLabel = secondaryColor;
export const multiSelectSpinner = secondaryColor;
export const multiSelectTextError = dangerColor;
/**************************************APPOINTMENT**************************************/
//event item
export const eventContainer = '#FFFFFF';
export const eventEditable = successColor;
export const eventNonEditable = dangerColor;
export const eventTime = secondaryColor;
export const eventDescriptionText = secondaryColor;
//calendar
export const calendarBackground = '#FFFFFF';//Color de fondo del calendario
export const textSectionTitleColor = secondaryColor;//Color de los labels de los dias (do, lu, etc)
export const selectedDayBackgroundColor = accentColor;//Color de fondo de la fecha actualmente seleccionada
export const selectedDayTextColor = '#FFFFFF';//Color del label de la fecha actualmente seleccionada
export const todayTextColor = primaryColor;//Color del label del dia de la fecha actual
export const dayTextColor = darkDisabledColor;//Color de los labels de los dias (1, 2, etc)
export const textDisabledColor = lightDisabledColor;//Color de los labels de los dias que esta desabilitados (1, 2, etc)
export const dotColor = primaryColor;//Color del indicador de que hay un evento en esa fecha
export const selectedDotColor = '#FFFFFF';//Color del indicador de que hay un evento en esa fecha, cuando esta seleccionado
export const agendaKnobColor = primaryColor;//Color del boton para mostrar la lista de calendarios
export const monthTextColor = secondaryColor;//Color de los labels de los meses de las lista de calendarios
export const backgroundColor = '#FFFFFF';//Color de fondo de la agenda
export const agendaDayNumColor = secondaryColor;//Color de los labels de los dias de la agenda (1, 2, etc)
export const agendaDayTextColor = secondaryColor;//Color de los labels de los dias de la agenda (do, lun, etc)
export const agendaTodayColor = primaryColor;//Color del label de la fecha actual de la agenda
export const agendaEmptyActionsText = secondaryColor;//Color del texto que se muestra cuando no hay acciones para una fecha seleccionada
/******************************************HOME******************************************/
export const homeButtonContainerBorder = secondaryColor;
export const homeButtonBackground = primaryColor;
export const homeButtonText = secondaryColor;

export const homeFooter = primaryColor;
/***************************************LEAD STATUS***************************************/
export const leadStatusButtonContainerBorder = secondaryColor;
export const leadStatusButtonBackground = primaryColor;
export const leadStatusButtonText = secondaryColor;

export const leadStatusSummaryButton = secondaryColor;
export const leadStatusSummaryButtonText = '#FFFFFF';

export const leadStatusFooter = primaryColor;
/*************************************LEADS SUB STATUS*************************************/
export const subStatusContent = secondaryColor;

export const subStatusButton = '#FFFFFF';
export const subStatusButtonText = secondaryColor;
export const subStatusButtonBadge = secondaryColor;
export const subStatusButtonBadgeText = '#FFFFFF';

export const subStatusFooter = primaryColor;
/***************************************ACTION VIEWER***************************************/
export const actionViewerItemBackground = lightestDisabledColor;
export const actionViewerItemLeftSideText = lightDisabledColor;
export const actionViewerItemRightSideText = lightDisabledColor;
/***************************************CONFIRM MODAL***************************************/
export const confirmModalBackground = primaryColor;

export const confirmModalTitle = secondaryColor;
export const confirmModalNote = secondaryColor;

export const confirmModalRightButton = accentColor;
export const confirmModalRightButtonText = '#FFFFFF';
export const confirmModalLeftButton = accentColor;
export const confirmModalLeftButtonText = '#FFFFFF';
/****************************************NOTIFICATION****************************************/
export const notificationSeenIcon = secondaryColor;

//list item
export const notificationListItemOddBackgroundColor = '#D0CECE';
export const notificationListItemPairBackgroundColor = '#E6E6E6';

export const notificationListItemTextColor = '#616161';

//fab
export const notificationUpButtonBackgroundColor = accentColor;
export const notificationUpButtonIconColor = '#FFFFFF';

//list footer
export const notificationFooterSpinnerColor = secondaryColor;
export const notificationFooterButtonTextColor = secondaryColor;

export const notificationEmptyListText = secondaryColor;
/**************************************APP NOTIFICATION**************************************/
export const appNotificationContainer = primaryColor;
export const appNotificationTitle = secondaryColor;
export const appNotificationBody = secondaryColor;