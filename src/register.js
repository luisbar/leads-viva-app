/***********************
 * Node modules import *
 ***********************/
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
/******************
 * Project import *
 ******************/
import Store from './store';

import Main from './scene/main/index.js';
import Login from './scene/login/index.js';
import Home from './scene/home/index.js';
import LeadsList from './scene/home/scene/leadStatus/scene/leadSubStatus/scene/leadsList/index.js';
import LeadRegistration from './scene/leadRegistration/index.js';
import LeadEdition from './scene/leadEdition/index.js';
import PendingAssignment from './scene/pendingAssignment/index.js';
import Appointment from './scene/appointment/index.js';
import ObservedLead from './scene/observedLead/index.js';
import Configuration from './scene/configuration/index.js';
import ActionsRecord from './scene/actionsRecord/index.js';
import Comment from './scene/comment/index.js';
import LeadViewer from './scene/leadViewer/index.js';
import ActionRegistration from './scene/actionRegistration/index.js';
import Filter from './scene/filter/index.js';
import LeadStatus from './scene/home/scene/leadStatus/index.js';
import LeadSubStatus from './scene/home/scene/leadStatus/scene/leadSubStatus/index.js';
import ActionEdition from './scene/actionEdition/index.js';
import ActionViewer from './scene/actionViewer/index.js';
import ApprobationOrRejection from './scene/pendingAssignment/scene/approbationOrRejection/index.js';
import Camera from './scene/camera/index.js';
import Product from './scene/product/index.js';
import Notification from './scene/home/scene/notification/index.js';
import Place from './scene/place/index.js';
import DrawerMenu from './component/drawerMenu/index.js';
import ConfirmModal from './component/confirmModal/index.js';
import ProgressModal from './component/progressModal/index.js';
import AppNotification from './component/appNotification/index.js';
/**
 * Metodo que registra todas las vistas que manejara la pila de vistas, se le
 * pasa el store y el provider para manejar sus estados con redux
 */
export function registerScenes() {
  Navigation.registerComponent('leads.viva.Main', () => Main, Store, Provider);
  Navigation.registerComponent('leads.viva.Login', () => Login, Store, Provider);
  Navigation.registerComponent('leads.viva.Home', () => Home, Store, Provider);
  Navigation.registerComponent('leads.viva.LeadsList', () => LeadsList, Store, Provider);
  Navigation.registerComponent('leads.viva.LeadRegistration', () => LeadRegistration, Store, Provider);
  Navigation.registerComponent('leads.viva.LeadEdition', () => LeadEdition, Store, Provider);
  Navigation.registerComponent('leads.viva.PendingAssignment', () => PendingAssignment, Store, Provider);
  Navigation.registerComponent('leads.viva.Appointment', () => Appointment, Store, Provider);
  Navigation.registerComponent('leads.viva.ObservedLead', () => ObservedLead, Store, Provider);
  Navigation.registerComponent('leads.viva.Configuration', () => Configuration, Store, Provider);
  Navigation.registerComponent('leads.viva.ActionsRecord', () => ActionsRecord, Store, Provider);
  Navigation.registerComponent('leads.viva.Comment', () => Comment, Store, Provider);
  Navigation.registerComponent('leads.viva.LeadViewer', () => LeadViewer, Store, Provider);
  Navigation.registerComponent('leads.viva.ActionRegistration', () => ActionRegistration, Store, Provider);
  Navigation.registerComponent('leads.viva.Filter', () => Filter, Store, Provider);
  Navigation.registerComponent('leads.viva.LeadStatus', () => LeadStatus);
  Navigation.registerComponent('leads.viva.LeadSubStatus', () => LeadSubStatus, Store, Provider);
  Navigation.registerComponent('leads.viva.ActionEdition', () => ActionEdition, Store, Provider);
  Navigation.registerComponent('leads.viva.ActionViewer', () => ActionViewer, Store, Provider);
  Navigation.registerComponent('leads.viva.ApprobationOrRejection', () => ApprobationOrRejection, Store, Provider);
  Navigation.registerComponent('leads.viva.Camera', () => Camera);
  Navigation.registerComponent('leads.viva.DrawerMenu', () => DrawerMenu, Store, Provider);
  Navigation.registerComponent('leads.viva.Product', () => Product);
  Navigation.registerComponent('leads.viva.ConfirmModal', () => ConfirmModal);
  Navigation.registerComponent('leads.viva.ProgressModal', () => ProgressModal);
  Navigation.registerComponent('leads.viva.Notification', () => Notification, Store, Provider);
  Navigation.registerComponent('leads.viva.Place', () => Place, Store, Provider);
  Navigation.registerComponent('leads.viva.AppNotification', () => AppNotification);
}
