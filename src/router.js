/***********************
 * Node modules import *
 ***********************/
import {
  Navigation
} from 'react-native-navigation';
/******************
 * Project import *
 ******************/
import { modalBackground } from './color.js';
import {
  appointment,
  configuration,
  home,
  leadRegistration,
  observedLead,
  pendingAssignment,
} from './config.js';
/*************
 * Constants *
 *************/
let router;
/**
 * Administra todas las rutas
 */
export default class Router {

  constructor(navigator) {
    this.navigator = navigator;
    router = this;
  }

  static getInstance() {
    return router;
  }
  /**
   * Renderiza la vista con el id enviado por parametro
   * @param  {string} screen id de la vista a mostrar
   * @param  {string} title titulo que se mostrará en el toolbar
   */
  resetTo(screen, props) {
    this.navigator.resetTo({
      screen: screen,
      passProps: props,
      animated: true,
      animationType: 'slide-down',
      navigatorStyle: {
        navBarHidden: true,
      },
    });
  }
  /**
   * Renderiza la vista con el id enviado por parametro
   * @param  {string} screen id de la vista a mostrar
   * @param  {string} title titulo que se mostrará en el toolbar
   */
  push(screen, props) {
    this.navigator.push({
      screen: screen,
      passProps: props,
      animated: true,
      animationType: 'slide-down',
      navigatorStyle: {
        navBarHidden: true,
      },
    });
  }
  /**
   * Despliega un modal con color de fondo
   */
  showModal(screen, props) {
    this.navigator.showModal({
      screen: screen,
      passProps: props,
      navigatorStyle: {
        navBarHidden: true,
        screenBackgroundColor: modalBackground,
      },
      animationType: 'slide-up',
      overrideBackPress: true,
    });
  }
  /**
   * Despliega un modal con fondo transparente
   */
  showTransparentModal(screen, props) {
    this.navigator.showModal({
      screen: screen,
      passProps: props,
      navigatorStyle: {
        navBarHidden: true,
        screenBackgroundColor: 'rgba(0,0,0,0.3)',
      },
      animationType: 'slide-up',
      overrideBackPress: true,
    });
  }
}
