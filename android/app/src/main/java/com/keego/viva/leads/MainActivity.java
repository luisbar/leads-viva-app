package com.keego.viva.leads;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reactnativenavigation.controllers.SplashActivity;

public class MainActivity extends SplashActivity {

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public LinearLayout createSplashLayout() {
    LinearLayout container = new LinearLayout(this);
    LinearLayout content = new LinearLayout(this);
    ImageView img = new ImageView(this);
    TextView text = new TextView(this);
    //Image view
    LinearLayout.LayoutParams imgParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    imgParam.gravity = Gravity.CENTER;

    img.setImageDrawable(getResources().getDrawable(R.mipmap.logo));
    img.setLayoutParams(imgParam);
    //Text
    LinearLayout.LayoutParams textParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    textParam.gravity = Gravity.CENTER_HORIZONTAL;

    text.setTypeface(Typeface.createFromAsset(this.getAssets(), "fonts/" + "Museo700.otf"));
    text.setText("SISTEMA LEAD");
    text.setTextColor(Color.parseColor("#FFFFFF"));
    text.setLayoutParams(textParam);
    //Content view
    LinearLayout.LayoutParams contentParam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    contentParam.gravity = Gravity.CENTER;

    content.setLayoutParams(contentParam);
    content.setOrientation(LinearLayout.VERTICAL);
    content.addView(img);
    content.addView(text);
    //Container
    container.setBackgroundColor(Color.parseColor("#C0E83E"));
    container.addView(content);

    return container;
  }

}
